﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFControlConnection;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using DllServerGnssProperties.Models;

namespace EphemerisAlmanac
{
    internal class MainWindowViewModel : INotifyPropertyChanged
    {

        private LocalProperties _localPropertiesVM;
        public LocalProperties LocalPropertiesVM
        {
            get => _localPropertiesVM; 
            set
            {
                if (_localPropertiesVM != value)
                {
                    _localPropertiesVM = value;
                    OnPropertyChanged();
                }
            }
        }


        #region Connection

        private ConnectionStates _stateConnectionGps = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionGps
        {
            get => _stateConnectionGps; 
            set
            {
                if (_stateConnectionGps != value)
                {
                    _stateConnectionGps = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool isRTSEnabled = false;

        public bool IsRTSEnabled
        {
            get => isRTSEnabled;
            set
            {
                if (isRTSEnabled != value)
                {
                    isRTSEnabled = value;
                    OnPropertyChanged();
                }
            }
        }

        
        private bool isDSREnabled = false;

        public bool IsDSREnabled
        {
            get => isDSREnabled;
            set
            {
                if (isDSREnabled != value)
                {
                    isDSREnabled = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool isDTREnabled = false;

        public bool IsDTREnabled
        {
            get => isDTREnabled;
            set
            {
                if (isDTREnabled != value)
                {
                    isDTREnabled = value;
                    OnPropertyChanged();
                }
            }
        }


        private bool isCTSEnabled = false;

        public bool IsCTSEnabled
        {
            get => isCTSEnabled;
            set
            {
                if (isCTSEnabled != value)
                {
                    isCTSEnabled = value;
                    OnPropertyChanged();
                }
            }
        }


        private ConnectionStates _stateConnectionGlonass = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionGlonass
        {
            get => _stateConnectionGlonass; 
            set
            {
                if (_stateConnectionGlonass != value)
                {
                    _stateConnectionGlonass = value;
                    OnPropertyChanged();
                }
            }
        }


        private bool isRTSEnabledGeos = false;

        public bool IsRTSEnabledGeos
        {
            get => isRTSEnabledGeos;
            set
            {
                if (isRTSEnabledGeos != value)
                {
                    isRTSEnabledGeos = value;
                    OnPropertyChanged();
                }
            }
        }


        private bool isDSREnabledGeos = false;

        public bool IsDSREnabledGeos
        {
            get => isDSREnabledGeos;
            set
            {
                if (isDSREnabledGeos != value)
                {
                    isDSREnabledGeos = value;
                    OnPropertyChanged();
                }
            }
        }


        private ConnectionStates _stateConnectionServer = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionServer
        {
            get => _stateConnectionServer;
            set
            {
                if (_stateConnectionServer != value)
                {
                    _stateConnectionServer = value;
                    OnPropertyChanged();
                }
            }
        }

        private ConnectionStates _stateConnectionMosaic = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionMosaic
        {
            get => _stateConnectionMosaic;
            set
            {
                if (_stateConnectionMosaic != value)
                {
                    _stateConnectionMosaic = value;
                    OnPropertyChanged();
                }
            }
        }

        #endregion

        //private bool _noGpsTime;

        //public bool NoGpsTime
        //{
        //    get { return _noGpsTime; }
        //    set
        //    {
        //        if (_noGpsTime != value)
        //        {
        //            _noGpsTime = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        private DateTime _receiverTime;

        public DateTime ReceiverTime
        {
            get { return _receiverTime; }
            set
            {
                if (_receiverTime != value)
                {
                    _receiverTime = value;
                    OnPropertyChanged();
                }
            }
        }



        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion
    }
}
