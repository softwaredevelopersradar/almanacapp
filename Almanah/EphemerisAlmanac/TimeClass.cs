﻿using System;
using System.Runtime.InteropServices;
using System.Linq;
using System.Collections.ObjectModel;

namespace EphemerisAlmanac
{
    public class TimeClass
    {
        ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();

        [StructLayout(LayoutKind.Sequential)]
        public struct SYSTEMTIME
        {
            public short wYear;
            public short wMonth;
            public short wDayOfWeek;
            public short wDay;
            public short wHour;
            public short wMinute;
            public short wSecond;
            public short wMilliseconds;
        }

        //[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        //public struct DynamicTimeZoneInformation
        //{
        //    public int Bias;
        //    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        //    public string StandardName;
        //    public SYSTEMTIME StandardDate;
        //    public int StandardBias;
        //    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        //    public string DaylightName;
        //    public SYSTEMTIME DaylightDate;
        //    public int DaylightBias;
        //    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        //    public string TimeZoneKeyName;
        //    [MarshalAs(UnmanagedType.U1)]
        //    public bool DynamicDaylightTimeDisabled;
        //}

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetSystemTime(ref SYSTEMTIME st);

        //[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        //private static extern bool SetDynamicTimeZoneInformation([In] ref DynamicTimeZoneInformation lpTimeZoneInformation);

        public TimeClass(DateTime GNSStime, string timeZoneName)
        {
            //var pos = timeZones.IndexOf(timeZones.Where(item => item.DisplayName == timeZoneName).FirstOrDefault());
            //if (pos != -1)
            //{
            //    TimeZoneInfo zone = timeZones[pos];
            //    DynamicTimeZoneInformation tz = new DynamicTimeZoneInformation();
            //    tz.TimeZoneKeyName = zone.DisplayName;
            //    SetDynamicTimeZoneInformation(ref tz);
            //}
            SYSTEMTIME st = new SYSTEMTIME();
            st.wYear = (short)GNSStime.Year; 
            st.wMonth = (short)GNSStime.Month;
            st.wDay = (short)GNSStime.Day;
            //st.wHour = (short)(GNSStime.Hour + timeZones[pos].BaseUtcOffset.Hours); //проверить т.к. 12часов, вместо 24
            //st.wMinute = (short)(GNSStime.Minute + timeZones[pos].BaseUtcOffset.Minutes);
            st.wHour = (short)(GNSStime.Hour); //проверить т.к. 12часов, вместо 24
            st.wMinute = (short)(GNSStime.Minute);
            st.wSecond = (short)GNSStime.Second;

            SetSystemTime(ref st);
        }
    }
}
