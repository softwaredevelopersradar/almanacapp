﻿using DllServerGnssProperties.Models;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace EphemerisAlmanac
{
    /// <summary>
    /// Interaction logic for TrimbleLogger.xaml
    /// </summary>
    public partial class TrimbleLogger : Window
    {
        bool canShowRX = false;
        bool canShowTX = false;
        bool isStopPressed = false;
        FlowDocument myFlowDoc;

        public TrimbleLogger()
        {
            InitializeComponent();
            myFlowDoc = new FlowDocument();
            DataContext = this;
        }

        private void RX_Checked(object sender, RoutedEventArgs e)
        {
            if (RX.IsChecked == true)
            {
                canShowRX = true;
            }
            else
            {
                canShowRX = false;
            }
        }

        private void TX_Checked(object sender, RoutedEventArgs e)
        {
            if (TX.IsChecked == true)
            {
                canShowTX = true;
            }
            else
            {
                canShowTX = false;
            }
        }

        public void PrintText(List<byte> data, bool isItMyData)
        {
            try
            {
                if (RichTextBox.Document.Blocks.Count == 100)
                {
                    RichTextBox.Document.Blocks.Remove(RichTextBox.Document.Blocks.FirstBlock);
                }
                string forCommit = "";
                if (isItMyData && canShowTX)
                {
                    forCommit = BitConverter.ToString(data.ToArray()).Replace("-", " ");
                    TextRange rangeOfText1 = new TextRange(RichTextBox.Document.ContentEnd, RichTextBox.Document.ContentEnd);
                    rangeOfText1.Text = "\n" + forCommit;
                    rangeOfText1.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Red);
                }
                if (!isItMyData && canShowRX)
                {
                    forCommit = BitConverter.ToString(data.ToArray()).Replace("-", " ");
                    TextRange rangeOfText1 = new TextRange(RichTextBox.Document.ContentEnd, RichTextBox.Document.ContentEnd);
                    rangeOfText1.Text = "\n" + forCommit;
                    rangeOfText1.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.ForestGreen);
                }
                RichTextBox.ScrollToEnd();
            }
            catch { }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RichTextBox.Document.Blocks.Clear();
            }
            catch { }
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            if (isStopPressed) 
            {
                btnStop.Content = "Stop";
                isStopPressed = false;
                if (TX.IsChecked == true)
                {
                    canShowTX = true;
                }

                if (RX.IsChecked == true)
                {
                    canShowRX = true;
                }
                
            }
            else
            {
                btnStop.Content = "Start";
                isStopPressed = true;
                canShowTX = false;
                canShowRX = false;
            }
        }


        public void SetLogerLanguage(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/ServerGNSS;component/Languages/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case Languages.RU:
                        dict.Source = new Uri("/ServerGNSS;component/Languages/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.AZ:
                        dict.Source = new Uri("/ServerGNSS;component/Languages/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.SRB:
                        dict.Source = new Uri("/ServerGNSS;component/Languages/StringResource.SRB.xaml",
                            UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }
    }
}
