﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;

namespace EphemerisAlmanac
{
    public struct ForQueue
    {
        
        public DateTime time { get; set; }
        public string message { get; set; }

        public ForQueue(DateTime _time, string _mes)
        {
            time = _time;
            message = _mes;
        }
    }

    public class AlmanacLogger
    {
        static string fileLogGNSS { get; set; } = ".\\logGNSS.txt";
        //public static Queue<ForQueue> LoggerRequests;
        //private bool isLoggerStarted = false;



        public AlmanacLogger()
        {
            
            System.IO.File.WriteAllText(fileLogGNSS, string.Empty);
            //StartLogger();
        }


        #region Thread
        //public void StartLogger()
        //{
        //    LoggerRequests = new Queue<ForQueue>();
        //    isLoggerStarted = true;
        //    Task.Run(() => CheckQueue());
        //}

        //public void StopLogger()
        //{
        //    isLoggerStarted = false;
        //}

        //async Task CheckQueue()
        //{
        //    while (isLoggerStarted)
        //    {
        //        if (LoggerRequests.Count != 0)
        //        {
        //            await Log(LoggerRequests.Peek());
        //            LoggerRequests.Dequeue();
        //            Thread.Sleep(100);
        //        }
        //    }
        //}

        //public async Task Log(ForQueue logMessage)
        //{
        //    StringBuilder fullAlm = new StringBuilder();
        //    fullAlm.AppendLine($"{logMessage.time.ToLongTimeString()} {logMessage.time.ToLongDateString()} : {logMessage.message}");


        //    using (StreamWriter outfile = File.AppendText(fileLogGNSS))
        //    {
        //        outfile.Write(fullAlm.ToString());
        //        outfile.Dispose();
        //        outfile.Close();
        //    }
        //}
        #endregion


        #region 1 time

        //async Task CheckQueue()
        //{
        //    while (isLoggerStarted)
        //    {
        //        if (LoggerRequests.Count != 0)
        //        {
        //            await Log(LoggerRequests.Peek());
        //            LoggerRequests.Dequeue();
        //            Thread.Sleep(100);
        //        }
        //    }
        //}

        public void Log(ForQueue logMessage)
        {
            StringBuilder fullAlm = new StringBuilder();
            fullAlm.AppendLine($"{logMessage.time.ToLongTimeString()} {logMessage.time.ToLongDateString()} : {logMessage.message}");


            using (StreamWriter outfile = File.AppendText(fileLogGNSS))
            {
                outfile.Write(fullAlm.ToString());
                outfile.Dispose();
                outfile.Close();
            }
        }
        #endregion
    }
}
