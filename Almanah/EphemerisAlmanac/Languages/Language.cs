﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.IO;
using System.Xml;
using DllServerGnssProperties.Models;

namespace EphemerisAlmanac
{
    public partial class MainWindow : Window
    {

        public void SetLanguage(Languages language)
        {
            SetResourceLanguage(language);
            Translator.LoadDictionary(language);
            _trimbleLogger?.SetLogerLanguage(language);
        }

        private void SetResourceLanguage(Languages language)
        {
            this.Resources.MergedDictionaries.Clear(); 
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/ServerGNSS;component/Languages/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case Languages.RU:
                        dict.Source = new Uri("/ServerGNSS;component/Languages/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.AZ:
                        dict.Source = new Uri("/ServerGNSS;component/Languages/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.SRB:
                        dict.Source = new Uri("/ServerGNSS;component/Languages/StringResource.SRB.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/EphemerisAlmanac;component/Languages/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch
            { }

        }

        public class Translator
        {
                static Dictionary<string, string> TranslateDic;
            public static void LoadDictionary(Languages language)
            {
                XmlDocument xDoc = new XmlDocument();



                if (File.Exists(Directory.GetCurrentDirectory() + "\\Languages\\Translator.xml"))
                    xDoc.Load(Directory.GetCurrentDirectory() + "\\Languages\\Translator.xml");


                TranslateDic = new Dictionary<string, string>();

                // получим корневой элемент
                XmlElement xRoot = xDoc.DocumentElement;
                foreach (XmlNode x2Node in xRoot.ChildNodes)
                {
                    if (x2Node.NodeType == XmlNodeType.Comment)
                        continue;

                    // получаем атрибут ID
                    if (x2Node.Attributes.Count > 0)
                    {
                        XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                        if (attr != null)
                        {
                            foreach (XmlNode childnode in x2Node.ChildNodes)
                            {
                                // если узел - language
                                if (childnode.Name == language.ToString())
                                {
                                    if (!TranslateDic.ContainsKey(attr.Value))
                                        TranslateDic.Add(attr.Value, childnode.InnerText);
                                }
                            }
                        }
                    }
                }

                if (TranslateDic.Count > 0 && TranslateDic != null)
                {
                    RenameMessages(TranslateDic);
                }
            }
        }


        /// <summary>
        /// Переименование сообщений при смене языка
        /// </summary>
        /// <param name="Dictionary"></param>
        public static void RenameMessages(Dictionary<string, string> TranslateDic)
        {
            if (TranslateDic.ContainsKey("meaningAskWindow"))
                SMeaning.meaningAskWindow = TranslateDic["meaningAskWindow"];

            if (TranslateDic.ContainsKey("meaningAskAlm"))
                SMeaning.meaningAskAlm = TranslateDic["meaningAskAlm"];

            if (TranslateDic.ContainsKey("meaningAskEph"))
                SMeaning.meaningAskEph = TranslateDic["meaningAskEph"];

            if (TranslateDic.ContainsKey("meaningLogWindow"))
                SMeaning.meaningLogWindow = TranslateDic["meaningLogWindow"];

            if (TranslateDic.ContainsKey("meaningAskEpoch"))
                SMeaning.meaningAskEpoch = TranslateDic["meaningAskEpoch"];

            if (TranslateDic.ContainsKey("meaningWarning"))
                SMeaning.meaningWarning = TranslateDic["meaningWarning"];
        }


        public struct SMeaning
        {
            public static string meaningAskWindow = "File updating";
            public static string meaningAskAlm = "Would you like to update Almanac file?";
            public static string meaningAskEph = "Would you like to update Ephemeris file?";
            public static string meaningLogWindow = "Receiver log";
            public static string meaningAskEpoch = "It is not recommended to update files when changing epochs!";
            public static string meaningWarning = "Warning!";
            //public static string meaningAskWindow = "Обновление файла";
            //public static string meaningAskAlm = "Обновить файл альманаха?";
            //public static string meaningAskEph = "Обновить файл эфемерид?";

            //public static void InitSMeaning()
            //{
            //    meaningAddRecord =;
            //    meaningChangeRecord ;
            //}
        }

        //private void basicProperties_OnLanguageChanged(object sender, Languages language)
        //{


        //}


    }
}
