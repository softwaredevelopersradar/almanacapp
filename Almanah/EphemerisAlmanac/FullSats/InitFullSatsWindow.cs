﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Threading;
using EphemerisAlmanac.FullSats;
using SatelliteTableControl;

namespace EphemerisAlmanac
{
    public partial class MainWindow : Window
    {
        private EphemerisAlmanac.FullSats.FullSatsWindow fullSatsWindow;


        private void InitFullSatsWindow()
        {
            fullSatsWindow = new FullSatsWindow();
        }



        private void UpdateTablePattern(ObservableCollection<FullSatellite> sats)
        {

            try
            {

                Dispatcher.BeginInvoke(new Action(() =>
                {
                    fullSatsWindow.UpdateSats(sats);
                }), DispatcherPriority.Background);

                //_ = Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, (ThreadStart)delegate ()
                //{
                //    try
                //    {

                //        SatTableControl.Satellites = new System.Collections.ObjectModel.ObservableCollection<SatFullInfoTable>(sats.Select(u => new SatFullInfoTable(u.PRN, u.SNR, u.Azimuth, u.Elevation, TypeGnss.Gps, u.TrueAcquisitionFlag, u.EphemerisFlag, u.OldMeasureFlag, u.BadDataFlag)));
                //    }
                //    catch (Exception e)
                //    {
                //        Gnss_Message(this, "z8, error: " + e);
                //    }
                //});
            }

            catch
            {
            }


        }

        


    }
}
