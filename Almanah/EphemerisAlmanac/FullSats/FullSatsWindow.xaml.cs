﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;
using SatelliteTableControl;

namespace EphemerisAlmanac.FullSats
{
    /// <summary>
    /// Interaction logic for FullSatsWindow.xaml
    /// </summary>
    public partial class FullSatsWindow : Window, INotifyPropertyChanged
    {
        public FullSatsWindow()
        {
            InitializeComponent();
            DataContext=this;
        }
        

        //public ObservableCollection<FullSatellite> Satellites { get; set; }

        public void UpdateSats(ObservableCollection<FullSatellite> sats)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    listSats.Satellites = sats;
                }), DispatcherPriority.Background);
            }

            catch
            {
            }
        }
        ///// <summary>
        ///// Обновить значение координат
        ///// </summary>
        //public void UpdateSatInfo(List<Satellite> targets)
        //{
        //    try
        //    {
        //        if (targets == null)
        //            return;
        //        //deleteSelection = true;


        //        ((GlobalState)paramSatellite.DataContext).FullParams.Clear();
        //        for (int i = 0; i < targets.Count; i++)
        //        {
        //            ((GlobalState)paramSatellite.DataContext).FullParams.Add(targets[i].Clone());
        //        }


        //        //paramSatellite.ItemsSource = ((GlobalState)paramSatellite.DataContext).FullParams;
        //        // AddEmptyRows();
        //        //deleteSelection = false;
        //    }
        //    catch { }
        //}








        //public void SetLanguage(DllGrozaSProperties.Models.Languages language)
        //{
        //    ResourceDictionary dict = new ResourceDictionary();
        //    try
        //    {
        //        switch (language)
        //        {
        //            case DllGrozaSProperties.Models.Languages.EN:
        //                dict.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.EN.xaml",
        //                              UriKind.Relative);
        //                break;

        //            case DllGrozaSProperties.Models.Languages.RU:
        //                dict.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.RU.xaml",
        //                                   UriKind.Relative);
        //                break;
        //            case DllGrozaSProperties.Models.Languages.AZ:
        //                dict.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.AZ.xaml",
        //                                   UriKind.Relative);
        //                break;
        //        }

        //        this.Resources.MergedDictionaries.Add(dict);
        //    }
        //    catch (Exception ex)
        //    { }
        //}

        //public void SetPropertyLanguage(DllGrozaSProperties.Models.Languages language)
        //{
        //    tempProperty.SetLanguagePropertyGrid(language);
        //}

        private void WindowSats_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;

        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion
    }
}
