﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using TSIP;
using System.Threading;
using System.Windows.Threading;
using ToRinexConverter;

namespace EphemerisAlmanac
{
    using System.Threading.Tasks;

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private AlmanacConverter almanacConverter;
        private EphemerisConverter ephemerisConverter;
        //enum units
        //{ Test, Format} //временно

        //Dictionary<byte, string> TypeCodeCollection = new Dictionary<byte, string>() { { (byte)DeviceType.ThunderboltE, "Thunderbolt E" }, { (byte)DeviceType.MiniTGG, "Mini-T GG" } };
        //enum DeviceType : byte
        //{
        //    ThunderboltE = 1,
        //    MiniTGG = 2
        //}



        MainWindowViewModel mainWindowViewModel;
        GridLength SavedGridLength; //ширина окна PropertyGrid
        GridLength SavedLogWidth { get; set; } //ширина Server Log

        TrimbleLogger _trimbleLogger;
        public AlmanacLogger AlmanacLogger;

        public MainWindow()
        {
            InitializeComponent();
            mainWindowViewModel = new MainWindowViewModel();
            DataContext = mainWindowViewModel;

            Uri iconUri = new Uri("pack://application:,,,/ServerGNSS;component/Images/space.ico");
            FullWindow.Icon = BitmapFrame.Create(iconUri);
            //AlmanacLogger = new AlmanacLogger();

            settingsContr.SetPassword("256");
            InitLocalProperties();
            SetLanguage(settingsContr.Local.General.Language); //мб не надо тут

            //конверторы
            almanacConverter = new AlmanacConverter(settingsContr.Local.Gps.AlmanacFileName, settingsContr.Local.Glonass.AlmanacFileNameGlonass);
            almanacConverter.OnGpsAlmanacUpdated += UpdateAlmStatus;
            ephemerisConverter = new EphemerisConverter(settingsContr.Local.Gps.EphemerisFileName, settingsContr.Local.Glonass.EphemerisFileNameGlonass);
            ephemerisConverter.OnGpsEphemerisUpdated += UpdateEphStatus;
            ephemerisConverter.OnGlonassEphemerisUpdated += UpdateFilesStatus;
            ephemerisConverter.Message += EphemerisConverter_Message;
            //ephemerisConverter.MessageSatsForLog += Gnss_SatMessage;

            SavedLogWidth = new System.Windows.GridLength(300);
            AlmanacLogger = new AlmanacLogger();
            SetLanguage(settingsContr.Local.General.Language);
            //ConnectGNSS();
            StartServer();

            if (mainWindowViewModel.LocalPropertiesVM.Gps.Existance)
            {
                ConnectGps();
            }
            else
            {
                if (mainWindowViewModel.LocalPropertiesVM.Glonass.Existance)
                {
                    ConnectGlonass();
                }
                if (mainWindowViewModel.LocalPropertiesVM.MosaicX5.Existance)
                {
                    ConnectMosaicX5();
                }
            }
           
            //InitFullSatsWindow();
        }

        private void EphemerisConverter_Message(object sender, string e)
        {
            Gnss_Message(this, e);
        }






        #region Buttons
        /// <summary>
        /// Сокрытие или отображение поля настроек
        /// </summary>
        private void BtnSettings_Click(object sender, RoutedEventArgs e)
        {
            if (BaseField.ColumnDefinitions[2].Width != new GridLength(0))
            {
                SavedGridLength = BaseField.ColumnDefinitions[2].Width;
                BaseField.ColumnDefinitions[2].Width = new System.Windows.GridLength(0);
                Lp_Splitter(false, gridGeneralSplitterV); //убрать разделитель
            }
            else
            {
                BaseField.ColumnDefinitions[2].Width = SavedGridLength;
                Lp_Splitter(true, gridGeneralSplitterV);
            }
        }

        private void Lp_Splitter(bool value, GridSplitter gridSplitter)
        {
            if (value)
            {
                gridSplitter.IsEnabled = true;
            }
            else
            {
                gridSplitter.IsEnabled = false;
            }

        }


        /// <summary>
        ///Открыть Лог-файл сервера
        /// </summary>
        private void LogServer_Click(object sender, RoutedEventArgs e)
        {
            if (BaseField.ColumnDefinitions[6].Width != new GridLength(0) && ServerLogger!=null)
            {
                SavedGridLength = BaseField.ColumnDefinitions[6].Width;
                BaseField.ColumnDefinitions[6].Width = new System.Windows.GridLength(0);
                Lp_Splitter(false, gridLogSplitter); //убрать разделитель
            }
            else
            {
                BaseField.ColumnDefinitions[6].Width = SavedLogWidth;
                Lp_Splitter(true, gridLogSplitter);
            }
        }


        /// <summary>
        ///Открыть Лог-файл приемника
        /// </summary>
        private void LogReciever_Click(object sender, RoutedEventArgs e)
        {
            if (_trimbleLogger!= null)
            {
                _trimbleLogger.Close();
                _trimbleLogger = null;
            }
            else
            {
                _trimbleLogger = new TrimbleLogger();
                _trimbleLogger.Title = SMeaning.meaningLogWindow;
                _trimbleLogger.SetLogerLanguage(settingsContr.Local.General.Language);
                _trimbleLogger.Show();
                _trimbleLogger.Closed += _trimbleLogger_Closed; ;
            }
            
        }


        private void _trimbleLogger_Closed(object sender, EventArgs e)
        {
            LogReciever_Click(this, new RoutedEventArgs());
            LogReciever.IsChecked = false;
        }


        private void WarmReset_Click(object sender, RoutedEventArgs e)
        {
            if (gpsReceiver == null)
                return;

            gpsReceiver.WarmReset();
        }


        private void ColdReset_Click(object sender, RoutedEventArgs e)
        {
            if (gpsReceiver == null)
                return;

            gpsReceiver.ColdReset();
        }
        #endregion



        private void USRPConnection_Click(object sender, RoutedEventArgs e)
        {

        }

        //public void HandlerProperties(object sender, BusinessObject arg)
        //{
        //    YamlSave(arg);
        //    SetLanguage(settingsContr.MySettings.Language);
        //}
        

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            Gnss_Message(this, "Closing window");
            DisconnectGps();
            DisconnectGlonass();
            StopServer();
            System.Windows.Threading.Dispatcher.ExitAllFrames();
        }




        

        private void UpdateAlmanac_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (gpsReceiver == null)
                {
                    return;
                }

                if (almanacNotReady)
                {
                    return;
                }

                if (collectingAlmanac)
                {
                    return;
                }

                while (collectingEphemeris)
                {
                    Thread.Sleep(20);
                }

                UpdateGpsAlmanacByButton();
            });

        }

        private void UpdateEphemeris_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (gpsReceiver == null)
                {
                    return;
                }

                //if (settingsContr.Local.Testing.WaitForAlmanac & almanacNotReady)
                //{
                //    return;
                //}

                if (collectingEphemeris)
                {
                    return;
                }

                while (collectingAlmanac)
                {
                    Thread.Sleep(20);
                }


                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    var time = RadarControl.ReceiverTime;
                    var isSpooofAvailable = CheckSpoofingAvailability(time);
                    if (!isSpooofAvailable.Item1)
                    {
                        MessageBox.Show(SMeaning.meaningAskEpoch, SMeaning.meaningWarning, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
                    }
                    else UpdateGpsEphemerisByButton();
                });

            });
            
        }

        private void Test_Click(object sender, RoutedEventArgs e)
        {

           

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.DataBind, (ThreadStart)delegate ()
            {
                var time = RadarControl.ReceiverTime;
                var isSpooofAvailable = CheckSpoofingAvailability(time);
                if (!isSpooofAvailable.Item1 && !settingsContr.Local.Gps.AutoControlEpochChange)
                {
                    MessageBox.Show(SMeaning.meaningAskEpoch, SMeaning.meaningWarning, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
                }
                Thread.Sleep(100);

                if (isSpooofAvailable.Item1 && settingsContr.Local.Gps.AutoControlEpochChange ? isSpooofAvailable.Item1 : isSpooofAvailable.Item1 ? AskUser(SMeaning.meaningAskEph) : false)
                {
                    //Thread.Sleep(100);
                    RadarControl.TickSystemTime(this, null);
                    time = RadarControl.ReceiverTime;
                    isSpooofAvailable = CheckSpoofingAvailability(time);
                    if (!isSpooofAvailable.Item1)
                    {
                        MessageBox.Show(SMeaning.meaningAskEpoch, SMeaning.meaningWarning, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
                    }
                    else { UpdateGpsEphemerisByButton(); }
                }
                else
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        RadarControl.ContinueTimerEph(settingsContr.Local.Gps.AutoControlEpochChange ? isSpooofAvailable.Item2 : settingsContr.Local.Gps.IntervalEphemeris);
                    });
                    //timerEph.Change((settingsContr.Local.Gps.AutoControlEpochChange ? isSpooofAvailable.Item2 : settingsContr.Local.Gps.IntervalEphemeris) * 60_000, Timeout.Infinite);
                }
            });
        }

        private void Time_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                RadarControl.ReceiverTime = new DateTime(2021, 9, 13, 11, 53, 50);
            });
        }

        private void FullSatsWindow_Click(object sender, RoutedEventArgs e)
        {
            if (fullSatsWindow.IsVisible)
                fullSatsWindow.Hide();
            else
                fullSatsWindow.Show();
        }

        private void btnBlockRTSTrimble_Click(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
                {
                    this.BlockComPortTrimble(!this.mainWindowViewModel.IsRTSEnabled);
                    BlockComPortEscope(!this.mainWindowViewModel.IsDTREnabled);
                });
        }

        private void btnBlockRTSGeos_Click(object sender, RoutedEventArgs e)
        {
            Task.Run(() => BlockComPortGeos(!this.mainWindowViewModel.IsRTSEnabledGeos));
        }

        private void MosaicConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.mosaicReceiver != null)
                {
                    DisconnectMosaicX5();
                }
                else
                {
                    this.ConnectMosaicX5();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateEphemerisMosaic_Click(object sender, RoutedEventArgs e)
        {
            mosaicReceiver?.GetAllEphemeris();
        }
    }
}
