﻿
using GeostarBinaryProtocol;
using System;
using System.Collections.Generic;
using System.Threading;

namespace EphemerisAlmanac
{
    public partial class MainWindow
    {
        private GeoS glonassReceiver;
        private bool collectingGlonass = false;
        private List<EphemerisGlonass> ephemerisGeo = new List<EphemerisGlonass>();
        private Dictionary<byte,List<EphemerisGlonass>> ephemerisGeoAccumulated = new Dictionary<byte, List<EphemerisGlonass>>();
        private List<AlmanacGlonass> almanacGeo = new List<AlmanacGlonass>();
        private Dictionary<byte, List<EphemerisGps>> ephemerisGpsAccumulated = new Dictionary<byte, List<EphemerisGps>>();
        private List<AlmanacGps> almanacGps = new List<AlmanacGps>();

        private void ConnectGlonass()
        {
            if (glonassReceiver != null)
            {
                glonassReceiver.Disconnect();
                glonassReceiver = null;
            }

            glonassReceiver = new GeoS();
            glonassReceiver.OnConnect += HandlerConnect_Glonass;
            glonassReceiver.OnGetAlmanacGlonass += HandlerGetAlmanac_Glonass;
            glonassReceiver.OnGetEphemeriesGlonass += HandlerGetEphemeries_Glonass;
            glonassReceiver.OnGetAlmanacGps += GlonassReceiver_OnGetAlmanacGps; 
            glonassReceiver.OnGetEphemeriesGps += GlonassReceiver_OnGetEphemeriesGps; 
            glonassReceiver.OnGetTimeInfo += HandlerGetTimeInfo_Glonass;
            glonassReceiver.OnDisconnect += HandlerDisconnect_Glonass;
            glonassReceiver.OnGetSatellitesInfo += GlonassReceiver_OnGetSatellitesInfo;
            glonassReceiver.Message += GlonassReceiver_Message;
            glonassReceiver.OnGetLocation += GlonassReceiver_OnGetLocation;
            glonassReceiver.OnPortState += GlonassReceiver_OnPortState;
            glonassReceiver.Connect(settingsContr.Local.Glonass.ComPort, settingsContr.Local.Glonass.ComPortSpeed, (GeoS.VersionR)settingsContr.Local.Glonass.Version); //115200
            //glonassReceiver.GetTimeInfo();
            InitAccumulating();
        }

        private void BlockComPortGeos(bool isEnable)
        {
            if (glonassReceiver != null)
            {
                glonassReceiver.BlockComPortRTS(isEnable);
                glonassReceiver.BlockAntennaSupply(isEnable);
            }
        }


        private void GlonassReceiver_Message(object sender, string e)
        {
            Thread.Sleep(100);
            try
            {
                AlmanacLogger.Log(new ForQueue(DateTime.Now, e));
            }
            catch (Exception ex)
            { }
        }

        private void InitAccumulating()
        {
            ephemerisGeoAccumulated = new Dictionary<byte, List<EphemerisGlonass>>();
            for (int i = 1; i <= 24; i++)
            {
                ephemerisGeoAccumulated.Add((byte)i, new List<EphemerisGlonass>());
            }

            ephemerisGpsAccumulated = new Dictionary<byte, List<EphemerisGps>>();
            for (int i = 1; i <= 32; i++)
            {
                ephemerisGpsAccumulated.Add((byte)i, new List<EphemerisGps>());
            }
        }


        private void DisconnectGlonass()
        {
            if (glonassReceiver == null)
                return;

            glonassReceiver.Disconnect();

            glonassReceiver.OnConnect -= HandlerConnect_Glonass;
            glonassReceiver.OnGetAlmanacGlonass -= HandlerGetAlmanac_Glonass;
            glonassReceiver.OnGetEphemeriesGlonass -= HandlerGetEphemeries_Glonass;
            glonassReceiver.OnGetAlmanacGps -= GlonassReceiver_OnGetAlmanacGps; 
            glonassReceiver.OnGetEphemeriesGps -= GlonassReceiver_OnGetEphemeriesGps; 
            glonassReceiver.OnGetTimeInfo -= HandlerGetTimeInfo_Glonass;
            glonassReceiver.OnDisconnect -= HandlerDisconnect_Glonass; 
            glonassReceiver.OnGetSatellitesInfo -= GlonassReceiver_OnGetSatellitesInfo;
            glonassReceiver.Message -= GlonassReceiver_Message;
            glonassReceiver.OnGetLocation -= GlonassReceiver_OnGetLocation;
            glonassReceiver.OnPortState -= GlonassReceiver_OnPortState;
            glonassReceiver = null;
        }


        


    }
}
