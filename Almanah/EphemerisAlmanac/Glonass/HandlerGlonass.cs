﻿using System;
using System.Threading;
using ToRinexConverter;
using GeostarBinaryProtocol;
using System.Windows;
using WPFControlConnection;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Linq;
using Radar;
using SatelliteTableControl;

namespace EphemerisAlmanac
{
    public partial class MainWindow
    {
        Timer timerGlonassFiles;
        Timer timerLostGlonassFiles;



        private void GlonassConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (glonassReceiver != null)
                {
                    DisconnectGlonass();
                }
                else
                {
                    ConnectGlonass();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void HandlerConnect_Glonass(object sender, string e)
        {
            mainWindowViewModel.StateConnectionGlonass = ConnectionStates.Connected;


            TimerCallback timerCallbackAlm = new TimerCallback(GlonassFilesRequest);
            timerGlonassFiles = new Timer(timerCallbackAlm, null, 0, Timeout.Infinite);


            //таймеры на ожидание сбора
            TimerCallback timerCallbackLostAlm = new TimerCallback(GlonassFilesLost);
            timerLostGlonassFiles = new Timer(timerCallbackLostAlm, null, Timeout.Infinite, Timeout.Infinite);
        }

        private void HandlerDisconnect_Glonass(object sender, bool e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                mainWindowViewModel.StateConnectionGlonass = ConnectionStates.Disconnected;
            });

            RadarControl.UpdateGlonassFilesState(Radar.Status.Empty);

            _ = Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, (ThreadStart)delegate ()
            {
                SatTableControl.Satellites = new System.Collections.ObjectModel.ObservableCollection<SatelliteTableControl.Satellite>(new List<SatelliteTableControl.Satellite>());
                //SatTableControl.UpdateSatInfo(new List<Satellite>());
            });

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                RadarControl.ClearAll();
            });
        }


        #region Auto requests 

        /// <summary>
        /// Запрос альманаха по таймеру
        /// </summary>
        private void GlonassFilesRequest(object obj)
        {
            if (glonassReceiver == null)
            {
                return;
            }
            UpdateGlonassFiles();
        }

        private void GlonassFilesLost(object obj)
        {
            try
            {
                collectingGlonass = false;
                RadarControl.UpdateGlonassFilesState(Radar.Status.Updated);
                //gpsReceiver?.ClearAlmanacs();
                timerGlonassFiles.Change(0, Timeout.Infinite);
            }
            catch (Exception e)
            { Gnss_Message(this, "z20, error: " + e); }
        }
        #endregion


        private void HandlerGetTimeInfo_Glonass(object sender, TimeGeoS e)
        {
            string mess = string.Empty;

            mess += $"UTS :" + e.C_utc + ", " +
            $"  Gps : {e.C_gps}," +
            $"  B1 : {e.B1}," +
            $"  B2 : {e.B2}," +
            $"  N4 : {e.N4}," +
            $"  Nsutki : {e.Nsutki}," +
            $"  Nalm :{e.Nalm}," + "\n";



            System.IO.File.WriteAllText(".\\logTime.txt", mess);
        }

        private void AccumulateRecords(List<GeostarBinaryProtocol.EphemerisGlonass> listGlo)
        {
            foreach (var record in listGlo)
            {
                if (!ephemerisGeoAccumulated.ContainsKey(record.Num))
                    continue;
                if (ephemerisGeoAccumulated[record.Num].Exists(c=> c.tb == record.tb) || record.tb == 0)
                    continue;

                ephemerisGeoAccumulated[record.Num].Add(record);
            }
            
        }

        private void AccumulateRecordsGps(List<GeostarBinaryProtocol.EphemerisGps> listGps)
        {
            foreach (var record in listGps)
            {
                if (!ephemerisGpsAccumulated.ContainsKey(record.sv_number))
                    continue;
                if (ephemerisGpsAccumulated[record.sv_number].Exists(c => c.t_oe == record.t_oe) || record.t_oe == 0)
                    continue;

                ephemerisGpsAccumulated[record.sv_number].Add(record);
            }

        }

        private void HandlerGetEphemeries_Glonass(object sender, List<GeostarBinaryProtocol.EphemerisGlonass> e)
        {
            //EphemerisConverter._fileNameGlonass = settingsContr.Local.Glonass.EphemerisFileName;

            ephemerisGeo = new List<GeostarBinaryProtocol.EphemerisGlonass>(e);
            AccumulateRecords(ephemerisGeo);

            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        almanacConverter.WriteFile(settingsContr.Local.Glonass.FolderAlmanacGlonass, almanacGeo, settingsContr.Local.Testing.AccumulateFiles);
                    }
                    catch (Exception ex)
                    { }

                    try
                    {
                        //ephemerisConverter.WriteToRinexGlonass(settingsContr.Local.Glonass.FolderEphemeris, ephemerisGeo, almanacGeo, settingsContr.Local.Testing.AccumulateFiles);
                        ephemerisConverter.WriteToRinexGlonass(settingsContr.Local.Glonass.FolderEphemerisGlonass, ephemerisGeoAccumulated, almanacGeo, settingsContr.Local.Testing.AccumulateFiles);
                    }
                    catch (Exception ex)
                    { }
                });


                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        RadarControl.UpdateLastTimeGlonassFiles(DateTime.Now, settingsContr.Local.Glonass.IntervalData);
                    }
                    catch (Exception ex)
                    {
                    }
                });

                timerGlonassFiles.Change(settingsContr.Local.Glonass.IntervalData * 60000, Timeout.Infinite);
                collectingGlonass = false;
                timerLostGlonassFiles.Change(Timeout.Infinite, Timeout.Infinite);

                //try
                //{
                //    Almanacs = ConvertAlmanacsForClient(almanacTSIP);
                //}
                //catch (Exception e)
                //{
                //    Gnss_Message(this, "z4, error: " + e);
                //}
            }
            catch (Exception ex)
            {  }
            


        }

        private void HandlerGetAlmanac_Glonass(object sender, List<GeostarBinaryProtocol.AlmanacGlonass> e)
        {
            try
            {
                almanacGeo = new List<GeostarBinaryProtocol.AlmanacGlonass>(e);

                almanacConverter.WriteFile(settingsContr.Local.Glonass.FolderAlmanacGlonass, almanacGeo, settingsContr.Local.Testing.AccumulateFiles);
            }
            catch (Exception ex)
            { }
        }

        private void GlonassReceiver_OnGetEphemeriesGps(object sender, List<GeostarBinaryProtocol.EphemerisGps> e)
        {
            var ephemeris = new List<GeostarBinaryProtocol.EphemerisGps>(e);
            AccumulateRecordsGps(ephemeris);

            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        //ephemerisConverter.WriteToRinexGlonass(settingsContr.Local.Glonass.FolderEphemeris, ephemerisGeo, almanacGeo, settingsContr.Local.Testing.AccumulateFiles);
                        ephemerisConverter.WriteFile(settingsContr.Local.Glonass.FolderEphemerisGps, ephemerisGpsAccumulated, settingsContr.Local.Testing.AccumulateFiles);
                    }
                    catch (Exception ex)
                    { }
                });

                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        RadarControl.UpdateLastTimeEph(DateTime.Now, settingsContr.Local.Glonass.IntervalData);
                    }
                    catch (Exception ex)
                    {
                        Gnss_Message(this, "z2, error: " + ex);
                    }
                });

            }
            catch (Exception ex)
            { }
        }


        private void GlonassReceiver_OnGetLocation(object sender, GeoCoordinates data)
        {
            try
            {
                double x = data.Latitude * 180 / Math.PI;
                double y = data.Longitude * 180 / Math.PI;
                double z = data.GeodeticHeight + data.EllipsoidAndGeoidDeviation;

                almanacNotReady = !data.State.IsGpsAlmanacEvailable;
                almanacGlonassNotReady = !data.State.IsGlonassAlmanacEvailable;

                if (almanacNotReady)
                {
                    RadarControl.UpdateAlmanacState(Radar.Status.NotComplete);
                }

                if (almanacGlonassNotReady)
                {
                    RadarControl.UpdateGlonassFilesState(Radar.Status.NotComplete);
                }
                

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RadarControl.UpdateGNSSCoordinates(x, y, z);
                });

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        RadarControl.UpdateSatCount((int)data.SatsUsedInFix);
                    }
                    catch (Exception ex)
                    {
                        Gnss_Message(this, "z9, error: " + ex);
                    }
                });
                RecLocation = new ClientServerLib.ReceiverLocation(x, y, z);
            }
            catch (Exception e)
            { Gnss_Message(this, "z7, error: " + e); }
        }




        private void GlonassReceiver_OnGetAlmanacGps(object sender, List<GeostarBinaryProtocol.AlmanacGps> e)
        {
            try
            {
                almanacGps = new List<GeostarBinaryProtocol.AlmanacGps>(e);

                almanacConverter.WriteFile(settingsContr.Local.Glonass.FolderAlmanacGps, almanacGps, settingsContr.Local.Testing.AccumulateFiles);

                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        RadarControl.UpdateLastTimeAlm(DateTime.Now, settingsContr.Local.Glonass.IntervalData);
                    }
                    catch (Exception ex)
                    {
                        Gnss_Message(this, "z5, error: " + ex);
                    }
                });
            }
            catch (Exception ex)
            { }
        }

        private void UpdateGlonassData_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (glonassReceiver == null)
                {
                    return;
                }

                UpdateGlonassFiles();

            });
        }

        private async void UpdateGlonassFiles()
        {
            collectingGlonass = true;
            await Task.Run(() => RadarControl.UpdateGlonassFilesState(Radar.Status.Collecting));
            await Task.Run(() =>
            {
                //if (!almanacGlonassNotReady) //под вопросом
                //{
                    glonassReceiver?.GetAlmanacGlonass();
                //}
                
                glonassReceiver?.GetEphemeriesGlonass();

                //if (!almanacNotReady)
                //{
                    glonassReceiver?.GetAlmanacGps();
                //}
                glonassReceiver?.GetEphemeriesGps();
            });




            if (collectingGlonass)
            { timerLostGlonassFiles.Change(60_000, Timeout.Infinite); }
        }


        private void GlonassWarmReset_Click(object sender, RoutedEventArgs e)
        {
            glonassReceiver?.WarmReset();
        }


        private void GlonassColdReset_Click(object sender, RoutedEventArgs e)
        {
            glonassReceiver?.ColdReset();
        }

        private void GlonassFactoryReset_Click(object sender, RoutedEventArgs e)
        {
            glonassReceiver?.FactorySettingsReset();
        }


        private void GlonassReceiver_OnGetSatellitesInfo(object sender, VisibleSatellites e)
        {
            try
            {
                if (e.Satellites.Count == 0)
                {
                    return;
                }

                var tergetsForTable = e.Satellites.Where(t => t.PRN <= 32 || t.PRN >= 65).Select(u => new SatelliteTableControl.Satellite() { Azimuth = (float)(u.Azimuth * 180 / Math.PI), Elevation = (float)(u.Elevation * 180 / Math.PI), SNR = u.SNR, Type = u.PRN <= 32 ? TypeGnss.Gps : TypeGnss.Glonass, PRN = u.PRN % 64 }).ToList();
                var targets = e.Satellites.Where(t => t.PRN <=32 || t.PRN >= 65).Select(u => new SkyplotPoint() { Azimuth = (float)(u.Azimuth * 180 / Math.PI), Elevation = (float)(u.Elevation * 180 / Math.PI), Type = u.PRN <= 32 ? GnssType.Gps : GnssType.Glonass, PRN = u.PRN % 64, State= (SatelliteState)u.State }).ToList();
                    
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        RadarControl.UpdatePoints(targets);
                    }
                    catch (Exception ex)
                    {
                        Gnss_Message(this, "z10, error: " + ex);
                    }
                });

                if ((DateTime.Now - lastCoordUpdate) >= TimeSpan.FromSeconds(5))
                {
                    _ = Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, (ThreadStart)delegate ()
                    {
                          try
                          {

                             SatTableControl.Satellites = new System.Collections.ObjectModel.ObservableCollection<SatelliteTableControl.Satellite>(tergetsForTable);
                            
                          //SatTableControl.UpdateSatInfo(tergetsForTable);
                          }
                          catch (Exception ex)
                          {
                              Gnss_Message(this, "z8, error: " + ex);
                          }
                    });
                    
                    lastCoordUpdate = DateTime.Now;
                }
            }
            catch (Exception ex)
            { Gnss_Message(this, "z17, error: " + ex); }
            
        }

        DateTime lastCoordUpdate = DateTime.Now;


        private void GlonassReceiver_OnPortState(object sender, PortState e)
        {
            this.mainWindowViewModel.IsRTSEnabledGeos = e.RtsIsEvalable;
            this.mainWindowViewModel.IsDSREnabledGeos = e.DsrIsEvalable;
        }
    }

    
}
