﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EphemerisAlmanac
{
    public class LoggerX
    {
        string fileLogGNSS { get; set; } = "logGNSS.txt";

        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            w.WriteLine("  :");
            w.WriteLine($"  :{logMessage}");
            w.WriteLine("-------------------------------");
        }

        public static void DumpLog(StreamReader r)
        {
            string line;
            while ((line = r.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
        }

        public LoggerX()
        {
            StreamWriter w = File.AppendText(fileLogGNSS);
            StreamReader r = File.OpenText(fileLogGNSS);
        }
    }
}
