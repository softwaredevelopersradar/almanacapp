﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EphemerisAlmanac
{
    public partial class MainWindow
    {
        private void AccumulateRecords(List<SeptentrioSBF.EphemerisGlonass> listGlo)
        {
            foreach (var record in listGlo)
            {
                if (!ephemerisGlonassAccumulatedMosaic.ContainsKey(record.SvId))
                    continue;
                if (ephemerisGlonassAccumulatedMosaic[record.SvId].Exists(c => c.tb == record.tb) || record.tb == 0)
                    continue;

                ephemerisGlonassAccumulatedMosaic[record.SvId].Add(record);
            }

        }
    }
}
