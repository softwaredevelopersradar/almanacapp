﻿using System;
using System.Collections.Generic;

namespace EphemerisAlmanac
{
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Markup;
    using System.Windows.Threading;

    using Radar;
    using ToRinexConverter;

    using WPFControlConnection;

    using EphemerisGlonass = SeptentrioSBF.EphemerisGlonass;

    public partial class MainWindow
    {
        private SeptentrioSBF.MosaicX5 mosaicReceiver;
        //private Dictionary<byte, List<EphemerisGps>> ephemerisGpsAccumulatedMosaic = new Dictionary<byte, List<EphemerisGps>>();
        DateTime lastMosaicSatsUpdate = DateTime.Now;
        DateTime lastMosaicTimeUpdate = new DateTime();

        private Dictionary<byte, List<EphemerisGlonass>> ephemerisGlonassAccumulatedMosaic = new Dictionary<byte, List<EphemerisGlonass>>();

        private void ConnectMosaicX5()
        {
            if (mosaicReceiver != null)
            {
                mosaicReceiver.Disconnect();
                mosaicReceiver = null;
            }

            mosaicReceiver = new SeptentrioSBF.MosaicX5();
            mosaicReceiver.OnConnect += MosaicReceiver_OnConnect; ;
            mosaicReceiver.OnDisconnect += MosaicReceiver_OnDisconnect; 
            mosaicReceiver.Message += MosaicX5_Message;
            this.mosaicReceiver.OnGetEphemeriesGps += MosaicReceiver_OnGetEphemeriesGps;
            this.mosaicReceiver.OnGetAlmanacGlonass += MosaicReceiver_OnGetAlmanacGlonass;
            this.mosaicReceiver.OnGetEphemeriesGlonass += MosaicReceiver_OnGetEphemeriesGlonass;
            this.mosaicReceiver.OnGetEphemeriesBeidou += MosaicReceiver_OnGetEphemeriesBeidou;

            this.mosaicReceiver.OnGetFullGps += MosaicReceiver_OnGetFullGps;
            this.mosaicReceiver.OnGetFullGlonass += MosaicReceiver_OnGetFullGlonass;
            this.mosaicReceiver.OnGetFullBeidou += MosaicReceiver_OnGetFullBeidou; 
            this.mosaicReceiver.OnGetFullGalileo += MosaicReceiver_OnGetFullGalileo;

            this.mosaicReceiver.OnGetSats += MosaicReceiver_OnGetSats;
            this.mosaicReceiver.OnGetReceiverTime += MosaicReceiver_OnGetReceiverTime;
            this.mosaicReceiver.OnGetGeodeticPosition += MosaicReceiver_OnGetLocalPosition; 
            mosaicReceiver.Connect(settingsContr.Local.MosaicX5.ComPort, settingsContr.Local.MosaicX5.ComPortSpeed);

            InitAccumulatingMosaicX5();
        }

        private void MosaicReceiver_OnGetLocalPosition(object sender, SeptentrioSBF.Models.PVTGeodetic e)
        {
            try
            {
                double x = e.Latitude * 180 / Math.PI;
                double y = e.Longitude * 180 / Math.PI;
                double z = e.Altitude;

                Dispatcher.BeginInvoke(
                    System.Windows.Threading.DispatcherPriority.Normal,
                    (ThreadStart)delegate() { RadarControl.UpdateGNSSCoordinates(x, y, z); });

                Dispatcher.BeginInvoke(
                    System.Windows.Threading.DispatcherPriority.Normal,
                    (ThreadStart)delegate()
                        {
                            try
                            {
                                RadarControl.UpdateSatCount((int)e.NrSV);
                            }
                            catch (Exception ex)
                            {
                                Gnss_Message(this, "z9, error: " + ex);
                            }
                        });

                RecLocation = new ClientServerLib.ReceiverLocation(x, y, z);
            }
            catch
            {
            }
        }

        private void MosaicReceiver_OnGetReceiverTime(object sender, SeptentrioSBF.Models.ReceiverTime e)
        {
            var time = new DateTime(e.UTCYear + 2000, e.UTCMonth <= 0 ? 1 : e.UTCMonth, e.UTCDay <= 0 ? 1 : e.UTCDay, e.UTCHour < 0 ? 0 : e.UTCHour, e.UTCMin < 0 ? 0 : e.UTCMin, e.UTCSec < 0 ? 0 : e.UTCSec);
            
            if (settingsContr.Local.General.AutoUpdateTime && Math.Abs((DateTime.Now - this.lastMosaicTimeUpdate).TotalMinutes) >= this.mainWindowViewModel.LocalPropertiesVM.General.IntervalTimeUpdate)
            {
                var newTime = new TimeClass(time, settingsContr.Local.General.TimeZone);
                this.lastMosaicTimeUpdate = DateTime.Now;
            }
            
            //RadarControl.CorrectCurrentTime(data.Currentdate);
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RadarControl.ReceiverTime = time;
                });
        }

        private void MosaicReceiver_OnGetSats(object sender, List<SeptentrioSBF.Models.SatellitePosition> e)
        {
            try
            {
                if (e.Count == 0)
                {
                    return;
                }

                if (Math.Abs((DateTime.Now - this.lastMosaicSatsUpdate).TotalSeconds) < this.mainWindowViewModel.LocalPropertiesVM.MosaicX5.IntervalCoordUpdate)
                {
                    return;
                }

                var targetsForTable = e.Select(u => new SatelliteTableControl.Satellite() { Azimuth = u.Azimuth, Elevation = u.Elevation, Type = (SatelliteTableControl.TypeGnss)GetTypeBySVID(u.SVID).Item1, PRN = GetTypeBySVID(u.SVID).Item2 }).ToList();
                targetsForTable.RemoveAll(u => u.PRN == 0);
               

                var targets = e.Select(u => new SkyplotPoint() { Azimuth = u.Azimuth, Elevation = u.Elevation, Type = GetTypeBySVID(u.SVID).Item1, PRN = GetTypeBySVID(u.SVID).Item2, State = SatelliteState.Tracked }).ToList();
                targets.RemoveAll(u => u.PRN == 0);
                

                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        try
                        {
                            RadarControl.UpdatePoints(targets);
                        }
                        catch (Exception ex)
                        {
                            Gnss_Message(this, "z10, error: " + ex);
                        }
                    });

                
                _ = Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, (ThreadStart)delegate ()
                    {
                        try
                        {

                            SatTableControl.Satellites = new System.Collections.ObjectModel.ObservableCollection<SatelliteTableControl.Satellite>(targetsForTable);
                        }
                        catch (Exception ex)
                        {
                            Gnss_Message(this, "z8, error: " + ex);
                        }
                    });


                this.lastMosaicSatsUpdate = DateTime.Now;
                
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private (GnssType, int) GetTypeBySVID(byte id)
        {
            if (id <= 37)
            {
                return (GnssType.Gps, id);
            }

            if (id <= 68)
            {
                return (GnssType.Glonass, id <= 61 ? id - 37 : id - 38);
            }

            if (id <= 106)
            {
                return (GnssType.Galileo, id - 70);
            }

            if (id >= 141 && id <= 180)
            {
                return (GnssType.Beidou, id - 140);
            }

            if (id >= 223 && id <= 245)
            {
                return (GnssType.Beidou, id - 182);
            }

            return (GnssType.Unknown, 0);
        }
        

        private void InitAccumulatingMosaicX5()
        {
            this.ephemerisGlonassAccumulatedMosaic = new Dictionary<byte, List<EphemerisGlonass>>();
            for (int i = 38; i <= 61; i++)
            {
                ephemerisGlonassAccumulatedMosaic.Add((byte)i, new List<EphemerisGlonass>());
            }

            for (int i = 63; i <= 68; i++)
            {
                ephemerisGlonassAccumulatedMosaic.Add((byte)i, new List<EphemerisGlonass>());
            }
        }

        private void MosaicReceiver_OnGetFullGalileo(object sender, SeptentrioSBF.MosaicX5.Galileo data)
        {
            try
            {
                var header = new HeaderEphemerisGalileo()
                                 {
                                     A0 = data.Ionosphere.a_i0,
                                     A1 = data.Ionosphere.a_i1,
                                     A2 = data.Ionosphere.a_i2,
                                     A0Delta = data.Utc.A0,
                                     A1Delta = data.Utc.A1,
                                     Tot = data.Utc.Tot,
                                     WeekNumT = adjgpsweek(data.Utc.WeekNumT),
                                     DeltaTLs = data.Utc.DeltaTLs,
                                     A0DeltaGPS = data.Offset.A_0G,
                                    A1DeltaGPS = data.Offset.A_1G,
                                    TotGPS = data.Offset.TOW * 0.001f,
                                    WeekNumTGPS = adjgpsweek(data.Utc.WeekNumT),
                };

                ephemerisConverter.WriteToFile(settingsContr.Local.MosaicX5.FolderEphemeris, data.Ephemeris, settingsContr.Local.Testing.AccumulateFiles, header);

                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        try
                        {
                            RadarControl.UpdateLastTimeGalileo(DateTime.Now, 0);
                        }
                        catch (Exception ex)
                        {
                        }
                    });
            }
            catch (Exception e)
            {
                Gnss_Message(this, "z3, error: " + e);
            }
        }

        private void MosaicReceiver_OnGetFullBeidou(object sender, SeptentrioSBF.MosaicX5.Beidou data)
        {
            try
            {
                var header = new HeaderEphemerisGps()
                                 {
                                     A0 = data.Ionosphere.A0,
                                     A1 = data.Ionosphere.A1,
                                     A2 = data.Ionosphere.A2,
                                     A3 = data.Ionosphere.A3,
                                     B0 = data.Ionosphere.B0,
                                     B1 = data.Ionosphere.B1,
                                     B2 = data.Ionosphere.B2,
                                     B3 = data.Ionosphere.B3,
                                     A0Delta = data.Utc.A0,
                                     A1Delta = data.Utc.A1,
                                     //Tot = 0,
                                     //WeekNumT = 0,
                    Tot = data.Utc.TOW * 0.001f, //не уверена
                    WeekNumT = data.Utc.WNc, //не уверена
                    DeltaTLs = (sbyte)((int)data.Utc.DeltaTLs + 14)
                };

                ephemerisConverter.WriteToFile(settingsContr.Local.MosaicX5.FolderEphemeris, data.Ephemeris, settingsContr.Local.Testing.AccumulateFiles, header);


                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        try
                        {
                            RadarControl.UpdateLastTimeBeidou(DateTime.Now, 0);
                        }
                        catch (Exception ex)
                        {
                        }
                    });

            }
            catch (Exception e)
            {
                Gnss_Message(this, "z3, error: " + e);
            }
        }

        private void MosaicReceiver_OnGetFullGlonass(object sender, SeptentrioSBF.MosaicX5.Glonass data)
        {
            try
            {
                this.AccumulateRecords(data.Ephemeris);

                var header = new HeaderEphemerisGlonass()
                {
                     A0Delta = data.Utc.tau_C,
                     A1Delta = 0,
                     Tot = 0,
                     WeekNumT = 0,
                     DeltaTLs = 0
                };

                ephemerisConverter.WriteToFile(settingsContr.Local.MosaicX5.FolderEphemeris, this.ephemerisGlonassAccumulatedMosaic, settingsContr.Local.Testing.AccumulateFiles, header);


                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        try
                        {
                            RadarControl.UpdateLastTimeGlonass(DateTime.Now);
                        }
                        catch (Exception ex)
                        {
                        }
                    });
            }
            catch (Exception e)
            {
                Gnss_Message(this, "z3, error: " + e);
            }
        }

        private void MosaicReceiver_OnGetFullGps(object sender, SeptentrioSBF.MosaicX5.Gps data)
        {
            try
            {
                var header = new HeaderEphemerisGps()
                {
                    A0 = data.Ionosphere.A0,
                    A1 = data.Ionosphere.A1,
                    A2 = data.Ionosphere.A2,
                    A3 = data.Ionosphere.A3,
                    B0 = data.Ionosphere.B0,
                    B1 = data.Ionosphere.B1,
                    B2 = data.Ionosphere.B2,
                    B3 = data.Ionosphere.B3,
                    A0Delta = data.Utc.A0,
                    A1Delta = data.Utc.A1,
                    Tot = data.Utc.Tot,
                    WeekNumT = adjgpsweek(data.Utc.WeekNumT), 
                    DeltaTLs = data.Utc.DeltaTLs
                };

                if (header.DeltaTLs == 0)
                {
                    ephemerisConverter.WriteToFile(
                        settingsContr.Local.MosaicX5.FolderEphemeris, data.Ephemeris, settingsContr.Local.Testing.AccumulateFiles);
                }
                else
                {
                    ephemerisConverter.WriteToFile(settingsContr.Local.MosaicX5.FolderEphemeris, data.Ephemeris, settingsContr.Local.Testing.AccumulateFiles, header);

                }

                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        try
                        {
                            RadarControl.UpdateLastTimeGps(DateTime.Now);
                        }
                        catch (Exception ex)
                        {
                        }
                    });
            }
            catch (Exception e)
            {
                Gnss_Message(this, "z3, error: " + e);
            }
        }

        private int adjgpsweek(int week)
        {
            var now = DateTime.UtcNow;
            var w = 0;
            double sec = 0;
            SeptentrioSBF.RtcmV3Helper.GetFromTime(SeptentrioSBF.RtcmV3Helper.Utc2Gps(now), ref w, ref sec);
            if (w < 1560) w = 1560; /* use 2009/12/1 if time is earlier than 2009/12/1 */
            return week + (w - week + 1) / 1024 * 1024;
        }

        private void MosaicReceiver_OnGetEphemeriesBeidou(object sender, List<SeptentrioSBF.EphemerisBeidou> e)
        {
            try
            {
                var data = new List<SeptentrioSBF.EphemerisBeidou>(e);
                this.ephemerisConverter.WriteToFile(settingsContr.Local.MosaicX5.FolderEphemeris, data, settingsContr.Local.Testing.AccumulateFiles);
            }
            catch (Exception ex)
            { }
        }

        private void MosaicReceiver_OnGetEphemeriesGlonass(object sender, List<EphemerisGlonass> e)
        {
            try
            {
                var data = new List<SeptentrioSBF.EphemerisGlonass>(e);
                this.ephemerisConverter.WriteToFile(settingsContr.Local.MosaicX5.FolderEphemeris, data, settingsContr.Local.Testing.AccumulateFiles);
            }
            catch (Exception ex)
            { }
        }

        private void MosaicReceiver_OnGetAlmanacGlonass(object sender, List<SeptentrioSBF.AlmanacGlonass> e)
        {
            try
            {
                var data = new List<SeptentrioSBF.AlmanacGlonass>(e);
                almanacConverter.WriteFile(settingsContr.Local.MosaicX5.FolderAlmanac, data, settingsContr.Local.Testing.AccumulateFiles);
            }
            catch (Exception ex)
            { }
        }

        private void MosaicReceiver_OnGetEphemeriesGps(object sender, List<SeptentrioSBF.EphemerisGps> e)
        {
            try
            {
                var data = new List<SeptentrioSBF.EphemerisGps>(e);
                this.ephemerisConverter.WriteToFile(settingsContr.Local.MosaicX5.FolderEphemeris, data, settingsContr.Local.Testing.AccumulateFiles);
            }
            catch (Exception ex)
            { }
        }

        private void MosaicReceiver_OnDisconnect(object sender, bool e)
        {
            mainWindowViewModel.StateConnectionMosaic = ConnectionStates.Disconnected;
        }

        private void MosaicReceiver_OnConnect(object sender, string e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    mainWindowViewModel.StateConnectionMosaic = ConnectionStates.Connected;
                });
        }

        private void BlockComPortMosaic(bool isEnable)
        {
            if (mosaicReceiver != null)
            {
                mosaicReceiver.BlockComPortRTS(isEnable);
            }
        }


        private void MosaicX5_Message(object sender, string e)
        {
            Thread.Sleep(100);
            try
            {
                AlmanacLogger.Log(new ForQueue(DateTime.Now, e));
            }
            catch (Exception ex)
            { }
        }


        private void DisconnectMosaicX5()
        {
            if (mosaicReceiver == null)
                return;

            mosaicReceiver.Disconnect();

            mosaicReceiver.OnConnect -= MosaicReceiver_OnConnect; ;
            mosaicReceiver.OnDisconnect -= MosaicReceiver_OnDisconnect;
            mosaicReceiver.Message -= MosaicX5_Message;

            mosaicReceiver.OnGetEphemeriesGps -= MosaicReceiver_OnGetEphemeriesGps;
            mosaicReceiver.OnGetAlmanacGlonass -= MosaicReceiver_OnGetAlmanacGlonass;
            this.mosaicReceiver.OnGetEphemeriesGlonass -= MosaicReceiver_OnGetEphemeriesGlonass;
            this.mosaicReceiver.OnGetEphemeriesBeidou -= MosaicReceiver_OnGetEphemeriesBeidou;

            this.mosaicReceiver.OnGetFullGps -= MosaicReceiver_OnGetFullGps;
            this.mosaicReceiver.OnGetFullGlonass -= MosaicReceiver_OnGetFullGlonass;
            this.mosaicReceiver.OnGetFullBeidou -= MosaicReceiver_OnGetFullBeidou;
            this.mosaicReceiver.OnGetFullGalileo -= MosaicReceiver_OnGetFullGalileo;

            this.mosaicReceiver.OnGetSats -= MosaicReceiver_OnGetSats;
            this.mosaicReceiver.OnGetReceiverTime -= MosaicReceiver_OnGetReceiverTime;
            this.mosaicReceiver.OnGetGeodeticPosition -= MosaicReceiver_OnGetLocalPosition;

            mosaicReceiver = null;

            //mosaicNmea.MessageReceived -= device_NmeaMessageReceived;
            //this.mosaicNmea.CloseAsync();
        }





    }
}
