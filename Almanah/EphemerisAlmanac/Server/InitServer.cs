﻿using System.Collections.Generic;
using Server;
using ClientServerLib;
using TSIP;

namespace EphemerisAlmanac
{
    public partial class MainWindow
    {
        private ServerObject server;

        #region ForSendToClients
        private List<ClientServerLib.Almanac> Almanacs { get; set; }         //для хранения и передачи через сервак 
        private List<ClientServerLib.Ephemeries> Ephemerises { get; set; }
        private List<ClientServerLib.SatInfo> Sats { get; set; }
        private ClientServerLib.ReceiverLocation RecLocation { get; set; }

        private string PathAlmanacGlonass { get; set; }
        private string PathEphemerisGlonass { get; set; }
        //private DateTime RecTime { get; set; }
        #endregion


        private void StartServer()
        {
            if (server != null)
            {
                StopServer();
            }

            server = new ServerObject(settingsContr.Local.Server.Port, settingsContr.Local.Server.IpAddress);

            server.OnCreateServer += OnServerConnected;
            server.OnDeleteServer += OnServerDisconnected;
            server.OnAddClient += OnAddClient;
            server.OnDeleteClient += OnDeleteClient;
            server.OnRequest += OnClientRequest;

            InitDataForSend();

            server.Start();

            YamlSave(settingsContr.Local);
        }

        
        private void StopServer()
        {
            if (server == null)
                return;

            server.Disconnect();

            server.OnCreateServer -= OnServerConnected;
            server.OnDeleteServer -= OnServerDisconnected;
            server.OnAddClient -= OnAddClient;
            server.OnDeleteClient -= OnDeleteClient;
            server.OnRequest -= OnClientRequest;

            server = null;
        }


        private void InitDataForSend()
        {
            Almanacs = new List<ClientServerLib.Almanac>();
            Ephemerises = new List<Ephemeries>();
            Sats = new List<ClientServerLib.SatInfo>();
            RecLocation = new ClientServerLib.ReceiverLocation();

            PathAlmanacGlonass = settingsContr.Local.Glonass.FolderAlmanacGlonass + "\\" + settingsContr.Local.Glonass.AlmanacFileNameGlonass;
            PathEphemerisGlonass = settingsContr.Local.Glonass.FolderEphemerisGlonass + "\\" + settingsContr.Local.Glonass.FolderEphemerisGlonass;
            //RecTime = new DateTime();
            //TestInitData();    //автозаполнение для теста передачи по сети
        }


        private void TestInitData()
        {
            Almanacs.Add(new ClientServerLib.Almanac() { PRN = 1 });
            Almanacs.Add(new ClientServerLib.Almanac() { PRN = 2 });
            Ephemerises.Add(new Ephemeries() { sv_number = 5, a_f2 = 54 });
            Sats.Add(new ClientServerLib.SatInfo() { PRN = 20, Azimuth = 2 });
            RecLocation = new ClientServerLib.ReceiverLocation() { ReceiverAltitude = 50, ReceiverLatitude = 40, ReceiverLongitude = 30 };
            PathAlmanacGlonass = settingsContr.Local.Glonass.FolderAlmanacGlonass + "\\" + settingsContr.Local.Glonass.AlmanacFileNameGlonass;
            PathEphemerisGlonass = settingsContr.Local.Glonass.FolderEphemerisGlonass + "\\" + settingsContr.Local.Glonass.FolderEphemerisGlonass;
            //RecTime = DateTime.Now;
        }
    }
}
