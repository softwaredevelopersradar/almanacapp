﻿using System;
using System.Collections.Generic;
using System.Windows;
using Server;
using System.Threading;
using ClientServerLib;
using TSIP;

namespace EphemerisAlmanac
{
    using System.Threading.Tasks;

    public partial class MainWindow : Window
    {

        private void ServerConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (server != null)
                {
                    StopServer();
                }
                else
                {
                    StartServer();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        #region EventHandlers
        private void OnServerConnected(object sender, string e)
        {

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ServerLogger.Clear();
            });
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ServerConnection.ShowConnect();
            });
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ServerLogger.PrintText(e);
            });
        }


        private void OnServerDisconnected(object sender, string e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ServerConnection.ShowDisconnect();
            });
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ServerLogger.PrintText(e);
            });
        }


        private void OnAddClient(object sender, MyEventArgsAddDelete e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ServerLogger.PrintText("Added Client #"+ e.Id);
            });
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tbClientsCount.Text = e.Count.ToString();
            });
        }


        private void OnDeleteClient(object sender, MyEventArgsAddDelete e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ServerLogger.PrintText("Client #" + e.Id + " disconnected");
            });
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tbClientsCount.Text = e.Count.ToString();
            });
        }


        void OnClientRequest(object sender, MyEventArgsClientQuery e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ServerLogger.PrintText("Client #"+e.Id+" asked "+(Operations)e.Querycode);
            });

            switch (e.Querycode)
            {
                case 1:
                    server.SendInfo(e.Id, e.Querycode, Ephemerises);
                    break;
                case 2:
                    server.SendInfo(e.Id, e.Querycode, Almanacs);
                    break;
                case 4:
                    server.SendInfo(e.Id, e.Querycode, Sats);
                    break;
                case 5:
                    server.SendInfo(e.Id, e.Querycode, RecLocation);
                    break;
                case 6:
                    server.SendInfo(e.Id, e.Querycode, DateTime.Now);
                    break;
                case 7:
                    PathAlmanacGlonass = settingsContr.Local.Glonass.FolderAlmanacGlonass + "\\" + settingsContr.Local.Glonass.AlmanacFileNameGlonass;
                    PathEphemerisGlonass = settingsContr.Local.Glonass.FolderEphemerisGlonass + "\\" + settingsContr.Local.Glonass.EphemerisFileNameGlonass;
                    server.SendInfo(e.Id, e.Querycode, new List<string>() { PathAlmanacGlonass, PathEphemerisGlonass });
                    break;

                case 8:
                    if (settingsContr.Local.Testing.WriteSatsLogToTxt) //WriteSatsLogToTxt = Получать блокировку порта 
                    {
                        Task.Run(() => BlockComPortTrimble(false));
                        Task.Run(() => BlockComPortGeos(false));
                    }
                    break;

                case 9:
                    if (settingsContr.Local.Testing.WriteSatsLogToTxt)
                    {
                        Task.Run(() => BlockComPortTrimble(true));
                        Task.Run(() => BlockComPortGeos(true));
                    }
                    break;
                case 10:
                    if (settingsContr.Local.Testing.WriteSatsLogToTxt) //WriteSatsLogToTxt = Получать блокировку порта 
                    {
                        Task.Run(() => this.BlockComPortEscope(false));
                    }
                    break;

                case 11:
                    if (settingsContr.Local.Testing.WriteSatsLogToTxt)
                    {
                        Task.Run(() => BlockComPortEscope(true));
                    }
                    break;
                default:
                    break;
            }

        }
        #endregion


        #region Converters
        private List<ClientServerLib.Almanac> ConvertAlmanacsForClient(List<TSIP.Almanac> dataFromTrimble)
        {
            var outputAlmanacs = new List<ClientServerLib.Almanac>();
            for (int i = 0; i < dataFromTrimble.Count; i++)
            {
                outputAlmanacs.Add(new ClientServerLib.Almanac()
                {
                    PRN = dataFromTrimble[i].PRN,
                    weeknum = dataFromTrimble[i].weeknum,
                    t_zc = dataFromTrimble[i].t_zc,
                    ODOT_n = dataFromTrimble[i].ODOT_n,
                    OMEGA_n = dataFromTrimble[i].OMEGA_n,
                    n = dataFromTrimble[i].n,
                    Axis = dataFromTrimble[i].Axis,
                    a_f1 = dataFromTrimble[i].a_f1,
                    a_f0 = dataFromTrimble[i].a_f0,
                    wn_oa = dataFromTrimble[i].wn_oa,
                    M_0 = dataFromTrimble[i].M_0,
                    OMEGA_0 = dataFromTrimble[i].OMEGA_0,
                    sqrt_A = dataFromTrimble[i].sqrt_A,
                    OMEGADOT = dataFromTrimble[i].OMEGADOT,
                    i_o = dataFromTrimble[i].i_o,
                    t_oa = dataFromTrimble[i].t_oa,
                    e = dataFromTrimble[i].e,
                    SV_HEALTH = dataFromTrimble[i].SV_HEALTH,
                    t_oa_raw = dataFromTrimble[i].t_oa_raw,
                    omega = dataFromTrimble[i].omega
                });
            }
            return outputAlmanacs;
        }

        private List<ClientServerLib.Ephemeries> ConvertEphemeriesForClient(List<TSIP.Ephemeris> dataFromTrimble)
        {
            var outputEphemerises = new List<ClientServerLib.Ephemeries>();
            for (int i = 0; i < dataFromTrimble.Count; i++)
            {
                outputEphemerises.Add(new ClientServerLib.Ephemeries()
                {
                    sv_number = dataFromTrimble[i].sv_number,         // 0
                    t_ephem = dataFromTrimble[i].t_ephem,         // 1 ---
                    weeknum = dataFromTrimble[i].weeknum,          // 2
                    codeL2 = dataFromTrimble[i].codeL2,             // 3
                    L2Pdata = dataFromTrimble[i].L2Pdata,            // 4
                    SVacc_raw = dataFromTrimble[i].SVacc_raw,         // 5 ---
                    SV_health = dataFromTrimble[i].SV_health,         // 6
                    IODC = dataFromTrimble[i].IODC,             // 7
                    T_GD = dataFromTrimble[i].T_GD,             // 8
                    t_oc = dataFromTrimble[i].t_oc,             // 9
                    a_f2 = dataFromTrimble[i].a_f2,             // 10
                    a_f1 = dataFromTrimble[i].a_f1,             // 11
                    a_f0 = dataFromTrimble[i].a_f0,             // 12
                    SVacc = dataFromTrimble[i].SVacc,            // 13
                    IODE = dataFromTrimble[i].IODE,               // 14
                    fit_interval = dataFromTrimble[i].fit_interval,      // 15
                    C_rs = dataFromTrimble[i].C_rs,             // 16
                    delta_n = dataFromTrimble[i].delta_n,          // 17
                    M_0 = dataFromTrimble[i].M_0,              // 18
                    C_uc = dataFromTrimble[i].C_uc,            // 19
                    e = dataFromTrimble[i].e,                // 20
                    C_us = dataFromTrimble[i].C_us,             // 21
                    sqrt_A = dataFromTrimble[i].sqrt_A,           // 22
                    t_oe = dataFromTrimble[i].t_oe,             // 23
                    C_ic = dataFromTrimble[i].C_ic,             // 24
                    OMEGA_0 = dataFromTrimble[i].OMEGA_0,          // 25
                    C_is = dataFromTrimble[i].C_is,             // 26
                    i_o = dataFromTrimble[i].i_o,              // 27
                    C_rc = dataFromTrimble[i].C_rc,            // 28
                    omega = dataFromTrimble[i].omega,          // 29
                    OMEGADOT = dataFromTrimble[i].OMEGADOT,         // 30
                    IDOT = dataFromTrimble[i].IDOT,             // 31
                    Axis = dataFromTrimble[i].Axis,             // 32 --- sqrt_A^2
                    n = dataFromTrimble[i].n,                // 33 ---
                    r1me2 = dataFromTrimble[i].r1me2,            // 34 ---
                    OMEGA_n = dataFromTrimble[i].OMEGA_n,         // 35 ---
                    ODOT_n = dataFromTrimble[i].ODOT_n
                });
            }
            return outputEphemerises;
        }

        private List<ClientServerLib.SatInfo> ConvertSatInfoForClient(List<TSIP.SatInfo> dataFromTrimble)
        {
            var outputSats = new List<ClientServerLib.SatInfo>();
            for (int i = 0; i < dataFromTrimble.Count; i++)
            {
                outputSats.Add(new ClientServerLib.SatInfo(dataFromTrimble[i].PRN, dataFromTrimble[i].SNR, dataFromTrimble[i].Azimuth, dataFromTrimble[i].Elevation, dataFromTrimble[i].AcquisitionFlag));
            }
            return outputSats;
        }
        #endregion 
    }
}
