﻿using System;
using System.Windows;

namespace EphemerisAlmanac
{
    public partial class MainWindow : Window
    {
        private void UpdateEphStatus(object sender, EventArgs data)
        {
            RadarControl.UpdateEphemerisState(Radar.Status.Updated);
        }

        private void UpdateAlmStatus(object sender, EventArgs data)
        {
            RadarControl.UpdateAlmanacState(Radar.Status.Updated);
        }

        private void UpdateFilesStatus(object sender, EventArgs data)
        {
            RadarControl.UpdateGlonassFilesState(Radar.Status.Updated);
        }

        private void ShowPointOnRadar(object sender, float[] angleAmplitude)
        {
            RadarControl.SelectPointFromTable(angleAmplitude[0], angleAmplitude[1]);
        }
    }
}
