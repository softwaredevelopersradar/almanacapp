﻿using System;
using System.Threading;
using System.IO;
using TSIP;

namespace EphemerisAlmanac
{
    public partial class MainWindow
    {
        private Tsip gpsReceiver;


        #region flags
        private bool collectingAlmanac = false;
        private bool collectingEphemeris = false;
        private bool almanacNotReady = true;
        private bool almanacGlonassNotReady = true;
        #endregion



        private void ConnectGps()
        {
            //RadarControl.SelectPointEvent += ShowInTable;
            //SatTableControl.SelectPointEvent += ShowPointOnRadar;

            if (gpsReceiver != null)
            {
                DisconnectGps();
            }

            gpsReceiver = new Tsip();

            gpsReceiver.OnConnect += HandlerConnect_Gps;
            gpsReceiver.OnGetSatInfo += HandlerGetSatellitesInfo_Gps;
            gpsReceiver.OnGetReceiverCoordinates += HandlerGetReceiverLocation_Gps;
            gpsReceiver.OnGetTime += HandlerGetTime_Gps;
            gpsReceiver.OnGetAlmanac += HandlerGetAlmanac_Gps;
            gpsReceiver.OnGetEphemeries += HandlerGetEphemeris_Gps;
            gpsReceiver.OnDisconnect += HandlerDisconnect_Gps;
            gpsReceiver.OnWriteByte += HandlerWriteByte_Gps;
            gpsReceiver.OnReadByte += HandlerReadByte_Gps;
            gpsReceiver.Message += Gnss_Message;
            //gpsReceiver.OnGetFullSatInfo += GpsReceiver_OnGetFullSatInfo;
            gpsReceiver.OnPortStateRTS += this.GpsReceiverOnPortStateRts;
            this.gpsReceiver.OnPortStateDTR += GpsReceiver_OnPortStateDTR;
            gpsReceiver.Connect(settingsContr.Local.Gps.ComPort, settingsContr.Local.Gps.ComPortSpeed);
            

            //if (ephemerisConverter != null)
            //{ 
            //    ephemerisConverter.Message += Gnss_Message;
            //    ephemerisConverter.MessageSatsForLog += Gnss_SatMessage;
            //}
            YamlSave(settingsContr.Local);
        }



        private void BlockComPortTrimble(bool isEnable)
        {
            if (gpsReceiver != null) 
            {
                gpsReceiver.BlockComPortRTS(isEnable);
            }
        }

        private void BlockComPortEscope(bool isEnable)
        {
            if (gpsReceiver != null)
            {
                gpsReceiver.BlockComPortDtr(isEnable);
            }
        }


        private void DisconnectGps()
        {
            if (gpsReceiver == null)
                return;

            gpsReceiver.Disconnect();

            //RadarControl.SelectPointEvent -= ShowInTable;
            //SatTableControl.SelectPointEvent -= ShowPointOnRadar;

            gpsReceiver.OnDisconnect -= HandlerDisconnect_Gps;
            gpsReceiver.OnWriteByte -= HandlerWriteByte_Gps;
            gpsReceiver.OnReadByte -= HandlerReadByte_Gps;
            gpsReceiver.OnConnect -= HandlerConnect_Gps;
            gpsReceiver.OnGetSatInfo -= HandlerGetSatellitesInfo_Gps;
            gpsReceiver.OnGetReceiverCoordinates -= HandlerGetReceiverLocation_Gps;
            gpsReceiver.OnGetTime -= HandlerGetTime_Gps;
            gpsReceiver.OnGetAlmanac -= HandlerGetAlmanac_Gps;
            gpsReceiver.OnGetEphemeries -= HandlerGetEphemeris_Gps;
            //gpsReceiver.OnGetFullSatInfo -= GpsReceiver_OnGetFullSatInfo;
            gpsReceiver.Message -= Gnss_Message;
            gpsReceiver.OnPortStateRTS -= this.GpsReceiverOnPortStateRts; 
            this.gpsReceiver.OnPortStateDTR -= GpsReceiver_OnPortStateDTR;

            gpsReceiver = null;
        }



        //private void Gnss_SatMessage(object sender, string e)
        //{
        //    try
        //    {
        //        var directry = settingsContr.Local.Gps.FolderEphemeris;
        //        var fileName = "SatTimeInfo" + DateTime.Now.ToString("_yyyyMMdd_HHmm") + ".txt";

        //        if (!Directory.Exists(directry))    //если директории не существует, сохранить в папку с запускаемым файлом
        //        {
        //            directry = ".";
        //        }

        //        directry += "\\Log";

        //        if (!Directory.Exists(directry))
        //        {
        //            Directory.CreateDirectory(directry);
        //        }

        //        string fullPath = directry + "\\" + fileName;

        //        if (!settingsContr.Local.Testing.WriteSatsLogToTxt)
        //        {
        //            return;
        //        }

        //        System.IO.File.WriteAllText(fullPath, string.Empty);

        //        using (StreamWriter outfile = new StreamWriter(fullPath))
        //        {
        //            outfile.Write(e);
        //        }
        //    }
        //    catch (Exception ex)
        //    { }
        //}

        //private void Gnss_Message(object sender, string e)
        //{
        //    AlmanacLogger.LoggerRequests.Enqueue(new ForQueue(DateTime.Now, e));
        //}

        private void Gnss_Message(object sender, string e)
        {
            Thread.Sleep(100);
            try
            {
                AlmanacLogger.Log(new ForQueue(DateTime.Now, e));
            }
            catch (Exception ex)
            { }
        }
    }
}
