﻿using Radar;
using SatelliteTableControl;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using ToRinexConverter;
using TSIP;
using WPFControlConnection;

namespace EphemerisAlmanac
{
    public partial class MainWindow
    {
        Timer timerAlm;
        Timer timerEph;
        Timer timerSatellites;
        Timer timerTime;

        Timer timerLostAlm;
        Timer timerLostEph;



        //public AlmanacLogger AlmanacLogger;

        #region Buttons
        private void GpsConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (gpsReceiver != null)
                {
                    DisconnectGps();
                }
                else
                {
                    ConnectGps();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void HandlerConnect_Gps(object sender, string e)
        {
            //AlmanacLogger.StartLogger();
            //AlmanacLogger.LoggerRequests.Enqueue(new ForQueue(DateTime.Now, "Port was opened"));
            //GpsConnection.ShowConnect();
            mainWindowViewModel.StateConnectionGps = ConnectionStates.Connected;

            //таймеры на запросы
            TimerCallback timerCallbackTime = new TimerCallback(GpsTimeRequest);
            timerTime = new Timer(timerCallbackTime, null, 0, settingsContr.Local.General.IntervalTimeUpdate * 60000);

            Thread.Sleep(500);

            TimerCallback timerCallbackSatellites = new TimerCallback(GpsSatellitesRequest);
            timerSatellites = new Timer(timerCallbackSatellites, null, 0, settingsContr.Local.Gps.IntervalCoordUpdate * 1000);

            TimerCallback timerCallbackAlm = new TimerCallback(GpsAlmanacRequest);
            timerAlm = new Timer(timerCallbackAlm, null, 0, Timeout.Infinite);

            TimerCallback timerCallbackEph = new TimerCallback(GpsEphemerisRequest);
            timerEph = new Timer(timerCallbackEph, null, 0, Timeout.Infinite);


            //таймеры на ожидание сбора
            TimerCallback timerCallbackLostAlm = new TimerCallback(GpsAlmanacLost);
            timerLostAlm = new Timer(timerCallbackLostAlm, null, Timeout.Infinite, Timeout.Infinite);

            TimerCallback timerCallbackLostEph = new TimerCallback(GpsEphemerisLost);
            timerLostEph = new Timer(timerCallbackLostEph, null, Timeout.Infinite, Timeout.Infinite);




            //ephemerisConverter.Message += Gnss_Message;
            //ephemerisConverter.MessageSatsForLog += Gnss_SatMessage;


        }

        private void ChangedTimersTrimble()
        {
            timerSatellites?.Change(0, settingsContr.Local.Gps.IntervalCoordUpdate * 1000);
            timerTime?.Change(0, settingsContr.Local.General.IntervalTimeUpdate * 60000);
        }


        byte[] listOfHours = new byte[13] { 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24 };

        //private void CheckSpoofingAvailability()
        //{
        //    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
        //    {
        //        var time = RadarControl.ReceiverTime;
        //        foreach (byte hour in listOfHours)
        //        {
        //            if (time.Year != 1 && ((MathMod(hour - time.Hour, 24) == 1 && time.Minute >= 50) || (time.Hour == hour && time.Minute <= 10)))
        //            {
        //                //MessageBox.Show(, SMeaning.meaningWarning);
        //                MessageBox.Show(SMeaning.meaningAskEpoch, SMeaning.meaningWarning, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
        //                return;
        //            }
        //        }
        //    });

        //}

        private (bool, int) CheckSpoofingAvailability(DateTime time)
        {
            foreach (byte hour in listOfHours)
            {
                if (time.Year != 1 && ((MathMod(hour - time.Hour, 24) == 1 && time.Minute >= 60 - settingsContr.Local.Gps.BadEphemerisTime) || (time.Hour == hour && time.Minute <  settingsContr.Local.Gps.BadEphemerisTime)))
                {
                    int waitTime;
                    if (time.Minute >= 60 - settingsContr.Local.Gps.BadEphemerisTime)
                    { waitTime = 60 - time.Minute + settingsContr.Local.Gps.BadEphemerisTime; }
                    else waitTime = settingsContr.Local.Gps.BadEphemerisTime + 1 - time.Minute;

                    return (false, waitTime);
                }
            }
            return (true, 0);
        }


        static int MathMod(int a, int b)
        {
            return (Math.Abs(a * b) + a) % b;
        }

        private void HandlerDisconnect_Gps(object sender, bool IsNotReadError)
        {
            //AlmanacLogger.LoggerRequests?.Enqueue(new ForQueue(DateTime.Now, "Port was closed"));
            //AlmanacLogger.StopLogger();
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                mainWindowViewModel.StateConnectionGps = ConnectionStates.Disconnected;
            });

            if (IsNotReadError)
            {

                RadarControl.UpdateAlmanacState(Radar.Status.Empty);
                RadarControl.UpdateEphemerisState(Radar.Status.Empty);

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    SatTableControl.Satellites = new System.Collections.ObjectModel.ObservableCollection<SatelliteTableControl.Satellite>(new List<Satellite>());
                    //SatTableControl.UpdateSatInfo(new List<Satellite>());
                });

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RadarControl.ClearAll();
                });
            }
        }
        #endregion




        #region Auto requests TSIP

        /// <summary>
        /// Запрос альманаха по таймеру
        /// </summary>
        private void GpsAlmanacRequest(object obj)
        {
            if (gpsReceiver == null)
            {
                return;
            }

            var fileCheckingResult = CheckIfAlmanacLifeTimeIsOver();

            while ((collectingEphemeris || almanacNotReady) && fileCheckingResult.Item1)
            {
                Thread.Sleep(20);
            }

            Thread.Sleep(50); //если сразу после almanacNotReady=false, то приходит фигня. поэтому подожду

            if (fileCheckingResult.Item1)
            {
                UpdateGpsAlmanacByButton();
            }
            else
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        RadarControl.UpdateLastTimeAlm(fileCheckingResult.Item2, settingsContr.Local.Gps.IntervalAlmanac);
                    }
                    catch (Exception e)
                    {
                    }
                });
                //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                //{
                //    RadarControl.ContinueTimerAlm(settingsContr.Local.Gps.IntervalAlmanac);
                //});
                timerAlm?.Change(settingsContr.Local.Gps.IntervalAlmanac * 60_000, Timeout.Infinite);
            }
        }

        private (bool, DateTime) CheckIfAlmanacLifeTimeIsOver()
        {
            var creationTime = File.GetLastWriteTime(settingsContr.Local.Gps.FolderAlmanac + "\\" + settingsContr.Local.Gps.AlmanacFileName);
            if (creationTime.Year == 1601)
                return (true, DateTime.MaxValue);

            var dif = DateTime.Now - creationTime;
            if (dif.TotalMinutes > settingsContr.Local.Gps.LifetimeAlmanac)
                return (true, creationTime);
            return (false, creationTime);
        }



        private void GpsAlmanacLost(object obj)
        {
            try
            {
                collectingAlmanac = false;
                RadarControl.UpdateAlmanacState(Radar.Status.Updated);
                gpsReceiver?.ClearAlmanacs();
                timerAlm?.Change(0, Timeout.Infinite);
            }
            catch (Exception e)
            { Gnss_Message(this, "z20, error: " + e); }
        }


        /// <summary>
        /// Запрос эфемериды по таймеру
        /// </summary>
        private void GpsEphemerisRequest(object obj)
        {
            if (gpsReceiver == null)
            {
                return;
            }

            while (collectingAlmanac)
            {
                Thread.Sleep(20);
            }

            RadarControl.UpdateEphemerisState(Radar.Status.Question);


            switch (settingsContr.Local.Testing.AccumulateFiles)
            {
                case true:
                    UpdateGpsEphemerisByButton();
                    break;

                case false:
                    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        var time = RadarControl.ReceiverTime;
                        var isSpooofAvailable = CheckSpoofingAvailability(time);
                        if (!isSpooofAvailable.Item1 && !settingsContr.Local.Gps.AutoControlEpochChange)
                        {
                            MessageBox.Show(SMeaning.meaningAskEpoch, SMeaning.meaningWarning, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
                        }
                        Thread.Sleep(100);

                        if (isSpooofAvailable.Item1 && settingsContr.Local.Gps.AutoControlEpochChange ? isSpooofAvailable.Item1 : isSpooofAvailable.Item1 ? AskUser(SMeaning.meaningAskEph) : false)
                        {
                            RadarControl.TickSystemTime(this, null);
                            time = RadarControl.ReceiverTime;
                            isSpooofAvailable = CheckSpoofingAvailability(time);
                            if (!isSpooofAvailable.Item1)
                            {
                                MessageBox.Show(SMeaning.meaningAskEpoch, SMeaning.meaningWarning, MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
                            }
                            else { UpdateGpsEphemerisByButton();}
                            
                        }
                        else
                        {
                            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                            {
                                RadarControl.ContinueTimerEph(settingsContr.Local.Gps.AutoControlEpochChange ? isSpooofAvailable.Item2 : settingsContr.Local.Gps.IntervalEphemeris);
                            });
                            timerEph.Change((settingsContr.Local.Gps.AutoControlEpochChange ? isSpooofAvailable.Item2 : settingsContr.Local.Gps.IntervalEphemeris) * 60_000, Timeout.Infinite);
                        }
                    });
                    
                    
                    //CheckSpoofingAvailability();
                    ////ShowMessageIfSpoofingUnavailable();
                    //Thread.Sleep(100);
                    //if (AskUser(SMeaning.meaningAskEph))
                    //{
                    //    UpdateGpsEphemerisByButton();
                    //}
                    //else
                    //{
                    //    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    //    {
                    //        RadarControl.ContinueTimerEph(settingsContr.Local.Gps.IntervalEphemeris);
                    //    });
                    //    timerEph.Change(settingsContr.Local.Gps.IntervalEphemeris * 60_000, Timeout.Infinite);
                    //}
                    break;
                default:
                    break;
            }

        }


        private void GpsEphemerisLost(object obj)
        {
            try
            {
                collectingEphemeris = false;
                RadarControl.UpdateEphemerisState(Radar.Status.Updated);
                gpsReceiver?.ClearEphemeris();
                timerEph.Change(0, Timeout.Infinite);
            }
            catch (Exception e)
            { Gnss_Message(this, "z19, error: " + e); }
        }


        /// <summary>
        /// Запрос координат с периодом
        /// </summary>
        private void GpsSatellitesRequest(object obj)
        {
            if (gpsReceiver == null)
            {
                return;
            }

            while (collectingAlmanac || collectingEphemeris)
            {
                Thread.Sleep(20);
            }

            gpsReceiver?.GetSatInfo();
        }


        /// <summary>
        /// Запрос времени с периодом
        /// </summary>
        private void GpsTimeRequest(object obj)
        {
            if (gpsReceiver == null)
            {
                return;
            }

            while (collectingAlmanac || collectingEphemeris)
            {
                Thread.Sleep(20);
            }

            gpsReceiver?.GetTimeInfo();
        }


        private bool AskUser(string message)
        {
            bool result = false;

            MessageBoxResult resultAsk = MessageBox.Show(message, SMeaning.meaningAskWindow, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
            switch (resultAsk)
            {
                case MessageBoxResult.Yes:
                    result = true;
                    break;
                case MessageBoxResult.No:
                    result = false;
                    break;
            }

            return result;
        }

        #endregion




        #region Get data from TSIP

        private void HandlerReadByte_Gps(object sender, ListByteEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                _trimbleLogger?.PrintText(e.Data, false);
            });
        }


        private void HandlerWriteByte_Gps(object sender, ListByteEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                _trimbleLogger?.PrintText(e.Data, true);
            });
        }


        /// <summary>
        /// Получение данных спутников об их положении, уровне сигнала и т.д.
        /// </summary>
        private void HandlerGetSatellitesInfo_Gps(object sender, SatInfoEventArgs satellites)
        {
            try
            {
                if (satellites.Sattelites.Count == 0)
                {
                    return;
                }

                var satsFiltered = satellites.Sattelites.GroupBy(x => x.PRN).Select(x => x.First()).ToList();
               
                var targets = satsFiltered.Select(u => new SkyplotPoint() { Azimuth = u.Azimuth, Elevation = u.Elevation, Type = u.PRN <= 32 ? GnssType.Gps : GnssType.Glonass, PRN = u.PRN, State = (SatelliteState)u.AcquisitionFlag  }).ToList();

                var sats = satsFiltered.GetRange(0, satsFiltered.Count).OrderBy(t => t.PRN).ToList();
                Sats = ConvertSatInfoForClient(sats);
                var countTrackedSats = satellites.FixedSats.Count;


                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        RadarControl.UpdatePoints(targets);
                    }
                    catch (Exception ex)
                    {
                        Gnss_Message(this, "z10, error: " + ex);
                    }
                });
                //if (satellites.Sattelites.Count > 0)
                //{
                //    var targets = new float[satellites.Sattelites.Count * 5];
                //    List<SatInfo> satsFiltered = new List<SatInfo>(satellites.Sattelites.GroupBy(x => x.PRN).Select(x => x.First()).ToList());
                //    List<SatInfo> sats = new List<SatInfo>();
                //    sats = satsFiltered.GetRange(0, satsFiltered.Count);
                //    Sats = ConvertSatInfoForClient(sats);

                //    int countTrackedSats = satellites.FixedSats.Count;
                //    for (int i = 0; i < satellites.Sattelites.Count; i++)
                //    {
                //        targets[i * 5] = satellites.Sattelites[i].Azimuth;
                //        targets[i * 5 + 1] = satellites.Sattelites[i].Elevation;
                //        targets[i * 5 + 2] = satellites.Sattelites[i].PRN;
                //        targets[i * 5 + 3] = satellites.Sattelites[i].AcquisitionFlag;
                //        targets[i * 5 + 4] = satellites.Sattelites[i].SNR;
                //    }
                //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                //    {
                //        try
                //        {
                //            RadarControl.UpdatePoints(targets);
                //        }
                //        catch (Exception e)
                //        {
                //            Gnss_Message(this, "z10, error: " + e);
                //        }
                //    });
                    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        try
                        {
                            RadarControl.UpdateSatCount(countTrackedSats);  //посчитаем кол-во спутников
                        }
                        catch (Exception e)
                        {
                            Gnss_Message(this, "z9, error: " + e);
                        }
                    });
                    _ = Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, (ThreadStart)delegate ()
                    {
                        try
                        {
                            //SatTableControl.UpdateSatInfo(sats.Select(u => new Satellite(u.PRN, u.SNR, u.Azimuth, u.Elevation, TypeGnss.Gps)).ToList());
                            SatTableControl.Satellites = new System.Collections.ObjectModel.ObservableCollection<Satellite>(sats.Select(u => new Satellite(u.PRN, u.SNR, u.Azimuth, u.Elevation, u.PRN <= 32 ? TypeGnss.Gps : TypeGnss.Glonass, u.TrueAcquisitionFlag, u.EphemerisFlag, u.OldMeasureFlag, u.BadDataFlag)));
                        }
                        catch (Exception e)
                        {
                            Gnss_Message(this, "z8, error: " + e);
                        }
                    });
                //}
            }
            catch (Exception e)
            { Gnss_Message(this, "z17, error: " + e); }
        }


        //private void GpsReceiver_OnGetFullSatInfo(object sender, System.Collections.Generic.List<FullSatelliteStatus> satellites)
        //{
        //    try
        //    {
        //        if (satellites.Count == 0)
        //        {
        //            return;
        //        }

        //        var satsFiltered = satellites.GroupBy(x => x.PRN).Select(x => x.First()).ToList();

        //        var sats = satsFiltered.GetRange(0, satsFiltered.Count).OrderBy(t => t.PRN).ToList();

        //        var newSats = new System.Collections.ObjectModel.ObservableCollection<FullSatellite>(sats.Select(u => new FullSatellite(u.PRN, u.SNR, u.Azimuth, u.Elevation, u.PRN <= 32 ? TypeGnss.Gps : TypeGnss.Glonass, u.TimeOfLastMeasure, u.AcquisitionFlag, u.EphemerisFlag, u.OldMeasureFlag, u.BadDataFlag, u.IntegerMsecFlag, u.DataCollictionFlag)));
        //        UpdateTablePattern(newSats);
                
        //    }
        //    catch (Exception e)
        //    { Gnss_Message(this, "z17, error: " + e); }
        //}



        /// <summary>
        /// Получение координат приемника
        /// </summary>
        private void HandlerGetReceiverLocation_Gps(object sender, ReceiverCoordEventArgs data)
        {
            try
            {
                double x = data.ReceiverCoord.ReceiverLatitude;
                double y = data.ReceiverCoord.ReceiverLongitude;
                double z = data.ReceiverCoord.ReceiverAltitude;
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {

                    RadarControl.UpdateGNSSCoordinates(x, y, z);
                    switch (data.AlmanacStatus)
                    {
                        case 1:
                            almanacNotReady = true;
                            RadarControl.UpdateAlmanacState(Radar.Status.NotComplete);
                            break;
                        default:
                            almanacNotReady = false;
                            break;
                    }
                    //RadarControl.UpdateNoGpsFlag(data.NoGpsTime);
                });
                RecLocation = new ClientServerLib.ReceiverLocation(x, y, z);
            }
            catch (Exception e)
            { Gnss_Message(this, "z7, error: " + e); }
        }


        /// <summary>
        /// Получение альманахов 
        /// </summary>
        private void HandlerGetAlmanac_Gps(object sender, AlmanacEventArgs data)
        {
            try
            {
                List<Almanac> almanacTSIP = new List<Almanac>(data.almanacs.GroupBy(x => x.PRN).Select(x => x.First()).ToList());

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        almanacConverter.WriteFile(settingsContr.Local.Gps.FolderAlmanac, almanacTSIP, settingsContr.Local.Testing.AccumulateFiles);
                    }
                    catch (Exception e)
                    {
                        Gnss_Message(this, "z6, error: " + e);
                    }
                });


                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        RadarControl.UpdateLastTimeAlm(DateTime.Now, settingsContr.Local.Gps.IntervalAlmanac);
                    }
                    catch (Exception e)
                    {
                        Gnss_Message(this, "z5, error: " + e);
                    }
                });

                timerAlm.Change(settingsContr.Local.Gps.IntervalAlmanac * 60000, Timeout.Infinite);
                collectingAlmanac = false;
                timerLostAlm?.Change(Timeout.Infinite, Timeout.Infinite);

                try
                {
                    Almanacs = ConvertAlmanacsForClient(almanacTSIP);
                }
                catch (Exception e)
                {
                    Gnss_Message(this, "z4, error: " + e);
                }
            }
            catch (Exception e)
            { Gnss_Message(this, "z16, error: " + e); }
        }


        /// <summary>
        /// Получение эфемерид
        /// </summary>
        private void HandlerGetEphemeris_Gps(object sender, EphemerisEventArgs data)
        {
            try
            {
                
                //EphemerisConverter._fileNameGps = settingsContr.Local.Gps.EphemerisFileName;
                List<Ephemeris> ephemerisTSIP = new List<Ephemeris>(data.Ephemeries.GroupBy(x => x.sv_number).Select(x => x.First()).ToList());
                List<Ephemeris> filteredEphemerisTSIP = new List<Ephemeris>();

                //Gnss_Message(this, "Получила от TSIP: " + data.Ephemeries.Count);

                foreach (Ephemeris ephemeris in ephemerisTSIP)
                {
                    if (!settingsContr.Local.Gps.SatelliteFilter || ephemeris.hasSNR)
                    {
                        filteredEphemerisTSIP.Add(ephemeris);
                    }
                }
                //Gnss_Message(this, "Передаю для файла: " + filteredEphemerisTSIP.Count);
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        var header = new HeaderEphemerisGps()
                                         {
                                             A0 = data.Ionosphere.A0,
                                             A1 = data.Ionosphere.A1,
                                             A2 = data.Ionosphere.A2,
                                             A3 = data.Ionosphere.A3,
                                             B0 = data.Ionosphere.B0,
                                             B1 = data.Ionosphere.B1,
                                             B2 = data.Ionosphere.B2,
                                             B3 = data.Ionosphere.B3,
                                             A0Delta = data.Utc.A0,
                                             A1Delta = data.Utc.A1,
                                             Tot = data.Utc.Tot,
                                             WeekNumT = data.Utc.WeekNumT + 1024 * 2, //дается порядковый номер недели от 6 января 1980 г по модулю %1024.
                                                                                      //P.S. Хз где брать множитель для 1024 (пока 2)
                                             DeltaTLs = data.Utc.DeltaTLs
                                         };
                        if (header.DeltaTLs == 0)
                        {
                            ephemerisConverter.WriteFile(
                                settingsContr.Local.Gps.FolderEphemeris,
                                filteredEphemerisTSIP,
                                settingsContr.Local.Testing.AccumulateFiles);
                        }
                        else
                        {
                            ephemerisConverter.WriteFile(settingsContr.Local.Gps.FolderEphemeris, filteredEphemerisTSIP, settingsContr.Local.Testing.AccumulateFiles, header);
                   
                        }
                    }
                    catch (Exception e)
                    {
                        Gnss_Message(this, "z3, error: " + e);
                    }
                });

                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        RadarControl.UpdateLastTimeEph(DateTime.Now, settingsContr.Local.Gps.IntervalEphemeris);
                    }
                    catch (Exception e)
                    {
                        Gnss_Message(this, "z2, error: " + e);
                    }
                });

                timerEph.Change(settingsContr.Local.Gps.IntervalEphemeris * 60000, Timeout.Infinite);
                collectingEphemeris = false;
                timerLostEph.Change(Timeout.Infinite, Timeout.Infinite);

                try
                {
                    Ephemerises = ConvertEphemeriesForClient(filteredEphemerisTSIP);
                }
                catch (Exception e)
                {
                    Gnss_Message(this, "z1, error: " + e);
                }
            }
            catch (Exception e)
            {
                Gnss_Message(this, "z15, error: " + e);
            }
        }


        /// <summary>
        /// Установка системного времени, если выбран режим авто
        /// </summary>
        private void HandlerGetTime_Gps(object sender, TimeEventArgs data)
        {
            Gnss_Message(this, data.Currentdate.ToString());

            if (settingsContr.Local.General.AutoUpdateTime)
            {
                var newTime = new TimeClass(data.Currentdate, settingsContr.Local.General.TimeZone);
            }

            //RadarControl.CorrectCurrentTime(data.Currentdate);
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                RadarControl.ReceiverTime = data.Currentdate;
            });

        }


        private void GpsReceiverOnPortStateRts(object sender, PortState e)
        {
            this.mainWindowViewModel.IsRTSEnabled = e.RtsIsEvalable;
            this.mainWindowViewModel.IsDSREnabled = e.DsrIsEvalable;
        }


        private void GpsReceiver_OnPortStateDTR(object sender, PortState e)
        {
            this.mainWindowViewModel.IsDTREnabled = e.RtsIsEvalable;
            this.mainWindowViewModel.IsCTSEnabled = e.DsrIsEvalable;
        }

        #endregion




        private async void UpdateGpsAlmanacByButton()
        {
            collectingAlmanac = true;
            await Task.Run(() => RadarControl.UpdateAlmanacState(Radar.Status.Collecting));

            await Task.Run(() => gpsReceiver?.GetAlmanac(settingsContr.Local.Gps.FolderAlmanac)); //зачем передавать путь?


            if (collectingAlmanac)
            { timerLostAlm?.Change(60_000, Timeout.Infinite); }
        }

        private async void UpdateGpsEphemerisByButton()
        {
            collectingEphemeris = true;
            await Task.Run(() => RadarControl.UpdateEphemerisState(Radar.Status.Collecting));

            await Task.Run(() => gpsReceiver?.GetEphemeries(settingsContr.Local.Gps.FolderEphemeris)); //зачем передавать путь?


            if (collectingEphemeris)
            { timerLostEph?.Change(60_000, Timeout.Infinite); }
        }
    }
}
