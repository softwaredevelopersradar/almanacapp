﻿
namespace EphemerisAlmanac
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    public class IndicatorFillConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            LinearGradientBrush FillPath;
            var gradientStops = new GradientStopCollection();

            if ((bool?)value == true)
            {
                //gray
                gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FF525151"), 0));
                gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FF7e8585"), 1));
            }
            else
            {
                //red
                gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFFF0000"), 0));
                gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFD21515"), 1));
            }

            FillPath = new LinearGradientBrush(gradientStops);
            FillPath.EndPoint = new Point(0, 1);
            FillPath.StartPoint = new Point(0, 0);

            return FillPath;
        }

        //yellow
        //gradientStops.Add(new GradientStop((Color) ColorConverter.ConvertFromString("#FFDFD991"), 0));
        //gradientStops.Add(new GradientStop((Color) ColorConverter.ConvertFromString("#FFFD9804"), 1));

        //orange
        //gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFFF953E"), 0));
        //gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFFF7604"), 1));

        //    //green
        //    gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FF32ce33"), 0));
        //    gradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FF04FF04"), 1));
        //    break;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
