﻿using System;
using DllServerGnssProperties.Models;
using System.Windows.Data;
using Radar;

namespace EphemerisAlmanac
{
    public class FormatCoordConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is ViewCoord))
                return Binding.DoNothing;


            CoordFormat gg = (CoordFormat)value;
            return (CoordFormat)value;

            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
