﻿using System;
using System.Windows;
using System.Windows.Data;
using WPFControlConnection;

namespace EphemerisAlmanac
{

    public class BoolToConnectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is bool))
                return Binding.DoNothing;

            return (bool)value ? ConnectionStates.Connected : ConnectionStates.Disconnected;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
