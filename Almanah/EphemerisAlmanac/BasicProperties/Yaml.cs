﻿using System;
using System.IO;
using YamlDotNet.Serialization;
using DllServerGnssProperties.Models;
using YamlDotNet.Serialization.TypeResolvers;

namespace EphemerisAlmanac
{
    public partial class MainWindow
    {
        public LocalProperties YamlLoad(string PathFile)
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(PathFile, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var localProperties = new LocalProperties();
            try
            {
                localProperties = deserializer.Deserialize<LocalProperties>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //if (localProperties == null)
            //{
            //    localProperties = GenerateDefaultLocalProperties();
            //    YamlSave(localProperties);
            //}
            return localProperties;
        }


        private LocalProperties GenerateDefaultLocalProperties()
        {
            var localProperties = new LocalProperties();
            //localProperties.ComPortSpeed = 9600;
            //localProperties.ComPort = "COM1";
            //localProperties.ServerIP = "127.0.0.1";
            //localProperties.ServerPort = 9999;
            //localProperties.IntervalA = 20;
            //localProperties.IntervalE = 20;
            //localProperties.PathA = "E:\\KAKORENKO\\GNSS Data";
            //localProperties.PathE = "E:\\KAKORENKO\\GNSS Data";
            //localProperties.General.TimeZone = TimeZoneInfo.Local.DisplayName;
            //localProperties.Gps.SatelliteFilter = true;

            return localProperties;
        }

        public T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //if (t == null)
            //{
            //    t = new T();
            //    YamlSave(t, NameDotYaml);
            //}
            return t;
        }

        public void YamlSave(LocalProperties localProperties)
        {
            try
            {
                var serializer = new SerializerBuilder().WithTypeResolver(new StaticTypeResolver()).Build();
                
                var yaml = serializer.Serialize(localProperties);
                string key = "DependencyObjectType";
                int pos = yaml.IndexOf(key);
                if (pos != -1)
                    yaml = yaml.Replace(yaml.Substring(pos), "");

                using (StreamWriter sw = new StreamWriter("LocalProperties.yaml", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().ConfigureDefaultValuesHandling(DefaultValuesHandling.Preserve).Build();
                var yaml = serializer.Serialize(t);

                using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
