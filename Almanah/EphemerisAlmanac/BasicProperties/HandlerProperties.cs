﻿using DllServerGnssProperties.Models;
using System;

namespace EphemerisAlmanac
{
    public partial class MainWindow
    {


        LocalProperties defaultLocalProperties = new LocalProperties();

        public void InitDefaultProperties()
        {
            defaultLocalProperties = YamlLoad(AppDomain.CurrentDomain.BaseDirectory + "DefaultLocalProperties.yaml");
        }


        public void InitLocalProperties()
        {
            mainWindowViewModel.LocalPropertiesVM = YamlLoad(AppDomain.CurrentDomain.BaseDirectory + "LocalProperties.yaml");

            SetLocalProperties();
        }



        #region HandlerLocalProperties

        public void HandlerLocalProperties(object sender, LocalProperties arg)
        {
            YamlSave(arg);

            InitLocalProperties();

            SetLanguage(settingsContr.Local.General.Language);

            ChangedTimersTrimble();
        }

        


        public void SetLocalProperties()
        {
            try
            {
                settingsContr.Local = mainWindowViewModel.LocalPropertiesVM;
            }
            catch { }
        }




        private void HandlerLocalDefaultProperties_Click(object sender, EventArgs e)
        {
            try
            {
                InitDefaultProperties();

                HandlerLocalProperties(this, defaultLocalProperties);

                SetLocalProperties();
            }
            catch
            { }
        }


        private void basicProperties_OnLanguageChanged(object sender, DllServerGnssProperties.Models.Languages language)
        {
            SetLanguage(settingsContr.Local.General.Language);
        }
        #endregion

    }
}
