﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System;

namespace TestGeoStar
{
    public partial class MainWindow : INotifyPropertyChanged
    {
        private Status ephState = Status.Empty;
        private DateTime lastUpdateEph;

        public Status EphState
        {
            get => ephState;
            private set
            {
                if (ephState == value)
                    return;
                ephState = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastUpdateEph
        {
            get => lastUpdateEph;
            private set
            {
                if (lastUpdateEph == value)
                    return;
                lastUpdateEph = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
