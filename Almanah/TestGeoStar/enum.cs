﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGeoStar
{
    public enum Status
    {
        Empty,
        NotComplete,
        Collecting,
        Updated
    }
}
