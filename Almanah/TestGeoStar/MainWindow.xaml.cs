﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows;
using GeostarBinaryProtocol;
using ToRinexConverter;
using EphemerisForecastLib;

namespace TestGeoStar
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GeoS _geoS;
        private AlmanacConverter almanacConverter;
        private EphemerisConverter ephemerisConverter;

        private List<GeostarBinaryProtocol.EphemerisGlonass> ephemerisGeo;
        private List<GeostarBinaryProtocol.AlmanacGlonass> almanacGeo;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            Ports.ItemsSource = SerialPort.GetPortNames().ToList();
            Ports.SelectedIndex = 0;
        }

        private void OpenPort_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_geoS != null)
                {
                    _geoS.Disconnect();
                    btnPort.Content = "Open port";
                    EphState = Status.Empty;
                    _geoS = null;
                }
                else
                {
                    _geoS = new GeoS();
                    EphState = Status.Empty;
                    SubscribeToEvents();
                    _geoS.Connect(Ports.SelectedItem.ToString(), 115200, GeoS.VersionR.v3);
                    btnPort.Content = "Close port";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SubscribeToEvents()
        {
            //converterLib.OnOpenPort += (sender, zero) => {
            //    DispatchIfNecessary(() =>
            //    { tbxResult.Text += "\n" + "OpenPort"; });
            //};
            //converterLib.OnClosePort += (sender, zero) => {
            //    DispatchIfNecessary(() =>
            //    { tbxResult.Text += "\n" + "ClosePort"; });
            //};
            _geoS.OnGetAlmanacGlonass += _geoS_OnGetAlmanac;
            _geoS.OnGetEphemeriesGlonass += _geoS_OnGetEphemeries;
            _geoS.OnGetTimeInfo += _geoS_OnGetTimeInfo;
            almanacConverter = new AlmanacConverter("", "geoAlmanac.Agl");
            ephemerisConverter = new EphemerisConverter("", "geoEphemeris.rnx");
            ephemerisConverter.OnGpsEphemerisUpdated += EphemerisConverter_EphemerisWasUpdated;

        }

        private void EphemerisConverter_EphemerisWasUpdated(object sender, EventArgs e)
        {
            EphState = Status.Updated;
            LastUpdateEph = DateTime.Now;
        }

        private void _geoS_OnGetTimeInfo(object sender, TimeGeoS e)
        {
            string mess = string.Empty;
            
                mess += $"UTS :" + e.C_utc + ", " +
                $"  Gps : {e.C_gps}," +
                $"  B1 : {e.B1}," +
                $"  B2 : {e.B2}," +
                $"  N4 : {e.N4}," +
                $"  Nsutki : {e.Nsutki}," +
                $"  Nalm :{e.Nalm}," +  "\n";

            

            System.IO.File.WriteAllText(".\\logTime.txt", mess);
        }

        private void _geoS_OnGetEphemeries(object sender, List<GeostarBinaryProtocol.EphemerisGlonass> e)
        {
            ephemerisGeo = new List<GeostarBinaryProtocol.EphemerisGlonass>(e);
            string mess = string.Empty;
            foreach (GeostarBinaryProtocol.EphemerisGlonass ephemeris in e)
            {
                mess += $"GLN #" + ephemeris.Num + ", " +
                $"  dtrc: {ephemeris.Toc}," +
                $"  t_b: {ephemeris.tb}," +
                $"  DateNum: {ephemeris.GeneralizedDateNum}," +
                $"  pX: {ephemeris.PosX}," +
                $"  pY: {ephemeris.PosY}," +
                $"  pZ: {ephemeris.PosZ}," +
                $"  vX: {ephemeris.VelocityXDot}," +
                $"  vY: {ephemeris.VelocityYDot}," +
                $"  vZ: {ephemeris.VelocityZDot}," +
                $"  aX: {ephemeris.AccelerationX}," +
                $"  aY: {ephemeris.AccelerationY}," +
                $"  aZ: {ephemeris.AccelerationZ}," +
                $"  qm: {ephemeris.SVFreqBias}," +
                $"  tau: {ephemeris.tauN}," +
                $"  tauc: {ephemeris.C_utc}," +
                $"  taugps: {ephemeris.C_gps}," +
                $"  enE: {ephemeris.AgeInfo}," +
                $"  Ft: {ephemeris.Ft}," +
                //$"  N4t: {ephemeris.DayNum}," +
                $"  NA: {ephemeris.DayNum}," + "\n";
                //$"  prNNK: {ephemeris.DayNum}," +
                //$"  pss: {ephemeris.tau}," 
                
            }

            System.IO.File.WriteAllText(fileEphemerisLog, mess);

            //try
            //{
            //    almanacConverter.WriteFile("", almanacGeo, false);
            //}
            //catch (Exception ex)
            //{ }

            try
            {
                //Dictionary<byte, List<GeostarBinaryProtocol.EphemerisGlonass>> newDictionary = e.ToDictionary(u => u..Num);
                //ephemerisConverter.WriteToRinexGlonass("", newDictionary, almanacGeo);
            }
            catch (Exception ex)
            { }

            
        }

        private void _geoS_OnGetAlmanac(object sender, List<GeostarBinaryProtocol.AlmanacGlonass> e)
        {
            almanacGeo = new List<GeostarBinaryProtocol.AlmanacGlonass>(e);
            string mess = string.Empty;
            foreach (GeostarBinaryProtocol.AlmanacGlonass almanac in e)
            {
                mess += $"GLN #" + almanac.Num + ", " +
                $"  e : {almanac.e}," +
                $"  ddT : {almanac.TDot}," +
                $"  Omg : {almanac.w}," +
                $"  dT : {almanac.deltaT}," +
                $"  tl : {almanac.t_lambda}," +
                $"  Lmd :{almanac.lambda}," +
                $"  di :{almanac.dI}," +
                $"  ta :{almanac.tau},"+
                $"  day :{almanac.DayNum}," +
                $"  aLt :{almanac.f_channel}," +
                $"  Cn_mdf :{almanac.Modification}," + "\n";

            }

            System.IO.File.WriteAllText(fileAlmanacLog, mess);


            //try
            //{
            //    almanacConverter.WriteFile("", e, false);
            //}
            //catch (Exception ex)
            //{}
        }


        static string fileAlmanacLog { get; set; } = ".\\logAlmanac.txt";
        static string fileEphemerisLog { get; set; } = ".\\logEphemeris.txt";



        private void GetEphemeris_Click(object sender, RoutedEventArgs e)
        {
            _geoS.GetEphemeriesGlonass();
        }

        private void GetAlmanac_Click(object sender, RoutedEventArgs e)
        {
            _geoS.GetAlmanacGlonass();
        }

        private void GetAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_geoS != null)
                {
                    EphState = Status.Collecting;
                    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        _geoS.GetAlmanacGlonass();
                        _geoS.GetEphemeriesGlonass();
                    });


                }
            }
            catch(Exception ex) { }
           
        }

        private void GetTime_Click(object sender, RoutedEventArgs e)
        {
            _geoS.GetTimeInfo();
        }

        private void StartSimulation_Click(object sender, RoutedEventArgs e)
        {
            var input = new EphemerisGlonassData( 2.187842773438d * Math.Pow(10,6), -1.092325439453d * Math.Pow(10, 7), 2.297793505859d * Math.Pow(10, 7), 3.132025718689d * Math.Pow(10, 3), -1.998300552368d * Math.Pow(10, 2), -3.941860198975d * Math.Pow(10, 2), -1.862645149231d * Math.Pow(10, -6), 0d, -1.862645149231d * Math.Pow(10, -6), 29700);
            var result = EphemerisForecast.ForecastGlonass(input, 1800);

            var realresult = new EphemerisGlonassData(7.621845214844d * Math.Pow(10, 6), -1.167203369141d * Math.Pow(10, 7), 2.139223388672d * Math.Pow(10, 7), 2.857068061829d * Math.Pow(10, 3), -6.158590316772d * Math.Pow(10, 2), -1.356359481812d * Math.Pow(10, 3), -9.313225746155d * Math.Pow(10, -7), 9.313225746155d * Math.Pow(10, -7), -1.862645149231d * Math.Pow(10, -6), 31500);
            var Dimaresult = new EphemerisGlonassData(7.621845941475911d * Math.Pow(10, 6), -1.167203585779921d * Math.Pow(10, 7), 2.139223588883269d * Math.Pow(10, 7), 2.857068241934579d * Math.Pow(10, 3), -6.158610422448503d * Math.Pow(10, 2), -1.356357088329511d * Math.Pow(10, 3), -1.862645149231d * Math.Pow(10, -6), 0d, -1.862645149231d * Math.Pow(10, -6), 31500);
            var delta = new EphemerisGlonassData(realresult.PosX - result.PosX, realresult.PosY - result.PosY, realresult.PosZ - result.PosZ, realresult.VelocityXDot - result.VelocityXDot, realresult.VelocityYDot - result.VelocityYDot, realresult.VelocityZDot - result.VelocityZDot, realresult.AccelerationX - result.AccelerationX, realresult.AccelerationY - result.AccelerationY, realresult.AccelerationZ - result.AccelerationZ, 0);
            var deltaDima = new EphemerisGlonassData(Dimaresult.PosX - result.PosX, Dimaresult.PosY - result.PosY, Dimaresult.PosZ - result.PosZ, Dimaresult.VelocityXDot - result.VelocityXDot, Dimaresult.VelocityYDot - result.VelocityYDot, Dimaresult.VelocityZDot - result.VelocityZDot, Dimaresult.AccelerationX - result.AccelerationX, Dimaresult.AccelerationY - result.AccelerationY, Dimaresult.AccelerationZ - result.AccelerationZ, 0);
            //_geoS.GetTimeInfo();
        }
    }
}
