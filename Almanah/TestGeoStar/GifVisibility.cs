﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;

namespace TestGeoStar
{
    public class GifVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Status status = (Status)value;
            Visibility result = Visibility.Hidden;
            if (status == Status.Collecting)
            {
                result = Visibility.Visible;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
