﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;


namespace SeptentrioSBF
{
    using SeptentrioSBF.Models;

    public class MosaicX5
    {
        private const byte MAX_SIZE = 255; //TODO: мб сделать больше

        private SerialPort _port;
        private Thread thrRead;
        //private CancellationTokenSource cancellationTokenSource;

        #region Codes
        private readonly byte[] Preamble = { 0x24, 0x40 };

        //private const byte NAV_TASK_VECTOR = 0x13;
        //private const byte TIME = 0x14;
        //private const byte POSITION = 0x20;
        //private const byte SATELLITES_GLO = 0x22;
        //private const byte ALMANAС_GLO = 0x89;
        //private const byte EPHEMERIS_GLO = 0x8B;
        //private const byte TIME_GLO = 0x9E;
        //private const byte RESTART = 0xC2;
        //private const byte ALMANAС_GPS = 0x88;
        //private const byte EPHEMERIS_GPS = 0x8A;
        //private const byte ANTENNA_SUPPLY = 0xC7;

        private enum Commands : ushort
        {
            EPHEMERIS_GPS = 5891,
            ALMANAС_GPS = 5892,
            EPHEMERIS_GLONASS = 4004,
            ALMANAС_GLONASS = 4005,
            EPHEMERIS_BEIDOW = 4081,
            ALMANAС_BEIDOW = 4119,
            EPHEMERIS_GALILEO = 4002,
            ALMANAС_GALILEO = 4003,
            UTC_GPS = 5894,
            UTC_GLONASS = 4036,
            UTC_BEIDOU = 4121,
            UTC_GALILEO = 4031,
            OFFSET_GPS_GALILEO = 4032,
            ION_GPS = 5893,
            ION_BEIDOU = 4120,
            ION_GALILEO = 4030,
            SAT_VISIBILITY = 4012,
            POS_LOCAL = 4052,
            PVT_GEODETIC = 4007,
            RECEIVER_TIME = 5914
            //SATELLITES = 
        }

        private enum Resets : uint
        {
            Hot = 0,
            Warm = 1,
            Cold = 3,
            FactorySettings = 7
        }
        
        #endregion

        #region Events
        public event EventHandler<string> OnConnect;
        public event EventHandler<bool> OnDisconnect;
        public event EventHandler<string> OnError;
        public event EventHandler<string> Message;
        //public event EventHandler<List<AlmanacGlonass>> OnGetAlmanacGlonass;
        //public event EventHandler<List<EphemerisGlonass>> OnGetEphemeriesGlonass;
        //public event EventHandler<List<AlmanacGps>> OnGetAlmanacGps;
        //public event EventHandler<List<EphemerisGps>> OnGetEphemeriesGps;
        //public event EventHandler<TimeGeoS> OnGetTimeInfo;
        //public event EventHandler<VisibleSatellites> OnGetSatellitesInfo;
        //public event EventHandler<GeoCoordinates> OnGetLocation;
        public event EventHandler<PortState> OnPortState;
        public event EventHandler<List<EphemerisGps>> OnGetEphemeriesGps;
        public event EventHandler<List<EphemerisGlonass>> OnGetEphemeriesGlonass;
        public event EventHandler<List<EphemerisBeidou>> OnGetEphemeriesBeidou;
        public event EventHandler<List<EphemerisGalileo>> OnGetEphemeriesGalileo;

        public event EventHandler<List<AlmanacGlonass>> OnGetAlmanacGlonass;

        public event EventHandler<List<SatellitePosition>> OnGetSats;
        public event EventHandler<LocalPosition> OnGetLocalPosition;
        public event EventHandler<PVTGeodetic> OnGetGeodeticPosition;
        public event EventHandler<ReceiverTime> OnGetReceiverTime;

        public event EventHandler<Gps> OnGetFullGps;
        public event EventHandler<Glonass> OnGetFullGlonass;
        public event EventHandler<Beidou> OnGetFullBeidou;
        public event EventHandler<Galileo> OnGetFullGalileo;
        #endregion

        private List<AlmanacGlonass> almanacsGlonass;
        private List<EphemerisGlonass> ephemerisGlonass;
        private List<EphemerisGps> ephemeriscsGps;
        private List<EphemerisBeidou> ephemeriscsBeidou;
        private List<EphemerisGalileo> ephemeriscsGalileo;

        private Gps gpsStore = new Gps();
        private Glonass glonassStore = new Glonass();
        private Beidou beidouStore = new Beidou();
        private Galileo galileoStore = new Galileo();

        public struct Gps
        {
            public List<EphemerisGps> Ephemeris;

            public IonosphereGps Ionosphere;

            public UtcGps Utc;
        }

        public struct Glonass
        {
            public List<EphemerisGlonass> Ephemeris;

            public UtcGlonass Utc;
        }

        public struct Beidou
        {
            public List<EphemerisBeidou> Ephemeris;

            public IonosphereGps Ionosphere;

            public UtcBeidou Utc;
        }

        public struct Galileo
        {
            public List<EphemerisGalileo> Ephemeris;

            public IonosphereGalileo Ionosphere;

            public UtcGps Utc;
            public OffsetGalileo Offset;
        }
        //public delegate void OnReceiveHandler(byte[] buffer);

        //public event OnReceiveHandler OnReceive;

        //public void Dispose()
        //{
        //    if (this.cancellationTokenSource != null)
        //        this.cancellationTokenSource.Cancel();

        //    if (this._udpClient != null)
        //        this._udpClient.Close();
        //}

        //private void Listen(CancellationToken token = default)
        //{
        //    Task.Run(async () =>
        //        {
        //            while (true && !token.IsCancellationRequested)
        //            {
        //                var receivedResult = await this._udpClient.ReceiveAsync();
        //                if (receivedResult != null && receivedResult.Buffer.Length != 0)
        //                    this.OnReceive?.Invoke(receivedResult.Buffer);
        //            }

        //            if (this.cancellationTokenSource != null && token.IsCancellationRequested)
        //                this.cancellationTokenSource.Dispose();
        //        });
        //}


        ///<summary>
        /// Connecting the receiver
        /// </summary>
        public void Connect(string portNumber, int portRate)
        {
            //this.cancellationTokenSource = new CancellationTokenSource();
            //CancellationToken token = this.cancellationTokenSource.Token;

            //satephemeries = new List<EphemerisGlonass>();
            //satAlmanacs = new List<AlmanacGlonass>();
            //almanacsGps = new List<AlmanacGps>();
            //ephemerisGps = new List<EphemerisGps>();
            this.almanacsGlonass = new List<AlmanacGlonass>();
            this.ephemeriscsGps = new List<EphemerisGps>();
            this.ephemerisGlonass = new List<EphemerisGlonass>();
            this.ephemeriscsBeidou = new List<EphemerisBeidou>();
            this.ephemeriscsGalileo = new List<EphemerisGalileo>();

            if (_port == null)
                _port = new SerialPort();

            if (_port.IsOpen)
                Disconnect();

            try
            {
                // set parameters of port

                _port.PortName = portNumber;
                _port.BaudRate = portRate;
                _port.Parity = Parity.None;
                _port.DataBits = 8;
                _port.StopBits = StopBits.One;
                _port.RtsEnable = false;
                _port.DtrEnable = true;
                _port.ReceivedBytesThreshold = 1000;
                _port.Open();
                //OnPortState?.Invoke(this, new PortState(!_port.RtsEnable, !_port.DsrHolding));

                // create the thread for reading data from the port
                
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception)
                { }

                OnConnect?.Invoke(this, "Connected");
                Message?.Invoke(this, "Port opened");

                ConfigReceiver(5);
            }
            catch (System.Exception e)
            {
                // событие закрытия порта порта
                OnDisconnect?.Invoke(this, true);
                Message?.Invoke(this, "Open port error");
            }
        }


        public void ConfigReceiver(int periodSec)
        {
            //this.Write("setDataInOut,USB2,,+SBF");
            this.Write("setSBFOutput,Stream1,USB2,SatVisibility+ReceiverTime+PVTGeodetic,sec5");
        }

        public void GetAllEphemeris()
        {
            this.Write("exeSBFOnce,USB2,GPS+GLO+GAL+BDS");
        }

        /// <summary>
        /// Block/unblock RTS pin of SerialPort
        /// </summary>
        public void BlockComPortRTS(bool isEnable)
        {
            if (_port == null)
                return;

            try
            {
                //Задавать инвертированное значение (1 - отключение, 0 - вкл. питание)
                _port.RtsEnable = !isEnable;
                Thread.Sleep(500);
                OnPortState?.Invoke(this, new PortState(!_port.RtsEnable, !_port.DsrHolding));
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public bool Write(string command)
        {
            try
            {
                var result = command + Encoding.Default.GetString(new byte[] { 0x0D, 0x0A });
                _port.Write(result);
            }
            catch
            {
            }
            return true;
        }


        /// <summary>
        /// Disconnecting the receiver
        /// </summary>
        public void Disconnect()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception e)
            { }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception e)
            { }

            try
            {
                // destroy thread of reading
                try
                {
                    if (thrRead != null)
                    {
                        thrRead.Abort();
                        thrRead.Join(500);
                        thrRead = null;
                    }
                }
                catch (Exception e)
                { Message?.Invoke(this, "D0, Error: " + e); }

                _port.Close();
                //OnPortState?.Invoke(this, new PortState(false, false));
                OnDisconnect?.Invoke(this, true);
                Message?.Invoke(this, "Port closed");
            }
            catch (System.Exception e)
            { Message?.Invoke(this, "Closing error: " + e); }
        }

        #region Reading port

        byte[] bBufSave = null;

        /// <summary>
        /// Reading port
        /// </summary>
        private void ReadData()
        {
            // buffer for reading bytes
            byte[] bBufRead;
            bBufRead = new byte[MAX_SIZE];

            // buffer to transfer and save reading bytes
            byte[] new_bufSave = null;
            bBufSave = null; // new byte[MAX_SIZE];

            // length of reading bytes
            int iReadByte = -1;
            int iTempLength = 0;

            // while port open
            while (true)
            {
                try
                {
                    bBufRead = new byte[MAX_SIZE];
                    // clear bufer of reading
                    try
                    {
                        Array.Clear(bBufRead, 0, bBufRead.Length);
                    }
                    catch (Exception e)
                    { Message?.Invoke(this, "P1, Error: " + e); }
                    // read data
                    try
                    {
                        iReadByte = _port.Read(bBufRead, 0, MAX_SIZE);
                    }
                    catch (Exception e)
                    {
                        Message?.Invoke(this, "P2, Error: " + e);
                        //return;
                    }

                    // if bytes was read
                    if (iReadByte > 0)
                    {
                        try
                        {
                            // resize buffer for real size of reading bytes
                            Array.Resize(ref bBufRead, iReadByte);

                            //// Событие чтения байт
                            //OnReadByte?.Invoke(this, new ListByteEventArgs(bBufRead.ToList()));

                            if (bBufSave == null)
                                bBufSave = new byte[iReadByte];
                            else
                                Array.Resize(ref bBufSave, bBufSave.Length + iReadByte);

                            // copy buffer for read to buffer for save
                            Array.Copy(bBufRead, 0, bBufSave, iTempLength, iReadByte);

                            // set current value of saving bytes
                            iTempLength += iReadByte;
                        }
                        catch (System.Exception e)
                        {
                            Message?.Invoke(this, "P3, Error: " + e);
                            Disconnect();
                        }

                        var param_cmd = FindCmd(bBufSave);

                        while (param_cmd.Item1 > -1 && param_cmd.Item2 > 0)
                        {
                            try
                            {

                                try
                                {
                                    //if (CheckChecksum(bDecode))
                                    //{
                                    DecodeCmd(param_cmd.Item3.Value.Id, param_cmd.Item4);
                                    //}
                                    //else
                                    //{ }
                                }
                                catch (Exception e)
                                {
                                    Message?.Invoke(this, "Decoding Error: " + e);
                                }

                                try
                                {
                                    new_bufSave = new byte[bBufSave.Length - param_cmd.Item2];
                                    Array.Copy(bBufSave, 0, new_bufSave, 0, param_cmd.Item1);
                                    Array.Copy(bBufSave, param_cmd.Item1 + param_cmd.Item2, new_bufSave, param_cmd.Item1, bBufSave.Length - param_cmd.Item1 - param_cmd.Item2);
                                }
                                catch (Exception e)
                                { Message?.Invoke(this, "P6, Error: " + e); }
                                try
                                {
                                    iTempLength = new_bufSave.Length; //хз зачем
                                }
                                catch (Exception ex)
                                { Message?.Invoke(this, "P7, Error: " + ex); }

                                try
                                {
                                    param_cmd = FindCmd(new_bufSave);
                                }
                                catch (Exception e)
                                { Message?.Invoke(this, "P8, Error: " + e); }


                                Array.Resize(ref bBufSave, new_bufSave.Length);
                                Array.Copy(new_bufSave, bBufSave, new_bufSave.Length);
                            }
                            catch (System.Exception ex)
                            { Message?.Invoke(this, "P9, Error: " + ex); Disconnect(); }
                        }

                        if (bBufSave.Length > 0 && param_cmd.Item2 > 0)
                        {
                            try
                            {
                                Array.Reverse(bBufSave);
                                Array.Resize(ref bBufSave, bBufSave.Length - param_cmd.Item2);
                                Array.Reverse(bBufSave);
                            }
                            catch (Exception e)
                            { }
                        }
                    }
                    else
                    { // close port
                        Message?.Invoke(this, "P10");
                        Disconnect();
                    }
                }
                catch (System.Exception ee)
                {
                    Message?.Invoke(this, "P11, Error: " + ee);
                    Disconnect();
                }
            }
        }


        public static int FindSubarrayStartIndex(byte[] array, byte[] subArray)
        {
            for (int i = 0; i < array.Length - subArray.Length + 1; i++)
                if (ContainsAtIndex(array, subArray, i)) return i;
            return -1;
        }


        static bool ContainsAtIndex(byte[] array, byte[] subArray, int index)
        {

            for (int j = 0; j < subArray.Length; j++)
            {
                if (array[index + j] != subArray[j])
                {
                    return false;
                }
            }
            return true;
        }


        //private Tuple<int, int> FindCmd(byte[] bArray)
        //{
        //    int len = -1;
        //    int firstBt = -1;

        //    if (bArray.Length < 8)
        //    { return Tuple.Create<int, int>(firstBt, len); }

        //    firstBt = FindSubarrayStartIndex(bArray, Preamble);

        //    if (firstBt == -1)
        //    {
        //        return Tuple.Create<int, int>(firstBt, len);
        //    }

        //    if (firstBt + 8 > bArray.Length)
        //    {
        //        return Tuple.Create<int, int>(firstBt, len);
        //    }

        //    var crc = BitConverter.ToUInt16(bArray, firstBt + 2);
        //    //TODO:добавить проверку crc

        //    var temp = new BitArray(bArray.Skip(firstBt + 4).Take(2).ToArray()); 
        //    var array = new ushort[1];
        //    temp.CopyTo(array, 0);

        //    var blockId = (ushort)(array[0] << 3 >> 3);

        //    var length = BitConverter.ToUInt16(bArray, firstBt + 6);

        //    if (length % 4 != 0 || firstBt + length > bArray.Length)
        //    {
        //        return Tuple.Create<int, int>(firstBt, len);
        //    }

        //    length -= 8;

        //    var header = new BlockHeader(crc, blockId, length);


        //    return Tuple.Create<int, int>(firstBt, len);
        //}
        private Tuple<int, int, BlockHeader?, byte[]> FindCmd(byte[] bArray)
        {
            int len = -1;
            int firstBt = -1;

            if (bArray.Length < 8)
            {
                return Tuple.Create<int, int, BlockHeader?, byte[]>(firstBt, len, null, null);
            }

            firstBt = FindSubarrayStartIndex(bArray, Preamble);

            if (firstBt == -1)
            {
                return Tuple.Create<int, int, BlockHeader?, byte[]>(firstBt, len, null, null);
            }

            if (firstBt + 8 > bArray.Length)
            {
                return Tuple.Create<int, int, BlockHeader?, byte[]>(firstBt, len, null, null);
            }

            var crc = BitConverter.ToUInt16(bArray, firstBt + 2);
            //TODO:добавить проверку crc

            byte[] bytes = bArray.Skip(firstBt + 4).Take(2).ToArray();
            ushort value = (ushort)((bytes[1] << 8) | bytes[0]);
            ushort blockId = (ushort)(value & 0x1FFF);


            var length = BitConverter.ToUInt16(bArray, firstBt + 6);
            
            if (length % 4 != 0 || firstBt + length > bArray.Length)
            {
                return Tuple.Create<int, int, BlockHeader?, byte[]>(firstBt, len, null, null);
            }
            len = length;
            var calculatedHash = SbfCrc16.checksum(bArray, firstBt + 4, length - 4);
            //if (calculatedHash == crc)
            //{
            //    var span = new ReadOnlySpan<byte>(_buffer, 0, _length);
            //    ParsePacket(_msgId, ref span, true);
            //    return true;
            //}
            //else
            //{
            //    PublishWhenCrcError();
            //}

            length -= 8;

            var header = new BlockHeader(crc, blockId, length);

            var block = new byte[length];

            Array.Copy(bArray, firstBt + 8, block, 0, length);

            return Tuple.Create<int, int, BlockHeader?, byte[]>(firstBt, len, header, block);
        }



        //internal static bool CheckChecksum(byte[] data)
        //{
        //    byte[] packet = new byte[data.Length - 4];
        //    byte[] checksum = new byte[4];

        //    Array.Copy(data, 0, packet, 0, packet.Length);
        //    Array.Copy(data, packet.Length, checksum, 0, 4);

        //    return Enumerable.SequenceEqual(CountChecksum(packet), checksum);
        //}

        bool canSendGlonassEphemeris = true;
        bool canSendGpsEphemeris = true;
        bool canSendBeidouEphemeris = true;
        bool canSendGalileoEphemeris = true;

        public void SetCanSendEphemeris(bool gps, bool glonass, bool beidou, bool galileo)
        {
            canSendGpsEphemeris = gps;
            canSendGlonassEphemeris = glonass;
            canSendBeidouEphemeris = beidou;
            canSendGalileoEphemeris = galileo;
        }

        //определение вида кодограммы
        private void DecodeCmd(ushort commandId, byte[] data)
        {
            var oldcanSendGlonassEphemeris = canSendGlonassEphemeris;
            var oldcanSendGpsEphemeris = canSendGpsEphemeris;
            var oldcanSendBeidouEphemeris = canSendBeidouEphemeris;
            var oldcanSendGalileoEphemeris = canSendGalileoEphemeris;

            switch ((Commands)commandId)
            {
                case Commands.EPHEMERIS_GPS:

                    this.SetCanSendEphemeris(false, true, true, true);
                    this.ParseEphemerisGps(data);
                    break;

                case Commands.EPHEMERIS_GLONASS:

                    this.SetCanSendEphemeris(true, false, true, true);
                    this.ParseEphemerisGlonass(data);
                    break;

                case Commands.EPHEMERIS_BEIDOW:

                    this.SetCanSendEphemeris(true, true, false, true);
                    this.ParseEphemerisBeidou(data);
                    break;

                case Commands.EPHEMERIS_GALILEO:

                    this.SetCanSendEphemeris(true, true, true, false);
                    this.ParseEphemerisGalileo(data);
                    break;

                case Commands.ALMANAС_GLONASS:

                    this.SetCanSendEphemeris(true, true, true, true);
                    ParseAlmanacGlonass(data);
                    break;

                case Commands.ION_GPS:

                    this.SetCanSendEphemeris(true, true, true, true);
                    this.ParseIonosphereGps(data);
                    break;

                case Commands.ION_BEIDOU:

                    this.SetCanSendEphemeris(true, true, true, true);
                    this.ParseIonosphereBeidou(data);
                    break;

                case Commands.ION_GALILEO:

                    this.SetCanSendEphemeris(true, true, true, true);
                    this.ParseIonosphereGalileo(data);
                    break;

                case Commands.UTC_GPS:

                    this.SetCanSendEphemeris(true, true, true, true);

                    ParseUtcGps(data);
                    OnGetFullGps?.Invoke(this, this.gpsStore);
                    break;

                case Commands.UTC_GLONASS:

                    this.SetCanSendEphemeris(true, true, true, true);

                    ParseUtcGlonass(data);
                    OnGetFullGlonass?.Invoke(this, this.glonassStore);
                    break;

                case Commands.UTC_BEIDOU:

                    this.SetCanSendEphemeris(true, true, true, true);

                    this.ParseUtcBeidou(data);
                    OnGetFullBeidou?.Invoke(this, this.beidouStore);
                    break;

                case Commands.UTC_GALILEO:

                    this.SetCanSendEphemeris(true, true, true, true);

                    ParseUtcGalileo(data);
                    break;

                case Commands.OFFSET_GPS_GALILEO:

                    this.SetCanSendEphemeris(true, true, true, true);

                    ParseOffsetGpsGalileo(data); 
                    OnGetFullGalileo?.Invoke(this, this.galileoStore);
                    break;

                case Commands.SAT_VISIBILITY:

                    this.SetCanSendEphemeris(true, true, true, true);
                    ParseSatInfo(data);
                    break;

                case Commands.RECEIVER_TIME:

                    this.SetCanSendEphemeris(true, true, true, true);
                    ParseReceiverTime(data);
                    break;

                case Commands.PVT_GEODETIC:

                    this.SetCanSendEphemeris(true, true, true, true);
                    this.ParsePVTGeodetic(data);
                    break;


                default:

                    this.SetCanSendEphemeris(true, true, true, true);
                    break;
            }


            if (canSendGlonassEphemeris && !oldcanSendGlonassEphemeris)
            {
                glonassStore.Ephemeris = new List<EphemerisGlonass>(ephemerisGlonass);
                //OnGetEphemeriesGlonass?.Invoke(this, ephemerisGlonass);
                ephemerisGlonass.Clear();
            }
            if (canSendGpsEphemeris && !oldcanSendGpsEphemeris)
            {
                gpsStore.Ephemeris = new List<EphemerisGps>(ephemeriscsGps);
                //OnGetEphemeriesGps?.Invoke(this, ephemeriscsGps);
                ephemeriscsGps.Clear();
            }
            if (canSendBeidouEphemeris && !oldcanSendBeidouEphemeris)
            {
                beidouStore.Ephemeris = new List<EphemerisBeidou>(ephemeriscsBeidou);
                //OnGetEphemeriesBeidou?.Invoke(this, ephemeriscsBeidou);
                ephemeriscsBeidou.Clear();
            }
            if (canSendGalileoEphemeris && !oldcanSendGalileoEphemeris)
            {
                galileoStore.Ephemeris = new List<EphemerisGalileo>(ephemeriscsGalileo);
                //OnGetEphemeriesGalileo?.Invoke(this, ephemeriscsGalileo);
                ephemeriscsGalileo.Clear();
            }
            //OnReadByte?.Invoke(this, new ListByteEventArgs(bCmd.ToList()));
        }
        #endregion


        #region Parsing Ephemeris

        /// <summary>
        /// Parse GPS ephemeris 
        /// </summary>
        private void ParseEphemerisGps(byte[] packet)
        {
            var ephemeris = new EphemerisGps();

            ephemeris.TOW = BitConverter.ToUInt32(packet, 0);
            ephemeris.WNc = BitConverter.ToUInt16(packet, 4);
            ephemeris.PRN = packet[6];
            ephemeris.weeknum = BitConverter.ToUInt16(packet, 8);

            ephemeris.codeL2 = packet[10];
            ephemeris.URA = packet[11];
            ephemeris.SV_health = packet[12];
            ephemeris.L2Pdata = packet[13];

            ephemeris.IODC = BitConverter.ToUInt16(packet, 14);

            ephemeris.IODE2 = packet[16];
            ephemeris.IODE3 = packet[17];
            ephemeris.fit_interval = packet[18];

            ephemeris.T_GD = BitConverter.ToSingle( packet, 20);
            ephemeris.t_oc = BitConverter.ToUInt32( packet, 24);
            ephemeris.a_f2 = BitConverter.ToSingle( packet, 28);
            ephemeris.a_f1 = BitConverter.ToSingle( packet, 32);
            ephemeris.a_f0 = BitConverter.ToSingle( packet, 36);

            ephemeris.C_rs = BitConverter.ToSingle( packet, 40);
            ephemeris.delta_n = BitConverter.ToSingle( packet, 44);
            ephemeris.M_0 = BitConverter.ToDouble( packet, 48);
            ephemeris.C_uc = BitConverter.ToSingle( packet, 56);
            ephemeris.e = BitConverter.ToDouble( packet, 60);
            ephemeris.C_us = BitConverter.ToSingle( packet, 68);
            ephemeris.sqrt_A = BitConverter.ToDouble( packet, 72);
            ephemeris.t_oe = BitConverter.ToUInt32( packet, 80);
            ephemeris.C_ic = BitConverter.ToSingle( packet, 84);

            ephemeris.OMEGA_0 = BitConverter.ToDouble( packet, 88);
            ephemeris.C_is = BitConverter.ToSingle( packet, 96);
            ephemeris.i_0 = BitConverter.ToDouble( packet, 100);
            ephemeris.C_rc = BitConverter.ToSingle( packet, 108);
            ephemeris.omega = BitConverter.ToDouble( packet, 112);
            ephemeris.OMEGADOT = BitConverter.ToSingle( packet, 120);
            ephemeris.IDOT = BitConverter.ToSingle( packet, 124);

            ephemeris.WNt_oc = BitConverter.ToUInt16( packet, 128);
            ephemeris.WNt_oe = BitConverter.ToUInt16( packet, 130);

            this.ephemeriscsGps.Add(ephemeris);
        }

        /// <summary>
        /// Parse Glonass ephemeris 
        /// </summary>
        private void ParseEphemerisGlonass(byte[] packet)
        {
            var ephemeris = new EphemerisGlonass();

            ephemeris.TOW = BitConverter.ToUInt32(packet, 0);
            ephemeris.WNc = BitConverter.ToUInt16(packet, 4);
            ephemeris.SvId = packet[6];
            ephemeris.FreqNr = packet[7];

            ephemeris.PosX = BitConverter.ToDouble(packet, 8);
            ephemeris.PosY = BitConverter.ToDouble(packet, 16);
            ephemeris.PosZ = BitConverter.ToDouble(packet, 24);

            ephemeris.VelocityXDot = BitConverter.ToSingle(packet, 32);
            ephemeris.VelocityYDot = BitConverter.ToSingle(packet, 36);
            ephemeris.VelocityZDot = BitConverter.ToSingle(packet, 40);
            ephemeris.AccelerationX = BitConverter.ToSingle(packet, 44);
            ephemeris.AccelerationY = BitConverter.ToSingle(packet, 48);
            ephemeris.AccelerationZ = BitConverter.ToSingle(packet, 52);

            ephemeris.gamma = BitConverter.ToSingle(packet, 56);
            ephemeris.tauN = BitConverter.ToSingle(packet, 60);
            ephemeris.dtau = BitConverter.ToSingle(packet, 64);
            ephemeris.t_oe = BitConverter.ToUInt32(packet, 68);
            ephemeris.WN_toe = BitConverter.ToUInt16(packet, 72);

            ephemeris.P1 = packet[74];
            ephemeris.P2 = packet[75];
            ephemeris.E = packet[76];
            ephemeris.B = packet[77];

            ephemeris.tb = BitConverter.ToUInt16(packet, 78);
            ephemeris.tb /= 15;

            ephemeris.M = packet[80];
            ephemeris.P = packet[81];
            ephemeris.l = packet[82];
            ephemeris.P4 = packet[83];

            ephemeris.N_T = BitConverter.ToUInt16(packet, 84);
            ephemeris.Ft = BitConverter.ToUInt16(packet, 86);
            //ephemeris.C = packet[88]; //приходит 88 байт, видимо лишнее поле

            this.ephemerisGlonass.Add(ephemeris);
        }


        /// <summary>
        /// Parse GLONASS almanac
        /// </summary>
        private void ParseAlmanacGlonass(byte[] packet)
        {
            var almanac = new AlmanacGlonass();

            almanac.TOW = BitConverter.ToUInt32(packet, 0);
            almanac.WNc = BitConverter.ToUInt16(packet, 4);
            almanac.SvId = packet[6];
            almanac.FreqNr = packet[7];
            almanac.E = BitConverter.ToSingle(packet, 8);
            almanac.t_ao = BitConverter.ToUInt32(packet, 12);
            almanac.dI = BitConverter.ToSingle(packet, 16);
            almanac.Lamda = BitConverter.ToSingle(packet, 20);
            almanac.t_ln = BitConverter.ToSingle(packet, 24);
            almanac.w = BitConverter.ToSingle(packet, 28);
            almanac.dT = BitConverter.ToSingle(packet, 32);
            almanac.dTT = BitConverter.ToSingle(packet, 36);
            almanac.tau = BitConverter.ToSingle(packet, 40);
            almanac.WN_a = packet[44];
            almanac.SV_HEALTH = packet[45];
            almanac.DayNum = BitConverter.ToUInt16(packet, 46);
            almanac.Modification = packet[48];
            almanac.N_4 = packet[49];

            this.almanacsGlonass.Add(almanac);

            if (this.almanacsGlonass.Count == 24)
            {
                OnGetAlmanacGlonass?.Invoke(this, this.almanacsGlonass);
                this.almanacsGlonass.Clear();
            }
        }


        /// <summary>
        /// Parse Beidou ephemeris 
        /// </summary>
        private void ParseEphemerisBeidou(byte[] packet)
        {
            var ephemeris = new EphemerisBeidou();

            ephemeris.TOW = BitConverter.ToUInt32(packet, 0);
            ephemeris.WNc = BitConverter.ToUInt16(packet, 4);
            ephemeris.PRN = packet[6];
            ephemeris.WN = BitConverter.ToUInt16(packet, 8);

            ephemeris.URA = packet[10];
            ephemeris.SV_health = packet[11];
            ephemeris.IODC = packet[12];
            ephemeris.IODE = packet[13];

            ephemeris.T_GD1 = BitConverter.ToSingle(packet, 16);
            ephemeris.T_GD2 = BitConverter.ToSingle(packet, 20);
            ephemeris.t_oc = BitConverter.ToUInt32(packet, 24);
            ephemeris.a_f2 = BitConverter.ToSingle(packet, 28);
            ephemeris.a_f1 = BitConverter.ToSingle(packet, 32);
            ephemeris.a_f0 = BitConverter.ToSingle(packet, 36);

            ephemeris.C_rs = BitConverter.ToSingle(packet, 40);
            ephemeris.delta_n = BitConverter.ToSingle(packet, 44);
            ephemeris.M_0 = BitConverter.ToDouble(packet, 48);
            ephemeris.C_uc = BitConverter.ToSingle(packet, 56);
            ephemeris.e = BitConverter.ToDouble(packet, 60);
            ephemeris.C_us = BitConverter.ToSingle(packet, 68);
            ephemeris.sqrt_A = BitConverter.ToDouble(packet, 72);
            ephemeris.t_oe = BitConverter.ToUInt32(packet, 80);
            ephemeris.C_ic = BitConverter.ToSingle(packet, 84);

            ephemeris.OMEGA_0 = BitConverter.ToDouble(packet, 88);
            ephemeris.C_is = BitConverter.ToSingle(packet, 96);
            ephemeris.i_0 = BitConverter.ToDouble(packet, 100);
            ephemeris.C_rc = BitConverter.ToSingle(packet, 108);
            ephemeris.omega = BitConverter.ToDouble(packet, 112);
            ephemeris.OMEGADOT = BitConverter.ToSingle(packet, 120);
            ephemeris.IDOT = BitConverter.ToSingle(packet, 124);

            ephemeris.WNt_oc = BitConverter.ToUInt16(packet, 128);
            ephemeris.WNt_oe = BitConverter.ToUInt16(packet, 130);
            
            this.ephemeriscsBeidou.Add(ephemeris);
        }


        /// <summary>
        /// Parse GALILEO ephemeris 
        /// </summary>
        private void ParseEphemerisGalileo(byte[] packet)
        {
            var ephemeris = new EphemerisGalileo();

            ephemeris.TOW = BitConverter.ToUInt32(packet, 0);
            ephemeris.WNc = BitConverter.ToUInt16(packet, 4);
            ephemeris.SvId = packet[6];

            ephemeris.Source = packet[7];
            ephemeris.sqrt_A = BitConverter.ToDouble(packet, 8);
            ephemeris.M_0 = BitConverter.ToDouble(packet, 16);
            ephemeris.e = BitConverter.ToDouble(packet, 24);
            ephemeris.i_0 = BitConverter.ToDouble(packet, 32);
            ephemeris.omega = BitConverter.ToDouble(packet, 40);
            ephemeris.OMEGA_0 = BitConverter.ToDouble(packet, 48);
            ephemeris.OMEGADOT = BitConverter.ToSingle(packet, 56);
            ephemeris.IDOT = BitConverter.ToSingle(packet, 60);
            ephemeris.delta_n = BitConverter.ToSingle(packet, 64);
            ephemeris.C_uc = BitConverter.ToSingle(packet, 68);
            ephemeris.C_us = BitConverter.ToSingle(packet, 72);
            ephemeris.C_rc = BitConverter.ToSingle(packet, 76);
            ephemeris.C_rs = BitConverter.ToSingle(packet, 80);
            ephemeris.C_ic = BitConverter.ToSingle(packet, 84);
            ephemeris.C_is = BitConverter.ToSingle(packet, 88);
            ephemeris.t_oe = BitConverter.ToUInt32(packet, 92);
            ephemeris.t_oc = BitConverter.ToUInt32(packet, 96);
            ephemeris.a_f2 = BitConverter.ToSingle(packet, 100);
            ephemeris.a_f1 = BitConverter.ToSingle(packet, 104);
            ephemeris.a_f0 = BitConverter.ToDouble(packet, 108);
            ephemeris.WNt_oe = BitConverter.ToUInt16(packet, 116);
            ephemeris.WNt_oc = BitConverter.ToUInt16(packet, 118);
            ephemeris.IODnav = BitConverter.ToUInt16(packet, 120);
            ephemeris.Health_OSSOL = BitConverter.ToUInt16(packet, 122);
            ephemeris.Health_PRS = packet[124];
            ephemeris.SISA_L1E5a = packet[125];
            ephemeris.SISA_L1E5b = packet[126];
            ephemeris.SISA_L1AE6A = packet[127];
            ephemeris.BGD_L1E5a = BitConverter.ToSingle(packet, 128);
            ephemeris.BGD_L1E5b = BitConverter.ToSingle(packet, 132);
            ephemeris.BGD_L1AE6A = BitConverter.ToSingle(packet, 136);
            ephemeris.CNAVenc = packet[140];

            this.ephemeriscsGalileo.Add(ephemeris);
        }
        #endregion


        #region Parce UTC

        /// <summary>
        /// Parse GPS UTC
        /// </summary>
        private void ParseUtcGps(byte[] packet)
        {
            var utc = new UtcGps();

            utc.TOW = BitConverter.ToUInt32(packet, 0);
            utc.WNc = BitConverter.ToUInt16(packet, 4);
            utc.PRN = packet[6];
            utc.A1 = BitConverter.ToSingle(packet, 8);
            utc.A0 = BitConverter.ToDouble(packet, 12);
            utc.Tot = BitConverter.ToUInt32(packet, 20);

            utc.WeekNumT = packet[24];
            utc.DeltaTLs = (sbyte)packet[25];
            utc.WN_LSF = packet[26];
            utc.DN = packet[27];
            utc.DeltaTLsf = (sbyte)packet[28];

            gpsStore.Utc = utc;
        }


        /// <summary>
        /// Parse Glonass UTC 
        /// </summary>
        private void ParseUtcGlonass(byte[] packet)
        {
            var utc = new UtcGlonass();

            utc.TOW = BitConverter.ToUInt32(packet, 0);
            utc.WNc = BitConverter.ToUInt16(packet, 4);
            utc.SvId = packet[6];
            utc.FreqNr = packet[7];

            utc.N_4 = packet[8];
            utc.KP = packet[9];
            utc.N = BitConverter.ToUInt16(packet, 10);

            utc.tau_GPS = BitConverter.ToSingle(packet, 12);
            utc.tau_C = BitConverter.ToDouble(packet, 16);
            utc.B1 = BitConverter.ToSingle(packet, 24);
            utc.B2 = BitConverter.ToSingle(packet, 28);

            this.glonassStore.Utc = utc;
        }


        /// <summary>
        /// Parse Beidou UTC
        /// </summary>
        private void ParseUtcBeidou(byte[] packet)
        {
            var utc = new UtcBeidou();

            utc.TOW = BitConverter.ToUInt32(packet, 0);
            utc.WNc = BitConverter.ToUInt16(packet, 4);
            utc.PRN = packet[6];
            utc.A1 = BitConverter.ToSingle(packet, 8);
            utc.A0 = BitConverter.ToDouble(packet, 12);

            utc.DeltaTLs = (sbyte)packet[20];
            utc.WN_LSF = packet[21];
            utc.DN = packet[22];
            utc.DeltaTLsf = (sbyte)packet[23];

            beidouStore.Utc = utc;
        }


        /// <summary>
        /// Parse GALILEO UTC
        /// </summary>
        private void ParseUtcGalileo(byte[] packet)
        {
            var utc = new UtcGps();

            utc.TOW = BitConverter.ToUInt32(packet, 0);
            utc.WNc = BitConverter.ToUInt16(packet, 4);
            utc.PRN = packet[6];
            utc.Reserved = packet[7];
            utc.A1 = BitConverter.ToSingle(packet, 8);
            utc.A0 = BitConverter.ToDouble(packet, 12);
            utc.Tot = BitConverter.ToUInt32(packet, 20);

            utc.WeekNumT = packet[24];
            utc.DeltaTLs = (sbyte)packet[25];
            utc.WN_LSF = packet[26];
            utc.DN = packet[27];
            utc.DeltaTLsf = (sbyte)packet[28];

            galileoStore.Utc = utc;
        }


        private void ParseOffsetGpsGalileo(byte[] packet)
        {
            var offset = new OffsetGalileo();
            offset.TOW = BitConverter.ToUInt32(packet, 0);
            offset.WNc = BitConverter.ToUInt16(packet, 4);
            offset.SvId = packet[6];
            offset.Source = packet[7];
            offset.A_1G = BitConverter.ToSingle(packet, 8);
            offset.A_0G = BitConverter.ToSingle(packet, 12);
            offset.t_oG = BitConverter.ToUInt32(packet, 16);
            offset.WN_oG = packet[18];

            galileoStore.Offset = offset;
        }


        #endregion



        #region Parce Ionosphere

        /// <summary>
        /// Parse GPS ION
        /// </summary>
        private void ParseIonosphereGps(byte[] packet)
        {
            var ionosphere = new IonosphereGps();

            ionosphere.TOW = BitConverter.ToUInt32(packet, 0);
            ionosphere.WNc = BitConverter.ToUInt16(packet, 4);
            ionosphere.PRN = packet[6];
            ionosphere.A0 = BitConverter.ToSingle(packet, 8);
            ionosphere.A1 = BitConverter.ToSingle(packet, 12);
            ionosphere.A2 = BitConverter.ToSingle(packet, 16);
            ionosphere.A3 = BitConverter.ToSingle(packet, 20);

            ionosphere.B0 = BitConverter.ToSingle(packet, 24);
            ionosphere.B1 = BitConverter.ToSingle(packet, 28);
            ionosphere.B2 = BitConverter.ToSingle(packet, 32);
            ionosphere.B3 = BitConverter.ToSingle(packet, 36);

            gpsStore.Ionosphere = ionosphere;
        }


        /// <summary>
        /// Parse BEIDOU ION
        /// </summary>
        private void ParseIonosphereBeidou(byte[] packet)
        {
            var ionosphere = new IonosphereGps();

            ionosphere.TOW = BitConverter.ToUInt32(packet, 0);
            ionosphere.WNc = BitConverter.ToUInt16(packet, 4);
            ionosphere.PRN = packet[6];
            ionosphere.A0 = BitConverter.ToSingle(packet, 8);
            ionosphere.A1 = BitConverter.ToSingle(packet, 12);
            ionosphere.A2 = BitConverter.ToSingle(packet, 16);
            ionosphere.A3 = BitConverter.ToSingle(packet, 20);

            ionosphere.B0 = BitConverter.ToSingle(packet, 24);
            ionosphere.B1 = BitConverter.ToSingle(packet, 28);
            ionosphere.B2 = BitConverter.ToSingle(packet, 32);
            ionosphere.B3 = BitConverter.ToSingle(packet, 36);

            beidouStore.Ionosphere = ionosphere;
        }


        /// <summary>
        /// Parse GALILEO ION
        /// </summary>
        private void ParseIonosphereGalileo(byte[] packet)
        {
            var ionosphere = new IonosphereGalileo();

            ionosphere.TOW = BitConverter.ToUInt32(packet, 0);
            ionosphere.WNc = BitConverter.ToUInt16(packet, 4);
            ionosphere.PRN = packet[6];
            ionosphere.Source = packet[7];
            ionosphere.a_i0 = BitConverter.ToSingle(packet, 8);
            ionosphere.a_i1 = BitConverter.ToSingle(packet, 12);
            ionosphere.a_i2 = BitConverter.ToSingle(packet, 16);
            ionosphere.StormFlags = packet[20];
            
            galileoStore.Ionosphere = ionosphere;
        }

        #endregion


        private void ParseSatInfo(byte[] packet)
        {
            var list = new List<SatellitePosition>();
            var numSats = (int)packet[6];
            var subblockLength = (int)packet[7];
            int totalBytes = numSats * subblockLength;
            for (int i = 8; i < 8 + totalBytes; i += subblockLength)
            {
                var sat = new SatellitePosition();
                sat.SVID = packet[i];
                sat.FreqNr = packet[i + 1];
                sat.Azimuth = (ushort)(BitConverter.ToUInt16(packet, i + 2) / 100);
                sat.Elevation = (short)(BitConverter.ToInt16(packet, i + 4) / 100);
                sat.RiseSet = packet[i + 6];
                sat.SatelliteInfo = packet[i + 7];
                list.Add(sat);
            }


            OnGetSats?.Invoke(this, list);
        }

        private void ParsePosLocal(byte[] packet)
        {
            var rec = new LocalPosition
                          {
                              Mode = packet[6],
                              Error = packet[7],
                              Latitude = BitConverter.ToDouble(packet, 8),
                              Longitude = BitConverter.ToDouble(packet, 16),
                              Altitude = BitConverter.ToDouble(packet, 24),
                              Datum = packet[32]
                          };

            OnGetLocalPosition?.Invoke(this, rec);
        }


        private void ParsePVTGeodetic(byte[] packet)
        {
            var rec = new PVTGeodetic()
                          {
                              Mode = packet[6],
                              Error = packet[7],
                              Latitude = BitConverter.ToDouble(packet, 8),
                              Longitude = BitConverter.ToDouble(packet, 16),
                              Altitude = BitConverter.ToDouble(packet, 24),
                              Undulation = BitConverter.ToSingle(packet, 32),
                              Vn = BitConverter.ToSingle(packet, 36),
                              Ve = BitConverter.ToSingle(packet, 40),
                              Vu = BitConverter.ToSingle(packet, 44),
                              COG = BitConverter.ToSingle(packet, 48),
                              RxClkBias = BitConverter.ToDouble(packet, 52),
                              RxClkDrift = BitConverter.ToSingle(packet, 60),
                              TimeSystem = packet[64],
                                Datum = packet[65],
                                NrSV = packet[66],
                                WACorrInfo = packet[67],
                                ReferenceID = BitConverter.ToUInt16(packet, 68),
                                MeanCorrAge = BitConverter.ToUInt16(packet, 70),
                                SignalInfo = BitConverter.ToUInt32(packet, 72),
                                AlertFlag = packet[76]

            };

            OnGetGeodeticPosition?.Invoke(this, rec);
            //OnGetLocalPosition?.Invoke(this, rec);
        }

        private void ParseReceiverTime(byte[] packet)
        {
            var rec = new ReceiverTime()
                          {
                              UTCYear = (sbyte)packet[6],
                              UTCMonth = (sbyte)packet[7],
                              UTCDay = (sbyte)packet[8],
                              UTCHour = (sbyte)packet[9],
                              UTCMin = (sbyte)packet[10],
                              UTCSec = (sbyte)packet[11],
                              SyncLevel = packet[12]
            };

            OnGetReceiverTime?.Invoke(this, rec);
        }
    }


}
