﻿namespace SeptentrioSBF.Models
{
    public struct LocalPosition
    {
        public byte Mode;
        public byte Error;

        public double Latitude;
        public double Longitude;
        public double Altitude;

        public byte Datum;
    }
}
