﻿
namespace SeptentrioSBF
{
    public struct IonosphereGps
    {
        public uint TOW;
        public ushort WNc;
        public byte PRN;
        public float A0 { get; set; }
        public float A1 { get; set; }
        public float A2 { get; set; }
        public float A3 { get; set; }
        public float B0 { get; set; }
        public float B1 { get; set; }
        public float B2 { get; set; }
        public float B3 { get; set; }
    }
}
