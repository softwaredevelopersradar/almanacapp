﻿namespace SeptentrioSBF
{
    public struct BlockHeader
    {
        public ushort Crc;

        public ushort Id;

        public ushort Length;

        public BlockHeader(ushort crc, ushort id, ushort length)
        {
            this.Crc = crc;
            this.Id = id;
            this.Length = length;
        }
    }
}
