﻿
namespace SeptentrioSBF
{
    public struct EphemerisGlonass
    {
        public uint TOW;
        public ushort WNc;

        public byte SvId;                 // Satellite system (R), satellite number (slot number in sat.constellation) (nn = SVID-37)
        public byte FreqNr;

        // ORBIT 1
        public double PosX;              // Satellite position X (km)
        public float VelocityXDot;      // velocity X dot (km/sec)
        public float AccelerationX;     // X acceleration (km/sec2)

        // ORBIT 2
        public double PosY;              // Satellite position Y (km)
        public float VelocityYDot;      // velocity Y dot (km/sec)
        public float AccelerationY;     // Y acceleration (km/sec2)

        // ORBIT 3
        public double PosZ;              // Satellite position Y (km)
        public float VelocityZDot;      // velocity Y dot (km/sec)
        public float AccelerationZ;     // Y acceleration (km/sec2)
        


        public float gamma;
        public float tauN;
        public float dtau;
        public uint t_oe;
        public ushort WN_toe;
        public byte P1;
        public byte P2;

        public byte E;

        public byte B;

        public ushort tb;

        public byte M;

        public byte P;

        public byte l;

        public byte P4;
        
        public ushort N_T;            
        public ushort Ft; //Фактор точности измерений
        public byte C;
    }
}
