﻿
namespace SeptentrioSBF.Models
{
    public struct PVTGeodetic
    {
        public byte Mode;
        public byte Error;

        public double Latitude;
        public double Longitude;
        public double Altitude;

        public float Undulation;

        public float Vn;
        public float Ve;
        public float Vu;

        public float COG;
        public double RxClkBias;
        public float RxClkDrift;

        public byte TimeSystem;

        public byte Datum;

        public byte NrSV;
        public byte WACorrInfo;
        public ushort ReferenceID;
        public ushort MeanCorrAge;
        public uint SignalInfo;
        public byte AlertFlag;
    }
}
