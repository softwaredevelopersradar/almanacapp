﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeptentrioSBF
{
    public struct UtcBeidou
    {
        public uint TOW;
        public ushort WNc;


        public byte PRN;
        public double A0;
        public float A1;
        public sbyte DeltaTLs;

        public byte WN_LSF;

        public byte DN;

        public sbyte DeltaTLsf;

        //Note that BDT(BeiDou time) started on January 1st, 2006 (GPS week 1356). Therefore
        //    the delta time between BDT and UTC due to leap seconds is 14 less than the value in
        //GPSUtc.
    }
}
