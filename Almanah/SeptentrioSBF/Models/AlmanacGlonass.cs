﻿
namespace SeptentrioSBF
{
    public struct AlmanacGlonass
    {
        public uint TOW;
        public ushort WNc;


        public byte SvId; 
        public byte FreqNr;
        public float E; //eccentricity

        public uint t_ao; //Reference time-of-week in GPS time frame
        public float dI; //correction to the mean value of inclination, in semi-cycles
        public float Lamda; //longitude of first ascending node passage, angle in semi-cycles

        public float t_ln; //time of first ascending node passage
        public float w; //argument of perigee, in semi-cycles
        public float dT; //correction to the mean value of Draconian period, in second
        public float dTT; // rate of change of Draconian period, in second / coil

        public float tau; //coarse correction to satellite time

        public byte WN_a; //Reference week in GPS time frame (modulo 256)
        public byte SV_HEALTH;      //SV health indicator(0 -> bad, 1 -> healthy, operational)
        public ushort DayNum; //calendar day number within 4 year period
        public byte Modification; // 2-bit GLONASS-M satellite identifier

        public byte N_4; //4 year interval number, starting from 1996

    }
}
