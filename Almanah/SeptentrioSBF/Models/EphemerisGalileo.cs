﻿namespace SeptentrioSBF
{
    public struct EphemerisGalileo
    {
        public uint TOW;
        public ushort WNc;

        public byte SvId;

        public byte Source; 
        public double sqrt_A;
        public double M_0;
        public double e;
        public double i_0;
        public double omega;
        public double OMEGA_0;

        public float OMEGADOT;
        public float IDOT;
        public float delta_n;
        public float C_uc;
        public float C_us;
        public float C_rc;        
        public double C_rs;        
        public float C_ic;
        public float C_is;

        public uint t_oe;
        public uint t_oc;

        public float a_f2;
        public float a_f1;
        public double a_f0;

        public ushort WNt_oc;
        public ushort WNt_oe;

        public ushort IODnav;

        public ushort Health_OSSOL;

        public byte Health_PRS;
        public byte SISA_L1E5a;
        public byte SISA_L1E5b;
        public byte SISA_L1AE6A;
        public float BGD_L1E5a;
        public float BGD_L1E5b;
        public float BGD_L1AE6A;
        public byte CNAVenc;

        
    }
}
