﻿
namespace SeptentrioSBF
{
    public struct UtcGps
    {
        public uint TOW;
        public ushort WNc;


        public byte PRN;
        public byte Reserved;
        public double A0 { get; set; }
        public float A1 { get; set; }
        public uint Tot { get; set; }
        public byte WeekNumT { get; set; }
        public sbyte DeltaTLs { get; set; }

        public byte WN_LSF;

        public byte DN;

        public sbyte DeltaTLsf;
        
    }
}
