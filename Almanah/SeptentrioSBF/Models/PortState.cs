﻿namespace SeptentrioSBF
{
    public struct PortState
    {
        public bool RtsIsEvalable { get; set; }
        public bool DsrIsEvalable { get; set; }

        public PortState(bool rtsIsEvalable, bool dsrIsEvalable)
        {
            RtsIsEvalable = rtsIsEvalable;
            DsrIsEvalable = dsrIsEvalable;
        }
    }
}
