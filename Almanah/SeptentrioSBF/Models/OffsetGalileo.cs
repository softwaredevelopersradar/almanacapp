﻿
namespace SeptentrioSBF
{
     public struct OffsetGalileo
    {
        public uint TOW;
        public ushort WNc;


        public byte SvId;
        public byte Source;
        public float A_0G { get; set; }
        public float A_1G { get; set; }
        public uint t_oG { get; set; }
        public byte WN_oG { get; set; }
    }
}
