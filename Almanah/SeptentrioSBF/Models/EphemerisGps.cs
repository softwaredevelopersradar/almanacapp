﻿
namespace SeptentrioSBF
{
    public struct EphemerisGps
    {
        public uint TOW;
        public ushort WNc;


        public byte PRN;

        public float a_f2;             
        public float a_f1;             
        public float a_f0; 
        
        // ORBIT 1
        public byte IODE2;
        public byte IODE3;
        public double C_rs;                 
        public float delta_n;              
        public double M_0;                  
        // ORBIT 2
        public float C_uc;                 
        public double e;                    
        public float C_us;                 
        public double sqrt_A;               
        // ORBIT 3
        public uint t_oe;                 
        public float C_ic;                 
        public double OMEGA_0;              
        public float C_is;                 
        // ORBIT 4
        public double i_0;                  
        public float C_rc;                 
        public double omega;                
        public float OMEGADOT;             
        // ORBIT 5
        public float IDOT;                 
        public byte codeL2;
        public ushort weeknum;             
        public double L2Pdata;              
        // ORBIT 6
        public byte URA;                
        public byte SV_health;            
        public double T_GD;                 
        public ushort IODC;                 
        // ORBIT 7
        //public double HOW;                  
        public byte fit_interval;

        public uint t_oc;
        public ushort WNt_oc;
        public ushort WNt_oe;
    }
}
