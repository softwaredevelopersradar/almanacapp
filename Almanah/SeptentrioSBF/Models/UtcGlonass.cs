﻿
namespace SeptentrioSBF
{
    public struct UtcGlonass
    {
        public uint TOW;
        public ushort WNc;


        public byte SvId;

        public byte FreqNr;
        public byte N_4;

        public byte KP;

        public ushort N;

        public float tau_GPS;

        public double tau_C;

        public float B1;
        public float B2;
        
    }
}
