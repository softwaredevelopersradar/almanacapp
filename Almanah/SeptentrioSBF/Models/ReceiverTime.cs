﻿
namespace SeptentrioSBF.Models
{
    public struct ReceiverTime
    {
        public sbyte UTCYear;

        public sbyte UTCMonth;
        public sbyte UTCDay;
        public sbyte UTCHour;

        public sbyte UTCMin;
        public sbyte UTCSec;

        public sbyte DeltaLS;
        public byte SyncLevel;
    }
}
