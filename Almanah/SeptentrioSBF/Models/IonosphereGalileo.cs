﻿
namespace SeptentrioSBF
{
    public struct IonosphereGalileo
    {
        public uint TOW;
        public ushort WNc;
        public byte PRN;
        public byte Source;
        public float a_i0 { get; set; }
        public float a_i1 { get; set; }
        public float a_i2 { get; set; }
        public byte StormFlags { get; set; }
        
    }
}
