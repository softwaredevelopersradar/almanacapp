﻿namespace SeptentrioSBF
{
    public struct EphemerisBeidou
    {
        public uint TOW;
        public ushort WNc;


        public byte PRN;

        public ushort WN;
        public byte URA;
        public byte SV_health;
        
        public byte IODC;
        public byte IODE;
        public float T_GD1;
        public float T_GD2;
        public uint t_oc;

        public float a_f2;
        public float a_f1;
        public float a_f0;

        public float C_rs;
        public float delta_n;
        public double M_0;
        // ORBIT 2
        public float C_uc;
        public double e;
        public float C_us;        
        public double sqrt_A;
        public uint t_oe;
        public float C_ic;

        // ORBIT 3
        
        
        public double OMEGA_0;
        public float C_is;
        // ORBIT 4
        public double i_0;
        public float C_rc;
        public double omega;
        public float OMEGADOT;
        // ORBIT 5
        public float IDOT;
        public ushort WNt_oc;
        public ushort WNt_oe;
        
        
    }
}
