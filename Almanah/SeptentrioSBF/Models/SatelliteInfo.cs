﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeptentrioSBF.Models
{
    public struct SatelliteInfo
    {
        public uint TOW;
        public ushort WNc;


        public byte PRN;
    }

    public struct SatellitePosition
    {
        public byte SVID;
        public byte FreqNr;
        public ushort Azimuth;
        public short Elevation;

        public byte RiseSet; 
        //    0: satellite setting
        //1: satellite rising
        //255: elevation rate unknown

        public byte SatelliteInfo;
        //Satellite visibility info based on:
        //1: almanac
        //2: ephemeris
        //255: unknown

    }
}
