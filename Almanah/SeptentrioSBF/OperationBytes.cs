﻿using System;
using System.Collections.Generic;

namespace SeptentrioSBF
{
    public class OperationBytes
    {
        public static void InverseData(List<byte> Data1, int ind, byte[] Data2, int iLen)
        {
            if (Data1.Count > 0)
            {
                for (int i = 0; i < iLen; i++)
                {
                    Data2[iLen - 1 - i] = Data1[ind + i];
                }
            }
        }

        //копирует диапазон байт списка в массив и инвертирует его 
        public static byte[] InverseData(List<byte> packet, int index, int len)
        {
            byte[] result = new byte[len];
            Array.Copy(packet.ToArray(), index, result, 0, len);
            Array.Reverse(result);
            return result;
        }

        public static void InverseData(byte[] Data1, int ind, byte[] Data2, int iLen)
        {
            if (Data1.Length > 0)
            {
                for (int i = 0; i < iLen; i++)
                {
                    Data2[iLen - 1 - i] = Data1[ind + i];
                }
            }
        }

        //копирует диапазон байт из массива в массив и инвертирует его 
        public static byte[] InverseData(byte[] packet, int index, int len)
        {
            byte[] result = new byte[len];
            Array.Copy(packet, index, result, 0, len);
            Array.Reverse(result);
            return result;
        }
    }
}
