﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using ClientLib;
using ClientServerLib;
using ToRinexConverter;

namespace TestClientWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        string text = "";

        string host = "0";
        int port = 0;
        Client objClient;
        private AlmanacConverter almanacConverter;
        private EphemerisConverter ephemerisConverter;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            //almanacConverter = new AlmanacConverter();
            //ephemerisConverter = new EphemerisConverter();
        }

        public string TextMess
        {
            get { return text; }
            set
            {
                text = value;
                PropertyChanged(this, new PropertyChangedEventArgs("TextMess"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private void BtnConnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (objClient != null)
                {
                    objClient = null;
                }
                btnConnect.IsEnabled = false;
                host = tbxIp.Text;
                port = Convert.ToInt16(tbxPort.Text);
                if (!host.Equals("0") && port != 0)
                {
                    objClient = new Client(host, port);
                    if (objClient.CheckHandler())
                    {
                        objClient.OnGetData += ProcessIncomeData;
                    }
                    objClient.Connect();
                    TextMess += $"Connected to server"+ "\n";
                }

                else
                {
                    TextMess += "Рost or port data are incorrect!"+ "\n";
                }
                btnDisonnect.IsEnabled = true;
            }
            catch (Exception ex)
            {
                TextMess += "Port or host values are incorrect!" + "\n";
            }
        }

        int operationnumber = 0; //убрать, когда Олег заменит аргументы события

        public void ProcessIncomeData(object sender, MyEventArgsGetData e)
        {
            try
            {
                string objFull = null;

                switch (operationnumber)
                {
                    case 1:
                        
             
                        foreach (Ephemeries item in objClient.ephemeries)
                        {
                            objFull += "\n Ephemeris (" + item.sv_number.ToString() + ") \n";
                            IEnumerable<FieldInfo> fields = item.GetType().GetTypeInfo().DeclaredFields;

                            foreach (var field in fields.Where(x => !x.IsStatic)) //вывод всех полей объекта и их значений
                            {
                                objFull += field.Name + " = " + field.GetValue(item) + "\n";
                            }
                        }

                        TextMess += "\n All Ephemeris: \n" + objFull + "\n";

                        //преобразование в файлы для спуфинга
                        if (objClient.ephemeries.Count == 32)
                        {
                            //ephemerisConverter.WriteToRinex(null, objClient.ephemeries);
                        }
                        break;
                    case 2:

                        foreach (Almanac item in objClient.almanac)
                        {
                            objFull += "\n Almanac (" + item.PRN.ToString() + ") \n";
                            IEnumerable<FieldInfo> fields = item.GetType().GetTypeInfo().DeclaredFields;

                            foreach (var field in fields.Where(x => !x.IsStatic))
                            {
                                objFull += field.Name + " = " + field.GetValue(item) + "\n";
                            }
                        }

                        TextMess += "\n All Almanacs: \n" + objFull + "\n";

                        if (objClient.almanac.Count == 32)
                        {
                            //almanacConverter.WriteToSEM(null, objClient.almanac);
                        }
                        break;
                    case 4:
                        foreach (SatInfo item in objClient.satinfo)
                        {
                            objFull += "\n Sattelite (" + item.PRN.ToString() + ") \n";
                            IEnumerable<FieldInfo> fields = item.GetType().GetTypeInfo().DeclaredFields;

                            foreach (var field in fields.Where(x => !x.IsStatic))
                            {
                                objFull += field.Name + " = " + field.GetValue(item) + "\n";
                            }
                        }

                        TextMess += "\n All Sattelites: \n" + objFull + "\n";
                        break;
                    case 5:

                        IEnumerable<FieldInfo> fieldsLoc = objClient.reclocation.GetType().GetTypeInfo().DeclaredFields;

                        foreach (var field in fieldsLoc.Where(x => !x.IsStatic))
                        {
                            objFull += field.Name + " = " + field.GetValue(objClient.reclocation) + "\n";
                        }
                
                        TextMess += "\n Receiver location: \n" + objFull + "\n";
                        break;
                    case 6:
                        TextMess += "\n Time now: \n" +  objClient.time.ToString("dd-MM-yyyy H:mm:s") + "\n";
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                TextMess += ex.ToString() + "\n"; 
            }
        }

        private void BtnDisonnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnConnect.IsEnabled = true;
                objClient.Disconnect();
                btnDisonnect.IsEnabled = false;
                TextMess += "Disconnected from server" + "\n";
            }
            catch (Exception ex)
            {
                TextMess += ex.ToString() + "\n";
            }
        }

        private void BtnAlmanac_Click(object sender, RoutedEventArgs e)
        {
            objClient.SendMessage(Operations.Almanac);
            operationnumber = (int)Operations.Almanac;
        }

        private void BtnEphemeris_Click(object sender, RoutedEventArgs e)
        {
            objClient.SendMessage(Operations.Ephemeries);
            operationnumber = (int)Operations.Ephemeries;
        }

        private void BtnSatInfo_Click(object sender, RoutedEventArgs e)
        {
            objClient.SendMessage(Operations.SatInfo);
            operationnumber = (int)Operations.SatInfo;
        }

        private void BtnReceiver_Click(object sender, RoutedEventArgs e)
        {
            objClient.SendMessage(Operations.RecLocation);
            operationnumber = (int)Operations.RecLocation;
        }

        private void BtnTime_Click(object sender, RoutedEventArgs e)
        {
            objClient.SendMessage(Operations.Time);
            operationnumber = (int)Operations.Time;
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            TextMess = "";
        }
    }
}
