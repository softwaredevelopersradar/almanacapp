﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToRinexConverter
{
    internal static class Sem
    {
        /// <summary>
        /// метод формирует строку, которую запишут в файл (SEM)
        /// </summary>
        internal static string WriteData(List<AlmanacGps> records, ushort weekNum, uint timeOfAlmanac)
        {
            StringBuilder fullAlm = new StringBuilder();
            fullAlm.AppendLine(records.Count.ToString().PadRight(5) + "CURRENT.ALM");
            fullAlm.AppendLine(weekNum.ToString().PadRight(5) + timeOfAlmanac.ToString());

            for (int i = 0; i < records.Count; i++)
            {
                fullAlm.AppendLine("");
                fullAlm.AppendLine(records[i].PRN.ToString());
                fullAlm.AppendLine(records[i].SVN.ToString());
                fullAlm.AppendLine(records[i].aURA.ToString());

                fullAlm.Append(String.Format("{0,21:0.00000000000000E+00}", records[i].e) + " ");
                fullAlm.Append(String.Format("{0,21:0.00000000000000E+00}", records[i].i_o_sema) + " ");
                fullAlm.AppendLine(String.Format("{0,21:0.00000000000000E+00}", records[i].OMEGADOT));

                fullAlm.Append(String.Format("{0,21:0.00000000000000E+00}", records[i].sqrt_A) + " ");
                fullAlm.Append(String.Format("{0,21:0.00000000000000E+00}", records[i].OMEGA_0) + " ");
                fullAlm.AppendLine(String.Format("{0,21:0.00000000000000E+00}", records[i].omega));

                fullAlm.Append(String.Format("{0,21:0.00000000000000E+00}", records[i].M_0) + " ");
                fullAlm.Append(String.Format("{0,21:0.00000000000000E+00}", records[i].a_f0) + " ");
                fullAlm.AppendLine(String.Format("{0,21:0.00000000000000E+00}", records[i].a_f1));

                fullAlm.AppendLine(records[i].SV_HEALTH.ToString());
                fullAlm.AppendLine(records[i].SV_CONFIG.ToString());
                fullAlm.Replace(",", ".");
            }

            return fullAlm.ToString();
        }
    }
}
