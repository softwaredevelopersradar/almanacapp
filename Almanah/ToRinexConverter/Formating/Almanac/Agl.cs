﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToRinexConverter
{
    internal static class Agl
    {
        /// <summary>
        /// File creation Glonass almanac (AGL format)
        /// </summary>
        internal static string WriteData(List<AlmanacGlonass> records)
        {
            StringBuilder fullAlm = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullAlm.AppendLine(records[i].Day_A.ToString("00") + " " + records[i].Month_A.ToString("00") + " " + records[i].Year_A.ToString("0000") + records[i].t_d.ToString().PadLeft(8));

                fullAlm.Append(String.Format("{0,2}", records[i].Num) + records[i].f_channel.ToString().PadLeft(4) + records[i].SV_HEALTH.ToString().PadLeft(3) + "  ");
                fullAlm.Append(records[i].Day.ToString("00") + " " + records[i].Month.ToString("00") + " " + records[i].Year.ToString("0000") + " ");
                fullAlm.Append(String.Format("{0,16:Z.000000000E+00}", records[i].t_r) + " ");
                fullAlm.Append(String.Format("{0,16:Z.000000000E+00}", records[i].C_utc) + " ");
                fullAlm.Append(String.Format("{0,16:Z.000000000E+00}", records[i].C_gps) + " ");
                fullAlm.AppendLine(String.Format("{0,16:Z.000000000E+00}", records[i].C_sys));

                fullAlm.Append(String.Format("{0,14:Z.0000000E+00}", records[i].Lamda) + " ");
                fullAlm.Append(String.Format("{0,14:Z.0000000E+00}", records[i].dI) + " ");
                fullAlm.Append(String.Format("{0,14:Z.0000000E+00}", records[i].w) + " ");
                fullAlm.Append(String.Format("{0,14:Z.0000000E+00}", records[i].E) + " ");
                fullAlm.Append(String.Format("{0,14:Z.0000000E+00}", records[i].dT) + " ");
                fullAlm.AppendLine(String.Format("{0,14:Z.0000000E+00}", records[i].dTT));

                fullAlm.Replace(",", ".");
                fullAlm.Replace("Z.", "0.");
            }

            return fullAlm.ToString();
        }
    }
}
