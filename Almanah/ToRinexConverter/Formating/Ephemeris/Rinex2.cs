﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToRinexConverter
{
    internal static class Rinex2
    {

        #region Fake
        static float A0 = 0.00000001676f;
        static float A1 = -0.0000000149f;
        static float A2 = -0.0000000596f;
        static float A3 = 0.0000001788f;

        static float B0 = 129000;
        static float B1 = -180200;
        static float B2 = 65540;
        static float B3 = 131100;

        static double A_0 = -0.00000000465661287308d;
        static double A_1 = -0.000000000000008881784197d;
        static int timeUTC = 233472;
        static int weeknum = 1875;
        #endregion

        /// <summary>
        /// Формирование заголовока GPS
        /// </summary>
        internal static string WriteHeaderGPS(DateTime timeFileCreation)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format("{0,6}", 2).PadRight(20) + "NAVIGATION DATA".PadRight(40) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram2.PadRight(20) + Constants.nameOfAgency2.PadRight(20) + timeFileCreation.ToString("dd-") + Enum.GetName(typeof(Month), timeFileCreation.Month) + timeFileCreation.ToString("-y HH:mm").PadRight(14) + "PGM / RUN BY / DATE");  //12-SEP-90 15:22
            fullEph.AppendLine(Constants.comment2.PadRight(60) + "COMMENT");

            fullEph.AppendLine(A0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(15) + A1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + A2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + A3.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "ION ALPHA".PadLeft(19));
            fullEph.AppendLine(B0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(15) + B1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + B2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + B3.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "ION BETA".PadLeft(18));
            fullEph.AppendLine(A_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(22) + A_1.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + timeUTC.ToString().PadLeft(9) + weeknum.ToString().PadLeft(9) + " DELTA-UTC: A0,A1,T,W");
            fullEph.AppendLine(Constants.leapSeconds.ToString().PadLeft(6) + "LEAP SECONDS".PadLeft(66));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");
            return fullEph.ToString();
        }

        /// <summary>
        /// Формирование заголовока GPS
        /// </summary>
        internal static string WriteHeaderGPS(DateTime timeFileCreation, HeaderEphemerisGps data)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format("{0,6}", 2).PadRight(20) + "NAVIGATION DATA".PadRight(40) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram2.PadRight(20) + Constants.nameOfAgency2.PadRight(20) + timeFileCreation.ToString("dd-") + Enum.GetName(typeof(Month), timeFileCreation.Month) + timeFileCreation.ToString("-y HH:mm").PadRight(14) + "PGM / RUN BY / DATE");  //12-SEP-90 15:22
            fullEph.AppendLine(Constants.comment2.PadRight(60) + "COMMENT");

            fullEph.AppendLine(data.A0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(15) + data.A1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + data.A2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + data.A3.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "ION ALPHA".PadLeft(19));
            fullEph.AppendLine(data.B0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(15) + data.B1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + data.B2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + data.B3.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "ION BETA".PadLeft(18));
            fullEph.AppendLine(data.A0Delta.ToString("Z.000000000000E+00", Constants.ci).PadLeft(22) + data.A1Delta.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + data.Tot.ToString().PadLeft(9) + data.WeekNumT.ToString().PadLeft(9) + " DELTA-UTC: A0,A1,T,W");
            fullEph.AppendLine(data.DeltaTLs.ToString().PadLeft(6) + "LEAP SECONDS".PadLeft(66));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");
            return fullEph.ToString();
        }


        /// <summary>
        /// Формирование поля данных GPS
        /// </summary>
        internal static string WriteData(List<EphemerisGps> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.Append(String.Format("{0,2}", records[i].PRN) + " " + (records[i].Year % 100).ToString("00") + " " + String.Format("{0,2}", records[i].Month) + " ");
                fullEph.Append(String.Format("{0,2}", records[i].Day) + " " + String.Format("{0,2}", records[i].Hour) + " " + String.Format("{0,2}", records[i].Minute) + " " + records[i].Second.ToString("0.0", Constants.ci).PadLeft(4));
                fullEph.AppendLine(records[i].SVClockBias.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDrift.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDriftRate.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 1
                fullEph.Append(records[i].IODE.ToString("Z.000000000000E+00", Constants.ci).PadLeft(22) + records[i].C_rs.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].delta_n.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].M_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].C_uc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(22) + records[i].e.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].C_us.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].sqrt_A.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].T_oe.ToString("Z.000000000000E+00", Constants.ci).PadLeft(22) + records[i].C_ic.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].OMEGA_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].C_is.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].i_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(22) + records[i].C_rc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].omega.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].OMEGADOT.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 5
                fullEph.Append(records[i].IDOT.ToString("Z.000000000000E+00", Constants.ci).PadLeft(22) + records[i].codeL2.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].weeknum.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].L2Pdata.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 6
                fullEph.Append(records[i].SVacc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(22) + records[i].SV_health.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].T_GD.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].IODC.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 7
                fullEph.Append(records[i].HOW.ToString("Z.000000000000E+00", Constants.ci).PadLeft(22) + records[i].fit_interval.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));

                fullEph.Replace("Z.", "0.");
                fullEph = fullEph.Replace("E-", "e-");
                fullEph = fullEph.Replace("E+", "e+");
            }
            return fullEph.ToString();
        }


        /// <summary>
        /// Формирование заголовка GLONASS
        /// </summary>
        internal static StringBuilder WriteHeaderGLONASS(DateTime creationTime, double c_utc)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 2.01f).PadRight(20) + "GLONASS NAV DATA".PadRight(40) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram2Glo.PadRight(20) + Constants.nameOfAgency2Glo.PadRight(20) + creationTime.ToString("dd-") + Enum.GetName(typeof(Month), creationTime.Month) + creationTime.ToString("-y HH:mm").PadRight(14) + "PGM / RUN BY / DATE");  //12-SEP-90 15:22
            fullEph.AppendLine(Constants.comment2Glo.PadRight(60) + "COMMENT");

            //var fixingTime = creationTime.AddDays(-1); //not sure
            var fixingTime = creationTime; //not sure
            fullEph.AppendLine(fixingTime.Year.ToString().PadLeft(6) + fixingTime.Month.ToString().PadLeft(6)+ fixingTime.Day.ToString().PadLeft(6) + c_utc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(22) + "CORR TO SYSTEM TIME ".PadLeft(40));
            fullEph.AppendLine(Constants.leapSeconds.ToString().PadLeft(6) + "LEAP SECONDS        ".PadLeft(74));
            fullEph.AppendLine("END OF HEADER    ".PadLeft(77));

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph;
        }


        /// <summary>
        /// Формирование поля данных GLONASS
        /// </summary>
        internal static StringBuilder WriteDataGLONASS(List<EphemerisGlonass> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.Append(String.Format("{0,2}", records[i].PRN) +" " + (records[i].Year % 100).ToString("00") + " " + String.Format("{0,2}", records[i].Month) + " ");
                fullEph.Append(String.Format("{0,2}", records[i].Day) + " " + String.Format("{0,2}", records[i].Hour) + " " + String.Format("{0,2}", records[i].Minute) + " " + records[i].Second.ToString("0.0", Constants.ci).PadLeft(4));
                fullEph.AppendLine(records[i].SVClockBias.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVFreqBias.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].MessFrameTime.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));

                // ORBIT 1
                fullEph.Append(records[i].PosX.ToString("0.000000000000E+00", Constants.ci).PadLeft(22) + records[i].VelocityXDot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationX.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].Health.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].PosY.ToString("0.000000000000E+00", Constants.ci).PadLeft(22) + records[i].VelocityYDot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationY.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].FreqNum.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].PosZ.ToString("0.000000000000E+00", Constants.ci).PadLeft(22) + records[i].VelocityZDot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationZ.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].AgeInfo.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));


                fullEph = fullEph.Replace("E-", "D-");
                fullEph = fullEph.Replace("E+", "D+");
            }
            return fullEph;
        }
    }
}
