﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToRinexConverter
{
    internal static class Rinex3_02
    {
        #region Fake
        static float A0 = 0.00000001676f;
        static float A1 = -0.0000000149f;
        static float A2 = -0.0000000596f;
        static float A3 = 0.0000001788f;

        static float B0 = 129000;
        static float B1 = -180200;
        static float B2 = 65540;
        static float B3 = 131100;

        static double A_0 = -0.00000000465661287308d;
        static double A_1 = -0.000000000000008881784197d;
        static int timeUTC = 233472;
        static int weeknum = 1875;
        #endregion

        /// <summary>
        /// Формирование заголовока V3.02
        /// </summary>
        internal static string WriteHeaderGPS(DateTime timeFileCreation)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 3.02f).PadRight(20) + "N: GNSS NAV DATA".PadRight(20) + "G: GPS".PadRight(20) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram3_02.PadRight(40) + timeFileCreation.ToString("yyyyMMdd HHmmss").PadRight(16) + "UTC".PadRight(4) + "PGM / RUN BY / DATE");
            //fullEph.AppendLine("COMMENT".PadLeft(67));
            //fullEph.AppendLine("COMMENT".PadLeft(67));
            //fullEph.AppendLine("COMMENT".PadLeft(67));

            fullEph.AppendLine("GPSA" + A0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(14) + A1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + A2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + A3.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "IONOSPHERIC CORR".PadLeft(23));
            fullEph.AppendLine("GPSB" + B0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(14) + B1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + B2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + B3.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "IONOSPHERIC CORR".PadLeft(23));
            fullEph.AppendLine("GPUT" + A_0.ToString("Z.0000000000E+00", Constants.ci).PadLeft(18) + A_1.ToString("Z.000000000E+00", Constants.ci).PadLeft(16) + timeUTC.ToString().PadLeft(7) + weeknum.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            fullEph.AppendLine(Constants.leapSeconds.ToString().PadLeft(6) + "LEAP SECONDS".PadLeft(66));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph.ToString();
        }


        /// <summary>
        /// Формирование заголовока V3.02
        /// </summary>
        internal static string WriteHeaderGPS(DateTime timeFileCreation, HeaderEphemerisGps header)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 3.02f).PadRight(20) + "N: GNSS NAV DATA".PadRight(20) + "G: GPS".PadRight(20) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram3_02.PadRight(40) + timeFileCreation.ToString("yyyyMMdd HHmmss").PadRight(16) + "UTC".PadRight(4) + "PGM / RUN BY / DATE");

            fullEph.AppendLine("GPSA" + header.A0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(14) + header.A1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + header.A2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + header.A3.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "IONOSPHERIC CORR".PadLeft(23));
            fullEph.AppendLine("GPSB" + header.B0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(14) + header.B1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + header.B2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + header.B3.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "IONOSPHERIC CORR".PadLeft(23));
            fullEph.AppendLine("GPUT" + header.A0Delta.ToString("Z.0000000000E+00", Constants.ci).PadLeft(18) + header.A1Delta.ToString("Z.000000000E+00", Constants.ci).PadLeft(16) + header.Tot.ToString().PadLeft(7) + header.WeekNumT.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            fullEph.AppendLine(header.DeltaTLs.ToString().PadLeft(6) + "LEAP SECONDS".PadLeft(66));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph.ToString();
        }


        /// <summary>
        /// Формирование поля данных
        /// </summary>
        internal static string WriteData(List<EphemerisGps> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.Append("G" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDrift.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDriftRate.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 1
                fullEph.Append(records[i].IODE.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rs.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].delta_n.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].M_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].C_uc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].e.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].C_us.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].sqrt_A.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].T_oe.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_ic.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].OMEGA_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].C_is.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].i_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].omega.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].OMEGADOT.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 5
                fullEph.Append(records[i].IDOT.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].codeL2.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].weeknum.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].L2Pdata.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 6
                fullEph.Append(records[i].SVacc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].SV_health.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].T_GD.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].IODC.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 7
                fullEph.AppendLine(records[i].HOW.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].fit_interval.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                //fullEph.AppendLine(0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
            }

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph.ToString();
        }


        /// <summary>
        /// Формирование заголовка GLONASS
        /// </summary>
        internal static StringBuilder WriteHeaderGLONASS(DateTime creationTime)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 3.02f).PadRight(20) + "N: GNSS NAV DATA".PadRight(20) + "R: GLONASS".PadRight(20) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram3_02.PadRight(20) + Constants.nameOfAgency3_02.PadRight(20) + creationTime.ToString("yyyyMMdd HHmmss").PadRight(16) + "UTC".PadRight(4) + "PGM / RUN BY / DATE");  //19990903 152236 UTC
            //fullEph.AppendLine(Constants.comment3_02.PadRight(60) + "COMMENT");

            //TODO: мб убрать ионосферу
            //fullEph.AppendLine("GPSA" + A0.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(15) + A1.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(12) + A2.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(12) + A3.ToString("Z.0000E+00", Constants.ci).PadLeft(10) + "IONOSPHERIC CORR".PadLeft(23));
            //fullEph.AppendLine("GPSB" + B0.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(15) + B1.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(12) + B2.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(12) + B3.ToString("Z.0000E+00", Constants.ci).PadLeft(10) + "IONOSPHERIC CORR".PadLeft(23));
            //fullEph.AppendLine("GPUT" + A_0.ToString("Z.0000000000E+00", Constants.ci).PadLeft(18) + A_1.ToString("Z.000000000E+00", Constants.ci).PadLeft(16) + timeUTC.ToString().PadLeft(7) + weeknum.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            //fullEph.AppendLine("GLUT" + A_0.ToString("Z.0000000000E+00", Constants.ci).PadLeft(18) + A_1.ToString("Z.000000000E+00", Constants.ci).PadLeft(16) + timeUTC.ToString().PadLeft(7) + weeknum.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
           // fullEph.AppendLine(Constants.leapSeconds.ToString().PadLeft(6) + "LEAP SECONDS".PadLeft(66));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph;
        }

        /// <summary>
        /// Формирование заголовка GLONASS
        /// </summary>
        internal static StringBuilder WriteHeaderGLONASS(DateTime creationTime, HeaderEphemerisGlonass header)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 3.02f).PadRight(20) + "N: GNSS NAV DATA".PadRight(20) + "R: GLONASS".PadRight(20) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram3_02.PadRight(20) + Constants.nameOfAgency3_02.PadRight(20) + creationTime.ToString("yyyyMMdd HHmmss").PadRight(16) + "UTC".PadRight(4) + "PGM / RUN BY / DATE");  //19990903 152236 UTC

            fullEph.AppendLine("GLUT" + header.A0Delta.ToString("Z.0000000000E+00", Constants.ci).PadLeft(18) + header.A1Delta.ToString("Z.000000000E+00", Constants.ci).PadLeft(16) + header.Tot.ToString().PadLeft(7) + header.WeekNumT.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph;
        }


        /// <summary>
        /// Формирование поля данных GLONASS
        /// </summary>
        internal static StringBuilder WriteDataGLONASS(List<EphemerisGlonass> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.Append("R" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVFreqBias.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].MessFrameTime.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                
                // ORBIT 1
                fullEph.Append(records[i].PosX.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].VelocityXDot.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationX.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].Health.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].PosY.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].VelocityYDot.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationY.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].FreqNum.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].PosZ.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].VelocityZDot.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationZ.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].AgeInfo.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));

                fullEph.Replace("Z.", "0.");
                fullEph = fullEph.Replace("E-", "D-");
                fullEph = fullEph.Replace("E+", "D+");
            }
            return fullEph;
        }


        /// <summary>
        /// Формирование заголовка BEIDOU
        /// </summary>
        internal static StringBuilder WriteHeaderBEIDOU(DateTime creationTime)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 3.02f).PadRight(20) + "N: GNSS NAV DATA".PadRight(20) + "C: BDS".PadRight(20) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram3_02.PadRight(20) + Constants.nameOfAgency3_02.PadRight(20) + creationTime.ToString("yyyyMMdd HHmmss").PadRight(16) + "UTC".PadRight(4) + "PGM / RUN BY / DATE");  //19990903 152236 UTC
            fullEph.AppendLine(Constants.comment3_02.PadRight(60) + "COMMENT");

            //TODO: мб убрать ионосферу
            //BDSA = BDS alpha0 - alpha3
            //BDSB = BDS beta0 - beta3
            //BDS: alpha0 - alpha3 or beta0-beta3
            //BDUT = BDS to UTC a0 = A0UTC, a1 = A1UTC
            //fullEph.AppendLine("GPSA" + A0.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(15) + A1.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(12) + A2.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(12) + A3.ToString("Z.0000E+00", Constants.ci).PadLeft(10) + "IONOSPHERIC CORR".PadLeft(23));
            //fullEph.AppendLine("GPSB" + B0.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(15) + B1.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(12) + B2.ToString("Z.0000E+00" + "  ", Constants.ci).PadLeft(12) + B3.ToString("Z.0000E+00", Constants.ci).PadLeft(10) + "IONOSPHERIC CORR".PadLeft(23));
            //fullEph.AppendLine("GPUT" + A_0.ToString("Z.0000000000E+00", Constants.ci).PadLeft(18) + A_1.ToString("Z.000000000E+00", Constants.ci).PadLeft(16) + timeUTC.ToString().PadLeft(7) + weeknum.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            //fullEph.AppendLine("GLUT" + A_0.ToString("Z.0000000000E+00", Constants.ci).PadLeft(18) + A_1.ToString("Z.000000000E+00", Constants.ci).PadLeft(16) + timeUTC.ToString().PadLeft(7) + weeknum.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            fullEph.AppendLine(Constants.leapSeconds.ToString().PadLeft(6) + "LEAP SECONDS".PadLeft(66));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph;
        }

        /// <summary>
        /// Формирование заголовка BEIDOU
        /// </summary>
        internal static StringBuilder WriteHeaderBEIDOU(DateTime creationTime, HeaderEphemerisGps header)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 3.02f).PadRight(20) + "N: GNSS NAV DATA".PadRight(20) + "C: BEIDOU".PadRight(20) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram3_02.PadRight(20) + Constants.nameOfAgency3_02.PadRight(20) + creationTime.ToString("yyyyMMdd HHmmss").PadRight(16) + "UTC".PadRight(4) + "PGM / RUN BY / DATE");  //19990903 152236 UTC
                                                                                                                                                                                                                         //fullEph.AppendLine(Constants.comment3_02.PadRight(60) + "COMMENT");
            fullEph.AppendLine("BDSA" + header.A0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(14) + header.A1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + header.A2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + header.A3.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "IONOSPHERIC CORR".PadLeft(23));
            fullEph.AppendLine("BDSB" + header.B0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(14) + header.B1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + header.B2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + header.B3.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "IONOSPHERIC CORR".PadLeft(23));

            fullEph.AppendLine("BDUT" + header.A0Delta.ToString("Z.0000000000E+00", Constants.ci).PadLeft(18) + header.A1Delta.ToString("Z.000000000E+00", Constants.ci).PadLeft(16) + header.Tot.ToString().PadLeft(7) + header.WeekNumT.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            fullEph.AppendLine(header.DeltaTLs.ToString().PadLeft(6) + "LEAP SECONDS".PadLeft(66));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph;
        }


        /// <summary>
        /// Формирование поля данных BEIDOU
        /// </summary>
        internal static string WriteDataBEIDOU(List<EphemerisBeidou> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.Append("C" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDrift.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDriftRate.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 1
                fullEph.Append(records[i].IODE.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rs.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].delta_n.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].M_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].C_uc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].e.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].C_us.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].sqrt_A.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].T_oe.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_ic.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].OMEGA_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].C_is.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].i_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].omega.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].OMEGADOT.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 5
                fullEph.Append(records[i].IDOT.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].weeknum.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 6
                fullEph.Append(records[i].SVaccuracy.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].SV_health.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].T_GD1.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].T_GD2.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 7
                fullEph.AppendLine(records[i].HOW.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].IODC.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                //fullEph.AppendLine(0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
            }

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph.ToString();
        }


        /// <summary>
        /// Формирование заголовока V3.02
        /// </summary>
        internal static string WriteHeaderGalileo(DateTime timeFileCreation, HeaderEphemerisGalileo header)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 3.02f).PadRight(20) + "N: GNSS NAV DATA".PadRight(20) + "E: GALILEO".PadRight(20) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram3_02.PadRight(40) + timeFileCreation.ToString("yyyyMMdd HHmmss").PadRight(16) + "UTC".PadRight(4) + "PGM / RUN BY / DATE");

            fullEph.AppendLine("GAL " + header.A0.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(14) + header.A1.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + header.A2.ToString("Z.0000E+00" + " ", Constants.ci).PadLeft(12) + 0.ToString("Z.0000E+00", Constants.ci).PadLeft(11) + "IONOSPHERIC CORR".PadLeft(23));
            fullEph.AppendLine("GAUT" + header.A0Delta.ToString("Z.0000000000E+00", Constants.ci).PadLeft(18) + header.A1Delta.ToString("Z.000000000E+00", Constants.ci).PadLeft(16) + header.Tot.ToString().PadLeft(7) + header.WeekNumT.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            fullEph.AppendLine("GPGA" + header.A0DeltaGPS.ToString("Z.0000000000E+00", Constants.ci).PadLeft(18) + header.A1DeltaGPS.ToString("Z.000000000E+00", Constants.ci).PadLeft(16) + header.TotGPS.ToString().PadLeft(7) + header.WeekNumTGPS.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            fullEph.AppendLine(header.DeltaTLs.ToString().PadLeft(6) + "LEAP SECONDS".PadLeft(66));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph.ToString();
        }


        /// <summary>
        /// Формирование поля данных
        /// </summary>
        internal static string WriteDataGalileo(List<EphemerisGalileo> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.Append("E" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDrift.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDriftRate.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 1
                fullEph.Append(records[i].IODnav.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rs.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].delta_n.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].M_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].C_uc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].e.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].C_us.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].sqrt_A.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].T_oe.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_ic.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].OMEGA_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].C_is.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].i_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].omega.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].OMEGADOT.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 5
                fullEph.Append(records[i].IDOT.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].Source.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].weeknum.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19)); //+ 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 6
                fullEph.Append(records[i].SISA.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].Health_OSSOL.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].BGD_L1E5a.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].BGD_L1E5b.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 7
                fullEph.AppendLine(records[i].HOW.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23));
                //               + records[i].fit_interval.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                //fullEph.AppendLine(0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
            }

            fullEph.Replace("Z.", "0.");
            fullEph = fullEph.Replace("E-", "D-");
            fullEph = fullEph.Replace("E+", "D+");

            return fullEph.ToString();
        }
    }
}
