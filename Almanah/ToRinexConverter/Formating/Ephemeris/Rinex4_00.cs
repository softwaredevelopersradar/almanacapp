﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToRinexConverter
{
    internal static class Rinex4_00
    {
        #region Fake
        static float A0 = 0.00000001676f;
        static float A1 = -0.0000000149f;
        static float A2 = -0.0000000596f;
        static float A3 = 0.0000001788f;

        static float B0 = 129000;
        static float B1 = -180200;
        static float B2 = 65540;
        static float B3 = 131100;

        static double A_0 = -0.00000000465661287308d;
        static double A_1 = -0.000000000000008881784197d;
        static int timeUTC = 233472;
        static int weeknum = 1875;
        #endregion


        /// <summary>
        /// Формирование заголовока V4.00
        /// </summary>
        internal static string WriteHeader(DateTime timeFileCreation, int mergedFile, HeaderLeapSeconds leap)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 4.00f).PadRight(20) + "NAVIGATION DATA".PadRight(20) + "M".PadRight(20) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram3_03.PadRight(20) + Constants.nameOfAgency3_03.PadRight(20) + timeFileCreation.ToString("yyyyMMdd HHmmss").PadRight(16) + "UTC".PadRight(4) + "PGM / RUN BY / DATE "); //TODO: GMT
            fullEph.AppendLine(Constants.comment3_03.PadRight(60) + "COMMENT ");
            fullEph.AppendLine(Constants.comment3_03a.PadRight(60) + "COMMENT ");
            fullEph.AppendLine(Constants.link4_0.PadRight(60) + "DOI");
            fullEph.AppendLine(mergedFile.ToString().PadLeft(10) + "MERGED FILE".PadLeft(61));

            fullEph.AppendLine(leap.LeapSeconds.ToString().PadLeft(6) + leap.deltaT_LSF.ToString().PadLeft(6) + leap.weekNum_LSF.ToString().PadLeft(6) + leap.dayNum.ToString().PadLeft(6) + "LEAP SECONDS".PadLeft(48));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            //fullEph.Replace("Z.", "0.");
            //fullEph = fullEph.Replace("E-", "D-");
            //fullEph = fullEph.Replace("E+", "D+");

            return fullEph.ToString();
        }


        #region GPS
        
        /// <summary>
        /// Формирование поля данных GPS LNAV
        /// </summary>
        internal static string WriteDataGpsLnav(List<EphemerisGps> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.AppendLine("> EPH G" + records[i].PRN.ToString("00", Constants.ci) + " LNAV"); 
                fullEph.Append("G" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDrift.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDriftRate.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 1
                fullEph.Append(records[i].IODE.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rs.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].delta_n.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].M_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].C_uc.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].e.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].C_us.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].sqrt_A.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].T_oe.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_ic.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].OMEGA_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].C_is.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].i_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rc.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].omega.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].OMEGADOT.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 5
                fullEph.Append(records[i].IDOT.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].codeL2.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].weeknum.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].L2Pdata.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 6
                fullEph.Append(records[i].SVacc.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].SV_health.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].T_GD.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].IODC.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 7
                fullEph.AppendLine(records[i].HOW.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].fit_interval.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
            }

            fullEph = fullEph.Replace("E-", "e-");
            fullEph = fullEph.Replace("E+", "e+");

            return fullEph.ToString();
        }

        /// <summary>
        /// Формирование поля данных GPS CNAV
        /// </summary>
        internal static string WriteDataGpsCnav(List<EphemerisGps> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.AppendLine("> EPH G" + records[i].PRN.ToString("00", Constants.ci) + " CNAV"); 
                fullEph.Append("G" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDrift.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDriftRate.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 1
                fullEph.Append(records[i].A_DOT.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rs.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].delta_n0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].M_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].C_uc.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].e.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].C_us.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].sqrt_A.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].t_op.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_ic.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].OMEGA_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].C_is.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].i_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rc.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].omega.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].OMEGADOT.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 5
                fullEph.Append(records[i].IDOT.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].delta_n0_dot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].URAI_NED0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].URAI_NED1.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 6
                fullEph.Append(records[i].URAI_ED.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].SV_healthL1L2L5.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].T_GD.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].URAI_NED2.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 7
                fullEph.Append(records[i].ISC_L1CA.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].ISC_L2C.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].ISC_L5I5.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].ISC_L5Q5.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 8
                fullEph.Append(records[i].t_tm.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].Wn_op.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));

            }

            fullEph = fullEph.Replace("E-", "e-");
            fullEph = fullEph.Replace("E+", "e+");

            return fullEph.ToString();
        }

        /// <summary>
        /// Формирование поля данных GPS CNAV-2
        /// </summary>
        internal static string WriteDataGpsCnav2(List<EphemerisGps> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.AppendLine("> EPH G" + records[i].PRN.ToString("00", Constants.ci) + " CNV2"); 
                fullEph.Append("G" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDrift.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDriftRate.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 1
                fullEph.Append(records[i].A_DOT.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rs.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].delta_n0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].M_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].C_uc.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].e.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].C_us.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].sqrt_A.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].t_op.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_ic.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].OMEGA_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].C_is.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].i_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rc.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].omega.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].OMEGADOT.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 5
                fullEph.Append(records[i].IDOT.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].delta_n0_dot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].URAI_NED0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].URAI_NED1.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 6
                fullEph.Append(records[i].URAI_ED.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].SV_healthL1L2L5.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].T_GD.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].URAI_NED2.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 7
                fullEph.Append(records[i].ISC_L1CA.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].ISC_L2C.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].ISC_L5I5.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].ISC_L5Q5.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 8
                fullEph.Append(records[i].ISC_L1Cd.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].ISC_L1Cp.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 9
                fullEph.Append(records[i].t_tm.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].Wn_op.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));

            }

            fullEph = fullEph.Replace("E-", "e-");
            fullEph = fullEph.Replace("E+", "e+");

            return fullEph.ToString();
        }

        #endregion


        /// <summary>
        /// Формирование поля данных GLONASS FDMA
        /// </summary>
        internal static StringBuilder WriteDataGLONASS(List<EphemerisGlonass> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.AppendLine("> EPH R" + records[i].PRN.ToString("00", Constants.ci) + " FDMA");
                fullEph.Append("R" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVFreqBias.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].MessFrameTime.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));

                // ORBIT 1
                fullEph.Append(records[i].PosX.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].VelocityXDot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationX.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].Health.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].PosY.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].VelocityYDot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationY.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].FreqNum.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].PosZ.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].VelocityZDot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationZ.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].AgeInfo.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].StatusFlags.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].DeltaTL1L2.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].URAI.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].HealthFlags.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));

                fullEph = fullEph.Replace("E-", "e-");
                fullEph = fullEph.Replace("E+", "e+");
            }
            return fullEph;
        }


        #region BEIDOU
        

        /// <summary>
        /// Формирование поля данных BEIDOU
        /// </summary>
        internal static string WriteDataBEIDOUD1D2(List<EphemerisBeidou> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.AppendLine("> EPH C" + records[i].PRN.ToString("00", Constants.ci) + " D1"); //TODO:D1/D2
                fullEph.Append("C" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDrift.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDriftRate.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 1
                fullEph.Append(records[i].IODE.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rs.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].delta_n.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].M_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].C_uc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].e.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].C_us.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].sqrt_A.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].T_oe.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_ic.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].OMEGA_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].C_is.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].i_0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rc.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].omega.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].OMEGADOT.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 5
                fullEph.Append(records[i].IDOT.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].weeknum.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 6
                fullEph.Append(records[i].SVaccuracy.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].SV_health.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].T_GD1.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + records[i].T_GD2.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 7
                fullEph.AppendLine(records[i].HOW.ToString("Z.000000000000E+00", Constants.ci).PadLeft(23) + records[i].IODC.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                //fullEph.AppendLine(0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
            }

            fullEph = fullEph.Replace("E-", "e-");
            fullEph = fullEph.Replace("E+", "e+");

            return fullEph.ToString();
        }

        #endregion


        /// <summary>
        /// Формирование поля данных
        /// </summary>
        internal static string WriteDataGalileo(List<EphemerisGalileo> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.AppendLine("> EPH E" + records[i].PRN.ToString("00", Constants.ci) + " INAV"); //TODO: source=INAV/FNAV
                fullEph.Append("E" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDrift.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDriftRate.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 1
                fullEph.Append(records[i].IODnav.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rs.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].delta_n.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].M_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].C_uc.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].e.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].C_us.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].sqrt_A.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].T_oe.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_ic.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].OMEGA_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].C_is.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].i_0.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rc.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].omega.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].OMEGADOT.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 5
                fullEph.Append(records[i].IDOT.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].Source.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].weeknum.ToString("0.000000000000E+00", Constants.ci).PadLeft(19)); //+ 0.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 6
                fullEph.Append(records[i].SISA.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].Health_OSSOL.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].BGD_L1E5a.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].BGD_L1E5b.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 7
                fullEph.AppendLine(records[i].HOW.ToString("0.000000000000E+00", Constants.ci).PadLeft(23));
                //               + records[i].fit_interval.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
                //fullEph.AppendLine(0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19) + 0.ToString("Z.000000000000E+00", Constants.ci).PadLeft(19));
            }

            fullEph = fullEph.Replace("E-", "e-");
            fullEph = fullEph.Replace("E+", "e+");

            return fullEph.ToString();
        }
    }
}
