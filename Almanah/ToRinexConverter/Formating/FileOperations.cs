﻿using System;
using System.IO;

namespace ToRinexConverter
{
    internal static class FileOperations
    {
        internal static (string fullPath, string fullPathLoged) CreateFiles(string fileName, string path, bool isAccumulating, string logFolder, string constPart, string format)
        {
            if (!Directory.Exists(path))    //если директории не существует, сохранить в папку с запускаемым файлом
            {
                path = ".";
            }

            if (!Directory.Exists(path + logFolder))
            {
                Directory.CreateDirectory(path + logFolder);
            }

            var fullPath = path + "\\" + fileName;
            File.WriteAllText(fullPath, string.Empty);

            var fileNameLog = constPart + DateTime.Now.ToString("_yyyyMMdd_HHmm") + format;
            var fullPathFileLog = path + logFolder + "\\" + fileNameLog;

            if (isAccumulating)
            {
                File.WriteAllText(fullPathFileLog, string.Empty);
            }

            return (fullPath, fullPathFileLog);
        }


        internal static void WriteFiles(string data, (string fullPath, string fullPathLoged) paths, bool isAccumulating, EventHandler onReadyEvent)
        {
            using (StreamWriter outfile = new StreamWriter(paths.fullPath))
            {
                outfile.Write(data);
                onReadyEvent?.Invoke(null, null); //есть вопросики по отправителю
            }

            if (isAccumulating)
            {
                using (StreamWriter outfile = new StreamWriter(paths.fullPathLoged))
                {
                    outfile.Write(data);
                }
            }
        }
    }
}
