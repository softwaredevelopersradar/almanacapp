﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EphemerisForecastLib;

namespace ToRinexConverter
{
    using System.Runtime.CompilerServices;

    public class EphemerisConverter
    {
        private string _fileNameGps = "current.15n";
        private string _fileNameGlonass = "ephemerisGLONASS.rnx";

        #region EventHandlers
        public event EventHandler OnGpsEphemerisUpdated;
        public event EventHandler OnGlonassEphemerisUpdated;
        public event EventHandler OnBeidouEphemerisUpdated;
        public event EventHandler OnGalileoEphemerisUpdated;
        #endregion

        public event EventHandler<string> Message;
        //public event EventHandler<string> MessageSatsForLog;


        public EphemerisConverter(string fileNameGps, string fileNameGlonass)
        {
            _fileNameGlonass = fileNameGlonass;
            _fileNameGps = fileNameGps;
        }


        //public void WriteToRinexGlonass(string path, List<GeostarBinaryProtocol.EphemerisGlonass> EphemerisGeoS, List<GeostarBinaryProtocol.AlmanacGlonass> AlmanacGeoS, bool isAccumulating)
        //{
        //    var ephemRinexGlonass = new List<EphemerisGlonass>();

        //    var paths = FileOperations.CreateFiles(_fileNameGlonass, path, isAccumulating, Constants.logGlonass, Constants.constNameEphemeris, Constants.formatEphemerisGlonass);




        //    timeFileCreation = DateTime.UtcNow;


        //    string forLog = "\n" + "GLONASS before adding time: ";

        //    for (int i = 0; i < EphemerisGeoS.Count; i++)
        //    {
        //        if (EphemerisGeoS[i].Num > 0) //добавляем доступные спутники
        //        {
        //            ephemRinexGlonass.Add(new EphemerisGlonass(EphemerisGeoS[i], AlmanacGeoS[i]));
        //            forLog += "\n" + "No." + ephemRinexGlonass.Last().PRN.ToString() + ", Interval = " + ephemRinexGlonass.Last().dtTmp.ToString();
        //        }
        //    }
        //    MessageSatsForLog?.Invoke(this, forLog);
        //    ephemRinexGlonass = AddTimeOffsetGlonass();
        //    ////ephemRINEXs.OrderBy(u => u.Hour).ThenBy(u => u.PRN);
        //    //ephemRinexGlonass.OrderBy(u => u.PRN);

        //    if (ephemRinexGlonass.Count > 0)
        //    {
        //        var tempResult = WriteHeaderGLONASS().ToString() + WriteDataGLO().ToString();
        //        using (StreamWriter outfile = new StreamWriter(fullPath))
        //        {
        //            outfile.Write(tempResult);
        //            OnWithoutParam(OnGlonassEphemerisUpdated);
        //        }

        //        if (isAccumulating)
        //        {
        //            using (StreamWriter outfile = new StreamWriter(fullPathAdded))
        //            {
        //                outfile.Write(tempResult);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Convert ephemeris Binary Glonass format to RINEX 3.2 (for main program)
        /// </summary>
        public void WriteToRinexGlonass(string path, Dictionary<byte, List<GeostarBinaryProtocol.EphemerisGlonass>> EphemerisGeoS, List<GeostarBinaryProtocol.AlmanacGlonass> AlmanacGeoS, bool isAccumulating)
        {
            double c_utc = 0; //rinex2
            var paths = FileOperations.CreateFiles(_fileNameGlonass, path, isAccumulating, Constants.logGlonass, Constants.constNameEphemeris, Constants.formatEphemerisGlonass);

            var fileNameAccumEphemGlo = "accumulateEphem" + DateTime.Now.ToString("_yyyyMMdd_HHmm") + Constants.formatEphemerisGlonass;
            var fullPathAddedAccumulate = path + Constants.logGlonass + "\\" + fileNameAccumEphemGlo;
            if (isAccumulating)
            { 
                System.IO.File.WriteAllText(fullPathAddedAccumulate, string.Empty); 
            }


            Dictionary<byte, List<EphemerisGlonass>> tempDict = new Dictionary<byte, List<EphemerisGlonass>>();

            for (int i = 1; i <= EphemerisGeoS.Count; i++)
            {
                if (EphemerisGeoS[(byte)i].Count <= 0) 
                { continue; }

                c_utc = EphemerisGeoS[(byte)i].Last().C_utc; //подгон, лучше запрашивать параметры 0х
                var listRec = EphemerisGeoS[(byte)i].Select(u => new EphemerisGlonass(u, AlmanacGeoS.Count >= i ? AlmanacGeoS[i - 1] : new GeostarBinaryProtocol.AlmanacGlonass())).ToList();

                tempDict.Add((byte)i, listRec);
            }


            //var ephemerisRinex = AddTimeOffsetGlonassAccumulating(tempDict);
            var ephemerisRinex = AddForecastTimeOffsetGlonass(tempDict);

            var emptyEphemRinexGlonass = Accumulating(tempDict);

            if (emptyEphemRinexGlonass.Count > 0)
            {
                //var tempResultAccum = Rinex3_03.WriteHeaderGLONASS(DateTime.UtcNow).ToString() + Rinex3_03.WriteDataGLONASS(emptyEphemRinexGlonass);

                //Для Мартиновича rinex 2
                var tempResultAccum = Rinex2.WriteHeaderGLONASS(DateTime.UtcNow, -c_utc).ToString() + Rinex2.WriteDataGLONASS(emptyEphemRinexGlonass); 

                if (isAccumulating)
                {
                    using (StreamWriter outfile = new StreamWriter(fullPathAddedAccumulate))
                    {
                        outfile.Write(tempResultAccum);
                    }
                }
            }


            if (ephemerisRinex.Count() == 0)
            {
                return;
            }

            ////Для О.В. rinex 3.2
            //var tempResult = Rinex3_03.WriteHeaderGLONASS(DateTime.UtcNow).ToString() + Rinex3_03.WriteDataGLONASS(ephemerisRinex);
            //FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGlonassEphemerisUpdated);
            ////


            //Для Мартиновича rinex 2
            var result = ephemerisRinex.OrderBy(u => u.Hour).ThenBy(u => u.Minute).ThenBy(u => u.PRN);

            var sortedEphemRINEXs = new List<EphemerisGlonass>();
            foreach (var u in result)
                sortedEphemRINEXs.Add(u);
            var tempResult = Rinex2.WriteHeaderGLONASS(DateTime.UtcNow, -c_utc).ToString() + Rinex2.WriteDataGLONASS(sortedEphemRINEXs); //-c_utc или c_utc
            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGlonassEphemerisUpdated);
            //
        }


        /// <summary>
        /// Convert ephemeris TSIP format to RINEX 2.0 (for main program)
        /// </summary>
        public void WriteFile(string path, Dictionary<byte, List<GeostarBinaryProtocol.EphemerisGps>>EphemerisGeoS, bool isAccumulating)
        {
            //Message?.Invoke(this, "Get from main prog (ToRinexConverter)" + EphemerisTSIP.Count);
            var paths = FileOperations.CreateFiles(_fileNameGps, path, isAccumulating, Constants.logGps, Constants.constNameEphemeris, Constants.formatEphemerisGps);

            var fileNameAccumEphemGlo = "accumulateEphemGpsGeo" + DateTime.Now.ToString("_yyyyMMdd_HHmm") + Constants.formatEphemerisGps;
            var fullPathAddedAccumulate = path + Constants.logGps + "\\" + fileNameAccumEphemGlo;
            if (isAccumulating)
            {
                System.IO.File.WriteAllText(fullPathAddedAccumulate, string.Empty);
            }


            Dictionary<byte, List<EphemerisGps>> tempDict = new Dictionary<byte, List<EphemerisGps>>();

            for (int i = 1; i <= EphemerisGeoS.Count; i++)
            {
                if (EphemerisGeoS[(byte)i].Count <= 0)
                { continue; }


                var listRec = EphemerisGeoS[(byte)i].Select(u => new EphemerisGps(u)).ToList();

                tempDict.Add((byte)i, listRec);
            }

            var ephemerisRinex = AddTimeOffsetGpsAccumulating(tempDict);


            var emptyEphemRinexGps = Accumulating(tempDict);

            if (emptyEphemRinexGps.Count > 0)
            {
                var tempResultAccum = Rinex2.WriteHeaderGPS(DateTime.UtcNow).ToString() + Rinex2.WriteData(emptyEphemRinexGps);

                if (isAccumulating)
                {
                    using (StreamWriter outfile = new StreamWriter(fullPathAddedAccumulate))
                    {
                        outfile.Write(tempResultAccum);
                    }
                }
            }


            if (ephemerisRinex.Count() == 0)
            {
                return;
            }

            var tempResult = Rinex2.WriteHeaderGPS(DateTime.UtcNow).ToString() + Rinex2.WriteData(ephemerisRinex);
            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGpsEphemerisUpdated);
        }


        private List<EphemerisGps> AddTimeOffsetGpsAccumulating(Dictionary<byte, List<EphemerisGps>> accumulatedDict)
        {
            var newEphemRINEXs = new List<EphemerisGps>();
            var selected = accumulatedDict.Where(t => t.Value.Count != 0).ToList();

            double maxTime = 0;
            foreach (var record in selected)
            {
                var localMax = record.Value.Max(u => u.Hour);
                maxTime = maxTime < localMax ? localMax : maxTime;
            }

            foreach (var record in selected)
            {
                if (!record.Value.Exists(t => t.Hour == maxTime)) /*|| !record.Value.Exists(t => t.tb == maxTime - 2))*/ //добавлю, если надо 2 последние записи
                    continue;

                var minTime = Convert.ToInt32(record.Value.Min(u => u.Hour));
                newEphemRINEXs.AddRange(record.Value);

                for (int epoch = minTime - 2; epoch >= 0; epoch -= 2)
                {
                    var timeMinusEph = (EphemerisGps)record.Value.Where(t => t.Hour == minTime).FirstOrDefault().Clone();
                    timeMinusEph.Hour -= 2 * (minTime - epoch) / 2;
                    timeMinusEph.T_oe -= 7200 * (minTime - epoch) / 2;
                    timeMinusEph.HOW -= 7200 * (minTime - epoch) / 2;
                    newEphemRINEXs.Add(timeMinusEph);
                }


                for (int epoch = minTime + 2; epoch < maxTime; epoch += 2)
                {
                    if (record.Value.Exists(t => t.Hour == epoch))
                    { continue; }

                    EphemerisGps timeMinusEph = (EphemerisGps)record.Value.Where(t => t.Hour == minTime).FirstOrDefault().Clone();
                    timeMinusEph.Hour += 2 * (epoch - minTime) / 2;
                    timeMinusEph.T_oe += 7200 * (epoch - minTime) / 2;
                    timeMinusEph.HOW += 7200 * (epoch - minTime) / 2;
                    newEphemRINEXs.Add(timeMinusEph);
                }

                //for (int epoch = (int)maxTime + 2; epoch < 24; epoch += 2)
                //{

                //    EphemerisGps timeMinusEph = (EphemerisGps)record.Value.Where(t => t.Hour == maxTime).FirstOrDefault().Clone();
                //    timeMinusEph.Hour += 2 * (epoch - maxTime) / 2;
                //    timeMinusEph.T_oe += 7200 * (epoch - maxTime) / 2;
                //    timeMinusEph.HOW += 7200 * (epoch - maxTime) / 2;
                //    newEphemRINEXs.Add(timeMinusEph);
                //}


                ////if (realHour == maxTime)
                ////{
                //на 2 часа вперед
                EphemerisGps timePlusEph = (EphemerisGps)record.Value.Where(t => t.Hour == maxTime).FirstOrDefault().Clone();
                timePlusEph.Hour += 2;
                timePlusEph.T_oe += 7200;
                timePlusEph.HOW += 7200;
                newEphemRINEXs.Add(timePlusEph);
                ////}
            }

            var result = newEphemRINEXs.OrderBy(u => u.Hour).ThenBy(u => u.PRN);

            var sortedEphemRINEXs = new List<EphemerisGps>();
            foreach (EphemerisGps u in result)
                sortedEphemRINEXs.Add(u);
            return sortedEphemRINEXs;
        }


        #region Glonass for GeoS
        private List<EphemerisGlonass> AddTimeOffsetGlonass(List<EphemerisGlonass> ephemRinexGlonass)
        {
            var newEphemRINEXs = new List<EphemerisGlonass>();
            var sortedEphemRINEXs = new List<EphemerisGlonass>();

            var maxTime = ephemRinexGlonass.Max(u => u.tb);
            var newList = ephemRinexGlonass.Where(u => u.tb == maxTime).ToList();
            foreach (var record in newList)
            {
                for (int epoch = 1; epoch < record.tb; epoch += 2)
                {
                    EphemerisGlonass timeMinusEph = (EphemerisGlonass)record.Clone();
                    //var tempTime = timeMinusEph.dtTmp;
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddHours(-timeMinusEph.dtTmp.Hour).AddMinutes(-timeMinusEph.dtTmp.Minute).AddSeconds(-timeMinusEph.dtTmp.Second);
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddMinutes(epoch * 15);
                    timeMinusEph.Hour = timeMinusEph.dtTmp.Hour;
                    timeMinusEph.Minute = timeMinusEph.dtTmp.Minute;
                    newEphemRINEXs.Add(timeMinusEph);
                }

                for (int epoch = 95; epoch > record.tb; epoch -= 2)
                {
                    EphemerisGlonass timeMinusEph = (EphemerisGlonass)record.Clone();
                    //var tempTime = timeMinusEph.dtTmp;
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddHours(-timeMinusEph.dtTmp.Hour).AddMinutes(-timeMinusEph.dtTmp.Minute).AddSeconds(-timeMinusEph.dtTmp.Second);
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddMinutes(epoch * 15);
                    timeMinusEph.Hour = timeMinusEph.dtTmp.Hour;
                    timeMinusEph.Minute = timeMinusEph.dtTmp.Minute;
                    newEphemRINEXs.Add(timeMinusEph);
                }

            }

            newList.AddRange(newEphemRINEXs);
            var result = newList.OrderBy(u => u.PRN).ThenBy(u => u.Hour).ThenBy(u => u.Minute);
            foreach (EphemerisGlonass u in result)
                sortedEphemRINEXs.Add(u);
            return sortedEphemRINEXs;
        }


        private List<EphemerisGlonass> AddTimeOffsetGlonassAccumulating(Dictionary<byte, List<EphemerisGlonass>> accumulatedDict)
        {
            var newEphemRINEXs = new List<EphemerisGlonass>();
            var selected = accumulatedDict.Where(t => t.Value.Count != 0).ToList();

            byte maxTime = 0;
            foreach (var record in selected)
            {
                maxTime = maxTime < record.Value.Max(u => u.tb) ? record.Value.Max(u => u.tb) : maxTime;
            }

            foreach (var record in selected)
            {
                if (!record.Value.Exists(t => t.tb == maxTime) || !record.Value.Exists(t => t.tb == maxTime - 2))
                    continue;

                var minTime = record.Value.Min(u => u.tb);
                newEphemRINEXs.AddRange(record.Value);

                for (int epoch = 1; epoch < minTime; epoch += 2)
                {
                    EphemerisGlonass timeMinusEph = (EphemerisGlonass)record.Value.Where(t => t.tb == minTime).FirstOrDefault().Clone();
                    //var tempTime = timeMinusEph.dtTmp;
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddHours(-timeMinusEph.dtTmp.Hour).AddMinutes(-timeMinusEph.dtTmp.Minute).AddSeconds(-timeMinusEph.dtTmp.Second);
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddMinutes(epoch * 15);
                    timeMinusEph.Hour = timeMinusEph.dtTmp.Hour;
                    timeMinusEph.Minute = timeMinusEph.dtTmp.Minute;
                    newEphemRINEXs.Add(timeMinusEph);
                }

                for (int epoch = minTime + 2; epoch < maxTime; epoch += 2)
                {
                    if (record.Value.Exists(t => t.tb == epoch))
                    { continue; }

                    EphemerisGlonass timeMinusEph = (EphemerisGlonass)record.Value.Where(t => t.tb == minTime).FirstOrDefault().Clone();
                    //var tempTime = timeMinusEph.dtTmp;
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddHours(-timeMinusEph.dtTmp.Hour).AddMinutes(-timeMinusEph.dtTmp.Minute).AddSeconds(-timeMinusEph.dtTmp.Second);
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddMinutes(epoch * 15);
                    timeMinusEph.Hour = timeMinusEph.dtTmp.Hour;
                    timeMinusEph.Minute = timeMinusEph.dtTmp.Minute;
                    newEphemRINEXs.Add(timeMinusEph);
                }

                for (int epoch = 95; epoch > maxTime; epoch -= 2)
                {
                    EphemerisGlonass timeMinusEph = (EphemerisGlonass)record.Value.Where(t => t.tb == maxTime).FirstOrDefault().Clone();
                    //var tempTime = timeMinusEph.dtTmp;
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddHours(-timeMinusEph.dtTmp.Hour).AddMinutes(-timeMinusEph.dtTmp.Minute).AddSeconds(-timeMinusEph.dtTmp.Second);
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddMinutes(epoch * 15);
                    timeMinusEph.Hour = timeMinusEph.dtTmp.Hour;
                    timeMinusEph.Minute = timeMinusEph.dtTmp.Minute;
                    newEphemRINEXs.Add(timeMinusEph);
                }
            }

            var result = newEphemRINEXs.OrderBy(u => u.PRN).ThenBy(u => u.Hour).ThenBy(u => u.Minute);

            var sortedEphemRINEXs = new List<EphemerisGlonass>();
            foreach (EphemerisGlonass u in result)
                sortedEphemRINEXs.Add(u);
            return sortedEphemRINEXs;
        }


        private List<EphemerisGlonass> AddForecastTimeOffsetGlonass(Dictionary<byte, List<EphemerisGlonass>> accumulatedDict)
        {
            var newEphemRINEXs = new List<EphemerisGlonass>();
            var selected = accumulatedDict.Where(t => t.Value.Count != 0).ToList();

            byte maxTime = 0;
            foreach (var record in selected)
            {
                maxTime = maxTime < record.Value.Max(u => u.tb) ? record.Value.Max(u => u.tb) : maxTime;
            }

            foreach (var record in selected)
            {
                if (!record.Value.Exists(t => t.tb == maxTime))
                    continue;

                var minTime = record.Value.Min(u => u.tb);
                newEphemRINEXs.AddRange(record.Value);

                for (int epoch = 1; epoch < minTime; epoch += 2)
                {
                    EphemerisGlonass timeMinusEph = (EphemerisGlonass)record.Value.Where(t => t.tb == minTime).FirstOrDefault().Clone();
                    //var tempTime = timeMinusEph.dtTmp;
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddHours(-timeMinusEph.dtTmp.Hour).AddMinutes(-timeMinusEph.dtTmp.Minute).AddSeconds(-timeMinusEph.dtTmp.Second);
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddMinutes(epoch * 15);
                    timeMinusEph.Hour = timeMinusEph.dtTmp.Hour;
                    timeMinusEph.Minute = timeMinusEph.dtTmp.Minute;

                    var forecast = EphemerisForecast.ForecastGlonass(ConvertToForecast(timeMinusEph, epoch), (epoch - minTime) * 15 * 60);
                    timeMinusEph = ConvertFromForecast(timeMinusEph, forecast);
                    newEphemRINEXs.Add(timeMinusEph);
                }

                for (int epoch = maxTime - 2; epoch > minTime; epoch -= 2)
                {
                    if (record.Value.Exists(t => t.tb == epoch))
                    { continue; }

                    EphemerisGlonass timeMinusEph = (EphemerisGlonass)record.Value.Where(t => t.tb == maxTime).FirstOrDefault().Clone();
                    //var tempTime = timeMinusEph.dtTmp;
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddHours(-timeMinusEph.dtTmp.Hour).AddMinutes(-timeMinusEph.dtTmp.Minute).AddSeconds(-timeMinusEph.dtTmp.Second);
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddMinutes(epoch * 15);
                    timeMinusEph.Hour = timeMinusEph.dtTmp.Hour;
                    timeMinusEph.Minute = timeMinusEph.dtTmp.Minute;

                    var forecast = EphemerisForecast.ForecastGlonass(ConvertToForecast(timeMinusEph, epoch), (epoch - maxTime) * 15 * 60);
                    timeMinusEph = ConvertFromForecast(timeMinusEph, forecast);

                    newEphemRINEXs.Add(timeMinusEph);
                }

                for (int epoch = 95; epoch > maxTime; epoch -= 2)
                {
                    EphemerisGlonass timeMinusEph = (EphemerisGlonass)record.Value.Where(t => t.tb == maxTime).FirstOrDefault().Clone();
                    //var tempTime = timeMinusEph.dtTmp;
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddHours(-timeMinusEph.dtTmp.Hour).AddMinutes(-timeMinusEph.dtTmp.Minute).AddSeconds(-timeMinusEph.dtTmp.Second);
                    timeMinusEph.dtTmp = timeMinusEph.dtTmp.AddMinutes(epoch * 15);
                    timeMinusEph.Hour = timeMinusEph.dtTmp.Hour;
                    timeMinusEph.Minute = timeMinusEph.dtTmp.Minute;

                    var forecast = EphemerisForecast.ForecastGlonass(ConvertToForecast(timeMinusEph, epoch), (epoch - maxTime) * 15 * 60);
                    timeMinusEph = ConvertFromForecast(timeMinusEph, forecast);

                    newEphemRINEXs.Add(timeMinusEph);
                }
            }

            var result = newEphemRINEXs.OrderBy(u => u.PRN).ThenBy(u => u.Hour).ThenBy(u => u.Minute);

            var sortedEphemRINEXs = new List<EphemerisGlonass>();
            foreach (EphemerisGlonass u in result)
                sortedEphemRINEXs.Add(u);
            return sortedEphemRINEXs;
        }

        private EphemerisGlonass ConvertFromForecast(EphemerisGlonass baseInput, EphemerisGlonassData forecastInput)
        {
            baseInput.PosX = forecastInput.PosX / 1000d;
            baseInput.PosY = forecastInput.PosY / 1000d;
            baseInput.PosZ = forecastInput.PosZ / 1000d;
            baseInput.VelocityXDot = forecastInput.VelocityXDot / 1000d;
            baseInput.VelocityYDot = forecastInput.VelocityYDot / 1000d;
            baseInput.VelocityZDot = forecastInput.VelocityZDot / 1000d;
            baseInput.AccelerationX = forecastInput.AccelerationX / 1000d;
            baseInput.AccelerationY = forecastInput.AccelerationY / 1000d;
            baseInput.AccelerationZ = forecastInput.AccelerationZ / 1000d;
            return baseInput;
        }


        private EphemerisGlonassData ConvertToForecast(EphemerisGlonass input, int epoch)
        {
            return new EphemerisGlonassData(input.PosX * 1000, input.PosY * 1000, input.PosZ * 1000, input.VelocityXDot * 1000, input.VelocityYDot * 1000, input.VelocityZDot * 1000, input.AccelerationX * 1000, input.AccelerationY * 1000, input.AccelerationZ * 1000, epoch * 15 * 60);
        }


        private List<EphemerisGlonass> Accumulating(Dictionary<byte, List<EphemerisGlonass>> accumulatedDict)
        {
            var newEphemRINEXs = new List<EphemerisGlonass>();
            var selected = accumulatedDict.Where(t => t.Value.Count != 0).ToList();


            foreach (var record in selected)
            {
                var minTime = record.Value.Min(u => u.tb);
                newEphemRINEXs.AddRange(record.Value);
            }

            var result = newEphemRINEXs.OrderBy(u => u.PRN).ThenBy(u => u.Hour).ThenBy(u => u.Minute);

            var sortedEphemRINEXs = new List<EphemerisGlonass>();
            foreach (EphemerisGlonass u in result)
                sortedEphemRINEXs.Add(u);
            return sortedEphemRINEXs;
        }

        private List<EphemerisGps> Accumulating(Dictionary<byte, List<EphemerisGps>> accumulatedDict)
        {
            var newEphemRINEXs = new List<EphemerisGps>();
            var selected = accumulatedDict.Where(t => t.Value.Count != 0).ToList();


            foreach (var record in selected)
            {
                //var minTime = record.Value.Min(u => u.Hour);
                newEphemRINEXs.AddRange(record.Value);
            }

            var result = newEphemRINEXs.OrderBy(u => u.PRN).ThenBy(u => u.Hour);

            var sortedEphemRINEXs = new List<EphemerisGps>();
            foreach (EphemerisGps u in result)
                sortedEphemRINEXs.Add(u);
            return sortedEphemRINEXs;
        }
        #endregion

        #region Rinex2.01 for Geos

        
        #endregion

        /// <summary>
        /// Convert ephemeris TSIP format to RINEX 3.2 
        /// </summary>
        public void WriteToFile(string path, List<TSIP.Ephemeris> EphemerisTSIP, bool isAccumulating)
        {
            var paths = FileOperations.CreateFiles(_fileNameGps, path, isAccumulating, Constants.logGps, Constants.constNameEphemeris, Constants.formatEphemerisGps);

            var ephemRINEXs = EphemerisTSIP.Where(t => t.t_ephem >= 0).Select(u => new EphemerisGps(u)).ToList();

            if (ephemRINEXs.Count == 0)
            {
                return;
            }

            ephemRINEXs.OrderBy(u => u.PRN);

            var tempResult = Rinex3_02.WriteHeaderGPS(DateTime.Now) + Rinex3_02.WriteData(ephemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGpsEphemerisUpdated);
        }




        /// <summary>
        /// Convert ephemeris TSIP format to RINEX 2.0 (for main program)
        /// </summary>
        public void WriteFile(string path, List<TSIP.Ephemeris> EphemerisTSIP, bool isAccumulating)
        {
            //Message?.Invoke(this, "Get from main prog (ToRinexConverter)" + EphemerisTSIP.Count);
            var paths = FileOperations.CreateFiles(_fileNameGps, path, isAccumulating, Constants.logGps, Constants.constNameEphemeris, Constants.formatEphemerisGps);

            var ephemRINEXs = EphemerisTSIP.Where(t => t.t_ephem >= 0).Select(u => new EphemerisGps(u)).ToList();
            //var ephemRINEXs = EphemerisTSIP.Select(u => new EphemerisGps(u)).ToList();
            //Message?.Invoke(this, "Filtered (ToRinexConverter): " + ephemRINEXs.Count);
            if (ephemRINEXs.Count == 0)
            { 
                return; 
            }

            ephemRINEXs = RoundTime(ephemRINEXs);
            ephemRINEXs = AddTimeOffset(ephemRINEXs);
            Message?.Invoke(this, "After rounding and adding time (ToRinexConverter):" + ephemRINEXs.Count);
            var timeFileCreation = DateTime.Now;
            var tempResult = Rinex2.WriteHeaderGPS(timeFileCreation) + Rinex2.WriteData(ephemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGpsEphemerisUpdated);
        }


        /// <summary>
        /// Convert ephemeris TSIP format to RINEX 2.0 (for main program)
        /// </summary>
        public void WriteFile(string path, List<TSIP.Ephemeris> EphemerisTSIP, bool isAccumulating, HeaderEphemerisGps header)
        {
            var paths = FileOperations.CreateFiles(_fileNameGps, path, isAccumulating, Constants.logGps, Constants.constNameEphemeris, Constants.formatEphemerisGps);

            var ephemRINEXs = EphemerisTSIP.Where(t => t.t_ephem > 0).Select(u => new EphemerisGps(u)).ToList();

            if (ephemRINEXs.Count == 0)
            {
                //return;
            }
            else
            {
                ephemRINEXs = RoundTime(ephemRINEXs);
                ephemRINEXs = AddTimeOffset(ephemRINEXs);
                Message?.Invoke(this, "After rounding and adding time (ToRinexConverter):" + ephemRINEXs.Count);
            }

            
            var timeFileCreation = DateTime.Now;
            var tempResult = Rinex2.WriteHeaderGPS(timeFileCreation, header) + Rinex2.WriteData(ephemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGpsEphemerisUpdated);
        }


        /// <summary>
        /// Convert ephemeris TSIP format to RINEX 2.0 (for client)
        /// </summary>
        public void WriteFile(string path, List<ClientServerLib.Ephemeries> EphemerisTSIP, bool isAccumulating)
        {
            var paths = FileOperations.CreateFiles(_fileNameGps, path, isAccumulating, Constants.logGps, Constants.constNameEphemeris, Constants.formatEphemerisGps);

            var ephemRINEXs = EphemerisTSIP.Where(t => t.t_ephem >= 0).Select(u => new EphemerisGps(u)).ToList();

            if (ephemRINEXs.Count == 0)
            {
                return;
            }

            ephemRINEXs = RoundTime(ephemRINEXs);
            ephemRINEXs = AddTimeOffset(ephemRINEXs);

            var tempResult = Rinex2.WriteHeaderGPS(DateTime.Now) + Rinex2.WriteData(ephemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGpsEphemerisUpdated);
        }


        #region Rinex2.0 for Trimble
        DateTime dtGPSEp = new DateTime(1980, 1, 6);

        private List<EphemerisGps> RoundTime(List<EphemerisGps> ephemRINEXs)
        {
            string forLog = "After round time: " ;
            var newEphemRINEXs = new List<EphemerisGps>();
            for (int i = 0; i < ephemRINEXs.Count; i++)
            {
                EphemerisGps timeRoundEph = (EphemerisGps)ephemRINEXs[i].Clone();
                if (timeRoundEph.Minute != 0 || timeRoundEph.Second != 0)
                {
                    timeRoundEph.T_oe += 3600 - (timeRoundEph.Minute * 60 + timeRoundEph.Second);
                    timeRoundEph.CountDataTime();

                    if ((timeRoundEph.Hour % 2) != 0)
                    {
                        timeRoundEph.T_oe -= 3600;
                        timeRoundEph.CountDataTime();
                    }
                }

                if (timeRoundEph.Hour == 24)
                {
                    timeRoundEph.CountDataTime(); 
                }
                newEphemRINEXs.Add(timeRoundEph);
                forLog += "\n No." + timeRoundEph.PRN.ToString() + ", t_oe = " + timeRoundEph.T_oe.ToString()  + ", Time = " + dtGPSEp.AddDays(7 * (timeRoundEph.weeknum + 1024 * 2)).AddSeconds(timeRoundEph.T_oe).ToString();
            }
            Message?.Invoke(this, forLog);
            return newEphemRINEXs;
        }

        private List<EphemerisGps> AddTimeOffset(List<EphemerisGps> ephemRINEXs)
        {
            var newEphemRINEXs = new List<EphemerisGps>();
            var sortedEphemRINEXs = new List<EphemerisGps>();
            //var maxTime = ephemRINEXs.Max(u => u.Hour);
            var maxTime = ephemRINEXs.Max(u => u.Hour);
            for (int i = 0; i < ephemRINEXs.Count; i++)
            {
                //double countTime = ephemRINEXs[i].Hour;
                int realHour = Convert.ToInt32(ephemRINEXs[i].Hour);

                //while (countTime >= 2)
                for (int epoch = realHour - 2; epoch >= 0; epoch -= 2)
                {
                    EphemerisGps timeMinusEph = (EphemerisGps)ephemRINEXs[i].Clone();
                    timeMinusEph.Hour -= 2 * (realHour - epoch) / 2;
                    timeMinusEph.T_oe -= 7200 * (realHour - epoch) / 2;
                    timeMinusEph.HOW -= 7200 * (realHour - epoch) / 2;
                    newEphemRINEXs.Add(timeMinusEph);
                }

                if (realHour == maxTime)
                {
                    EphemerisGps timePlusEph = (EphemerisGps)ephemRINEXs[i].Clone();
                    timePlusEph.Hour += 2;
                    timePlusEph.T_oe += 7200;
                    newEphemRINEXs.Add(timePlusEph);
                }
            }
            ephemRINEXs.AddRange(newEphemRINEXs);
            var result = ephemRINEXs.OrderBy(u => u.Hour).ThenBy(u => u.PRN);
            foreach (EphemerisGps u in result)
                sortedEphemRINEXs.Add(u);
            return sortedEphemRINEXs;
        }
        #endregion


        #region Rinex 3.02 MosaicX5 

        /// <summary>
        /// Convert Septentrio SBF GPS ephemeris to RINEX 3.2
        /// </summary>
        public void WriteToFile(string path, List<SeptentrioSBF.EphemerisGps> ephemeris, bool isAccumulating)
        {
            var paths = FileOperations.CreateFiles("gps.21n", path, isAccumulating, "\\Log_MosaicX5", "gps", ".21n");

            var ephemRINEXs = ephemeris.Where(t => t.t_oe >= 0).Select(u => new EphemerisGps(u)).ToList();

            if (ephemRINEXs.Count == 0)
            {
                return;
            }

            ephemRINEXs.OrderBy(u => u.PRN);

            var tempResult = Rinex3_02.WriteHeaderGPS(DateTime.UtcNow) + Rinex3_02.WriteData(ephemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGpsEphemerisUpdated);
        }

        public void WriteToFile(string path, List<SeptentrioSBF.EphemerisGps> ephemeris, bool isAccumulating, HeaderEphemerisGps header)
        {
            var paths = FileOperations.CreateFiles("gps.21n", path, isAccumulating, "\\Log_MosaicX5", "gps", ".21n");

            var ephemRINEXs = ephemeris.Where(t => t.t_oe >= 0).Select(u => new EphemerisGps(u)).ToList();

            if (ephemRINEXs.Count == 0)
            {
                return;
            }

            var timeFileCreation = DateTime.UtcNow;
            var tempResult = Rinex3_02.WriteHeaderGPS(timeFileCreation, header) + Rinex3_02.WriteData(ephemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGpsEphemerisUpdated);
        }


        /// <summary>
        /// Convert Septentrio SBF GLONASS ephemeris to RINEX 3.2
        /// </summary>
        public void WriteToFile(string path, List<SeptentrioSBF.EphemerisGlonass> ephemeris, bool isAccumulating)
        {
            var paths = FileOperations.CreateFiles("glonass.21g", path, isAccumulating, "\\Log_MosaicX5", "glonass", ".21g");

            var fileNameAccumEphemGlo = "accumulateEphem" + DateTime.Now.ToString("_yyyyMMdd_HHmm") + ".21g";
            var fullPathAddedAccumulate = path + Constants.logGlonass + "\\" + fileNameAccumEphemGlo;
            if (isAccumulating)
            {
                System.IO.File.WriteAllText(fullPathAddedAccumulate, string.Empty);
            }


            var ephemRINEXs = ephemeris.Where(t => t.t_oe >= 0).Select(u => new EphemerisGlonass(u)).ToList();

            if (ephemRINEXs.Count == 0)
            {
                return;
            }

            ephemRINEXs.OrderBy(u => u.PRN);

            var tempResult = Rinex3_02.WriteHeaderGLONASS(DateTime.UtcNow) + Rinex3_02.WriteDataGLONASS(ephemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGlonassEphemerisUpdated);
        }

        /// <summary>
        /// Convert Septentrio SBF GLONASS ephemeris to RINEX 3.2
        /// </summary>
        public void WriteToFile(string path, Dictionary<byte, List<SeptentrioSBF.EphemerisGlonass>> ephemeris, bool isAccumulating, HeaderEphemerisGlonass header)
        {
            var paths = FileOperations.CreateFiles("glonass.21g", path, isAccumulating, "\\Log_MosaicX5", "glonass", ".21g");

            //var fileNameAccumEphemGlo = "accumulateEphem" + DateTime.Now.ToString("_yyyyMMdd_HHmm") + ".21g";
            //var fullPathAddedAccumulate = path + Constants.logGlonass + "\\" + fileNameAccumEphemGlo;
            //if (isAccumulating)
            //{
            //    System.IO.File.WriteAllText(fullPathAddedAccumulate, string.Empty);
            //}
            Dictionary<byte, List<EphemerisGlonass>> tempDict = new Dictionary<byte, List<EphemerisGlonass>>();

            foreach (var sat  in ephemeris)
            {
                if (ephemeris[sat.Key].Count <= 0)
                { continue; }

                var listRec = ephemeris[sat.Key].Select(u => new EphemerisGlonass(u)).ToList();

                tempDict.Add(sat.Key, listRec);
            }

            //(int i = 1; i <= ephemeris.Count; i++)
            //{
            //    if (ephemeris[(byte)i].Count <= 0)
            //    { continue; }

            //    var listRec = ephemeris[(byte)i].Select(u => new EphemerisGlonass(u)).ToList();

            //    tempDict.Add((byte)i, listRec);
            //}
            //for (int i = 1; i <= ephemeris.Count; i++)
            //{
            //    if (ephemeris[(byte)i].Count <= 0)
            //    { continue; }

            //    var listRec = ephemeris[(byte)i].Select(u => new EphemerisGlonass(u)).ToList();

            //    tempDict.Add((byte)i, listRec);
            //}

            var ephemerisRinex = AddForecastTimeOffsetGlonass(tempDict);

            if (ephemerisRinex.Count() == 0)
            {
                return;
            }

            //Для Мартиновича rinex 2
            var result = ephemerisRinex.OrderBy(u => u.Hour).ThenBy(u => u.Minute).ThenBy(u => u.PRN);

            var sortedEphemRINEXs = new List<EphemerisGlonass>();
            foreach (var u in result)
                sortedEphemRINEXs.Add(u);

            //var ephemRINEXs = ephemeris.Where(t => t.t_oe >= 0).Select(u => new EphemerisGlonass(u)).ToList();

            //if (ephemRINEXs.Count == 0)
            //{
            //    return;
            //}

            //ephemRINEXs.OrderBy(u => u.PRN);

            var tempResult = Rinex3_02.WriteHeaderGLONASS(DateTime.UtcNow, header) + Rinex3_02.WriteDataGLONASS(sortedEphemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGlonassEphemerisUpdated);
        }


        /// <summary>
        /// Convert Septentrio SBF BEIDOU ephemeris to RINEX 3.2
        /// </summary>
        public void WriteToFile(string path, List<SeptentrioSBF.EphemerisBeidou> ephemeris, bool isAccumulating)
        {
            var paths = FileOperations.CreateFiles("beidou.21f", path, isAccumulating, "\\Log_MosaicX5", "beidou", ".21f");

            var ephemRINEXs = ephemeris.Where(t => t.t_oe >= 0).Select(u => new EphemerisBeidou(u)).ToList();

            if (ephemRINEXs.Count == 0)
            {
                return;
            }

            ephemRINEXs.OrderBy(u => u.PRN);

            var tempResult = Rinex3_02.WriteHeaderBEIDOU(DateTime.UtcNow) + Rinex3_02.WriteDataBEIDOU(ephemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnBeidouEphemerisUpdated);
        }

        /// <summary>
        /// Convert Septentrio SBF BEIDOU ephemeris to RINEX 3.2
        /// </summary>
        public void WriteToFile(string path, List<SeptentrioSBF.EphemerisBeidou> ephemeris, bool isAccumulating, HeaderEphemerisGps header)
        {
            var paths = FileOperations.CreateFiles("beidou.21f", path, isAccumulating, "\\Log_MosaicX5", "beidou", ".21f");

            var ephemRINEXs = ephemeris.Where(t => t.t_oe >= 0).Select(u => new EphemerisBeidou(u)).ToList();

            if (ephemRINEXs.Count == 0)
            {
                return;
            }

            ephemRINEXs.OrderBy(u => u.PRN);

            var tempResult = Rinex3_02.WriteHeaderBEIDOU(DateTime.UtcNow, header) + Rinex3_02.WriteDataBEIDOU(ephemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnBeidouEphemerisUpdated);
        }


        /// <summary>
        /// Convert Septentrio SBF GALILEO ephemeris to RINEX 3.2
        /// </summary>
        public void WriteToFile(string path, List<SeptentrioSBF.EphemerisGalileo> ephemeris, bool isAccumulating, HeaderEphemerisGalileo header)
        {
            var paths = FileOperations.CreateFiles("galileo.21l", path, isAccumulating, "\\Log_MosaicX5", "galileo", ".21l");

            var ephemRINEXs = ephemeris.Where(t => t.t_oe >= 0).Select(u => new EphemerisGalileo(u)).ToList();

            if (ephemRINEXs.Count == 0)
            {
                return;
            }

            ephemRINEXs.OrderBy(u => u.PRN);

            var tempResult = Rinex3_02.WriteHeaderGalileo(DateTime.UtcNow, header) + Rinex3_02.WriteDataGalileo(ephemRINEXs).ToString();

            FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnBeidouEphemerisUpdated);
        }
        #endregion
    }
}
