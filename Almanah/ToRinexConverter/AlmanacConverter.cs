﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ToRinexConverter
{
    public class AlmanacConverter
    {        
        private string _fileNameGps = "current.AL3";
        private string _fileNameGlonass = "almanacGLONASS.Agl";

        #region EventHandlers
        public event EventHandler OnGpsAlmanacUpdated;
        public event EventHandler OnGlonassAlmanacUpdated;
        #endregion


        public AlmanacConverter()
        {
        }

        public AlmanacConverter(string fileNameGps, string fileNameGlonass)
        {
            _fileNameGlonass = fileNameGlonass;
            _fileNameGps = fileNameGps;
        }


        /// <summary>
        /// Convert almanac TSIP format to SEM format 
        /// </summary>
        public void WriteFile(string path, List<TSIP.Almanac> almanacTSIP, bool isAccumulating)
        {
            try
            {
                var paths = FileOperations.CreateFiles(_fileNameGps, path, isAccumulating, Constants.logGps, Constants.constNameAlmanac, Constants.formatAlmanacGps);

                var tempForSorting = almanacTSIP.Where(t => t.t_zc >= 0).Select(u => new AlmanacGps(u));
                var almanacSEMs = tempForSorting.OrderBy(u => u.PRN).ToList();

                if (almanacSEMs.Count() == 0)
                { 
                    return; 
                }

                var tempResult = Sem.WriteData(almanacSEMs, almanacTSIP[almanacSEMs[0].PRN - 1].wn_oa, Convert.ToUInt32(almanacTSIP[almanacSEMs[0].PRN - 1].t_oa));

                FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGpsAlmanacUpdated);
            }
            catch(Exception ex)
            { }
        }


        /// <summary>
        /// Convert almanac ClientServerLib format to SEM format
        /// </summary>
        public void WriteFile(string path, List<ClientServerLib.Almanac> almanacTSIP, bool isAccumulating)
        {
            try
            {
                var paths = FileOperations.CreateFiles(_fileNameGps, path, isAccumulating, Constants.logGps, Constants.constNameAlmanac, Constants.formatAlmanacGps);

                var tempForSorting = almanacTSIP.Where(t => t.t_zc >= 0).Select(u => new AlmanacGps(u));
                var almanacSEMs = tempForSorting.OrderBy(u => u.PRN).ToList();

                if (almanacSEMs.Count() == 0)
                {
                    return;
                }

                var tempResult = Sem.WriteData(almanacSEMs, almanacTSIP[almanacSEMs[0].PRN - 1].wn_oa, Convert.ToUInt32(almanacTSIP[almanacSEMs[0].PRN - 1].t_oa));

                FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGpsAlmanacUpdated);
            }
            catch (Exception ex)
            { }
        }


        /// <summary>
        /// Convert almanac GeostarBinaryProtocol format to SEM format 
        /// </summary>
        public void WriteFile(string path, List<GeostarBinaryProtocol.AlmanacGlonass> almanacGeoS, bool isAccumulating)
        {
            try
            {
                var paths = FileOperations.CreateFiles(_fileNameGlonass, path, isAccumulating, Constants.logGlonass, Constants.constNameAlmanac, Constants.formatAlmanacGlonass);

                var tempForSorting = almanacGeoS.Select(u => new AlmanacGlonass(u));
                var almanacAGLs = tempForSorting.OrderBy(u => u.Num).ToList();

                if (almanacAGLs.Count() == 0)
                {
                    return;
                }
            
                var tempResult = Agl.WriteData(almanacAGLs);

                FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGlonassAlmanacUpdated);
            }
            catch (Exception ex)
            { }
        }


        /// <summary>
        /// Convert almanac GeostarBinaryProtocol format to SEM format 
        /// </summary>
        public void WriteFile(string path, List<GeostarBinaryProtocol.AlmanacGps> almanacGeoS, bool isAccumulating)
        {
            try
            {
                var paths = FileOperations.CreateFiles(_fileNameGps, path, isAccumulating, Constants.logGps, Constants.constNameAlmanac, Constants.formatAlmanacGps);

                var tempForSorting = almanacGeoS.Where(t => t.PRN != 0).Select(u => new AlmanacGps(u));
                var almanacSEMs = tempForSorting.OrderBy(u => u.PRN).ToList();

                if (almanacSEMs.Count() == 0)
                {
                    return;
                }

                var tempResult = Sem.WriteData(almanacSEMs, almanacGeoS[almanacSEMs[0].PRN - 1].wn_oa, Convert.ToUInt32(almanacGeoS[almanacSEMs[0].PRN - 1].t_oa));

                FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGpsAlmanacUpdated);
            }
            catch (Exception ex)
            { }
        }


        /// <summary>
        /// Convert almanac SeptentrioSBF format to SEM format 
        /// </summary>
        public void WriteFile(string path, List<SeptentrioSBF.AlmanacGlonass> almanacGeoS, bool isAccumulating)
        {
            try
            {
                var paths = FileOperations.CreateFiles("almanacGlonass.agl", path, isAccumulating, "\\Log_MosaicX5", "almanacGlonass", ".agl");

                var tempForSorting = almanacGeoS.Select(u => new AlmanacGlonass(u));
                var almanacAGLs = tempForSorting.OrderBy(u => u.Num).ToList();

                if (almanacAGLs.Count() == 0)
                {
                    return;
                }

                var tempResult = Agl.WriteData(almanacAGLs);

                FileOperations.WriteFiles(tempResult, paths, isAccumulating, OnGlonassAlmanacUpdated);
            }
            catch (Exception ex)
            { }
        }
    }
}
