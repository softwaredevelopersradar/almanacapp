﻿using System;

namespace ToRinexConverter
{
    public class EphemerisBeidou : ICloneable
    {
        DateTime dtGPSEp = new DateTime(1980, 1, 6);
        public DateTime dtTmp;

        public byte PRN;                   
        public double Year;
        public double Month;
        public double Day;
        public double Hour;
        public double Minute;
        public double Second;
        public double SVClockBias;          
        public double SVClockDrift;         
        public double SVClockDriftRate;     
        // ORBIT 1
        public double IODE;                 
        public double C_rs;                 
        public double delta_n;              
        public double M_0;                  
        // ORBIT 2
        public double C_uc;                 
        public double e;                    
        public double C_us;                 
        public double sqrt_A;               
        // ORBIT 3
        public double T_oe;                 
        public double C_ic;                 
        public double OMEGA_0;              
        public double C_is;                
        // ORBIT 4
        public double i_0;                  
        public double C_rc;                 
        public double omega;                
        public double OMEGADOT;            
        // ORBIT 5
        public double IDOT;                 
        public double weeknum;              
        // ORBIT 6
        public double SVaccuracy;                
        public double SV_health;           
        public double T_GD1; //B1/B3
        public double T_GD2; //B2/B3

        // ORBIT 7
        public double HOW;  
        public double IODC;


        //D1/D2
        // ORBIT 1
        public double AODE;
        // ORBIT 7
        public double AODC;


        //CNAV-1
        // ORBIT 1
        public double A_DOT;

        public double delta_n0;

        // ORBIT 5

        public double delta_n0_dot;
        public byte SatType;
        public double t_op;
        // ORBIT 6
        public double SISAI_oe;
        public double SISAI_ocb;
        public double SISAI_oc1;
        public double SISAI_oc2;
        // ORBIT 7
        public double ISC_B1Cd;
        
        public double TGD_B1Cp;
        public double TGD_B2ap;
        // ORBIT 8
        public double SISMAI;
        public byte Health;
        public double B1CIntegrityFlags;

        // ORBIT 9
        public double t_tm;

        //CNAV-2
        // ORBIT 7
        public double ISC_B2ad;
        // ORBIT 8
        public double B2aB1CintegrityFlags;

        public EphemerisBeidou(SeptentrioSBF.EphemerisBeidou ephemeris)
        {
            PRN = ephemeris.PRN <= 180 ? (byte)((int)ephemeris.PRN - 140) : (byte)((int)ephemeris.PRN - 182);

            //dtTmp = dtGPSEp.AddDays(7 * (ephemeris.weeknum + 1024 * 2)).AddSeconds(ephemeris.t_oe); //разобраться
            dtTmp = SeptentrioSBF.RtcmV3Helper.GetFromBeiDou(ephemeris.WN, ephemeris.t_oe);
            Year = dtTmp.Year;
            Month = dtTmp.Month;
            Day = dtTmp.Day;
            Hour = dtTmp.Hour;
            Minute = dtTmp.Minute;
            Second = dtTmp.Second;

            SVClockBias = ephemeris.a_f0;
            SVClockDrift = ephemeris.a_f1;
            SVClockDriftRate = ephemeris.a_f2;

            // ORBIT 1
            IODE = ephemeris.IODE; 
            C_rs = ephemeris.C_rs;
            delta_n = ephemeris.delta_n * Math.PI;  //radians
            M_0 = ephemeris.M_0 * Math.PI;

            // ORBIT 2
            C_uc = ephemeris.C_uc;
            e = ephemeris.e;
            C_us = ephemeris.C_us;
            sqrt_A = ephemeris.sqrt_A;

            // ORBIT 3
            T_oe = ephemeris.t_oe;
            C_ic = ephemeris.C_ic;
            OMEGA_0 = ephemeris.OMEGA_0 * Math.PI;
            C_is = ephemeris.C_is;

            // ORBIT 4
            i_0 = ephemeris.i_0 * Math.PI;
            C_rc = ephemeris.C_rc;
            omega = ephemeris.omega * Math.PI;
            OMEGADOT = ephemeris.OMEGADOT * Math.PI;

            // ORBIT 5
            IDOT = ephemeris.IDOT * Math.PI;
            weeknum = ephemeris.WN; // = ephemeris.WN + n * 8192, started at zero at 1-Jan-2006


            // ORBIT 6
            SVaccuracy = ephemeris.URA <= 6 ? Math.Round(Math.Pow(2 , 1 + ephemeris.URA / 2), 1) : Math.Round(Math.Pow(2, ephemeris.URA - 2), 1);
            SV_health = ephemeris.SV_health;
            T_GD1 = ephemeris.T_GD1;
            T_GD2 = ephemeris.T_GD2;


            // ORBIT 7

            HOW = ephemeris.TOW * 0.001d; 
            IODC = ephemeris.IODC;
        }

        public interface ICloneable
        {
            object Clone();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
