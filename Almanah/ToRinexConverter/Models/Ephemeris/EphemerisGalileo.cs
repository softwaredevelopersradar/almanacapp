﻿using System;

namespace ToRinexConverter
{
    public class EphemerisGalileo
    {
        DateTime dtGPSEp = new DateTime(1980, 1, 6);
        public DateTime dtTmp;

        public byte PRN;                    // 0    tsip
        public double Year;
        public double Month;
        public double Day;
        public double Hour;
        public double Minute;
        public double Second;
        public double SVClockBias;          // 12   tsip
        public double SVClockDrift;         // 11   tsip
        public double SVClockDriftRate;     // 10   tsip
        // ORBIT 1
        public double IODnav;                 // 14   tsip
        public double C_rs;                 // 16   tsip
        public double delta_n;              // 17   tsip
        public double M_0;                  // 18   tsip
        // ORBIT 2
        public double C_uc;                 // 19   tsip
        public double e;                    // 20   tsip
        public double C_us;                 // 21   tsip
        public double sqrt_A;               // 22   tsip
        // ORBIT 3
        public double T_oe;                 // 9,23 tsip
        public double C_ic;                 // 24   tsip
        public double OMEGA_0;              // 25   tsip
        public double C_is;                 // 26   tsip
        // ORBIT 4
        public double i_0;                  // 27   tsip
        public double C_rc;                 // 28   tsip
        public double omega;                // 29   tsip
        public double OMEGADOT;             // 30   tsip
        // ORBIT 5
        public double IDOT;                 // 31   tsip
        public int Source;               // 3    tsip
        public double weeknum;              // 2    tsip  = tsip + 1024
        //public double L2Pdata;              // 4    tsip
        // ORBIT 6
        public float SISA;
        public int Health_OSSOL;
        public float BGD_L1E5a;
        public float BGD_L1E5b;
        
        // ORBIT 7
        public double HOW;

        public EphemerisGalileo(SeptentrioSBF.EphemerisGalileo ephemeris)
        {
            this.PRN = (byte)((int)ephemeris.SvId - 70);

            dtTmp = dtGPSEp.AddDays(7 * adjgpsweek(ephemeris.WNt_oe)).AddSeconds(ephemeris.t_oe); //разобраться

            Year = dtTmp.Year;
            Month = dtTmp.Month;
            Day = dtTmp.Day;
            Hour = dtTmp.Hour;
            Minute = dtTmp.Minute;
            Second = dtTmp.Second;

            SVClockBias = ephemeris.a_f0;
            SVClockDrift = ephemeris.a_f1;
            SVClockDriftRate = ephemeris.a_f2;

            // ORBIT 1
            this.IODnav = ephemeris.IODnav;
            C_rs = ephemeris.C_rs;
            delta_n = ephemeris.delta_n * Math.PI;  //radians
            M_0 = ephemeris.M_0 * Math.PI;

            // ORBIT 2
            C_uc = ephemeris.C_uc;
            e = ephemeris.e;
            C_us = ephemeris.C_us;
            sqrt_A = ephemeris.sqrt_A;

            // ORBIT 3
            T_oe = ephemeris.t_oe;
            C_ic = ephemeris.C_ic;
            OMEGA_0 = ephemeris.OMEGA_0 * Math.PI;
            C_is = ephemeris.C_is;

            // ORBIT 4
            i_0 = ephemeris.i_0 * Math.PI;
            C_rc = ephemeris.C_rc;
            omega = ephemeris.omega * Math.PI;
            OMEGADOT = ephemeris.OMEGADOT * Math.PI;

            // ORBIT 5
            IDOT = ephemeris.IDOT * Math.PI;
            // Source = ephemeris.Source;
            Source = 517; //temp
             weeknum = adjgpsweek(ephemeris.WNt_oe);//check


            // ORBIT 6
            ////this.SISA = ephemeris.SISA_L1E5a;
            //Health_OSSOL = ephemeris.Health_OSSOL; //TODO: change
            this.SISA = 3.12f; //temp
            Health_OSSOL = 0; //temp
            this.BGD_L1E5a = ephemeris.BGD_L1E5a;
            this.BGD_L1E5b = ephemeris.BGD_L1E5b;

            // ORBIT 7
            HOW = ephemeris.TOW * 0.001d;

        }

        private int adjgpsweek(int week)
        {
            var now = DateTime.UtcNow;
            var w = 0;
            double sec = 0;
            SeptentrioSBF.RtcmV3Helper.GetFromTime(SeptentrioSBF.RtcmV3Helper.Utc2Gps(now), ref w, ref sec);
            if (w < 1560) w = 1560; /* use 2009/12/1 if time is earlier than 2009/12/1 */
            return week + (w - week + 1) / 1024 * 1024;
        }

    }
}
