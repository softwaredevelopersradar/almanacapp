﻿using System;

namespace ToRinexConverter
{
    public class EphemerisGlonass : ICloneable
    {
        DateTime dtGPSEp = new DateTime(2008, 1, 1);
        //DateTime dtGPSEp = new DateTime(1996, 1, 1);
        public DateTime dtTmp;
        public byte tb;           // номер 15-минутного интервала по шкале ГЛОНАСС = UTC + 3 часа

        public byte PRN;                 // Satellite system (R), satellite number (slot number in sat.constellation)
        public double Year;              // Epoch: Toc - Time of Clock(UTC)
        public double Month;
        public double Day;
        public double Hour;
        public double Minute;
        public double Second;
        public double SVClockBias;       // SV clock bias (sec) (-TauN)
        public double SVFreqBias;        // SV relative frequency bias (+GammaN)
        public double MessFrameTime;     // Message frame time (tk+nd*86400) in seconds of the UTC week
        // ORBIT 1
        public double PosX;              // Satellite position X (km)
        public double VelocityXDot;      // velocity X dot (km/sec)
        public double AccelerationX;     // X acceleration (km/sec2)
        public double Health;            // health (0=OK) (Bn)
        // ORBIT 2
        public double PosY;              // Satellite position Y (km)
        public double VelocityYDot;      // velocity Y dot (km/sec)
        public double AccelerationY;     // Y acceleration (km/sec2)
        public double FreqNum;           // frequency number(-7...+13) (-7...+6) ICD 5.1)        // ORBIT 3
        public double PosZ;              // Satellite position Y (km)
        public double VelocityZDot;      // velocity Y dot (km/sec)
        public double AccelerationZ;     // Y acceleration (km/sec2)
        public byte AgeInfo;           // Age of oper. information (days) (E)
        // ORBIT 4 (Rinex 4)
        public double StatusFlags;              // 9-bit binary number (BNKif Unknown)
        public double DeltaTL1L2 = 0.999999999999E+09;      // (sec)
        public double URAI = 15;     // ; GLO-M/K only – raw accuracy index FT
        public byte HealthFlags;           // Health Flags 3-bit binary number

        public EphemerisGlonass(GeostarBinaryProtocol.EphemerisGlonass ephemerisGeoS)
        {
            PRN = ephemerisGeoS.Num;
            dtTmp = dtGPSEp.AddSeconds(ephemerisGeoS.Toc); //разобраться
            tb = (byte)(ephemerisGeoS.tb - 12);
            //dtTmp = dtGPSEp.AddDays(ephemerisGeoS.GeneralizedDateNum);
            //DateTime first =Convert.ToDateTime( DateTime.UtcNow - dtTmp);

            //dtTmp.AddMinutes(ephemerisGeoS.tb * 15); //разобраться

            Year = dtTmp.Year;
            Month = dtTmp.Month;
            Day = dtTmp.Day;

            dtTmp = dtTmp.AddHours(-dtTmp.Hour).AddMinutes(-dtTmp.Minute).AddSeconds(-dtTmp.Second);
            dtTmp = dtTmp.AddMinutes(ephemerisGeoS.tb * 15).AddHours(-3);

            Hour = dtTmp.Hour;
            Minute = dtTmp.Minute;
            Second = dtTmp.Second;

            SVClockBias = -ephemerisGeoS.tauN; //or -tauN?
            SVFreqBias = ephemerisGeoS.SVFreqBias;
            //MessFrameTime = 

            PosX = ephemerisGeoS.PosX / 1000;
            VelocityXDot = ephemerisGeoS.VelocityXDot / 1000;
            AccelerationX = ephemerisGeoS.AccelerationX / 1000;
            Health = Convert.ToByte(ephemerisGeoS.Bn); //check

            PosY = ephemerisGeoS.PosY / 1000;
            VelocityYDot = ephemerisGeoS.VelocityYDot / 1000;
            AccelerationY = ephemerisGeoS.AccelerationY / 1000;
            //FreqNum = ephemerisGeoS.f; //check

            PosZ = ephemerisGeoS.PosZ / 1000;
            VelocityZDot = ephemerisGeoS.VelocityZDot / 1000;
            AccelerationZ = ephemerisGeoS.AccelerationZ / 1000;
            AgeInfo = ephemerisGeoS.AgeInfo;
        }        public EphemerisGlonass(GeostarBinaryProtocol.EphemerisGlonass ephemerisGeoS, GeostarBinaryProtocol.AlmanacGlonass almanacGeoS)
        {
            PRN = ephemerisGeoS.Num;
            dtTmp = dtGPSEp.AddSeconds(ephemerisGeoS.Toc); //разобраться
            tb = (byte)(ephemerisGeoS.tb - 12);
            //dtTmp = dtGPSEp.AddDays(ephemerisGeoS.GeneralizedDateNum);
            //DateTime first =Convert.ToDateTime( DateTime.UtcNow - dtTmp);
            //dtTmp.Hour = 0;
            //dtTmp.Minute = 0;
            //dtTmp.Second = 0;
            //dtTmp.AddMinutes(ephemerisGeoS.tb * 15); //разобраться
            Year = dtTmp.Year;
            Month = dtTmp.Month;
            Day = dtTmp.Day;

            dtTmp = dtTmp.AddHours(-dtTmp.Hour).AddMinutes(-dtTmp.Minute).AddSeconds(-dtTmp.Second);
            dtTmp = dtTmp.AddMinutes(ephemerisGeoS.tb * 15).AddHours(-3);
            
            Hour = dtTmp.Hour;
            Minute = dtTmp.Minute;
            Second = dtTmp.Second;


            SVClockBias = -ephemerisGeoS.tauN; //or -tauN?
            SVFreqBias = ephemerisGeoS.SVFreqBias;
            //MessFrameTime = 

            PosX = ephemerisGeoS.PosX / 1000;
            VelocityXDot = ephemerisGeoS.VelocityXDot / 1000;
            AccelerationX = ephemerisGeoS.AccelerationX / 1000;
            Health = Convert.ToByte(ephemerisGeoS.Bn); //check

            PosY = ephemerisGeoS.PosY / 1000;
            VelocityYDot = ephemerisGeoS.VelocityYDot / 1000;
            AccelerationY = ephemerisGeoS.AccelerationY / 1000;
            FreqNum = almanacGeoS.f_channel; 

            PosZ = ephemerisGeoS.PosZ / 1000;
            VelocityZDot = ephemerisGeoS.VelocityZDot / 1000;
            AccelerationZ = ephemerisGeoS.AccelerationZ / 1000;
            AgeInfo = ephemerisGeoS.AgeInfo;
        }

        private readonly DateTime baseTimeGps = new DateTime(1980, 1, 6, 0, 0, 0, DateTimeKind.Utc);
        public EphemerisGlonass(SeptentrioSBF.EphemerisGlonass ephemeris)
        {
            //PRN = (byte)((int)ephemeris.SvId - 37);
            PRN = ephemeris.SvId <= 61 ? (byte)((int)ephemeris.SvId - 37) : (byte)((int)ephemeris.SvId - 38);
            // Calculate GPS time at time of transmission
            DateTime dtTmp = this.baseTimeGps
                .AddDays(ephemeris.WNc * 7)
                .AddSeconds(ephemeris.TOW * 0.001d);
            //.AddSeconds(ephemeris.TOW * 0.001d - 10800);

            //var fromBeginningOfDay = gpsTime.TimeOfDay.TotalSeconds;
            //var tempTime = this.baseTimeGps.Add
            //dtTmp = dtGPSEp.AddSeconds(ephemeris.t); //разобраться
            tb = (byte)(ephemeris.tb - 12);

            Year = dtTmp.Year;
            Month = dtTmp.Month;
            Day = dtTmp.Day;

            dtTmp = dtTmp.Date;
            dtTmp = dtTmp.AddMinutes(ephemeris.tb * 15).AddHours(-3);
            //dtTmp = dtTmp.AddHours(-dtTmp.Hour).AddMinutes(-dtTmp.Minute).AddSeconds(-dtTmp.Second);
            //dtTmp = dtTmp.AddMinutes((ephemeris.tb - 12) * 15);

            Hour = dtTmp.Hour;
            Minute = dtTmp.Minute;
            Second = dtTmp.Second;


            SVClockBias = -ephemeris.tauN; 
            SVFreqBias = ephemeris.gamma;
            MessFrameTime = ephemeris.TOW * 0.001d;

            PosX = ephemeris.PosX;
            VelocityXDot = ephemeris.VelocityXDot;
            AccelerationX = ephemeris.AccelerationX;
            Health =ephemeris.l; 

            PosY = ephemeris.PosY;
            VelocityYDot = ephemeris.VelocityYDot;
            AccelerationY = ephemeris.AccelerationY;
            FreqNum = (short)((int)ephemeris.FreqNr - 8);

            PosZ = ephemeris.PosZ;
            VelocityZDot = ephemeris.VelocityZDot;
            AccelerationZ = ephemeris.AccelerationZ;
            AgeInfo = ephemeris.E;
        }        public interface ICloneable
        {
            object Clone();
        }        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
