﻿
namespace ToRinexConverter
{
    public struct HeaderLeapSeconds
    {
        public int LeapSeconds; //since 6 Jan 1980
        public int deltaT_LSF; //Future or past leap seconds ΔtLSF, i.e.future leap second if the week and day number are in the future. (BNK)

        public int weekNum_LSF; //Respective week number WN_LSF (continuous number) (BNK), weeks since 6-Jan-1980
        public int dayNum; //Respective day number (1-7) (BNK)

    }
}
