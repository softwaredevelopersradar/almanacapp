﻿
namespace ToRinexConverter
{
    public struct HeaderEphemerisGlonass
    {
        public double A0Delta { get; set; }
        public float A1Delta { get; set; }
        public float Tot { get; set; }
        public short DeltaTLs { get; set; }
        public int WeekNumT { get; set; }
    }
}
