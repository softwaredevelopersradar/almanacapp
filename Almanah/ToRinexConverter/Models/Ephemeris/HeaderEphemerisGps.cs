﻿
namespace ToRinexConverter
{
    public struct HeaderEphemerisGps
    {
        public float A0 { get; set; }
        public float A1 { get; set; }
        public float A2 { get; set; }
        public float A3 { get; set; }
        public float B0 { get; set; }
        public float B1 { get; set; }
        public float B2 { get; set; }
        public float B3 { get; set; }
        public double A0Delta { get; set; }
        public float A1Delta { get; set; }
        public float Tot { get; set; }
        public short DeltaTLs { get; set; }
        public int WeekNumT { get; set; }

    }
}
