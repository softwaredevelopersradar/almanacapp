﻿
namespace ToRinexConverter
{
    public struct HeaderEphemerisGalileo
    {
        public float A0 { get; set; }
        public float A1 { get; set; }
        public float A2 { get; set; }
       
        
        public double A0Delta { get; set; }
        public float A1Delta { get; set; }
        public float Tot { get; set; }
        public short DeltaTLs { get; set; }
        public int WeekNumT { get; set; }

        public double A0DeltaGPS { get; set; }
        public float A1DeltaGPS { get; set; }
        public float TotGPS { get; set; }
        public int WeekNumTGPS { get; set; }
    }
}
