﻿using System;

namespace ToRinexConverter
{
    public class AlmanacGlonass
    {
        public double Day_A; //day of Almanac
        public double Month_A; //month of Almanac
        public double Year_A; //year of Almanac
        public double t_d; //time of receipt of the Almanac from the beginning of the day, UTC
        
        public ushort Num; //number of spacecraft in the grouping(orbital slot number)
        public short f_channel; // number of frequency channel(-7 .. 6)
        public byte SV_HEALTH;      //SV health indicator(0 -> bad, 1 -> healthy, operational)
        public double Day; //day 
        public double Month; //month 
        public double Year; //year
        public double t_r; //the reference time of the almanac
        public double C_utc; //Correction from GLONASS to UTC
        public double C_gps; //Correction to GPS time relative GLONASS
        public double C_sys; //Correction of time relative to GLONASS system time,

        public double Lamda; //longitude of first ascending node passage, angle in semi-cycles
        public double dI; //correction to the mean value of inclination, in semi-cycles
        public double w; //argument of perigee, in semi-cycles
        public double E; //eccentricity
        public double dT; //correction to the mean value of Draconian period, in second
        public double dTT; // rate of change of Draconian period, in second / coil

        public AlmanacGlonass()
        { }

        public AlmanacGlonass(GeostarBinaryProtocol.AlmanacGlonass almanacGeoStar)
        {
            var dtTmp = new DateTime(DateTime.Today.Year, 1, 1 ).AddDays(almanacGeoStar.DayNum % (DateTime.IsLeapYear(DateTime.Today.Year) ? 366 : 365) - 1); //костыль с годом //(-2) для 5го Geos, для 3го (-1)
            //
            Year_A = dtTmp.Year;
            Month_A = dtTmp.Month;
            Day_A = dtTmp.Day;
            t_d = 1800 + almanacGeoStar.Num - 1; //херня

            Num = almanacGeoStar.Num;
            f_channel = almanacGeoStar.f_channel;
            SV_HEALTH = Convert.ToByte(almanacGeoStar.С);

            Year = dtTmp.Year;
            Month = dtTmp.Month;
            Day = dtTmp.Day;
            t_r = almanacGeoStar.t_lambda;

            C_utc = 0;
            C_gps = 0;
            C_sys = almanacGeoStar.tau;

            Lamda = almanacGeoStar.lambda;
            dI = almanacGeoStar.dI;
            w = almanacGeoStar.w;
            E = almanacGeoStar.e;
            dT = almanacGeoStar.deltaT;
            dTT = almanacGeoStar.TDot;
        }



        private readonly DateTime baseTimeGlonass = new DateTime(1996, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private readonly DateTime baseTimeGps = new DateTime(1980, 1, 6, 0, 0, 0, DateTimeKind.Utc);

        public AlmanacGlonass(SeptentrioSBF.AlmanacGlonass almanac)
        {
            var almanacTime = this.baseTimeGlonass.AddDays((almanac.N_4 - 1) * 1461).AddDays(almanac.DayNum - 1);

            // Convert reference time-of-week to seconds in UTC
            var referenceTime = almanac.TOW * 0.001d - 10800; 

            // Calculate GPS time at time of transmission
            DateTime gpsTime = this.baseTimeGps
                .AddDays(almanac.WNc * 7)
                .AddSeconds(referenceTime);

            var fromBeginningOfDay = gpsTime.TimeOfDay.TotalSeconds;

            //Year_A = almanacTime.Year;
            //Month_A = almanacTime.Month;
            //Day_A = almanacTime.Day;
            //t_d = fromBeginningOfDay; 
            Year_A = gpsTime.Year;
            Month_A = gpsTime.Month;
            Day_A = gpsTime.Day;
            t_d = fromBeginningOfDay;

            Num = (ushort)((int)almanac.SvId - 37);
            f_channel = (short)((int)almanac.FreqNr - 8);
            SV_HEALTH = Convert.ToByte(almanac.SV_HEALTH);

            Year = gpsTime.Year;
            Month = gpsTime.Month;
            Day = gpsTime.Day;
            t_r = almanac.t_ln; //не уверена
            
            C_utc = 0;
            C_gps = 0;
            C_sys = almanac.tau;

            Lamda = almanac.Lamda;
            dI = almanac.dI;
            w = almanac.w;
            E = almanac.E;
            dT = almanac.dT;
            dTT = almanac.dTT;
        }
    }
}
