﻿using System.Collections.Generic;
using System;

namespace ToRinexConverter
{
    public class AlmanacGps
    {
        public AlmanacGps()
        { }

        public byte PRN;            // str 4,  p 1,  ex:  1
        public byte SVN;            // str 5,  p 1,  ex:  63
        public byte aURA;           // str 6,  p 1,  ex:  0
        public double e;              // str 7,  p 1,  ex:  3.18574905395508E-03
        public double i_o_sema;       // str 7,  p 2,  ex:  5.83076477050781E-03
        public double OMEGADOT;       // str 7,  p 3,  ex: -2.52475729212165E-09
        public double sqrt_A;         // str 8,  p 1,  ex:  5.15361621093750E+03
        public double OMEGA_0;        // str 8,  p 2,  ex: -6.99212312698364E-01
        public double omega;          // str 8,  p 3,  ex:  1.01559400558472E-01
        public double M_0;            // str 9,  p 1,  ex: -5.26008248329163E-01
        public double a_f0;           // str 9,  p 2,  ex:  1.62124633789062E-05
        public double a_f1;           // str 9,  p 3,  ex:  0.00000000000000E+00
        public byte SV_HEALTH;      // str 10, p 1,  ex:  0
        public byte SV_CONFIG;      // str 11, p 1,  ex:  11


        List<byte> listSVN = new List<byte> { 0, 63, 61, 69, 34, 50, 67, 48, 72, 68, 73, 46, 58, 43, 41, 55, 56, 53, 34, 59, 51, 45, 47, 60, 65, 62, 71, 66, 44, 57, 64, 52, 70}; //SVN соответствующие PRN (по возрастанию PRN)
        List<byte> listSVConfig = new List<byte> { 0, 11, 9, 11, 9 , 10, 11, 10, 9, 11, 9, 9, 10, 9, 9, 10, 9, 10, 9, 9, 9, 9, 9, 9, 11, 11, 9, 11, 9, 10, 11, 10, 9}; //SV_config судя по файлу

        public AlmanacGps(TSIP.Almanac almanacTSIP)
        {
            PRN = almanacTSIP.PRN;          
            SVN = listSVN[PRN];   
            
            aURA = 0;           //пока что

            e = almanacTSIP.e;              
            i_o_sema = almanacTSIP.i_o;     
            sqrt_A = almanacTSIP.sqrt_A;        
            a_f0 = almanacTSIP.a_f0;          
            a_f1 = almanacTSIP.a_f1;           
            SV_HEALTH = almanacTSIP.SV_HEALTH;     
            SV_CONFIG = listSVConfig[PRN];

            // (Martinovich)  (в полукругах, как в стандарте SEMa)
            OMEGADOT = almanacTSIP.OMEGADOT / Math.PI;
            OMEGA_0 = almanacTSIP.OMEGA_0 / Math.PI;
            omega = almanacTSIP.omega / Math.PI;
            M_0 = almanacTSIP.M_0 / Math.PI;
        }

        public AlmanacGps(ClientServerLib.Almanac almanacTSIP)
        {
            PRN = almanacTSIP.PRN;
            SVN = listSVN[PRN];

            aURA = 0;           //пока что

            e = almanacTSIP.e;
            i_o_sema = almanacTSIP.i_o;
            sqrt_A = almanacTSIP.sqrt_A;
            a_f0 = almanacTSIP.a_f0;
            a_f1 = almanacTSIP.a_f1;
            SV_HEALTH = almanacTSIP.SV_HEALTH;
            SV_CONFIG = listSVConfig[PRN];

            // (Martinovich)  (в полукругах, как в стандарте SEMa)
            OMEGADOT = almanacTSIP.OMEGADOT / Math.PI;
            OMEGA_0 = almanacTSIP.OMEGA_0 / Math.PI;
            omega = almanacTSIP.omega / Math.PI;
            M_0 = almanacTSIP.M_0 / Math.PI;
        }

        public AlmanacGps(GeostarBinaryProtocol.AlmanacGps almanacGeo)
        {
            PRN = (byte)almanacGeo.PRN;
            SVN = listSVN[PRN];

            aURA = 0;           //пока что

            e = almanacGeo.e;
            i_o_sema = almanacGeo.i_o;
            sqrt_A = almanacGeo.sqrt_A;
            a_f0 = almanacGeo.a_f0;
            a_f1 = almanacGeo.a_f1;
            SV_HEALTH = (byte)almanacGeo.SV_HEALTH;
            SV_CONFIG = listSVConfig[PRN];


            OMEGADOT = almanacGeo.OMEGADOT / Math.PI;
            OMEGA_0 = almanacGeo.OMEGA_0 / Math.PI;
            omega = almanacGeo.omega / Math.PI;
            M_0 = almanacGeo.M_0 / Math.PI;
        }
    }
}
