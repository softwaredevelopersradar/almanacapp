﻿
namespace ToRinexConverter
{
    public enum GnssTypes : byte
    {
        GPS = 1,
        GLONASS
    }

    public enum Month
    { JAN = 1, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC }
    //public enum DeviceTypes : byte
    //{
    //    ThunderboltE = 1,
    //    GeoS,
    //    Mini_T
    //}

    public enum RinexFormat : byte
    {
        Rinex2_0 = 1,
        Rinex3_02,
        Rinex3_03
    }
}
