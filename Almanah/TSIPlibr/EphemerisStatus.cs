﻿using System;
using System.Collections.Generic;

namespace TSIP
{
    public class EphemerisStatus
    {
        public byte PRN { get; set; }
        public float TimeOfCollection { get; set; } = -1;
        public byte Health { get; set; }
        public byte IODE { get; set; }
        public float Toe { get; set; } = -1;
      
        public byte FitIntervalFlag { get; set; }
        public float SVAccuracy { get; set; } = -1;

        public EphemerisStatus(byte[] data)
        {
            if (data == null || data.Length != 20)
                return;

            PRN = data[2];
            TimeOfCollection = BitConverter.ToSingle(OperationBytes.InverseData(data, 3, 4), 0);
            Health = data[7];
            IODE = data[8];
            Toe = BitConverter.ToSingle(OperationBytes.InverseData(data, 9, 4), 0);
            FitIntervalFlag = data[13];
            SVAccuracy = BitConverter.ToSingle(OperationBytes.InverseData(data, 14, 4), 0);
        }


        public EphemerisStatus(List<byte> data)
        {
            if (data == null || data.Count != 20)
                return;

            PRN = data[2];
            TimeOfCollection = BitConverter.ToSingle(OperationBytes.InverseData(data, 3, 4), 0);
            Health = data[7];
            IODE = data[8];
            Toe = BitConverter.ToSingle(OperationBytes.InverseData(data, 9, 4), 0);
            FitIntervalFlag = data[13];
            SVAccuracy = BitConverter.ToSingle(OperationBytes.InverseData(data, 14, 4), 0);
        }

        public EphemerisStatus()
        {
        }
    }
}
