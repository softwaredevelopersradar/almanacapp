﻿using System;
using System.IO;

namespace TSIP
{
    public partial class Tsip
    {
        private string almpath = @".\MyAlmanac.txt";
        private string ephpath = @".\MyEphemerides.txt";

        //(not used)
        private void CreateAlmFile(string almanacfilepath)
        {
            //Directory.CreateDirectory(@"c:\temp");
            if (almanacfilepath.Length != 0) { almpath = @almanacfilepath + "\\MyAlmanac.txt"; }
            try
            {
                System.IO.File.WriteAllText(almpath, string.Empty);
            }
            catch (Exception ex)
            {
                OnError?.Invoke(this, "Almanac file path error!");
            }
        }

        //(not used)
        private void CreateEphFile(string ephemeridespath)
        {
            //Directory.CreateDirectory(@"c:\temp");
            if (ephemeridespath.Length != 0) { this.ephpath = @ephemeridespath + "\\MyEphemerides.txt"; }
            try
            {
                System.IO.File.WriteAllText(ephpath, string.Empty);
            }
            catch (Exception ex)
            {
                OnError?.Invoke(this, "Ephemeris file path error!");
            }
        }

        //запись Альманаха от приемника в файл (not used)
        private void WriteToAlmanac(Almanac almanac)
        {
            String alminfo = "";
            alminfo = alminfo + $"FullSatellite number:{almanac.PRN } ";
            alminfo = alminfo + "t_oa_raw: " + almanac.t_oa_raw.ToString("X2") + " ";
            alminfo = alminfo + "SV_HEALTH: " + almanac.SV_HEALTH.ToString("X2") + " ";
            alminfo = alminfo + "e: " + almanac.e.ToString() + " ";
            alminfo = alminfo + "t_oa: " + almanac.t_oa.ToString() + " ";
            alminfo = alminfo + "i_o: " + almanac.i_o.ToString() + " ";
            alminfo = alminfo + "OMEGADOT: " + almanac.OMEGADOT.ToString() + " ";
            alminfo = alminfo + "sqrt_A: " + almanac.sqrt_A.ToString() + " ";
            alminfo = alminfo + "OMEGA_0: " + almanac.OMEGA_0.ToString() + " ";
            alminfo = alminfo + "omega: " + almanac.omega.ToString() + " ";
            alminfo = alminfo + "M_0: " + almanac.M_0.ToString() + " ";
            alminfo = alminfo + "a_f0: " + almanac.a_f0.ToString() + " ";
            alminfo = alminfo + "a_f1: " + almanac.a_f1.ToString() + " ";
            alminfo = alminfo + "Axis: " + almanac.Axis.ToString() + " ";
            alminfo = alminfo + "n: " + almanac.n.ToString() + " ";
            alminfo = alminfo + "OMEGA_n: " + almanac.OMEGA_n.ToString() + " ";
            alminfo = alminfo + "ODOT_n: " + almanac.ODOT_n.ToString() + " ";
            alminfo = alminfo + "t_zc: " + almanac.t_zc.ToString() + " ";
            alminfo = alminfo + "weeknum: " + almanac.weeknum.ToString() + " ";
            alminfo = alminfo + "wn_oa: " + almanac.wn_oa.ToString() + " ";
            alminfo = alminfo + Environment.NewLine;
            File.AppendAllText(@almpath, alminfo);
        }

        //запись Эфемерид от приемника в файл
        private void WriteToFileEph(Ephemeris ephemeris)
        {
            String ephinfo = "";
            ephinfo = ephinfo + $"FullSatellite number:{ephemeris.sv_number} ";
            ephinfo = ephinfo + "sv_number: " + ephemeris.sv_number.ToString() + " ";
            ephinfo = ephinfo + "t_ephem: " + ephemeris.t_ephem.ToString() + " ";
            ephinfo = ephinfo + "weeknum: " + ephemeris.weeknum.ToString() + " ";
            ephinfo = ephinfo + "codeL2: " + ephemeris.codeL2.ToString() + " ";
            ephinfo = ephinfo + "L2Pdata: " + ephemeris.L2Pdata.ToString() + " ";
            ephinfo = ephinfo + "SVacc_raw: " + ephemeris.SVacc_raw.ToString() + " ";
            ephinfo = ephinfo + "SV_health: " + ephemeris.SV_health.ToString() + " ";
            ephinfo = ephinfo + "IODC: " + ephemeris.IODC.ToString() + " ";
            ephinfo = ephinfo + "T_GD: " + ephemeris.T_GD.ToString() + " ";
            ephinfo = ephinfo + "t_oc: " + ephemeris.t_oc.ToString() + " ";
            ephinfo = ephinfo + "a_f2: " + ephemeris.a_f2.ToString() + " ";
            ephinfo = ephinfo + "a_f1: " + ephemeris.a_f1.ToString() + " ";
            ephinfo = ephinfo + "a_f0: " + ephemeris.a_f0.ToString() + " ";
            ephinfo = ephinfo + "SVacc: " + ephemeris.SVacc.ToString() + " ";
            ephinfo = ephinfo + "IODE: " + ephemeris.IODE.ToString("X2") + " ";
            ephinfo = ephinfo + "fit_interval: " + ephemeris.fit_interval.ToString("X2") + " ";
            ephinfo = ephinfo + "C_rs: " + ephemeris.C_rs.ToString() + " ";
            ephinfo = ephinfo + "delta_n: " + ephemeris.delta_n.ToString() + " ";
            ephinfo = ephinfo + "M_0: " + ephemeris.M_0.ToString() + " ";
            ephinfo = ephinfo + "C_uc: " + ephemeris.C_uc.ToString() + " ";
            ephinfo = ephinfo + "e: " + ephemeris.e.ToString() + " ";
            ephinfo = ephinfo + "C_us: " + ephemeris.C_us.ToString() + " ";
            ephinfo = ephinfo + "sqrt_A: " + ephemeris.sqrt_A.ToString() + " ";
            ephinfo = ephinfo + "t_oe: " + ephemeris.t_oe.ToString() + " ";
            ephinfo = ephinfo + "C_ic: " + ephemeris.C_ic.ToString() + " ";
            ephinfo = ephinfo + "OMEGA_0: " + ephemeris.OMEGA_0.ToString() + " ";
            ephinfo = ephinfo + "C_is: " + ephemeris.C_is.ToString() + " ";
            ephinfo = ephinfo + "i_o: " + ephemeris.i_o.ToString() + " ";
            ephinfo = ephinfo + "C_rc: " + ephemeris.C_rc.ToString() + " ";
            ephinfo = ephinfo + "omega: " + ephemeris.omega.ToString() + " ";
            ephinfo = ephinfo + "OMEGADOT: " + ephemeris.OMEGADOT.ToString() + " ";
            ephinfo = ephinfo + "IDOT: " + ephemeris.IDOT.ToString() + " ";
            ephinfo = ephinfo + "Axis: " + ephemeris.Axis.ToString() + " ";
            ephinfo = ephinfo + "n: " + ephemeris.n.ToString() + " ";
            ephinfo = ephinfo + "r1me2: " + ephemeris.r1me2.ToString() + " ";
            ephinfo = ephinfo + "OMEGA_n: " + ephemeris.OMEGA_n.ToString() + " ";
            ephinfo = ephinfo + "ODOT_n: " + ephemeris.ODOT_n.ToString() + " ";
            ephinfo = ephinfo + Environment.NewLine;
            File.AppendAllText(@ephpath, ephinfo);
        }
    }
}
