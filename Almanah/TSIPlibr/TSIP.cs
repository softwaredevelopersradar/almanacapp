﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading;

namespace TSIP
{
    public partial class Tsip : ITSIPInterface
    {
        private SerialPort _port;
        private Thread thrRead;

        private const byte MAX_SIZE = 255;

        #region Codes
        private const byte DATE_TIME = 0x41;
        private const byte COORD1 = 0x8F;
        private const byte COORD2 = 0xAC;
        private const byte PARAM_SATELITE = 0x5c;
        private const byte ALMANAH_EPHEMERIS = 0x58;
        private const byte FIXED_POS = 0x6D;
        private const byte FIXED_POS_MINI = 0x6C;
        private const byte SAT_SNR = 0x47;
        private const byte EPHEMERIS_STATUS = 0x5B;

        private const byte bStart = 0x10;
        private const byte bStop = 0x03;
        #endregion

        #region Events
        public event EventHandler<ListByteEventArgs> OnWriteByte = (object sender, ListByteEventArgs data) => { };
        public event EventHandler<ListByteEventArgs> OnReadByte = (object sender, ListByteEventArgs data) => { };
        public event EventHandler<ListByteEventArgs> OnUnknownPocket = (object sender, ListByteEventArgs data) => { };
        public event EventHandler<string> OnConnect = (object sender, string data) => { };
        public event EventHandler<bool> OnDisconnect = (object sender, bool data) => { };
        public event EventHandler<string> OnError = (object sender, string data) => { };
        public event EventHandler<TimeEventArgs> OnGetTime = (object sender, TimeEventArgs data) => { };
        public event EventHandler<ReceiverCoordEventArgs> OnGetReceiverCoordinates = (object sender, ReceiverCoordEventArgs data) => { };
        public event EventHandler<SatInfoEventArgs> OnGetSatInfo = (object sender, SatInfoEventArgs data) => { };
        public event EventHandler<List<FullSatelliteStatus>> OnGetFullSatInfo;
        public event EventHandler<AlmanacEventArgs> OnGetAlmanac = (object sender, AlmanacEventArgs data) => { };
        public event EventHandler<EphemerisEventArgs> OnGetEphemeries = (object sender, EphemerisEventArgs data) => { };
        public event EventHandler<string> Message;
        public event EventHandler<List<EphemerisStatus>> OnGetEphemerisStatus;
        public event EventHandler<PortState> OnPortStateRTS;
        public event EventHandler<PortState> OnPortStateDTR;
        //public event EventHandler<ionosphereInfo> OnGetIonosphereInfo;
        //public event EventHandler<utcInfo> OnGetUtcInfo;
        #endregion

        private List<Ephemeris> satephemeries = new List<Ephemeris>();
        private List<Almanac> satAlmanacs = new List<Almanac>();
        private List<SatInfo> satInfo = new List<SatInfo>();
        private List<FullSatelliteStatus> satFullInfo = new List<FullSatelliteStatus>();
        private List<byte> fixedSat = new List<byte>();
        private List<short> hasSnrSatsList = new List<short>();
        private List<EphemerisStatus> ephemerisStatusList = new List<EphemerisStatus>();
        private IonosphereInfo ionosphereInfo;
        private UtcInfo utcInfo;
        private bool canSendSats = false;


        public Tsip()
        {
            
        }

        ///<summary>
        /// Connecting the receiver
        /// </summary>
        public void Connect(string Port_number, int Port_rate)
        {
            satephemeries = new List<Ephemeris>();
            satAlmanacs = new List<Almanac>();
            satInfo = new List<SatInfo>();
            satFullInfo = new List<FullSatelliteStatus>();

            if (_port == null)
                _port = new SerialPort();

            if (_port.IsOpen)
                Disconnect();

            try
            {
                // set parameters of port
                _port.PortName = Port_number;
                _port.BaudRate = Port_rate;
                _port.Parity = Parity.None;
                _port.DataBits = 8;
                _port.StopBits = StopBits.One;
                //_port.RtsEnable = true;
                _port.RtsEnable = false;
                //var t = this._port.CtsHolding;
                _port.DtrEnable = true;
                _port.ReceivedBytesThreshold = 1000;
                _port.Open();
                this.OnPortStateRTS?.Invoke(this, new PortState(!_port.RtsEnable, !_port.DsrHolding));
                this.OnPortStateDTR?.Invoke(this, new PortState(this._port.DtrEnable, this._port.CtsHolding));

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception)
                {}
                
                OnConnect?.Invoke(this, "Connected");
                Message?.Invoke(this, "Port opened");
            }
            catch (System.Exception e)
            {
                // событие закрытия порта порта
                OnDisconnect?.Invoke(this, true);
                Message?.Invoke(this, "Open port error");
            }
        }


        /// <summary>
        /// Block/unblock RTS pin of SerialPort
        /// </summary>
        public void BlockComPortRTS(bool isEnable)
        {
            if (_port == null) 
                return;

            try
            {
                //Задавать инвертированное значение (1 - отключение, 0 - вкл. питание)
                _port.RtsEnable = !isEnable;
                Thread.Sleep(500);
                this.OnPortStateRTS?.Invoke(this, new PortState(!_port.RtsEnable, !_port.DsrHolding));
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Block/unblock DTR pin of SerialPort
        /// </summary>
        public void BlockComPortDtr(bool isEnable)
        {
            if (_port == null)
                return;

            try
            {
                _port.DtrEnable = isEnable;
                Thread.Sleep(500);
                this.OnPortStateDTR?.Invoke(this, new PortState(this._port.DtrEnable, this._port.CtsHolding));
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        /// <summary>
        /// Disconnecting the receiver
        /// </summary>
        public void Disconnect()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception e)
            { }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception e)
            { }

            try
            {
                // destroy thread of reading
                try
                {
                    if (thrRead != null)
                    {
                        thrRead.Abort();
                        thrRead.Join(500);
                        thrRead = null;
                    }
                }
                catch (Exception e)
                { Message?.Invoke(this, "D0, Error: " + e); }

                _port.Close();
                this.OnPortStateRTS?.Invoke(this, new PortState(false, false));
                this.OnPortStateDTR?.Invoke(this, new PortState(false, false));

                OnDisconnect?.Invoke(this, true);
                Message?.Invoke(this, "Port closed");
            }
            catch (System.Exception e)
            { Message?.Invoke(this, "Closing error: " + e); }
        }


        public void DisconnectPrivate()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception e)
            { }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception e)
            { }

            try
            {
                // destroy thread of reading
                try
                {
                    if (thrRead != null)
                    {
                        thrRead.Abort();
                        thrRead.Join(500);
                        thrRead = null;
                    }
                }
                catch (Exception e)
                { Message?.Invoke(this, "D0, Error: " + e); }
                OnDisconnect?.Invoke(this, false);
                Message?.Invoke(this, "Port closed");
            }
            catch (System.Exception e)
            { Message?.Invoke(this, "Closing error: " + e); }
        }

        private bool WriteToARD(byte[] bSend)
        {
            try
            {
                bSend = AddByteReply(bSend);
                _port.Write(bSend, 0, bSend.Length);
                OnWriteByte?.Invoke(this, new ListByteEventArgs(bSend.ToList()));
                return true;
            }
            catch (System.Exception ex)
            {
                string strer = ex.Message;
                return false;
            }
        }

        //Удвоение 0х10 в данных на отправку
        private byte[] AddByteReply(byte[] bCmd)
        {
            for (int i = 1; i < bCmd.Length-2; i++)
            {
                if (bCmd[i] == bStart)
                {
                    byte[] newArray = new byte[bCmd.Length + 1];
                    Array.Copy(bCmd, 0, newArray, 0, i);
                    Array.Copy(bCmd, i , newArray, i, 1);
                    Array.Copy(bCmd, i, newArray, i+1, bCmd.Length - i);
                    Array.Resize(ref bCmd, newArray.Length);
                    Array.Copy(newArray, 0, bCmd, 0, newArray.Length);
                    i++;
                }
            }
            return bCmd;
        }


        #region Requests

        /// <summary>
        /// Requests available satellites coordinates and signal level
        /// </summary>
        public void GetSatInfo()
        {
            OnGetSatInfo?.Invoke(this, new SatInfoEventArgs(satInfo, fixedSat));
            OnGetFullSatInfo?.Invoke(this, satFullInfo);
            satInfo.Clear();
            satFullInfo.Clear();

            byte[] request = new byte[4];
            request[0] = 0x10;
            request[1] = 0x24;
            request[2] = 0x10;
            request[3] = 0x03;
            WriteToARD(request);

            Thread.Sleep(100);

            request = new byte[5];
            request[0] = 0x10;
            request[1] = 0x3C;
            request[2] = 0;
            request[3] = 0x10;
            request[4] = 0x03;
            WriteToARD(request);


            Thread.Sleep(100);

            for (int i = 0; i < 32; i++)
            {
                request = new byte[5];
                request[0] = 0x10;
                request[1] = 0x3B;
                request[2] = (byte)(i + 1);
                request[3] = 0x10;
                request[4] = 0x03;
                WriteToARD(request);
                Thread.Sleep(50);
            }
            
        }

        /// <summary>
        /// Requests Ephemeris for all satellites
        /// </summary>
        public void GetEphemeries(string ephemeridespath)
        {
            CN0_TestMode();
            Thread.Sleep(200);
            GetIonoshere();
            Thread.Sleep(200);
            GetUTC();
            Thread.Sleep(200);
            //bBufSave = new byte[0];
            for (int i = 0; i < 32; i++)
            {
                byte[] request = new byte[7];
                request[0] = 0x10;
                request[1] = 0x38;
                request[2] = 0x01;
                request[3] = 0x06;
                request[4] = Convert.ToByte(i + 1);
                request[5] = 0x10;
                request[6] = 0x03;
                WriteToARD(request);
                Thread.Sleep(500);
            }

            //Message?.Invoke(this, "Sent Eph request");
        }

        /// <summary>
        /// Requests Almanac for all satellites.
        /// </summary>
        public void GetAlmanac(string almanacfilepath)
        {
            //bBufSave = new byte[0];
            for (int i = 0; i < 32; i++)
            {
                byte[] request = new byte[7];
                request[0] = 0x10;
                request[1] = 0x38;
                request[2] = 0x01;
                request[3] = 0x02;
                request[4] = Convert.ToByte(i + 1);
                request[5] = 0x10;
                request[6] = 0x03;
                WriteToARD(request);
                Thread.Sleep(400);
            }

            //Message?.Invoke(this, "Sent Alm request");
        }

        /// <summary>
        /// Requests current GPS time. Week count begins on January 6, 1980. The seconds count begins with 0 each Sunday morning at midnight GPS time 
        /// </summary>
        public void GetTimeInfo()
        {
            byte[] request = new byte[4];
            request[0] = 0x10;
            request[1] = 0x21;
            request[2] = 0x10;
            request[3] = 0x03;
            WriteToARD(request);
        }


        /// <summary>
        ///  Initiate Warm reset
        /// </summary>
        public void WarmReset()
        {
            byte[] request = new byte[5];
            request[0] = 0x10;
            request[1] = 0x1E;
            request[2] = 0x0E;
            request[3] = 0x10;
            request[4] = 0x03;
            WriteToARD(request);
        }

        /// <summary>
        ///  Initiate Cold reset
        /// </summary>
        public void ColdReset()
        {
            byte[] request = new byte[5];
            request[0] = 0x10;
            request[1] = 0x1E;
            request[2] = 0x4B;
            request[3] = 0x10;
            request[4] = 0x03;
            WriteToARD(request);
        }


        public void CN0_TestMode()
        {
            byte[] request = new byte[4];
            request[0] = 0x10;
            request[1] = 0x27;
            request[2] = 0x10;
            request[3] = 0x03;
            WriteToARD(request);
        }

        public void GetIonoshere()
        {
            byte[] request = new byte[7];
            request[0] = 0x10;
            request[1] = 0x38;
            request[2] = 0x01;
            request[3] = 0x04;
            request[4] = 0;
            request[5] = 0x10;
            request[6] = 0x03;
            WriteToARD(request);
        }

        public void GetUTC()
        {
            byte[] request = new byte[7];
            request[0] = 0x10;
            request[1] = 0x38;
            request[2] = 0x01;
            request[3] = 0x05;
            request[4] = 0;
            request[5] = 0x10;
            request[6] = 0x03;
            WriteToARD(request);
        }


        public void ClearAlmanacs()
        {
            satAlmanacs.Clear();
        }

        public void ClearEphemeris()
        {
            satephemeries.Clear();
        }
        #endregion



        #region Replies

        // Обработка используемых спутников в определении позиции(0x6D)
        private void ParseFixPos(List<byte> packet)
        {
            //fixedSat = new List<byte>();
            fixedSat.Clear();
            for (int i = 19; i < packet.Count - 2; i++)
            {
                fixedSat.Add(packet[i]);
            }
        }

        // Обработка используемых спутников в определении позиции(0x6C)
        private void ParseFixPosMini(List<byte> packet)
        {
            fixedSat.Clear();
            for (int i = 20; i < packet.Count - 2; i++)
            {
                fixedSat.Add(packet[i]);
            }
        }

        //Выбор флага для спутника
        //private byte ChooseSatFlag(byte PRN, byte acquisitionFlag)
        //{
        //    byte flagColor;
        //    if (acquisitionFlag != 0)
        //    {
        //        if (fixedSat.Contains((byte)PRN))
        //        {
        //            flagColor = 2;
        //        }
        //        else flagColor = 1;
        //    }
        //    else
        //    {
        //        flagColor = 0;
        //    }

        //    return flagColor;
        //}
        private byte ChooseSatFlag(byte PRN, byte acquisitionFlag)
        {
            if (fixedSat.Contains((byte)PRN))
            {
                return (byte)2;
            }

            return acquisitionFlag != 0 ? (byte)1 : (byte)0;
        }

        public struct SnrPair
        {
            public short Prn {get;set;}
            public float Snr { get; set;}

        }

        // Обработка спутниковых уровней С/Ш (0x47)
        private void ParseSatSNR(List<byte> packet)
        {
            List<SnrPair> list = new List<SnrPair>();
            hasSnrSatsList = new List<short>();
            int count = packet[2];
            string forLog = "\n" + "Signals: ";
            for (int i = 3; i < count * 5; i += 5)
            {
                var temp = new SnrPair() { Prn = packet[i], Snr = BitConverter.ToSingle(OperationBytes.InverseData(packet, i + 1, 4), 0) };
                list.Add(temp);
                hasSnrSatsList.Add(temp.Prn);
                forLog += "\n" + "No." + temp.Prn.ToString() + ", SNR = " + temp.Snr.ToString();
            }
            

            Message?.Invoke(this, forLog);
        }

        // Обработка спутниковых координат и уровня С/Ш (0x5с)
        private void ParseSatInfo(List<byte> packet)
        {
            //if (canSendSats == true)
            //{
            //    OnGetSatInfo?.Invoke(this, new SatInfoEventArgs(satInfo, fixedSat));
            //    satInfo.Clear();
            //}

            SatInfo satellite = new SatInfo();

            satellite.PRN = packet[2];
            satellite.SNR = BitConverter.ToSingle(OperationBytes.InverseData(packet, 6, 4), 0);
            satellite.Elevation = (Single)Math.Round(BitConverter.ToSingle(OperationBytes.InverseData(packet, 14, 4), 0) * 180 / Math.PI, 2);
            satellite.Azimuth = (Single)Math.Round(BitConverter.ToSingle(OperationBytes.InverseData(packet, 18, 4), 0) * 180 / Math.PI, 2);
            satellite.AcquisitionFlag = ChooseSatFlag(packet[2], packet[4]);
            satellite.TrueAcquisitionFlag = packet[4];
            satellite.EphemerisFlag = packet[5];
            satellite.OldMeasureFlag = packet[22];
            satellite.BadDataFlag = packet[24];
            satInfo.Add(new SatInfo(satellite.PRN, satellite.SNR, satellite.Azimuth, satellite.Elevation, satellite.AcquisitionFlag, satellite.TrueAcquisitionFlag, satellite.EphemerisFlag, satellite.OldMeasureFlag, satellite.BadDataFlag));
            var fullsat = new FullSatelliteStatus(packet);
            satFullInfo.Add(fullsat);
        }


        // Обработка Эфемерид (0x58 ... 0x06)
        private void ParseEphInfo(List<byte> packet)
        {
            var ephemeris = new Ephemeris();

            ephemeris.sv_number = packet[6];

            ephemeris.t_ephem = BitConverter.ToSingle(OperationBytes.InverseData(packet, 7, 4), 0);
            ephemeris.weeknum = BitConverter.ToUInt16(OperationBytes.InverseData(packet, 11, 2), 0);

            ephemeris.codeL2 = packet[13];
            ephemeris.L2Pdata = packet[14];
            ephemeris.SVacc_raw = packet[15];
            ephemeris.SV_health = packet[16];

            ephemeris.IODC = BitConverter.ToUInt16(OperationBytes.InverseData(packet, 17, 2), 0);

            ephemeris.T_GD = BitConverter.ToSingle(OperationBytes.InverseData(packet, 19, 4), 0);
            ephemeris.t_oc = BitConverter.ToSingle(OperationBytes.InverseData(packet, 23, 4), 0);
            ephemeris.a_f2 = BitConverter.ToUInt16(OperationBytes.InverseData(packet, 27, 4), 0);
            ephemeris.a_f1 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 31, 4), 0);
            ephemeris.a_f0 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 35, 4), 0);
            ephemeris.SVacc = BitConverter.ToSingle(OperationBytes.InverseData(packet, 39, 4), 0);

            ephemeris.IODE = packet[43];
            ephemeris.fit_interval = packet[44];

            ephemeris.C_rs = BitConverter.ToSingle(OperationBytes.InverseData(packet, 45, 4), 0);
            ephemeris.delta_n = BitConverter.ToSingle(OperationBytes.InverseData(packet, 49, 4), 0);

            ephemeris.M_0 = BitConverter.ToDouble(OperationBytes.InverseData(packet, 53, 8), 0);

            ephemeris.C_uc = BitConverter.ToSingle(OperationBytes.InverseData(packet, 61, 4), 0);

            ephemeris.e = BitConverter.ToDouble(OperationBytes.InverseData(packet, 65, 8), 0);
            
            ephemeris.C_us = BitConverter.ToSingle(OperationBytes.InverseData(packet, 73, 4), 0);
            
            ephemeris.sqrt_A = BitConverter.ToDouble(OperationBytes.InverseData(packet, 77, 8), 0);
            
            ephemeris.t_oe = BitConverter.ToSingle(OperationBytes.InverseData(packet, 85, 4), 0);
            ephemeris.C_ic = BitConverter.ToSingle(OperationBytes.InverseData(packet, 89, 4), 0);

            ephemeris.OMEGA_0 = BitConverter.ToDouble(OperationBytes.InverseData(packet, 93, 8), 0);

            ephemeris.C_is = BitConverter.ToSingle(OperationBytes.InverseData(packet, 101, 4), 0);

            ephemeris.i_o = BitConverter.ToDouble(OperationBytes.InverseData(packet, 105, 8), 0);

            ephemeris.C_rc = BitConverter.ToSingle(OperationBytes.InverseData(packet, 113, 4), 0);
            
            ephemeris.omega = BitConverter.ToDouble(OperationBytes.InverseData(packet, 117, 8), 0);
            
            ephemeris.OMEGADOT = BitConverter.ToSingle(OperationBytes.InverseData(packet, 125, 4), 0);
            ephemeris.IDOT = BitConverter.ToSingle(OperationBytes.InverseData(packet, 129, 4), 0);

            ephemeris.Axis = BitConverter.ToDouble(OperationBytes.InverseData(packet, 133, 8), 0);
            ephemeris.n = BitConverter.ToDouble(OperationBytes.InverseData(packet, 141, 8), 0);
            ephemeris.r1me2 = BitConverter.ToDouble(OperationBytes.InverseData(packet, 149, 8), 0);
            ephemeris.OMEGA_n = BitConverter.ToDouble(OperationBytes.InverseData(packet, 157, 8), 0);
            ephemeris.ODOT_n = BitConverter.ToDouble(OperationBytes.InverseData(packet, 165, 8), 0);
            ephemeris.hasSNR = hasSnrSatsList.Contains(ephemeris.sv_number);
            satephemeries.Add(ephemeris);
            //WriteToFileEph(ephemeris);

            if (satephemeries.Count == 32)
            {
                OnGetEphemeries?.Invoke(this, new EphemerisEventArgs(satephemeries, this.ionosphereInfo, this.utcInfo));
                satephemeries.Clear();
            }
        }


        // Обработка Альманаха (0x58 ... 0x02)
        private void ParseAlmInfo(List<byte> packet)
        {
            var almanac = new Almanac();

            almanac.PRN = packet[4];
            almanac.t_oa_raw = packet[6];
            almanac.SV_HEALTH = packet[7];

            almanac.e = BitConverter.ToSingle(OperationBytes.InverseData(packet, 8, 4), 0);
            almanac.t_oa = BitConverter.ToSingle(OperationBytes.InverseData(packet, 12, 4), 0);
            almanac.i_o = BitConverter.ToSingle(OperationBytes.InverseData(packet, 16, 4), 0);
            almanac.OMEGADOT = BitConverter.ToSingle(OperationBytes.InverseData(packet, 20, 4), 0);
            almanac.sqrt_A = BitConverter.ToSingle(OperationBytes.InverseData(packet, 24, 4), 0);
            almanac.OMEGA_0 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 28, 4), 0);
            almanac.omega = BitConverter.ToSingle(OperationBytes.InverseData(packet, 32, 4), 0);
            almanac.M_0 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 36, 4), 0);
            almanac.a_f0 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 40, 4), 0);
            almanac.a_f1 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 44, 4), 0);
            almanac.Axis = BitConverter.ToSingle(OperationBytes.InverseData(packet, 48, 4), 0);
            almanac.n = BitConverter.ToSingle(OperationBytes.InverseData(packet, 52, 4), 0);
            almanac.OMEGA_n = BitConverter.ToSingle(OperationBytes.InverseData(packet, 56, 4), 0);
            almanac.ODOT_n = BitConverter.ToSingle(OperationBytes.InverseData(packet, 60, 4), 0);
            almanac.t_zc = BitConverter.ToSingle(OperationBytes.InverseData(packet, 64, 4), 0);
            
            almanac.weeknum = BitConverter.ToUInt16(OperationBytes.InverseData(packet, 68, 2), 0);
            almanac.wn_oa = BitConverter.ToUInt16(OperationBytes.InverseData(packet, 70, 2), 0);

            satAlmanacs.Add(almanac);
            //WriteToAlmanac(almanac);

            if (satAlmanacs.Count == 32)
            {
                OnGetAlmanac?.Invoke(this, new AlmanacEventArgs(satAlmanacs));
                satAlmanacs.Clear();
            }
        }


        // Обработка GPS времени (0x41)
        private void ParseTimeInfo(List<byte> packet)
        {
            float Time = BitConverter.ToSingle(OperationBytes.InverseData(packet, 2, 4), 0);
            int Week = BitConverter.ToUInt16(OperationBytes.InverseData(packet, 6, 2), 0);
            float Offset = BitConverter.ToSingle(OperationBytes.InverseData(packet, 8, 4), 0);
            
            Time = Time - Offset; //adding offset for utc format
            var firstdayString = "1/6/1980 00:00:00 AM";
            DateTime firstdate = DateTime.Parse(firstdayString, System.Globalization.CultureInfo.InvariantCulture);
            DateTime LastWeekDay = firstdate.AddDays(Week * 7);

            DateTime currentdate = LastWeekDay.AddSeconds(Time);

            OnGetTime?.Invoke(this, new TimeEventArgs(currentdate));
        }

        //  Ionosphere Data (0x58 ... 0x04)
        private void ParseIonosphere(List<byte> packet)
        {
            var a0 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 14, 4), 0);
            var a1 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 18, 4), 0);
            var a2 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 22, 4), 0);
            var a3 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 26, 4), 0);
            var b0 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 30, 4), 0);
            var b1 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 34, 4), 0);
            var b2 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 38, 4), 0);
            var b3 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 42, 4), 0);

            this.ionosphereInfo = new IonosphereInfo(a0, a1, a2, a3, b0, b1, b2, b3);
            //OnGetIonosphereInfo?.Invoke(this, new IonosphereInfo(a0, a1, a2, a3, b0, b1, b2, b3));
        }

        //  UTC Data (0x58 ... 0x04)
        private void ParseUTS(List<byte> packet)
        {
            var A0 = BitConverter.ToDouble(OperationBytes.InverseData(packet, 19, 8), 0);
            var A1 = BitConverter.ToSingle(OperationBytes.InverseData(packet, 27, 4), 0);
            var T = BitConverter.ToSingle(OperationBytes.InverseData(packet, 33, 4), 0);
            var W = BitConverter.ToUInt16(OperationBytes.InverseData(packet, 37, 2), 0);
            var leapSeconds = BitConverter.ToInt16(OperationBytes.InverseData(packet, 31, 2), 0);

            this.utcInfo = new UtcInfo(A0, A1, T, leapSeconds, W);
            //OnGetUtcInfo?.Invoke(this, new UtcInfo(A0, A1, T, leapSeconds, W));
        }


        // Обработка координат приемника (0x8F-AC приходит автоматом  1 раз в секунду)
        private void ParseCoord(List<byte> packet)
        {
            var coords = new ReceiverLocation(); 
            coords.ReceiverLatitude = Math.Round(BitConverter.ToDouble(OperationBytes.InverseData(packet, 38, 8), 0) * 180 / Math.PI, 6);
            coords.ReceiverLongitude = Math.Round(BitConverter.ToDouble(OperationBytes.InverseData(packet, 46, 8), 0) * 180 / Math.PI, 6);
            coords.ReceiverAltitude = Math.Round(BitConverter.ToDouble(OperationBytes.InverseData(packet, 54, 8), 0), 2);
            UInt16 minorAlarms = BitConverter.ToUInt16(OperationBytes.InverseData(packet, 12, 2), 0);
            byte almanacStatus = Convert.ToByte((minorAlarms >> 11) & 1);
            byte gpsDecodingStatus = packet[14];
            OnGetReceiverCoordinates?.Invoke(this, new ReceiverCoordEventArgs(coords, almanacStatus, gpsDecodingStatus == 1? true:false));
        }


        // Satellite Ephemeris Status (0x5B)
        private void ParseEphemerisStatusInfo(List<byte> packet)
        {
            var status = new EphemerisStatus(packet);
            ephemerisStatusList.Add(status);

            if (ephemerisStatusList.Count == 32)
            {
                OnGetEphemerisStatus?.Invoke(this, ephemerisStatusList);
                ephemerisStatusList.Clear();
            }
        }
        #endregion

        byte[] bBufSave = null;

        #region Reading port

        //private void ReadData()
        //{
        //    // buffer for reading bytes
        //    byte[] bBufRead;
        //    bBufRead = new byte[MAX_SIZE];

        //    // buffer to transfer and save reading bytes
        //    bBufSave = null; // new byte[MAX_SIZE];

        //    // length of reading bytes
        //    int iReadByte = -1;
        //    int iTempLength = 0;

        //    // while port open
        //    while (true)
        //    {
        //        try
        //        {
        //            bBufRead = new byte[MAX_SIZE];
        //            // clear bufer of reading
        //            try
        //            {
        //                Array.Clear(bBufRead, 0, bBufRead.Length);
        //            }
        //            catch(Exception e)
        //            { Message?.Invoke(this, "P1, Error: " + e); }
        //            // read data
        //            try
        //            {
        //                iReadByte = _port.Read(bBufRead, 0, MAX_SIZE);
        //            }
        //            catch (Exception e)
        //            { 
        //                DisconnectPrivate(); 
        //                Message?.Invoke(this, "P2, Error: " + e);
        //                //return;
        //            }

        //            // if bytes was read
        //            if (iReadByte > 0)
        //            {
        //                try
        //                {
        //                    // resize buffer for real size of reading bytes
        //                    Array.Resize(ref bBufRead, iReadByte);

        //                    //// Событие чтения байт
        //                    //OnReadByte?.Invoke(this, new ListByteEventArgs(bBufRead.ToList()));

        //                    if (bBufSave == null)
        //                        bBufSave = new byte[iReadByte];
        //                    else
        //                        Array.Resize(ref bBufSave, bBufSave.Length + iReadByte);

        //                    // copy buffer for read to buffer for save
        //                    Array.Copy(bBufRead, 0, bBufSave, iTempLength, iReadByte);

        //                    // set current value of saving bytes
        //                    iTempLength += iReadByte;
        //                }
        //                catch (System.Exception e)
        //                {
        //                    Message?.Invoke(this, "P3, Error: " + e);
        //                    Disconnect();
        //                }

        //                var param_cmd = FindCmd(bBufSave);

        //                while (param_cmd.Item1 > -1 && param_cmd.Item2 > 0)
        //                {
        //                    try
        //                    {

        //                        byte[] bDecode = new byte[param_cmd.Item2];


        //                        try
        //                        {
        //                            Array.Copy(bBufSave, param_cmd.Item1, bDecode, 0, param_cmd.Item2);
        //                        }
        //                        catch (Exception e)
        //                        { Message?.Invoke(this, "P4, Error: " + e); }

        //                        try
        //                        {
        //                            DecodeCmd(bDecode);
        //                        }
        //                        catch (Exception e)
        //                        { Message?.Invoke(this, "P5, Error: " + e); }
        //                        //if (param_cmd.Item1 != 0)
        //                        //{
        //                        //    var t = 8;
        //                        //}
        //                        try
        //                        {
        //                            Array.Reverse(bBufSave);
        //                            Array.Resize(ref bBufSave, bBufSave.Length - (param_cmd.Item1 + param_cmd.Item2));
        //                            Array.Reverse(bBufSave);
        //                        }
        //                        catch (Exception e)
        //                        { Message?.Invoke(this, "P6, Error: " + e); }
        //                        try
        //                        {
        //                            iTempLength = iTempLength - (param_cmd.Item1 + param_cmd.Item2);
        //                        }
        //                        catch (Exception ex)
        //                        { Message?.Invoke(this, "P7, Error: " + ex); }

        //                        try
        //                        {
        //                            param_cmd = FindCmd(bBufSave);
        //                        }
        //                        catch (Exception e)
        //                        { Message?.Invoke(this, "P8, Error: " + e); }

        //                    }
        //                    catch (System.Exception ex)
        //                    { Message?.Invoke(this, "P9, Error: " + ex); Disconnect(); }
        //                }
        //            }
        //            else
        //            { // close port
        //                Message?.Invoke(this, "P10");
        //                Disconnect();
        //            }
        //        }
        //        catch (System.Exception ee)
        //        {
        //            Message?.Invoke(this, "P11, Error: " + ee);
        //            Disconnect();
        //        }
        //    }
        //}

        //определение границ целой кодограммы (новый) с конца


        private void ReadData()
        {
            // buffer for reading bytes
            byte[] bBufRead;
            bBufRead = new byte[MAX_SIZE];

            // buffer to transfer and save reading bytes
            byte[] new_bufSave = null;
            bBufSave = null; // new byte[MAX_SIZE];

            // length of reading bytes
            int iReadByte = -1;
            int iTempLength = 0;

            // while port open
            while (true)
            {
                try
                {
                    bBufRead = new byte[MAX_SIZE];
                    // clear bufer of reading
                    try
                    {
                        Array.Clear(bBufRead, 0, bBufRead.Length);
                    }
                    catch (Exception e)
                    { Message?.Invoke(this, "P1, Error: " + e); }
                    // read data
                    try
                    {
                        iReadByte = _port.Read(bBufRead, 0, MAX_SIZE);
                    }
                    catch (Exception e)
                    {
                        DisconnectPrivate();
                        Message?.Invoke(this, "P2, Error: " + e);
                        //return;
                    }

                    // if bytes was read
                    if (iReadByte > 0)
                    {
                        try
                        {
                            // resize buffer for real size of reading bytes
                            Array.Resize(ref bBufRead, iReadByte);

                            //// Событие чтения байт
                            //OnReadByte?.Invoke(this, new ListByteEventArgs(bBufRead.ToList()));

                            if (bBufSave == null)
                                bBufSave = new byte[iReadByte];
                            else
                                Array.Resize(ref bBufSave, bBufSave.Length + iReadByte);

                            // copy buffer for read to buffer for save
                            Array.Copy(bBufRead, 0, bBufSave, iTempLength, iReadByte);

                            // set current value of saving bytes
                            iTempLength += iReadByte;
                        }
                        catch (System.Exception e)
                        {
                            Message?.Invoke(this, "P3, Error: " + e);
                            Disconnect();
                        }

                        var param_cmd = FindCmd(bBufSave);

                        while (param_cmd.Item1 > -1 && param_cmd.Item2 > 0)
                        {
                            try
                            {

                                byte[] bDecode = new byte[param_cmd.Item2];


                                try
                                {
                                    Array.Copy(bBufSave, param_cmd.Item1, bDecode, 0, param_cmd.Item2);
                                }
                                catch (Exception e)
                                { Message?.Invoke(this, "P4, Error: " + e); }

                                try
                                {
                                    if (bDecode.Length == 1)
                                    {
                                        var t = 8;
                                    }
                                    DecodeCmd(bDecode);
                                }
                                catch (Exception e)
                                { Message?.Invoke(this, "P5, Error: " + e); }
                                //if (param_cmd.Item1 != 0)
                                //{
                                //    var t = 8;
                                //}
                                try
                                {
                                    new_bufSave = new byte[bBufSave.Length - param_cmd.Item2];
                                    Array.Copy(bBufSave, 0, new_bufSave, 0, param_cmd.Item1);
                                    Array.Copy(bBufSave, param_cmd.Item1 + param_cmd.Item2, new_bufSave, param_cmd.Item1, bBufSave.Length - param_cmd.Item1 - param_cmd.Item2);
                                }
                                catch (Exception e)
                                { Message?.Invoke(this, "P6, Error: " + e); }
                                try
                                {
                                    iTempLength = new_bufSave.Length; //хз зачем
                                }
                                catch (Exception ex)
                                { Message?.Invoke(this, "P7, Error: " + ex); }

                                try
                                {
                                    param_cmd = FindCmd(new_bufSave);
                                }
                                catch (Exception e)
                                { Message?.Invoke(this, "P8, Error: " + e); }


                                Array.Resize(ref bBufSave, new_bufSave.Length);
                                Array.Copy(new_bufSave, bBufSave, new_bufSave.Length);
                            }
                            catch (System.Exception ex)
                            { Message?.Invoke(this, "P9, Error: " + ex); Disconnect(); }
                        }

                        if (bBufSave.Length > 0 && param_cmd.Item2 > 0)
                        {
                            try
                            {
                                Array.Reverse(bBufSave);
                                Array.Resize(ref bBufSave, bBufSave.Length - param_cmd.Item2);
                                Array.Reverse(bBufSave);
                            }
                            catch (Exception e)
                            { }
                        }
                    }
                    else
                    { // close port
                        Message?.Invoke(this, "P10");
                        Disconnect();
                    }
                }
                catch (System.Exception ee)
                {
                    Message?.Invoke(this, "P11, Error: " + ee);
                    Disconnect();
                }
            }
        }

        private Tuple<int, int> FindCmd(byte[] bArray)
        {
            //bool stopSearch = false;
            //byte[] fullStop = new byte[2] { 0x10, 0x03 };

            int len = -1;
            int firstBt = -1;
            bool foundEnd = false;

            if (bArray.Length < 3)
            { return Tuple.Create<int, int>(firstBt, len); }


            int lastBt = bArray.Length - 1;
            while (lastBt > 1) //почему -1. мб >1
            {
                if (bArray[lastBt] == bStop && bArray[lastBt - 1] == bStart)
                {
                    foundEnd = true;
                    firstBt = lastBt - 2;
                    while (firstBt > -1)
                    {
                        if (bArray[firstBt] == bStart)
                        {
                            if (firstBt == 0 || bArray[firstBt - 1] != bStart) // проверить, чтобы не заходило во второе условие при выполнении первого
                            {
                                if (lastBt - firstBt > 2)
                                {
                                    len = lastBt - firstBt + 1;
                                    return Tuple.Create<int, int>(firstBt, len);
                                }
                                else 
                                {
                                    lastBt -= 2;
                                    firstBt = -1;
                                    foundEnd = false;
                                    //выйти из цикла first = -1, last = -1
                                }
                            }
                            else
                            {
                                firstBt -= 2;
                            }
                        }
                        else 
                        {
                            firstBt -= 1;
                        }
                    }

                    if (foundEnd)
                    {
                        return Tuple.Create<int, int>(firstBt, lastBt + 1); //есть конец, но нет начала. мб как-то удалять такой кусок
                    }
                }
                lastBt--;
            }

            return Tuple.Create<int, int>(firstBt, len);
        }

        //определение границ целой кодограммы
        //private Tuple<int, int> FindCmd(byte[] bArray)
        //{
        //    bool stopSearch = false;
        //    byte[] fullStop = new byte[2] { 0x10, 0x03 };

        //    int len = -1;
        //    if (bArray.Length == 1)
        //    { var u = 9; }

        //    int pos = Array.IndexOf(bArray, bStart);
        //    if (pos == -1)
        //    {
        //        return Tuple.Create<int, int>(pos, len);
        //    }


        //    while (bArray.Length > pos+1 && ( bArray[pos + 1] == bStop || bArray[pos + 1] == bStart))
        //    {
        //        pos = Array.IndexOf(bArray, bStart, pos+1);
        //    }

        //    if (bArray.Length < pos + 4)
        //    {
        //        return Tuple.Create<int, int>(pos, len);
        //    }

        //    int lastIndex = Array.IndexOf(bArray, bStart, pos + 2);
        //    int nextlast = lastIndex + 1;
        //    if (pos>-1 && lastIndex > -1)
        //    {
        //        while (!stopSearch&& nextlast < bArray.Length)
        //        {
        //            if (bArray.Length > nextlast && bArray[nextlast-1]==bStart && bArray[nextlast] == bStop)
        //            {
        //                len = nextlast - pos + 1;
        //                stopSearch = true;
        //            }
        //            else
        //            { nextlast++; }
        //        }
        //    }
        //    return Tuple.Create<int, int>(pos, len);
        //}

        //Удаляем удвоение 0x10 в области данных


        private byte[] DeleteByteReply(byte[] bCmd)
        {
            for (int i = 1; i < bCmd.Length; i++)
            {
                if (bCmd[i] == bStart && bCmd[i - 1] == bStart)
                {
                    byte[] newArray = new byte[bCmd.Length - 1];
                    Array.Copy(bCmd, 0, newArray, 0, i);
                    Array.Copy(bCmd, i + 1, newArray, i, bCmd.Length - 1 - i);
                    Array.Resize(ref bCmd, newArray.Length);
                    Array.Copy(newArray, 0, bCmd, 0, newArray.Length);
                }
            }
            return bCmd;
        }

        //определение вида кодограммы
        private void DecodeCmd(byte[] bCmd)
        {
            try
            {
                bCmd = DeleteByteReply(bCmd);
            }
            catch { }

            byte Code = bCmd[1];
            byte Code2 = bCmd[2];

            switch (Code)
            {
                case DATE_TIME:
                    if (bCmd.Length == 14)
                    {
                        ParseTimeInfo(bCmd.Cast<byte>().ToList());
                    }
                    canSendSats = true;
                    break;

                case COORD1:
                    if (Code2 == COORD2 && bCmd.Length == 72)
                    {  ParseCoord(bCmd.Cast<byte>().ToList()); }
                     else
                    { OnUnknownPocket?.Invoke(this, new ListByteEventArgs(bCmd.Cast<byte>().ToList())); }
                    
                    canSendSats = true;
                    break;

                case PARAM_SATELITE:
                    if (bCmd.Length == 28)
                    {
                        ParseSatInfo(bCmd.Cast<byte>().ToList());
                    }
                    canSendSats = false;
                    break;

                case FIXED_POS:
                    if (bCmd.Length > 20)
                    {
                        ParseFixPos(bCmd.Cast<byte>().ToList());
                    }
                    canSendSats = true;
                    break;
                case FIXED_POS_MINI:
                    if (bCmd.Length > 21)
                    {
                        ParseFixPosMini(bCmd.Cast<byte>().ToList());
                    }
                    canSendSats = true;
                    break;
                case SAT_SNR:
                    
                     ParseSatSNR(bCmd.Cast<byte>().ToList());
                   
                    canSendSats = true;
                    break;

                case ALMANAH_EPHEMERIS:
                    if (bCmd[3] == 0x06)
                    {
                        if (bCmd.Length == 175)
                        { ParseEphInfo(bCmd.Cast<byte>().ToList()); }
                        //else
                        //{  }
                        //Message?.Invoke(this, "Get Eph length=" + bCmd.Length + ", cmd = " + BitConverter.ToString(bCmd.ToArray()));
                    }
                    else if (bCmd[3] == 0x02)
                    {
                        if (bCmd.Length == 74)
                        { ParseAlmInfo(bCmd.Cast<byte>().ToList()); }
                        //else 
                        //{  }
                        //Message?.Invoke(this, "Get Alm length=" + bCmd.Length + ", cmd = " + BitConverter.ToString(bCmd.ToArray()));
                    }
                    else if (bCmd[3] == 0x04)
                    {
                        if (bCmd.Length == 48)
                        { this.ParseIonosphere(bCmd.Cast<byte>().ToList()); }
                    }
                    else if (bCmd[3] == 0x05)
                    {
                        if (bCmd.Length == 47)
                        { this.ParseUTS(bCmd.Cast<byte>().ToList()); }
                    }
                    else { OnUnknownPocket?.Invoke(this, new ListByteEventArgs(bCmd.Cast<byte>().ToList())); }
                    canSendSats = true;
                    break;
                case EPHEMERIS_STATUS:
                    if (bCmd.Length == 20)
                    {
                        this.ParseEphemerisStatusInfo(bCmd.Cast<byte>().ToList());
                    }
                    canSendSats = true;
                    break;

                default:
                    OnUnknownPocket?.Invoke(this, new ListByteEventArgs(bCmd.Cast<byte>().ToList()));
                    if (Code != 0xAA)
                    {
                        canSendSats = true;
                    }
                    break;
            }
            OnReadByte?.Invoke(this, new ListByteEventArgs(bCmd.ToList()));
        }


        private void CheckSatsToShow()
        {
            if (satInfo.Count == 0)
            { 
                return; 
            }

            OnGetSatInfo?.Invoke(this, new SatInfoEventArgs(satInfo, fixedSat));
            satInfo.Clear();

        }
        #endregion
    }
}
