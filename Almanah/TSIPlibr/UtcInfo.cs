﻿namespace TSIP
{
    public struct UtcInfo
    {
        public double A0 { get; set; }
        public float A1 { get; set; }
        public float Tot { get; set; }
        public short DeltaTLs { get; set; }
        public ushort WeekNumT { get; set; }

        public UtcInfo(double a0, float a1, float tot, short deltaTLs, ushort weekNumT)
        {
            A0 = a0;
            A1 = a1;
            Tot = tot;
            DeltaTLs = deltaTLs;
            WeekNumT = weekNumT;
        }
    }
}
