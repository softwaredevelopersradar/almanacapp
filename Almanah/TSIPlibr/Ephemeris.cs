﻿using System;

namespace TSIP
{
    public class Ephemeris
    {
        public byte sv_number;          // 0
        public Single t_ephem;          // 1 ---
        public UInt16 weeknum;          // 2
        public byte codeL2;             // 3
        public byte L2Pdata;            // 4
        public byte SVacc_raw;          // 5 ---
        public byte SV_health;          // 6
        public UInt16 IODC;             // 7
        public Single T_GD;             // 8
        public Single t_oc;             // 9
        public Single a_f2;             // 10
        public Single a_f1;             // 11
        public Single a_f0;             // 12
        public Single SVacc;            // 13
        public byte IODE;               // 14
        public byte fit_interval;       // 15
        public Single C_rs;             // 16
        public Single delta_n;          // 17
        public double M_0;              // 18
        public Single C_uc;             // 19
        public double e;                // 20
        public Single C_us;             // 21
        public double sqrt_A;           // 22
        public Single t_oe;             // 23
        public Single C_ic;             // 24
        public double OMEGA_0;          // 25
        public Single C_is;             // 26
        public double i_o;              // 27
        public Single C_rc;             // 28
        public double omega;            // 29
        public Single OMEGADOT;         // 30
        public Single IDOT;             // 31
        public double Axis;             // 32 --- sqrt_A^2
        public double n;                // 33 ---
        public double r1me2;            // 34 ---
        public double OMEGA_n;          // 35 ---
        public double ODOT_n;


        public bool hasSNR;
        public Ephemeris()
        { }
    }
}
