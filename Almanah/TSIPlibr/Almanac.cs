﻿using System;

namespace TSIP
{
    public class Almanac
    {
        public byte PRN;

        public byte t_oa_raw;
        public byte SV_HEALTH;
        public Single e;
        public Single t_oa;
        public Single i_o;
        public Single OMEGADOT;
        public Single sqrt_A;
        public Single OMEGA_0;
        public Single omega;
        public Single M_0;
        public Single a_f0;
        public Single a_f1;
        public Single Axis;
        public Single n;
        public Single OMEGA_n;
        public Single ODOT_n;
        public Single t_zc;
        public UInt16 weeknum;
        public UInt16 wn_oa;

        public Almanac()
        { }
    }
}
