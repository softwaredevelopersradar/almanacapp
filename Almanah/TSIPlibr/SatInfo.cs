﻿using System;

namespace TSIP
{
    public class SatInfo
    {
        public int PRN { get; set; } = -1;
        public Single SNR { get; set; } = -1;
        public Single Azimuth { get; set; } = -1;
        public Single Elevation { get; set; } = -1;

        public byte AcquisitionFlag { get; set; }
        public int TrueAcquisitionFlag { get; set; } = -1;
        public int EphemerisFlag { get; set; } = -1;
        public int OldMeasureFlag { get; set; } = -1;
        public int BadDataFlag { get; set; } = -1;

        public SatInfo(int prn, Single snr, Single azimuth, Single elevation, byte acquisitionFlag, int trueAcquisition, int ephemerisFlag, int oldMeasureFlag, int badDataFlag)
        {
            PRN = prn;
            SNR = snr;
            Azimuth = azimuth;
            Elevation = elevation;
            AcquisitionFlag = acquisitionFlag;
            TrueAcquisitionFlag = trueAcquisition;
            EphemerisFlag = ephemerisFlag;
            OldMeasureFlag = oldMeasureFlag;
            BadDataFlag = badDataFlag;
        }

        public SatInfo()
        {
        }
    }
}
