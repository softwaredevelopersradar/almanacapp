﻿using System;
using System.Collections.Generic;

namespace TSIP
{
    public class AlmanacEventArgs : EventArgs
    {
        public List<Almanac> almanacs { get; set; }

        public AlmanacEventArgs (List<Almanac> almanacs)
        {
            this.almanacs = almanacs;
        }
    }
}
