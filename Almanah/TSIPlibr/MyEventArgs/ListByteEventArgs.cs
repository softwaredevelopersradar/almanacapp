﻿using System;
using System.Collections.Generic;

namespace TSIP
{
    public class ListByteEventArgs : EventArgs
    {
        public List<byte> Data { get; set; }

        public ListByteEventArgs(List<byte> data)
        {
            Data = data;
        }
    }
}
