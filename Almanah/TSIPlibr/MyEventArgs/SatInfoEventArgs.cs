﻿using System;
using System.Collections.Generic;

namespace TSIP
{
    public class SatInfoEventArgs : EventArgs
    {
        public List<SatInfo> Sattelites { get; set; }
        public List<byte> FixedSats { get; set; }

        public SatInfoEventArgs(List<SatInfo> sattelites, List<byte> fixedSats)
        {
            Sattelites = sattelites;
            FixedSats = fixedSats;
        }
    }
}
