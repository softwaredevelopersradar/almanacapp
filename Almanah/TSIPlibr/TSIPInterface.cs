﻿using System;

namespace TSIP
{
    public interface ITSIPInterface
    {
        void Connect(string Port_number, int Port_rate);
        void Disconnect();
        void GetEphemeries(string ephemeridespath);
        void GetAlmanac(string almanacpath);
        void GetTimeInfo();
        void GetSatInfo();
        
        event EventHandler<string> OnConnect;
        event EventHandler<bool> OnDisconnect;
        event EventHandler<ListByteEventArgs> OnReadByte;
        event EventHandler<ListByteEventArgs> OnWriteByte;
        event EventHandler<TimeEventArgs> OnGetTime;
        event EventHandler<ReceiverCoordEventArgs> OnGetReceiverCoordinates;
        event EventHandler<SatInfoEventArgs> OnGetSatInfo;
        event EventHandler<AlmanacEventArgs> OnGetAlmanac;
        event EventHandler<EphemerisEventArgs> OnGetEphemeries;

    }
}
