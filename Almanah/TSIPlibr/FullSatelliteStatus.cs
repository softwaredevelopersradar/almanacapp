﻿using System;
using System.Collections.Generic;

namespace TSIP
{
    public class FullSatelliteStatus
    {
        public int PRN { get; set; } = -1;
        public byte AcquisitionFlag { get; set; } = 0; 
        public byte EphemerisFlag { get; set; } = 0;
        public float SNR { get; set; } = -1;
        public float TimeOfLastMeasure { get; set; } = -1;
        public float Elevation { get; set; } = -1;
        public float Azimuth { get; set; } = -1;
        public byte OldMeasureFlag { get; set; } = 0;
        public byte IntegerMsecFlag { get; set; } = 0;
        public byte BadDataFlag { get; set; } = 0;
        public byte DataCollictionFlag { get; set; } = 0;

        public FullSatelliteStatus(byte[] data)
        {
            if (data == null || data.Length != 28)
                return;

            PRN = data[2];
            AcquisitionFlag = data[4];
            EphemerisFlag = data[5];
            SNR = BitConverter.ToSingle(OperationBytes.InverseData(data, 6, 4), 0);
            TimeOfLastMeasure = BitConverter.ToSingle(OperationBytes.InverseData(data, 10, 4), 0);
            Elevation = (Single)Math.Round(BitConverter.ToSingle(OperationBytes.InverseData(data, 14, 4), 0) * 180 / Math.PI, 2);
            Azimuth = (Single)Math.Round(BitConverter.ToSingle(OperationBytes.InverseData(data, 18, 4), 0) * 180 / Math.PI, 2);
            OldMeasureFlag = data[22];
            IntegerMsecFlag = data[23];
            BadDataFlag = data[24];
            DataCollictionFlag = data[25];
        }


        public FullSatelliteStatus(List<byte> data)
        {
            if (data == null || data.Count != 28)
                return;

            PRN = data[2];
            AcquisitionFlag = data[4];
            EphemerisFlag = data[5];
            SNR = BitConverter.ToSingle(OperationBytes.InverseData(data, 6, 4), 0);
            TimeOfLastMeasure = BitConverter.ToSingle(OperationBytes.InverseData(data, 10, 4), 0);
            Elevation = (Single)Math.Round(BitConverter.ToSingle(OperationBytes.InverseData(data, 14, 4), 0) * 180 / Math.PI, 2);
            Azimuth = (Single)Math.Round(BitConverter.ToSingle(OperationBytes.InverseData(data, 18, 4), 0) * 180 / Math.PI, 2);
            OldMeasureFlag = data[22];
            IntegerMsecFlag = data[23];
            BadDataFlag = data[24];
            DataCollictionFlag = data[25];
        }

        public FullSatelliteStatus()
        {
        }
    }
}
