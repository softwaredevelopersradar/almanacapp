﻿using System;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;


namespace Logger
{
    /// <summary>
    /// Interaction logic for Logger.xaml
    /// </summary>
    public partial class Logger : UserControl
    {
        FlowDocument myFlowDoc;

        public Logger()
        {
            InitializeComponent();

            myFlowDoc = new FlowDocument();
            DataContext = this;
        }

        public void PrintText(string text)
        {
            try
            {
                string forCommit = "";
                forCommit = DateTime.Now.ToString("hh:mm:ss.ff") + "  ";
                TextRange rangeOfText1 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
                rangeOfText1.Text = forCommit;
                rangeOfText1.ApplyPropertyValue(TextElement.ForegroundProperty, "#FF27E2C8");
                //forCommit += text;

                TextRange rangeOfText2 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
                rangeOfText2.Text = text + "\n";
                rangeOfText2.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.White);

                rtbServer.ScrollToEnd();
            }
            catch { }
        }

        public void PrintTextTest()
        {
            try
            {
                TipeTime();
                //forCommit += text;

                TextRange rangeOfText2 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
                rangeOfText2.Text = "Switch on jamming ST2" + "\n";
                rangeOfText2.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.White);

                Thread.Sleep(200);
                TipeTime();
                TextRange rangeOfText3 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
                rangeOfText3.Text = "Switch off jamming ST2" + "\n";
                rangeOfText3.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.White);

                Thread.Sleep(300);
                TipeTime();
                TextRange rangeOfText4 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
                rangeOfText4.Text = "Switch on jamming ST6" + "\n";
                rangeOfText4.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.White);

                Thread.Sleep(700);
                TipeTime();
                TextRange rangeOfText5 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
                rangeOfText5.Text = "Switch off jamming ST6" + "\n";
                rangeOfText5.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.White);

                Thread.Sleep(1000);
                TipeTime();
                TextRange rangeOfText6 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
                rangeOfText6.Text = "ST8 Error" + "\n";
                rangeOfText6.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Red);

                Thread.Sleep(3000);
                TipeTime();
                TextRange rangeOfText7 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
                rangeOfText7.Text = "Switch on jamming ST1" + "\n";
                rangeOfText7.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.White);

                Thread.Sleep(5000);
                TipeTime();
                TextRange rangeOfText8 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
                rangeOfText8.Text = "Switch off jamming ST1" + "\n";
                rangeOfText8.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.White);


                Thread.Sleep(1000);
                TipeTime();
                TextRange rangeOfText9 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
                rangeOfText9.Text = "ST8 Error" + "\n";
                rangeOfText9.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Red);

                rtbServer.ScrollToEnd();
            }
            catch { }
        }

        private void TipeTime()
        {
            string forCommit = "";
            forCommit = DateTime.Now.ToString("hh:mm:ss.ff") + "  ";
            TextRange rangeOfText1 = new TextRange(rtbServer.Document.ContentEnd, rtbServer.Document.ContentEnd);
            rangeOfText1.Text = forCommit;
            rangeOfText1.ApplyPropertyValue(TextElement.ForegroundProperty, "#FF27E2C8");
        }

        public void Clear()
        {
            try
            {
                rtbServer.Document.Blocks.Clear();
            }
            catch { }
        }
    }
}
