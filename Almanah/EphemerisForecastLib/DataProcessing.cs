﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EphemerisForecastLib
{
    internal static class DataProcessing
    {
        // Функция расчета правых частей дифферендиальных уравнений
        private static double[] RightPartCalculating(double[] vektorCoord, double[] vektorAcceleration)
        {
            var vector1 = new CoordVelocityVector() { X = vektorCoord[0], Y = vektorCoord[1], Z = vektorCoord[2], Vx = vektorCoord[3], Vy = vektorCoord[4], Vz = vektorCoord[5] };
            var vector2 = new AccelerationVector() { ax = vektorAcceleration[0], ay = vektorAcceleration[1], az= vektorAcceleration[2] };

            //var r = Math.Sqrt(vector1.X * vector1.X + vector1.Y * vector1.Y + vector1.Z * vector1.Z);
            var r = Math.Sqrt(Math.Pow(vector1.X, 2) + Math.Pow(vector1.Y ,2) + Math.Pow(vector1.Z,2));
            var SinFi = vector1.Z / r;

            var Sin2Fi = Math.Pow(SinFi, 2d);
            var r2 = Math.Pow(r, 2d);

           var Br = (-Constants.B0 - 1.5d * Constants.B2 * (5d * Sin2Fi - 1d) / r2) / r2;
           var Bxo = Br * vector1.X / r;
           var Byo = Br * vector1.Y / r;
           var Bzo = SinFi * (Br + 3d * Constants.B2 / r2 / r2);
           var Br1 = -1.875d * Constants.B4 * (7d * Sin2Fi * (3d * Sin2Fi - 2d) + 1d) / (r2 * r2 * r2);
           var Bx = Bxo + Br1 * vector1.X / r;
           var By = Byo + Br1 * vector1.Y / r;
           var Bz = Bzo + SinFi * (Br1 + 2.5d * Constants.B4 * (7d * Sin2Fi - 3d) / (r2 * r2 * r2));

            var resultVector = new double[6];

            resultVector[0] = vector1.Vx;
            resultVector[1] = vector1.Vy;
            resultVector[2] = vector1.Vz;
            resultVector[3] = Bx + 2d * Constants.OmegaZ * vector1.Vy + vector1.X * Math.Pow(Constants.OmegaZ, 2) + vector2.ax;
            resultVector[4] = By - 2d * Constants.OmegaZ * vector1.Vx + vector1.Y * Math.Pow(Constants.OmegaZ, 2) + vector2.ay;
            resultVector[5] = Bz + vector2.az;

            return resultVector;
        }


        // Функция интегрирования вектора координат и скоростей методом Рунге-Кутта
        private static (double[], bool) RungeKuttMethod(double[] vektorCoord, double[] vektorAcceleration, int step)
        {
            var PromVector1 = new double[6];
            var PromVector2 = new double[6];
            var PromVector3 = new double[6];
            var PromVector4 = new double[6];
            var PromVector5 = new double[6];
            var VectorPromZnach = new double[6];
            var VectorIntegrated = new double[6];
            var Erun = new double[6];

            var VectordX = RightPartCalculating(vektorCoord, vektorAcceleration);
            for (int i=0; i< 6; i++)
            {
                PromVector1[i] = (double)step / 3d * VectordX[i];
                VectorPromZnach[i] = vektorCoord[i] + PromVector1[i];
            }

            VectordX = RightPartCalculating(VectorPromZnach, vektorAcceleration);
            for (int i = 0; i < 6; i++)
            {
                PromVector2[i] = (double)step / 3d * VectordX[i];
                VectorPromZnach[i] = vektorCoord[i] + PromVector1[i] / 2d + PromVector2[i] / 2d;
            }

            VectordX = RightPartCalculating(VectorPromZnach, vektorAcceleration);
            for (int i = 0; i < 6; i++)
            {
                PromVector3[i] = (double)step / 3d * VectordX[i];
                VectorPromZnach[i] = vektorCoord[i] + 3d / 8d * PromVector1[i] + 9d / 8d * PromVector3[i];
            }

            VectordX = RightPartCalculating(VectorPromZnach, vektorAcceleration);
            for (int i = 0; i < 6; i++)
            {
                PromVector4[i] = (double)step / 3d * VectordX[i];
                VectorPromZnach[i] = vektorCoord[i] + 3d / 2d * PromVector1[i] + 6d * PromVector4[i] - 9d / 2d * PromVector3[i];
            }

            VectordX = RightPartCalculating(VectorPromZnach, vektorAcceleration);
            for (int i = 0; i < 6; i++)
            {
                PromVector5[i] = (double)step / 3d * VectordX[i];
                VectorIntegrated[i] = vektorCoord[i] + (PromVector1[i] + 4d * PromVector4[i] + PromVector5[i]) / 2d;
            }

            
            var prErun = false;                             // оценка локальной ошибки
            for (int i = 0; i < 6; i++)
            {
                Erun[i] = Math.Abs(1d / 5d * (PromVector1[i] - 9d / 2d * PromVector3[i] + 4d * PromVector4[i] - 1d / 2d * PromVector5[i]));
                if (Erun[i] >= 1)
                    prErun = true;
            }

            return (VectorIntegrated, prErun);
        }




        //Продедура прогноза положения НКА Глонасс(интегрирование вперёд)
        internal static EphemerisGlonassData CountPrognoz(EphemerisGlonassData InStructGlonass, int PrognozTime)
        { 
            var StartTime = InStructGlonass.Time;
            var FinishTime = StartTime + PrognozTime;

            var VectorKoordSkor = new double[6] {  InStructGlonass.PosX, InStructGlonass.PosY, InStructGlonass.PosZ, InStructGlonass.VelocityXDot, InStructGlonass.VelocityYDot, InStructGlonass.VelocityZDot};
            var VectorUskor = new double[3] { InStructGlonass.AccelerationX, InStructGlonass.AccelerationY, InStructGlonass.AccelerationZ };
           
            //InStructGlonass.PrProg = "False";
            int StepProm = 100;
            var CountSteps = 0;

            //% интегрирование no 6 параметрам
            while (StartTime < FinishTime) 
            {
                if ((StartTime + StepProm) > FinishTime)        // проверка чтобы не перескочили конечное время
                {
                    if (StartTime != FinishTime)
                        StepProm = FinishTime - StartTime;
                }


                var (VectorIntegrated, PrErun) = RungeKuttMethod(VectorKoordSkor, VectorUskor, StepProm);

                while (PrErun)                    // Если взведен флаг ошибки - делим шаг интегрирования и по новой
                { 
                    StepProm = StepProm / 2;
                    (VectorIntegrated, PrErun) = RungeKuttMethod(VectorKoordSkor, VectorUskor, StepProm);
                }

                VectorKoordSkor = VectorIntegrated;
                StartTime = StartTime + StepProm;
                CountSteps = CountSteps + 1;          // проверяем не исчерпан ли лимит итерадий
        
                if (CountSteps > Constants.TopSteps)
                        break;
            }

            if (CountSteps <= Constants.TopSteps) // сохраняем результаты только в случае успешного интегрирования
                return new EphemerisGlonassData(VectorKoordSkor[0], VectorKoordSkor[1], VectorKoordSkor[2], VectorKoordSkor[3], VectorKoordSkor[4], VectorKoordSkor[5], InStructGlonass.AccelerationX, InStructGlonass.AccelerationY, InStructGlonass.AccelerationZ, StartTime);

            return null;

        }

        //Продедура прогноза положения НКА Глонасс(интегрирование назад)
        internal static EphemerisGlonassData CountPrognozNazad(EphemerisGlonassData InStructGlonass, int PrognozTime)
        {
            var StartTime = InStructGlonass.Time;
            var FinishTime = StartTime - PrognozTime;

            var VectorKoordSkor = new double[6] { InStructGlonass.PosX, InStructGlonass.PosY, InStructGlonass.PosZ, InStructGlonass.VelocityXDot, InStructGlonass.VelocityYDot, InStructGlonass.VelocityZDot };
            var VectorUskor = new double[3] { InStructGlonass.AccelerationX, InStructGlonass.AccelerationY, InStructGlonass.AccelerationZ };

            //InStructGlonass.PrProg = "False";
            int StepProm = -100;
            var CountSteps = 0;

            //% интегрирование no 6 параметрам
            while (StartTime > FinishTime)
            {
                if ((StartTime + StepProm) < FinishTime)        // проверка чтобы не перескочили конечное время
                {
                    if (StartTime != FinishTime)
                        StepProm = FinishTime - StartTime;
                }


                var (VectorIntegrated, PrErun) = RungeKuttMethod(VectorKoordSkor, VectorUskor, StepProm);

                while (PrErun)                    // Если взведен флаг ошибки - делим шаг интегрирования и по новой
                {
                    StepProm = StepProm / 2;
                    (VectorIntegrated, PrErun) = RungeKuttMethod(VectorKoordSkor, VectorUskor, StepProm);
                }

                VectorKoordSkor = VectorIntegrated;
                StartTime = StartTime + StepProm;
                CountSteps = CountSteps + 1;          // проверяем не исчерпан ли лимит итерадий

                if (CountSteps > Constants.TopSteps)
                    break;
            }

            if (CountSteps <= Constants.TopSteps) // сохраняем результаты только в случае успешного интегрирования
                return new EphemerisGlonassData(VectorKoordSkor[0], VectorKoordSkor[1], VectorKoordSkor[2], VectorKoordSkor[3], VectorKoordSkor[4], VectorKoordSkor[5], InStructGlonass.AccelerationX, InStructGlonass.AccelerationY, InStructGlonass.AccelerationZ, StartTime);

            return null;

        }

    }
}
