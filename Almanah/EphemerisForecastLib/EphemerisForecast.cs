﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EphemerisForecastLib
{
    public static class EphemerisForecast
    {
        public static EphemerisGlonassData ForecastGlonass(EphemerisGlonassData inputData, int forecastTimeSeconds)
        {
            if (forecastTimeSeconds < 0)
            {
                return DataProcessing.CountPrognozNazad(inputData, Math.Abs(forecastTimeSeconds));
            }

            return DataProcessing.CountPrognoz(inputData, forecastTimeSeconds);
        }

    }
}
