﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EphemerisForecastLib
{
    internal struct CoordVelocityVector
    {
        public double X;              
        public double Y;              
        public double Z;                
        public double Vx;      
        public double Vy;      
        public double Vz;      
    }


    internal struct AccelerationVector
    {
        public double ax;
        public double ay;
        public double az;
    }
}
