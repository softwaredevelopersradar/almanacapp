﻿

namespace EphemerisForecastLib
{
    public class EphemerisGlonassData
    {
        public double PosX;              // Satellite position X (m)
        public double PosY;              // Satellite position Y (m)
        public double PosZ;              // Satellite position Y (m)        
        public double VelocityXDot;      // velocity X dot (m/sec)
        public double VelocityYDot;      // velocity Y dot (m/sec)
        public double VelocityZDot;      // velocity Y dot (m/sec)
        public double AccelerationX;     // X acceleration (m/sec2)
        public double AccelerationY;     // Y acceleration (m/sec2)
        public double AccelerationZ;     // Y acceleration (m/sec2)
        public int Time;        // Время приема эфемерид (c) от начала суток

        public EphemerisGlonassData()
        { }

        public EphemerisGlonassData(double x, double y, double z, double Vx, double Vy, double Vz, double ax, double ay, double az, int time)
        {
            PosX = x;
            PosY = y;
            PosZ = z;
            VelocityXDot = Vx;
            VelocityYDot = Vy;
            VelocityZDot = Vz;
            AccelerationX = ax;
            AccelerationY = ay;
            AccelerationZ = az;
            Time = time;
        }
    }
}
