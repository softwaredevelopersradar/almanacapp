﻿

namespace EphemerisForecastLib
{
    internal static class Constants
    {
        public static readonly double B0 = 3.9860044e14;              // Коэффициенты разложения потенциала Земли
        public static readonly double B2 = -1.7555783049e25;          //в ряд по сферическим функциям
        public static readonly double B4 = 1.5468202494e36;
        public static readonly double OmegaZ = 7.2921151467e-05;      // Угловая скорость вращения Земли
        public static readonly double TopSteps = 5000;                // Максимальное количество шагов интегрирования
    }
}
