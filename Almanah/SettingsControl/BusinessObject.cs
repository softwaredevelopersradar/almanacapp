﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations;
using System;
using System.Linq;
using System.Collections.ObjectModel;


namespace SettingsControl
{
    [CategoryOrder("GNSS", 0)]
    [CategoryOrder("Server", 1)]
    [CategoryOrder("Common", 2)]
    public class BusinessObject : DependencyObject, INotifyPropertyChanged
    {
        #region Fields

        private int _comPortSpeed;
        private string _comPort;
        private string _serverIP;
        private int _serverPort;
        private int _intervalA;
        private int _intervalE;
        private string _pathA;
        private string _pathE;
        private string _timeZone;
        private bool _autoUpdateTime;
        private int _intervalTimeUpdate;
        private int _intervalCoordUpdate;
        private string _units;
        private bool _endlessMode;

        #endregion

        public BusinessObject()
        {
            _comPortSpeed = 9600;
            _comPort = "COM2";
            _serverIP = "127.0.0.10";
            _serverPort = 8888;
            _intervalA = 10;
            _intervalE = 10;
            _pathA = "E:\\KAKORENKO\\GNSS Data";
            _pathE = "E:\\KAKORENKO\\GNSS Data";
            _timeZone = TimeZoneInfo.Local.DisplayName;
            _intervalTimeUpdate = 1;
            _intervalCoordUpdate = 10;
            _units = "Format";
            _autoUpdateTime = false;
            _almanacFileName = "current.AL3";
            _ephemerisFileName = "current.15n";
            _satelliteFilter = true;
            _deviceType = 1;
            _language = Languages.RU;
            _endlessMode = true;
            _writeSatsLogToTxt = true;
            _waitForAlmanac = false;
        }



        [Category("Server")]
        [DisplayName(nameof(ServerIP))]
        [Required]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string ServerIP
        {
            get { return _serverIP; }
            set
            {
                value = value.Replace(@",", ".");
                if (_serverIP == value)
                    return;
                _serverIP = value;
                OnPropertyChanged();
            }
        }

        [Category("Server")]
        [DisplayName(nameof(ServerPort))]
        [Required]
        [Range(1, 1000000)]
        public int ServerPort
        {
            get { return _serverPort; }
            set
            {
                if (_serverPort == value)
                    return;
                _serverPort = value;
                OnPropertyChanged();
            }
        }

        [Category("GNSS")]
        [PropertyOrder(0)]
        [Required]
        [DisplayName(nameof(ComPort))]
        public string ComPort
        {
            get { return _comPort; }
            set
            {
                if (_comPort == value) return;
                _comPort = value;
                OnPropertyChanged();
            }
        }



        [Category("GNSS")]
        [PropertyOrder(1)]
        [Required]
        [DisplayName(nameof(ComPortSpeed))]
        public int ComPortSpeed
        {
            get { return _comPortSpeed; }
            set
            {
                if (_comPortSpeed == value)
                    return;
                _comPortSpeed = value;
                OnPropertyChanged();
            }
        }

        [Category("GNSS")]
        [PropertyOrder(2)]
        [Required]
        [DisplayName(nameof(AutoUpdateTime))]
        public bool AutoUpdateTime
        {
            get { return _autoUpdateTime; }
            set
            {
                if (_autoUpdateTime == value)
                    return;
                _autoUpdateTime = value;
                OnPropertyChanged();
            }
        }

        [Category("GNSS")]
        [PropertyOrder(3)]
        [DisplayName(nameof(TimeZone))]
        public string TimeZone
        {
            get {
                TimeZoneInfo.ClearCachedData();
                return TimeZoneInfo.Local.DisplayName;
                //if (timeZones.IndexOf(timeZones.Where(item => item.DisplayName == _timeZone).FirstOrDefault()) == -1)
                //{
                //    return timeZones[timeZones.IndexOf(timeZones.Where(item => item.Id == "UTC").FirstOrDefault())].DisplayName;
                //}
                //return _timeZone; 
            }
            set
            {
                //if (_timeZone == value)
                //    return;
                //TimeZoneInfo.ClearCachedData();
                //_timeZone = TimeZoneInfo.Local.DisplayName; 
                OnPropertyChanged();
            }
        }

        [Category("GNSS")]
        [PropertyOrder(4)]
        [DisplayName(nameof(IntervalTimeUpdate))]
        [Required]
        public int IntervalTimeUpdate
        {
            get { return _intervalTimeUpdate; }
            set
            {
                if (_intervalTimeUpdate == value)
                    return;
                _intervalTimeUpdate = value;
                OnPropertyChanged();
            }
        }


        [Category("Common")]
        [PropertyOrder(0)]
        [Required]
        [DisplayName(nameof(IntervalA))]
        public int IntervalA
        {
            get { return _intervalA; }
            set
            {
                if (_intervalA == value)
                    return;
                _intervalA = value;
                OnPropertyChanged();
            }
        }

        [Category("Common")]
        [PropertyOrder(1)]
        [Required]
        [DisplayName(nameof(IntervalE))]
        public int IntervalE
        {
            get { return _intervalE; }
            set
            {
                if (_intervalE == value)
                    return;
                _intervalE = value;
                OnPropertyChanged();
            }
        }

        [Category("Common")]
        [PropertyOrder(2)]
        [DisplayName(nameof(PathA))]
        public string PathA
        {
            get { return _pathA; }
            set
            {
                if (_pathA == value)
                    return;
                _pathA = value;
                OnPropertyChanged();
            }
        }

        [Category("Common")]
        [PropertyOrder(3)]
        [DisplayName(nameof(PathE))]
        public string PathE
        {
            get { return _pathE; }
            set
            {
                if (_pathE == value)
                    return;
                _pathE = value;
                OnPropertyChanged();
            }
        }


        


        [Category("Common")]
        [PropertyOrder(4)]
        [Required]
        [DisplayName(nameof(IntervalCoordUpdate))]
        public int IntervalCoordUpdate
        {
            get { return _intervalCoordUpdate; }
            set
            {
                if (_intervalCoordUpdate == value)
                    return;
                _intervalCoordUpdate = value;
                OnPropertyChanged();
            }
        }

        [Category("Common")]
        [PropertyOrder(5)]
        [DisplayName("Units")]
        [Browsable(false)]
        public string Units
        {
            get { return _units; }
            set
            {
                if (_units == value) return;
                _units = value;
                OnPropertyChanged();
            }
        }


        private string _almanacFileName;
        [Category("Common")]
        [Browsable(false)]
        public string AlmanacFileName
        {
            get { return _almanacFileName; }
            set
            {
                if (_almanacFileName == value) return;
                _almanacFileName = value;
                OnPropertyChanged();
            }
        }


        private string _ephemerisFileName;
        [Category("Common")]
        [Browsable(false)]
        public string EphemerisFileName
        {
            get { return _ephemerisFileName; }
            set
            {
                if (_ephemerisFileName == value) return;
                _ephemerisFileName = value;
                OnPropertyChanged();
            }
        }

        private bool _satelliteFilter;
        [Category("Common")]
        [Browsable(false)]
        public bool SatelliteFilter
        {
            get { return _satelliteFilter; }
            set
            {
                if (_satelliteFilter == value) return;
                _satelliteFilter = value;
                OnPropertyChanged();
            }
        }


        private byte _deviceType;
        [Category("Common")]
        [DisplayName("Device")]
        //[Browsable(false)]
        [PropertyOrder(5)]
        public byte DeviceType
        {
            get { return _deviceType; }
            set
            {
                if (_deviceType == value) return;
                _deviceType = value;
                OnPropertyChanged();
            }
        }


        private Languages _language;
        [Category("Common")]
        [PropertyOrder(6)]
        [DisplayName(nameof(Language))]
        public Languages Language
        {
            get { return _language; }
            set
            {
                if (_language == value) return;
                _language = value;
                OnPropertyChanged();
            }
        }


        [Category("Common")]
        [PropertyOrder(7)]
        [DisplayName(nameof(EndlessMode))]
        public bool EndlessMode
        {
            get { return _endlessMode; }
            set
            {
                if (_endlessMode == value) return;
                _endlessMode = value;
                OnPropertyChanged();
            }
        }


        private bool _writeSatsLogToTxt;
        [Category("Common")]
        [PropertyOrder(9)]
        [DisplayName(nameof(WriteSatsLogToTxt))]
        public bool WriteSatsLogToTxt
        {
            get { return _writeSatsLogToTxt; }
            set
            {
                if (_writeSatsLogToTxt == value) return;
                _writeSatsLogToTxt = value;
                OnPropertyChanged();
            }
        }


        private bool _waitForAlmanac;
        [Category("Common")]
        [PropertyOrder(10)]
        [DisplayName(nameof(WaitForAlmanac))]
        public bool WaitForAlmanac
        {
            get { return _waitForAlmanac; }
            set
            {
                if (_waitForAlmanac == value) return;
                _waitForAlmanac = value;
                OnPropertyChanged();
            }
        }
        #region IMethod

        public BusinessObject Clone()
        {
            return new BusinessObject()
            {
                ComPort = _comPort,
                ComPortSpeed = _comPortSpeed,
                ServerIP = _serverIP,
                ServerPort = _serverPort,
                IntervalA = _intervalA,
                IntervalE = _intervalE,
                PathA = _pathA,
                PathE = _pathE,
                TimeZone = TimeZoneInfo.Local.DisplayName,
                AutoUpdateTime = _autoUpdateTime,
                IntervalTimeUpdate = _intervalTimeUpdate,
                IntervalCoordUpdate = _intervalCoordUpdate,
                Units = _units,
                AlmanacFileName = _almanacFileName,
                EphemerisFileName = _ephemerisFileName,
                SatelliteFilter = _satelliteFilter,
                DeviceType = _deviceType,
                Language = _language,
                EndlessMode = _endlessMode,
                WriteSatsLogToTxt = _writeSatsLogToTxt,
                WaitForAlmanac = _waitForAlmanac
            };
        }

        public void Update(BusinessObject data)
        {
            ComPort = data.ComPort;
            ServerIP = data.ServerIP;
            ServerPort = data.ServerPort;
            ComPortSpeed = data.ComPortSpeed;
            IntervalA = data.IntervalA;
            IntervalE = data.IntervalE;
            PathA = data.PathA;
            PathE = data.PathE;
            TimeZone = data.TimeZone;
            AutoUpdateTime = data.AutoUpdateTime;
            IntervalTimeUpdate = data.IntervalTimeUpdate;
            IntervalCoordUpdate = data.IntervalCoordUpdate;
            Units = data.Units;
            AlmanacFileName = data.AlmanacFileName;
            EphemerisFileName = data.EphemerisFileName;
            SatelliteFilter = data.SatelliteFilter;
            DeviceType = data.DeviceType;
            Language = data.Language;
            EndlessMode = data.EndlessMode;
            WriteSatsLogToTxt = data.WriteSatsLogToTxt;
            WaitForAlmanac = data.WaitForAlmanac;
        }

        public bool Compare(BusinessObject data)
        {
            if (_serverIP != data.ServerIP || _serverPort != data.ServerPort
                || _comPort != data.ComPort || _comPortSpeed != data.ComPortSpeed
                || _intervalA != data.IntervalA || _intervalE != data.IntervalE
                || _pathA != data.PathA || _pathE != data.PathE || _timeZone != data.TimeZone
                || _autoUpdateTime != data.AutoUpdateTime
                || _intervalTimeUpdate != data.IntervalTimeUpdate || _intervalCoordUpdate != data.IntervalCoordUpdate
                || _units != data.Units || _almanacFileName != data.AlmanacFileName || _ephemerisFileName != data.EphemerisFileName
                || _satelliteFilter != data.SatelliteFilter || _deviceType !=data.DeviceType
                || _language != data.Language
                || _endlessMode !=data.EndlessMode
                || _writeSatsLogToTxt != data.WriteSatsLogToTxt
                || _waitForAlmanac != data.WaitForAlmanac) 
                return false;
            return true;
        }
        #endregion



        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName]string propertyName = "")
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
