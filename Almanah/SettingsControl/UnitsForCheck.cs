﻿using System.Collections.ObjectModel;

namespace SettingsControl
{
    class UnitsForCheck : ObservableCollection<string>
    {
        public UnitsForCheck()
        {
            Add("Test");
            Add("Format");
        }
    }
}
