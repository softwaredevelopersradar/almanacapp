﻿
using System.ComponentModel;

namespace SettingsControl
{
    public enum Languages : byte
    {
        [Description("Русский")]
        RU,
        [Description("English")]
        EN,
        [Description("Azərbaycan")]
        AZ
    }
}
