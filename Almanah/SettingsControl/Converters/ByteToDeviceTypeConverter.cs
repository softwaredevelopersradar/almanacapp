﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SettingsControl
{
    public class ByteToDeviceTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var t = (byte)value;
            return DeviceTypes.GetType(t);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = (string)value;
            return DeviceTypes.GetNum(s);
        }
    }
}
