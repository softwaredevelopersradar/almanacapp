﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace SettingsControl
{
    public class EditorLocal : PropertyEditor
    {
        Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        {
            { nameof(BusinessObject.ComPort), "ComPortsTemplate" },
            { nameof(BusinessObject.ComPortSpeed), "ComPortSpeeds" },
            { nameof(BusinessObject.PathA), "PathAData" },
            { nameof(BusinessObject.PathE), "PathEData" },
            //{ nameof(BusinessObject.TimeZone), "TimeZonesData" },
            { nameof(BusinessObject.IntervalTimeUpdate), "IntTime"},
            { nameof(BusinessObject.IntervalCoordUpdate), "IntParameters"},
            { nameof(BusinessObject.Units), "Units"},
            { nameof(BusinessObject.AutoUpdateTime), "AutoSyncTime"},
            { nameof(BusinessObject.DeviceType), "DeviceType"}
        };

        public EditorLocal(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/SettingsControl;component/Resources/Dictionary.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        }
        
    }
}
