﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Globalization;

namespace SettingsControl
{
    public class TimeZones : ObservableCollection<string>
    {
        public TimeZones()
        {
            List<string> timezonenames = new List<string>();
            DateTimeFormatInfo dateFormats = CultureInfo.CurrentCulture.DateTimeFormat;
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            //StreamWriter sw = new StreamWriter(OUTPUTFILENAME, false);

            foreach (TimeZoneInfo timeZone in timeZones)
            {
                //bool hasDST = timeZone.SupportsDaylightSavingTime;
                //TimeSpan offsetFromUtc = timeZone.BaseUtcOffset;
                //TimeZoneInfo.AdjustmentRule[] adjustRules;
                //string offsetString;

                //sw.WriteLine("ID: {0}", timeZone.Id);
                ////sw.WriteLine("   Display Name: {0, 40}", timeZone.DisplayName);
                timezonenames.Add(timeZone.DisplayName);

                if (this.Equals(timezonenames))
                    return;
                var except = timezonenames.Except(this).ToList();
                foreach (var name in except)
                    Add(name);
                except = this.Except(timezonenames).ToList();
                foreach (var name in except)
                    Remove(name);
                //sw.WriteLine("   Standard Name: {0, 39}", timeZone.StandardName);
                //sw.Write("   Daylight Name: {0, 39}", timeZone.DaylightName);

                //offsetString = String.Format("{0} hours, {1} minutes", offsetFromUtc.Hours, offsetFromUtc.Minutes);
            }
        }
    }
    
}
