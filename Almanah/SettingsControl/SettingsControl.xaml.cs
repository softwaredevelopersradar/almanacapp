﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;

namespace SettingsControl
{
    /// <summary>
    /// Логика взаимодействия для SettingsControl.xaml
    /// </summary>
    public partial class SettingsControl : UserControl
    {
        public SettingsControl()
        {
            InitializeComponent();
            InitEditors();
            InitProperties();
            SetLanguagePropertyGrid(MySettings.Language);
        }

        private BusinessObject oldSettings;

        public BusinessObject MySettings
        {
            get
            {
                var a = (BusinessObject)Resources["businessObject"];
                return a;
            }
            set
            {
                if ((BusinessObject)Resources["businessObject"] == value)
                {
                    oldSettings = value.Clone();
                    return;
                }
                ((BusinessObject)Resources["businessObject"]).Update(value);
                oldSettings = MySettings.Clone();
            }
        }

        private void InitEditors()
        {
            ProgrammSettings.Editors.Add(new EditorLocal(nameof(BusinessObject.ComPort), MySettings.GetType()));
            ProgrammSettings.Editors.Add(new EditorLocal(nameof(BusinessObject.ComPortSpeed), MySettings.GetType()));
            ProgrammSettings.Editors.Add(new EditorLocal(nameof(BusinessObject.PathA), MySettings.GetType()));
            ProgrammSettings.Editors.Add(new EditorLocal(nameof(BusinessObject.PathE), MySettings.GetType()));
            //ProgrammSettings.Editors.Add(new EditorLocal(nameof(BusinessObject.TimeZone), MySettings.GetType()));
            ProgrammSettings.Editors.Add(new EditorLocal(nameof(BusinessObject.IntervalTimeUpdate), MySettings.GetType()));
            ProgrammSettings.Editors.Add(new EditorLocal(nameof(BusinessObject.IntervalCoordUpdate), MySettings.GetType()));
            ProgrammSettings.Editors.Add(new EditorLocal(nameof(BusinessObject.AutoUpdateTime), MySettings.GetType()));
            ProgrammSettings.Editors.Add(new EditorLocal(nameof(BusinessObject.Units), MySettings.GetType()));
            ProgrammSettings.Editors.Add(new EditorLocal(nameof(BusinessObject.DeviceType), MySettings.GetType()));

        }

        private void InitProperties()
        {
            oldSettings = MySettings.Clone();
            foreach (var property in ProgrammSettings.Properties)
            {
                property.PropertyValue.PropertyValueException += MyPropertyException;
            }
            
            OnUpdateProperties += SettingsControl_OnUpdateProperties;
            MySettings.PropertyChanged += MySettings_PropertyChanged;
        }

        private void MySettings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(BusinessObject.Language):
                    SetLanguagePropertyGrid(MySettings.Language);
                    break;
                case nameof(BusinessObject.DeviceType):
                    OnUpdatedDeviceType?.Invoke(this, MySettings.DeviceType);
                    break;
                default:
                    break;
            }
        }


        public event EventHandler<byte> OnUpdatedDeviceType;

        private void SettingsControl_OnUpdateProperties(object sender, BusinessObject e)
        {
            SetLanguagePropertyGrid(MySettings.Language);
        }

        public event EventHandler<BusinessObject> OnUpdateProperties;

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            if (!oldSettings.Compare(MySettings))
            {
                oldSettings = MySettings.Clone();

                OnUpdateProperties?.Invoke(this, MySettings);
            }
        }

        private void NotApplyClick(object sender, RoutedEventArgs e)
        {
            if (!MySettings.Compare(oldSettings))
                MySettings.Update(oldSettings);
        }


        public void InitDeviceTypes(Dictionary<byte, string> types)
        {
            DeviceTypes.UpdateTypes(types);
        }


        #region Properties exceptions
        private void MyPropertyException(object sender, ValueExceptionEventArgs e)
        {
            ProgrammSettings.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }
        #endregion


        public void DoExpandORCollapse(string name)
        {
            
            var list = this.ProgrammSettings.Categories;
            foreach (CategoryItem item in list)
            {
                if (item.Name != name)
                    item.IsExpanded = false;
                else item.IsExpanded = true;
            }
        }

        public void SetLanguagePropertyGrid(Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            Translator.ChangeLanguagePropertyGrid(language, ProgrammSettings);
        }

        private void LoadTranslatorPropertyGrid(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/EphemerisAlmanac;component/Languages/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/EphemerisAlmanac;component/Languages/StringResource.RU.xaml",
                                            UriKind.Relative);
                        break;
                    case Languages.AZ:
                        dict.Source = new Uri("/EphemerisAlmanac;component/Languages/StringResource.AZ.xaml",
                                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/EphemerisAlmanac;component/Languages/StringResource.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
