﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace SatelliteTableControl
{
    internal class FullTableViewModel : DependencyObject, INotifyPropertyChanged
    {
        public ObservableCollection<FullSatellite> FullSatellites { get; set; } = new ObservableCollection<FullSatellite>();


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}
