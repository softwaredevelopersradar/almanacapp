﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SatelliteTableControl
{
    /// <summary>
    /// Interaction logic for SatFullInfoTable.xaml
    /// </summary>
    public partial class SatFullInfoTable : UserControl
    {
        public SatFullInfoTable()
        {
            InitializeComponent();
            this.DataContext = tableViewModel;
            AddEmptyRows();
        }

        private FullTableViewModel tableViewModel = new FullTableViewModel();
        public ObservableCollection<FullSatellite> Satellites
        {
            get => tableViewModel.FullSatellites;
            set
            {
                if (tableViewModel.FullSatellites == value)
                    return;

                var newDrones = value;


                tableViewModel.FullSatellites.Clear();
                //Pills.Concat<Pill>(someOtherPillCollection); TODO: переделать так
                foreach (FullSatellite recDrone in newDrones)
                {
                    tableViewModel.FullSatellites.Add(recDrone);
                }
                AddEmptyRows();
               // GC.Collect();
                //paramSatellite.Items.Refresh(); //надо ли?
                //GC.Collect(); GC.WaitForPendingFinalizers();
            }
        }
        


        private void DataGridparamSatellite_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }


        /// <summary>
        /// Добавление пустых строк в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                //paramSatellite.Items.Refresh(); //надо ли?
                int сountRowsAll = fullParamSatellite.Items.Count; // количество имеющихся строк в таблице
                double hs = 23; // высота строки
                double ah = fullParamSatellite.ActualHeight; // визуализированная высота dataGrid
                double chh = fullParamSatellite.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++) //мб переставить циклы местами
                {
                    index = tableViewModel.FullSatellites.ToList().FindIndex(x => x.PRN < 0);
                    if (index != -1)
                    {
                        tableViewModel.FullSatellites.RemoveAt(index);
                    }
                }
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    tableViewModel.FullSatellites.Add(new FullSatellite());
                }
            }
            catch { }
        }

    }
}
