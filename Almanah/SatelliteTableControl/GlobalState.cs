﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SatelliteTableControl
{
    internal class GlobalState : INotifyPropertyChanged
    {
        public ObservableCollection<Satellite> FullParams { get; set; }

        public GlobalState()
        {
            FullParams = new ObservableCollection<Satellite> { };
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
