﻿
using System;

namespace SatelliteTableControl
{
    public class Satellite
    {
        public Satellite() { } 
        public Satellite(int prn, float snr, float azimuth, float elevation, TypeGnss type, int acquisitionFlag, int ephemerisFlag, int oldMeasureFlag, int badDataFlag) 
        {
            PRN = prn;
            SNR = snr;
            Azimuth = azimuth;
            Elevation = elevation;
            Type = type;
            AcquisitionFlag = acquisitionFlag;
            EphemerisFlag = ephemerisFlag;
            OldMeasureFlag = oldMeasureFlag;
            BadDataFlag = badDataFlag;
        }

        public int PRN { get; set; } = -1;
        public float SNR { get; set; } = -1;
        public float Azimuth { get; set; } = -1;
        public float Elevation { get; set; } = -1;
        public TypeGnss Type { get; set; }

        public int AcquisitionFlag { get; set; } = -1;

        public int EphemerisFlag { get; set; } = -1;
        public int OldMeasureFlag { get; set; } = -1;
        public int BadDataFlag { get; set; } = -1;

        public Satellite Clone() => new Satellite(PRN, SNR, Azimuth, Elevation, Type, AcquisitionFlag, EphemerisFlag, OldMeasureFlag, BadDataFlag);

    }


    public enum TypeGnss : byte
    {
        Gps = 1,
        Glonass,
        Beidou,
        Galileo
    }
         
}
