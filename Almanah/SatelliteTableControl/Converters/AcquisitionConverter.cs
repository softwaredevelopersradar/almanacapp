﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SatelliteTableControl
{
    public class AcquisitionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int input = (int)value;
            string result = String.Empty;

            switch ((int)value)
            {
                case 0:
                    result = "Never acquired";
                    break;
                case 1:
                    result = "Acquired";
                    break;
                case 2:
                    result = "Re-opened search";
                    break;
                default:
                    break;
            }
            
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
            //throw new NotImplementedException();
            //return value;
        }
    }
}
