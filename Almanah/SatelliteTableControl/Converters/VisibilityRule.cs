﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SatelliteTableControl
{
    //[ValueConversion(typeof(float), typeof(string))]
    public class VisibilityRule : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            float input = System.Convert.ToSingle(value);

            if (input == -1)
            {
                return String.Empty;
            }

            if (input == 0)
            {
                return 0.ToString();
            }

            return input.ToString("F2");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
            //throw new NotImplementedException();
            //return value;
        }
        
    }
}
