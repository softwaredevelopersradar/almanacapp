﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SatelliteTableControl
{
    public class EphemerisFlagConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int input = (int)value;
            string result = String.Empty;

            if ((int)value > 0) result = "GOOD";
            
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
