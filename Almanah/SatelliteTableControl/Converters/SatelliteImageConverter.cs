﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SatelliteTableControl
{
    internal class SatelliteImageConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch((TypeGnss)value)
            {
                case TypeGnss.Gps:
                    return @"Images\usa30.png";
                case TypeGnss.Glonass:
                    return @"Images\russia30.png";
                case TypeGnss.Beidou:
                    return @"Images\china.png";
                case TypeGnss.Galileo:
                    return @"Images\europe.png";
                default:
                    return Binding.DoNothing;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
