﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SatelliteTableControl
{
    public class IntegerMsecFlag : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int input = (int)value;
            string result = String.Empty;

            switch ((int)value)
            {
                case 0:
                    result = "Don't know msec";
                    break;
                case 1:
                    result = "Known from subframe";
                    break;
                case 2:
                    result = "Verified by bit crossing";
                    break;
                case 3:
                    result = "Verified by good fix";
                    break;
                case 4:
                    result = "Suspect msec error";
                    break;
                default:
                    break;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
            //throw new NotImplementedException();
            //return value;
        }
    }
}
