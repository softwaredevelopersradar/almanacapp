﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SatelliteTableControl
{
    public class BadDataFlagConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int input = (int)value;
            string result = String.Empty;

            switch ((int)value)
            {
                case 1:
                    result = "Bad parity";
                    break;
                case 2:
                    result = "Bad ephemeris health";
                    break;
                default:
                    break;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
