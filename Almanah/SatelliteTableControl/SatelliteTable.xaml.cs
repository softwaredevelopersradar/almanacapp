﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Collections.Specialized;

namespace SatelliteTableControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class SatelliteTable : UserControl
    {
        //public event EventHandler<float[]> SelectPointEvent;
        //protected virtual void OnSelectPoint(EventHandler<float[]> some_ev, float[] azElev) => some_ev?.Invoke(this, azElev);

        //bool deleteSelection = false;
        //bool selectFromRadar = true;
        //private List<Satellite> listZorkiRModel = new List<Satellite> { };
        //public List<Satellite> ListZorkiRModel
        //{
        //    get { return listZorkiRModel; }
        //    set
        //    {
        //        if (listZorkiRModel != null && listZorkiRModel.Equals(value)) return;
        //        listZorkiRModel = value;
        //        UpdateZorkiR();
        //    }
        //}
        private TableViewModel tableViewModel = new TableViewModel();
        public ObservableCollection<Satellite> Satellites
        {
            get => tableViewModel.FullParams;
            set
            {
                if (tableViewModel.FullParams == value)
                    return;

                //var newDrones = value;

                var sorted = value.OrderBy(u => u.Type).ThenBy(u => u.PRN);
                tableViewModel.FullParams.Clear();
                //Pills.Concat<Pill>(someOtherPillCollection); TODO: переделать так
                foreach (Satellite recDrone in sorted)
                {
                    tableViewModel.FullParams.Add(recDrone);
                }
                AddEmptyRows();
                //GC.Collect();
                //paramSatellite.Items.Refresh(); //надо ли?
                //GC.Collect(); GC.WaitForPendingFinalizers();
            }
        }

        public SatelliteTable()
        {
            InitializeComponent();
            //tableViewModel.FullParams = new ObservableCollection<Satellite>();
            this.DataContext = tableViewModel; 
            AddEmptyRows();



            //paramSatellite.DataContext = new GlobalState();
            //paramSatellite.ItemsSource = (paramSatellite.DataContext as GlobalState).FullParams;
           
            //paramSatellite.SelectionChanged += ParamSatellite_SelectedCellsChanged;
            //Testc();
        }

        ///// <summary>
        ///// Если в таблице выбрали строку
        ///// </summary>
        //private void ParamSatellite_SelectedCellsChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    try
        //    {
        //        if (!deleteSelection && !selectFromRadar)
        //        {
        //            DataGrid dataGrid = sender as DataGrid;
        //            var forSend = new float[2];
        //            forSend[0] = (dataGrid.SelectedCells[0].Item as Satellite).Azimuth;
        //            forSend[1] = (dataGrid.SelectedCells[0].Item as Satellite).Elevation;
        //            if (e.AddedItems.Count != 0 && forSend[0] != -1)
        //            {
        //                //OnSelectPoint(SelectPointEvent, forSend);
        //            }
        //        }
        //    }
        //    catch(Exception ex) { }
        //}


        
        ///// <summary>
        ///// Подсветить строку
        ///// </summary>
        //public void ShowSelectedSat(float satNum)
        //{
        //    try
        //    {
        //        selectFromRadar = true;
        //        var pos = ((GlobalState)paramSatellite.DataContext).FullParams.IndexOf(((GlobalState)paramSatellite.DataContext).FullParams.Where(item => item.PRN == satNum).FirstOrDefault());
        //        if (pos == -1)
        //        { paramSatellite.SelectedItem = null; }
        //        else
        //        { paramSatellite.SelectedItem = paramSatellite.Items[pos]; }
        //        selectFromRadar = false;
        //    }
        //    catch { }
        //}
        

        /// <summary>
        /// Обновить значение координат
        /// </summary>
        public void UpdateSatInfo(List<Satellite> targets)
        {
            try
            {
                if (targets == null)
                    return;
                //deleteSelection = true;


                ((GlobalState)paramSatellite.DataContext).FullParams.Clear();
                for (int i = 0; i < targets.Count; i++)
                {
                    ((GlobalState)paramSatellite.DataContext).FullParams.Add(targets[i].Clone());
                }


                //paramSatellite.ItemsSource = ((GlobalState)paramSatellite.DataContext).FullParams;
               // AddEmptyRows();
                //deleteSelection = false;
            }
            catch { }
        }

        


        private void DataGridparamSatellite_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }


        /// <summary>
        /// Добавление пустых строк в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                //paramSatellite.Items.Refresh(); //надо ли?
                int сountRowsAll = paramSatellite.Items.Count; // количество имеющихся строк в таблице
                double hs = 23; // высота строки
                double ah = paramSatellite.ActualHeight; // визуализированная высота dataGrid
                double chh = paramSatellite.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++) //мб переставить циклы местами
                {
                    index = tableViewModel.FullParams.ToList().FindIndex(x => x.PRN < 0);
                    if (index != -1)
                    {
                        tableViewModel.FullParams.RemoveAt(index);
                    }
                }
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    tableViewModel.FullParams.Add(new Satellite());
                }
            }
            catch { }
        }

    }

    //public class BetterObservableCollection<T> :
    //ObservableCollection<T>, ICollectionViewFactory
    //{
    //    public ICollectionView CreateView()
    //    {
    //        return new BetterListCollectionView(this);
    //    }
    //}

    //public class BetterListCollectionView :
    //    ListCollectionView, IWeakEventListener
    //{
    //    public BetterListCollectionView(IList list) : base(list)
    //    {
    //        INotifyCollectionChanged changed = list as INotifyCollectionChanged;
    //        if (changed != null)
    //        {
    //            // this fixes the problem
    //            changed.CollectionChanged -= this.OnCollectionChanged;
    //            CollectionChangedEventManager.AddListener(list, this);
    //        }
    //    }
    //}

    //internal CollectionView(IEnumerable collection, int moveToFirst)
    //{
    //    /* some code snipped for brevity */
    //    INotifyCollectionChanged changed = collection as INotifyCollectionChanged;
    //    if (changed != null)
    //    {
    //        // this is the problem
    //        changed.CollectionChanged += this.OnCollectionChanged;

    //        /* some more code */
    //    }

    //    /* still more code */
    //}
}
