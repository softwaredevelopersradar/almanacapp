﻿namespace SatelliteTableControl
{
    public class FullSatellite
    {
        public FullSatellite() { }
        public FullSatellite(int prn, float snr, float azimuth, float elevation, TypeGnss type, float time, int acquisitionFlag, int ephemerisFlag, int oldMeasureFlag, int badDataFlag, int msec, int dataColliction)
        {
            PRN = prn;
            SNR = snr;
            Azimuth = azimuth;
            Elevation = elevation;
            Type = type;
            AcquisitionFlag = acquisitionFlag;
            EphemerisFlag = ephemerisFlag;
            OldMeasureFlag = oldMeasureFlag;
            BadDataFlag = badDataFlag;

            TimeOfLastMeasure = time;
            IntegerMsecFlag = msec;
            DataCollictionFlag = dataColliction;
        }

        public int PRN { get; set; } = -1;
        public float SNR { get; set; } = -1;
        public float Azimuth { get; set; } = -1;
        public float Elevation { get; set; } = -1;
        public TypeGnss Type { get; set; }

        public float TimeOfLastMeasure { get; set; } = -1;
        public int AcquisitionFlag { get; set; } = -1;
        public int EphemerisFlag { get; set; } = -1;
        public int OldMeasureFlag { get; set; } = -1;
        public int BadDataFlag { get; set; } = -1;
        public int IntegerMsecFlag { get; set; } = -1;
        public int DataCollictionFlag { get; set; } = -1;

        public FullSatellite Clone() => new FullSatellite(PRN, SNR, Azimuth, Elevation, Type, TimeOfLastMeasure, AcquisitionFlag, EphemerisFlag, OldMeasureFlag, BadDataFlag, IntegerMsecFlag, DataCollictionFlag);
    }
}
