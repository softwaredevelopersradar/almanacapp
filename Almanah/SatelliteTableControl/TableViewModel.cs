﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace SatelliteTableControl
{
    public class TableViewModel : DependencyObject, INotifyPropertyChanged
    {
        public ObservableCollection<Satellite> FullParams { get; set; } = new ObservableCollection<Satellite>();


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}
