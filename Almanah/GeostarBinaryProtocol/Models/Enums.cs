﻿

namespace GeostarBinaryProtocol
{
    public enum StateSatellite : byte
    {
        NotInView = 0,
        Tracked,
        UsedInFix
    }
}
