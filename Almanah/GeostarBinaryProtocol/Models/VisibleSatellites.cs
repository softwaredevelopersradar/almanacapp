﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeostarBinaryProtocol
{
    public class VisibleSatellites
    {
        public uint Count;
        public List<Satellite> Satellites;
    }
}
