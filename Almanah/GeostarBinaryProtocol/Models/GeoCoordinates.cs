﻿

namespace GeostarBinaryProtocol
{
    public class GeoCoordinates
    {
        public double NavigationTaskTime;  // Время решения НЗ (UTC). Нулевое значение соответствует 1 января 2008г
        public double Latitude;              //рад
        public double Longitude;             //рад 
        public double GeodeticHeight;      // Высота над эллипсоидом
        public double EllipsoidAndGeoidDeviation;      // Отклонение эллипсоида от геоида
        public uint SatsUsedInFix;      // Число КА в решении
        public uint ReceiverState;      // Слово состояния приемника (см. ниже)

        public ReceiverState State;
        public double GDOP; //GDOP
        public double PDOP;         //PDOP
        public double TDOP;         //TDOP
        public double HDOP;         //HDOP
        public double VDOP;             // VDOP

        public uint EstimationOfNavigationTask; //Флаг достоверности решения НЗ:
        public uint NumberOfReliableNTinRow;  //Количество достоверных решений НЗ подряд
        public double PlannedSpeed;  //Плановая скорость
        public double HeadingAngle;  //Курс, рад
    }
}
