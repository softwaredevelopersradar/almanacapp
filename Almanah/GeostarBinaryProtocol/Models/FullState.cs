﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeostarBinaryProtocol.Models
{
    public struct FullState
    {
        private bool Reserv31;
        private bool IsPLLNormal;
        private byte Mode;
        private bool Reserv27;
        private bool IsPhaseEvailable;
        private bool Reserv25;
        private bool CoordsByExtrapolation;
        private bool Reserv23;
        private bool HasAssistedMode;
        private bool StateReceiver;
        private bool DifferentialMode;
        private bool HasRTCM;
        private bool HasSBAS;
        private bool HasSatsSBASinFix;

        private bool IsGpsAlmanacEvailable;
        private bool IsGlonassAlmanacEvailable;

        //public ReceiverState(bool hasGpsAlmanac, bool hasGlonassAlmanac)
        //{
        //    IsGpsAlmanacEvailable = hasGpsAlmanac;
        //    IsGlonassAlmanacEvailable = hasGlonassAlmanac;
        //}
    }
}
