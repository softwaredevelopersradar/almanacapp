﻿

namespace GeostarBinaryProtocol
{
    public struct ReceiverState
    {
        public bool IsGpsAlmanacEvailable;
        public bool IsGlonassAlmanacEvailable;

        public ReceiverState(bool hasGpsAlmanac, bool hasGlonassAlmanac)
        {
            IsGpsAlmanacEvailable = hasGpsAlmanac;
            IsGlonassAlmanacEvailable = hasGlonassAlmanac;
        }
    }
}
