﻿

namespace GeostarBinaryProtocol
{
    public class NavigationTaskStateVector
    {
        //WGS-84
        public double PosX;              // Satellite position X (m)
        public double PosY;              // Satellite position Y (m)
        public double PosZ;              // Satellite position Y (m)      
        public double C_gps;             // Сдвиг ШВ приемника относительно ШВ GPS      
        public double VelocityXDot;      // velocity X dot (m/sec)
        public double VelocityYDot;      // velocity Y dot (m/sec)
        public double VelocityZDot;      // velocity Y dot (m/sec)
        public double TimescaleDriftRate; //Скорость ухода ШВ приемника
        public double PDOPNorth;         //Составляющая PDOP по направлению на север
        public double PDOPEast;         //Составляющая PDOP по направлению на восток
        public double PDOPElevation;         //Составляющая PDOP по  по высоте
        public double C_glonass;             // Сдвиг ШВ приемника относительно ШВ ГЛОНАСС

        public double EstimationOfCoordinatesAccuracy; //Оценка точности определения координат (пространственных)
        public double EstimationOfVelocityAccuracy;  //Оценка точности определения скорости (пространственных)
        public double EstimationOf1PPCAccuracy;  //Оценка точности 1PPS
    }
}
