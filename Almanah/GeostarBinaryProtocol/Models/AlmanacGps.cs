﻿

namespace GeostarBinaryProtocol
{
    public class AlmanacGps
    {
        public double M_0;
        public double e;
        public double sqrt_A;
        public double OMEGA_0;
        public double i_o;
        public double omega;
        public double OMEGADOT;
        public double a_f0;
        public double a_f1;
        public int t_oa; //хз про тип
        public ushort wn_oa; //хз про тип
        public ushort SV_HEALTH;

        public ushort PRN;

        //public byte t_oa_raw;
        //public Single Axis;
        //public Single n;
        //public Single OMEGA_n;
        //public Single ODOT_n;
        //public Single t_zc;
        //public UInt16 weeknum;
        

        public AlmanacGps()
        { }
    }
}
