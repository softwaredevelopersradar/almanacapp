﻿
namespace GeostarBinaryProtocol
{
    public class EphemerisGlonass
    {
        public int Toc;                  //Время приема эфемерид (UTC). Нулевое значение соответствует 1 января 2008г
        public ushort DayNum;            //Номер суток, на которые передается альманах
        public ushort GeneralizedDateNum; //Обобщенный номер четырехлетия и суток 1461*N4 + (Nt – 1)
        public double PosX;              // Satellite position X (m)
        public double PosY;              // Satellite position Y (m)
        public double PosZ;              // Satellite position Y (m)        
        public double VelocityXDot;      // velocity X dot (m/sec)
        public double VelocityYDot;      // velocity Y dot (m/sec)
        public double VelocityZDot;      // velocity Y dot (m/sec)
        public double AccelerationX;     // X acceleration (m/sec2)
        public double AccelerationY;     // Y acceleration (m/sec2)
        public double AccelerationZ;     // Y acceleration (m/sec2)
        public double SVFreqBias;        // SV relative frequency bias (+GammaN)

        public double tauN;           // Cдвиг БШВ относительно ШВ ГЛОНАСС [seconds]   //C_sys?    //Correction of time relative to GLONASS system time
        public double C_utc;             // Correction from GLONASS to UTC
        public double C_gps;             // Correction to GPS time relative GLONASS

        //byte P1;
        public bool[] Bn;                  // признак недостоверности кадра
        //byte P2;
        //byte P3;
        //byte P;
        //byte P4;
        //byte DnGm;
        //byte M;
        //много всяких коэффициентов
        public byte Num;
        public byte Ft; //Фактор точности измерений
        public byte AgeInfo; //EnЭ, Возраст эфемеридной информации  (сутки)
        public byte tb; //Номер 15-ти минутного интервала, на который посчитаны эфемериды
    }
}
