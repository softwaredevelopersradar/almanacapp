﻿
namespace GeostarBinaryProtocol
{
    public class ChannelState
    {
        public byte PhaseMeasurementIndicator;
        public bool IsUsedInFix;
        public bool HasPresenceOfMultipath;
        public byte RejectionCode;
        public bool IsMeasurementInfoReady;
        public bool IsNavigationFrameValid;
        public bool GotEphemeris;
        public bool GotTime;
        public bool HasFrameSync;
        public bool HasSymbolSync;
        public bool IsPLLCaptured;
        public bool PowerOn;
    }
}
