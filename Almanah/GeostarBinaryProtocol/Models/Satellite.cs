﻿
namespace GeostarBinaryProtocol
{
    public class Satellite
    {
        public byte Channel;
        public byte PRN;
        public short Letter;
        public ChannelState ChannelState;
        public float SNR;
        public float Azimuth;
        public float Elevation;

        public StateSatellite State;
    }
}
