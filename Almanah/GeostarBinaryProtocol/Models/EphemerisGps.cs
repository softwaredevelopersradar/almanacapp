﻿
namespace GeostarBinaryProtocol
{
    public class EphemerisGps
    {
        public int t_ephem;          // 1 ---     
        public float delta_n;          // 17

        public double M_0;              // 18
        public double e;                // 20
        public double sqrt_A;           // 22
        public double OMEGA_0;          // 25
        public double i_o;              // 27
        public double omega;            // 29
        public double OMEGADOT;         // 30
        public double a_f0;             // 12
        public float IDOT;             // 31
        public float C_uc;             // 19
        public float C_us;             // 21
        public float C_rc;             // 28
        public float C_rs;             // 16
        public float C_ic;             // 24
        public float C_is;             // 26

        public float T_GD;             // 8
        public float a_f2;             // 10
        public float a_f1;             // 11
        public int t_oe;             // 23
        public int t_oc;             // 9

        public short IODC;             // 7
        public short weeknum;          // 2

        public byte IODE;               // 14
        public byte sv_number;          // 0
        public byte SV_health;          // 6
        public byte SVacc;            // 13



        //public byte codeL2;             // 3
        //public byte L2Pdata;            // 4
        //public byte SVacc_raw;          // 5 ---
        //public byte fit_interval;       // 15


        //public double Axis;             // 32 --- sqrt_A^2
        //public double n;                // 33 ---
        //public double r1me2;            // 34 ---
        //public double OMEGA_n;          // 35 ---
        //public double ODOT_n;


        //public bool hasSNR;
        public EphemerisGps()
        { }
    }
}
