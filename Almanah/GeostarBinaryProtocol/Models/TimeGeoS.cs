﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeostarBinaryProtocol
{
    public class TimeGeoS
    {
        
        public double C_utc;             // Correction from GLONASS to UTC
        public double C_gps;             // Correction to GPS time relative GLONASS
        public double B1;             // Correction from GLONASS to UTC
        public double B2;             // Correction to GPS time relative GLONASS
        public ushort Nsutki;             // Correction from GLONASS to UTC
        public ushort N4;             // Correction to GPS time relative GLONASS
        public ushort Nalm;
        //и т.д.
    }
}
