﻿
namespace GeostarBinaryProtocol
{
    public class AlmanacGlonass
    {
        public double e;        // eccentricity
        public double TDot;     // rate of change of Draconian period, [second / coil^2]
        public double w;        // argument of perigee, [semi-cycles]
        public double deltaT;   // correction to the mean value of Draconian period, [second/ coil]
        public double t_lambda; // Время прохождения первого восходящего узла орбиты [seconds]
        public double lambda;   // longitude of first ascending node passage, [semi-cycles]
        public double dI;       // correction to the mean value of inclination, [semi-cycles]
        public double tau;      // Грубый сдвиг БШВ относительно ШВ ГЛОНАСС [seconds]
        public ushort DayNum;   // Номер суток, на которые передается альманах
        public ushort Num;     // number of spacecraft in the grouping(orbital slot number)
        public short f_channel; // number of frequency channel(-7 .. 6)
        public bool С;          // обобщенный признак состояния (0 -> не пригоден для исп-я в навигац-х определений, 1 -> норм)
        public byte Modification; // модификация спутника (0 -> НКА Глонасс, 1 -> НКА Глонасс-М)

    }
}
