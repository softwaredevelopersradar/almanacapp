﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Threading;

namespace GeostarBinaryProtocol
{
    public class GeoS
    {
        private SerialPort _port;
        private Thread thrRead;

        private const byte MAX_SIZE = 255;

        private VersionR _version = VersionR.v3;

        #region Codes
        private static readonly byte[] Preamble = { 0x53, 0x4F, 0x45, 0x47, 0x53, 0x50, 0x33, 0x72 };
        private static readonly byte[] PreambleReal = {0x47, 0x45, 0x4F, 0x53, 0x72, 0x33, 0x50, 0x53 };

        private const byte NAV_TASK_VECTOR = 0x13;
        private const byte TIME = 0x14;
        private const byte POSITION = 0x20;
        private const byte SATELLITES_GLO = 0x22;
        private const byte ALMANAС_GLO = 0x89; 
        private const byte EPHEMERIS_GLO = 0x8B;
        private const byte TIME_GLO = 0x9E;
        private const byte RESTART = 0xC2;
        private const byte ALMANAС_GPS = 0x88;
        private const byte EPHEMERIS_GPS = 0x8A;
        private const byte ANTENNA_SUPPLY = 0xC7;

        private enum Resets : uint
        {
            Hot = 0,
            Warm = 1,
            Cold = 3,
            FactorySettings = 7
        }

        public enum VersionR : byte
        {
            v3 = 0,
            v5 = 1
        }
        #endregion

        #region Events
        public event EventHandler<string> OnConnect;
        public event EventHandler<bool> OnDisconnect;
        public event EventHandler<string> OnError;
        public event EventHandler<string> Message;
        public event EventHandler<List<AlmanacGlonass>> OnGetAlmanacGlonass;
        public event EventHandler<List<EphemerisGlonass>> OnGetEphemeriesGlonass;
        public event EventHandler<List<AlmanacGps>> OnGetAlmanacGps;
        public event EventHandler<List<EphemerisGps>> OnGetEphemeriesGps;
        public event EventHandler<TimeGeoS> OnGetTimeInfo;
        public event EventHandler<VisibleSatellites> OnGetSatellitesInfo;
        public event EventHandler<GeoCoordinates> OnGetLocation;
        public event EventHandler<PortState> OnPortState;
        #endregion

        private List<EphemerisGlonass> satephemeries;
        private List<AlmanacGlonass> satAlmanacs;

        private List<AlmanacGps> almanacsGps;
        private List<EphemerisGps> ephemerisGps;


        ///<summary>
        /// Connecting the receiver
        /// </summary>
        public void Connect(string Port_number, int Port_rate, VersionR version)
        {
            satephemeries = new List<EphemerisGlonass>();
            satAlmanacs = new List<AlmanacGlonass>();
            almanacsGps = new List<AlmanacGps>();
            ephemerisGps = new List<EphemerisGps>();
            _version = version;

            if (_port == null)
                _port = new SerialPort();

            if (_port.IsOpen)
                Disconnect();

            try
            {
                // set parameters of port
                _port.PortName = Port_number;
                _port.BaudRate = Port_rate;
                _port.Parity = Parity.None;
                _port.DataBits = 8;
                _port.StopBits = StopBits.One;
                _port.RtsEnable = false;
                _port.DtrEnable = true;
                _port.ReceivedBytesThreshold = 1000;
                _port.Open();
                OnPortState?.Invoke(this, new PortState(!_port.RtsEnable, !_port.DsrHolding));

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception)
                { }

                OnConnect?.Invoke(this, "Connected");
                Message?.Invoke(this, "Port opened");
            }
            catch (System.Exception e)
            {
                // событие закрытия порта порта
                OnDisconnect?.Invoke(this, true);
                Message?.Invoke(this, "Open port error");
            }
        }


        /// <summary>
        /// Block/unblock RTS pin of SerialPort
        /// </summary>
        public void BlockComPortRTS(bool isEnable)
        {
            if (_port == null)
                return;

            try
            {
                //Задавать инвертированное значение (1 - отключение, 0 - вкл. питание)
                _port.RtsEnable = !isEnable;
                Thread.Sleep(500);
                OnPortState?.Invoke(this, new PortState(!_port.RtsEnable, !_port.DsrHolding));
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        /// <summary>
        /// Disconnecting the receiver
        /// </summary>
        public void Disconnect()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception e)
            { }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception e)
            { }

            try
            {
                // destroy thread of reading
                try
                {
                    if (thrRead != null)
                    {
                        thrRead.Abort();
                        thrRead.Join(500);
                        thrRead = null;
                    }
                }
                catch (Exception e)
                { Message?.Invoke(this, "D0, Error: " + e); }

                _port.Close();
                OnPortState?.Invoke(this, new PortState(false, false));
                OnDisconnect?.Invoke(this, true);
                Message?.Invoke(this, "Port closed");
            }
            catch (System.Exception e)
            { Message?.Invoke(this, "Closing error: " + e); }
        }


        #region Requests
        /// <summary>
        /// Requests GLONASS Ephemeris for all satellites
        /// </summary>
        public void GetEphemeriesGlonass()
        {
            switch (_version)
            {
                case VersionR.v5:
                    var request = BitConverter.GetBytes(0);
                    WriteToARD(FormPacketForSend(EPHEMERIS_GLO, request));
                    break;
                default:
                    for (uint i = 1; i <= 24; i++)
                    {
                        var request1 = BitConverter.GetBytes(i);
                        WriteToARD(FormPacketForSend(EPHEMERIS_GLO, request1));

                        Thread.Sleep(10);
                    }
                    break;
            }
            ////for (uint i = 1; i <= 24; i++)
            ////{
            
            

            ////    Thread.Sleep(10);
            ////}
            //for (uint i = 1; i <= 24; i++)
            //{
            //    var request = BitConverter.GetBytes(i);
            //    WriteToARD(FormPacketForSend(EPHEMERIS_GLO, request));

            //    Thread.Sleep(10);
            //}
        }


        /// <summary>
        /// Requests GLONASS Almanac for all satellites.
        /// </summary>
        public void GetAlmanacGlonass()
        {
            switch (_version)
            {
                case VersionR.v5:
                    var request = BitConverter.GetBytes(0);
                    WriteToARD(FormPacketForSend(ALMANAС_GLO, request));
                    break;
                default:
                    for (uint i = 1; i <= 24; i++)
                    {
                        var request1 = BitConverter.GetBytes(i);
                        WriteToARD(FormPacketForSend(ALMANAС_GLO, request1));

                        Thread.Sleep(10);
                    }
                    break;
            }
            ////for (uint i = 1; i <= 24; i++)
            ////{


            ////    Thread.Sleep(10);
            ////}
            //for (uint i = 1; i <= 24; i++)
            //{
            //    var request = BitConverter.GetBytes(i);
            //    WriteToARD(FormPacketForSend(ALMANAС_GLO, request));

            //    Thread.Sleep(10);
            //}
        }

        /// <summary>
        /// Requests GPS Ephemeris for all satellites
        /// </summary>
        public void GetEphemeriesGps()
        {
            switch (_version)
            {
                case VersionR.v5:
                    var request = BitConverter.GetBytes(0);
                    WriteToARD(FormPacketForSend(EPHEMERIS_GPS, request));
                    break;
                default:
                    for (uint i = 1; i <= 32; i++)
                    {
                        var request1 = BitConverter.GetBytes(i);
                        WriteToARD(FormPacketForSend(EPHEMERIS_GPS, request1));

                        Thread.Sleep(10);
                    }
                    break;
            }
            
            //for (uint i = 1; i <= 32; i++)
            //{
            //    var request = BitConverter.GetBytes(i);
            //    WriteToARD(FormPacketForSend(EPHEMERIS_GPS, request));

            //    Thread.Sleep(10); 
            //}
        }


        /// <summary>
        /// Requests GPS Almanac for all satellites.
        /// </summary>
        public void GetAlmanacGps()
        {
            switch (_version)
            {
                case VersionR.v5:
                    var request = BitConverter.GetBytes(0);
                    WriteToARD(FormPacketForSend(ALMANAС_GPS, request));
                    break;
                default:
                    for (uint i = 1; i <= 32; i++)
                    {
                        var request1 = BitConverter.GetBytes(i);
                        WriteToARD(FormPacketForSend(ALMANAС_GPS, request1));

                        Thread.Sleep(10);
                    }
                    break;
            }
            
            //for (uint i = 1; i <= 32; i++)
            //{
            //    var request = BitConverter.GetBytes(i); 
            //    WriteToARD(FormPacketForSend(ALMANAС_GPS, request));

            //    Thread.Sleep(10);
            //}
        }


        /// <summary>
        /// Requests Almanac for all satellites.
        /// </summary>
        public void GetTimeInfo()
        {
            WriteToARD(FormPacketForSend(TIME_GLO, new byte[4]));
        }


        /// <summary>
        /// Warm reset
        /// </summary>
        public void WarmReset()
        {
            var request = BitConverter.GetBytes((uint)Resets.Warm);
            WriteToARD(FormPacketForSend(RESTART, request));
        }


        /// <summary>
        /// Cold reset
        /// </summary>
        public void ColdReset()
        {
            var request = BitConverter.GetBytes((uint)Resets.Cold);
            WriteToARD(FormPacketForSend(RESTART, request));
        }

        /// <summary>
        /// Factory settings reset
        /// </summary>
        public void FactorySettingsReset()
        {
            var request = BitConverter.GetBytes((uint)Resets.FactorySettings);
            WriteToARD(FormPacketForSend(RESTART, request));
        }


        /// <summary>
        /// Turn on/off antenna supply
        /// </summary>
        public void BlockAntennaSupply(bool isEnable)
        {
            var request = BitConverter.GetBytes(Convert.ToUInt32(isEnable));
            WriteToARD(FormPacketForSend(ANTENNA_SUPPLY, request));
        }
        #endregion



        /// <summary>
        /// Write to port
        /// </summary>
        private bool WriteToARD(byte[] bSend)
        {
            try
            {
                _port.Write(bSend, 0, bSend.Length);
                //OnWriteByte?.Invoke(this, new ListByteEventArgs(bSend.ToList()));
                return true;
            }
            catch (System.Exception ex)
            {
                string strer = ex.Message;
                return false;
            }
        }


        

        #region CreateArrayForSend

        private byte[] FormPacketForSend(byte code, byte[] data)
        {
            var packet = new byte[data.Length + 12];
            Array.Copy(PreambleReal, 0 , packet, 0, Preamble.Length);

            packet[Preamble.Length] = code;
            Array.Copy(BitConverter.GetBytes((short)data.Length / 4).ToArray(), 0, packet, Preamble.Length + 2, 2);
            
            Array.Copy(data, 0, packet, Preamble.Length + 4, data.Length);
           
            return packet.Concat(CountChecksum(packet)).ToArray();
        }

        internal static byte[] CountChecksum(byte[] data)
        {
            byte[] result = new byte[4];
            BitArray resultBits = new BitArray(32);

            for (int i = 0; i < data.Length; i += 4)
            {
                byte[] part = new byte[4];
                Array.Copy(data, i, part, 0, 4);
                var word = new BitArray(part);
                resultBits = resultBits.Xor(word);
            }

            resultBits.CopyTo(result, 0);

            return result;
        }
        #endregion


        private byte[] PreparePacketForReading(byte[] data)
        {
            var reverseData = new byte[data.Length];

            for (int i = 0; i < data.Length; i += 4)
            {
                byte[] part = new byte[4];
                Array.Copy(data, i, part, 0, 4); Array.Reverse(part);
                Array.Copy(part, 0, reverseData, i, 4);
            }

            return reverseData;
        }



        #region Reading port

        byte[] bBufSave = null;

        /// <summary>
        /// Reading port
        /// </summary>
        private void ReadData()
        {
            // buffer for reading bytes
            byte[] bBufRead;
            bBufRead = new byte[MAX_SIZE];

            // buffer to transfer and save reading bytes
            byte[] new_bufSave = null;
            bBufSave = null; // new byte[MAX_SIZE];

            // length of reading bytes
            int iReadByte = -1;
            int iTempLength = 0;

            // while port open
            while (true)
            {
                try
                {
                    bBufRead = new byte[MAX_SIZE];
                    // clear bufer of reading
                    try
                    {
                        Array.Clear(bBufRead, 0, bBufRead.Length);
                    }
                    catch (Exception e)
                    { Message?.Invoke(this, "P1, Error: " + e); }
                    // read data
                    try
                    {
                        iReadByte = _port.Read(bBufRead, 0, MAX_SIZE);
                    }
                    catch (Exception e)
                    {
                        Message?.Invoke(this, "P2, Error: " + e);
                        //return;
                    }

                    // if bytes was read
                    if (iReadByte > 0)
                    {
                        try
                        {
                            // resize buffer for real size of reading bytes
                            Array.Resize(ref bBufRead, iReadByte);

                            //// Событие чтения байт
                            //OnReadByte?.Invoke(this, new ListByteEventArgs(bBufRead.ToList()));

                            if (bBufSave == null)
                                bBufSave = new byte[iReadByte];
                            else
                                Array.Resize(ref bBufSave, bBufSave.Length + iReadByte);

                            // copy buffer for read to buffer for save
                            Array.Copy(bBufRead, 0, bBufSave, iTempLength, iReadByte);

                            // set current value of saving bytes
                            iTempLength += iReadByte;
                        }
                        catch (System.Exception e)
                        {
                            Message?.Invoke(this, "P3, Error: " + e);
                            Disconnect();
                        }

                        var param_cmd = FindCmd(bBufSave);

                        while (param_cmd.Item1 > -1 && param_cmd.Item2 > 0)
                        {
                            try
                            {

                                byte[] bDecode = new byte[param_cmd.Item2];


                                try
                                {
                                    Array.Copy(bBufSave, param_cmd.Item1, bDecode, 0, param_cmd.Item2);
                                }
                                catch (Exception e)
                                { Message?.Invoke(this, "P4, Error: " + e); }

                                try
                                {
                                    if (CheckChecksum(bDecode))
                                    {
                                        DecodeCmd(bDecode);
                                    }
                                    else
                                    { }
                                }
                                catch (Exception e)
                                { Message?.Invoke(this, "P5, Error: " + e); }

                                try
                                {
                                    new_bufSave = new byte[bBufSave.Length - param_cmd.Item2];
                                    Array.Copy(bBufSave, 0, new_bufSave, 0, param_cmd.Item1);
                                    Array.Copy(bBufSave, param_cmd.Item1 + param_cmd.Item2, new_bufSave, param_cmd.Item1, bBufSave.Length - param_cmd.Item1 - param_cmd.Item2);
                                }
                                catch (Exception e)
                                { Message?.Invoke(this, "P6, Error: " + e); }
                                try
                                {
                                    iTempLength = new_bufSave.Length; //хз зачем
                                }
                                catch (Exception ex)
                                { Message?.Invoke(this, "P7, Error: " + ex); }

                                try
                                {
                                    param_cmd = FindCmd(new_bufSave);
                                }
                                catch (Exception e)
                                { Message?.Invoke(this, "P8, Error: " + e); }


                                Array.Resize(ref bBufSave, new_bufSave.Length);
                                Array.Copy(new_bufSave, bBufSave, new_bufSave.Length);
                            }
                            catch (System.Exception ex)
                            { Message?.Invoke(this, "P9, Error: " + ex); Disconnect(); }
                        }

                        if (bBufSave.Length > 0 && param_cmd.Item2 > 0)
                        {
                            try
                            {
                                Array.Reverse(bBufSave);
                                Array.Resize(ref bBufSave, bBufSave.Length - param_cmd.Item2);
                                Array.Reverse(bBufSave);
                            }
                            catch (Exception e)
                            { }
                        }
                    }
                    else
                    { // close port
                        Message?.Invoke(this, "P10");
                        Disconnect();
                    }
                }
                catch (System.Exception ee)
                {
                    Message?.Invoke(this, "P11, Error: " + ee);
                    Disconnect();
                }
            }
        }


        public static int FindSubarrayStartIndex(byte[] array, byte[] subArray)
        {
            for (int i = 0; i < array.Length - subArray.Length + 1; i++)
                if (ContainsAtIndex(array, subArray, i)) return i;
            return -1;
        }


        static bool ContainsAtIndex(byte[] array, byte[] subArray, int index)
        {
            
            for (int j = 0; j < subArray.Length; j++)
            {
                if (array[index + j] != subArray[j])
                {
                    return false;
                }
            }
            return true;
        }

        private Tuple<int, int> FindCmd(byte[] bArray)
        {
            int len = -1;
            int firstBt = -1;

            if (bArray.Length < 20)
            { return Tuple.Create<int, int>(firstBt, len); }

            firstBt = FindSubarrayStartIndex(bArray, PreambleReal);

            if (firstBt == -1)
            {
                return Tuple.Create<int, int>(firstBt, len);
            }

            if (firstBt + 12 > bArray.Length)
            {
                return Tuple.Create<int, int>(firstBt, len);
            }

            int dataLength = BitConverter.ToInt16(bArray, firstBt + PreambleReal.Length + 2) * 4;

            int lastBt = firstBt + 15 + dataLength;

            if (lastBt < bArray.Length)
            { len = lastBt - firstBt + 1; }

            return Tuple.Create<int, int>(firstBt, len);
        }


        internal static bool CheckChecksum(byte[] data)
        {
            byte[] packet = new byte[data.Length - 4];
            byte[] checksum = new byte[4];

            Array.Copy(data, 0, packet, 0, packet.Length);
            Array.Copy(data, packet.Length, checksum, 0, 4);

            return Enumerable.SequenceEqual(CountChecksum(packet), checksum);
        }

        bool canSendGlonassEphemeris = true;
        bool canSendGpsEphemeris = true;
        //определение вида кодограммы
        private void DecodeCmd(byte[] bCmd)
        {
            byte Code = bCmd[PreambleReal.Length];
            //bCmd = PreparePacketForReading(bCmd);
            var packet = bCmd.Cast<byte>().ToList();
            var data = packet.GetRange(12, packet.Count - 16).ToArray();
            var oldcanSendGlonassEphemeris = canSendGlonassEphemeris;
            var oldcanSendGpsEphemeris = canSendGpsEphemeris;
            switch (Code)
            {
                case POSITION:
                    canSendGlonassEphemeris = true;
                    canSendGpsEphemeris = true;
                    //ParseLocation(data);
                    ParsePosition(data);
                    break;

                case ALMANAС_GLO:
                    canSendGlonassEphemeris = true;
                    canSendGpsEphemeris = true;
                    ParseAlmInfo(data);
                    break;

                case EPHEMERIS_GLO:
                    canSendGlonassEphemeris = false;
                    canSendGpsEphemeris = true;
                    ParseEphInfo(data);
                    break;

                case TIME_GLO:
                    canSendGlonassEphemeris = true;
                    canSendGpsEphemeris = true;
                    ParseTimeInfo(data);
                    break;

                case SATELLITES_GLO:
                    canSendGlonassEphemeris = true;
                    canSendGpsEphemeris = true;
                    ParseSatellitesInfo(data);
                    break;
                case ALMANAС_GPS:
                    canSendGlonassEphemeris = true;
                    canSendGpsEphemeris = true;
                    ParseAlmInfoGps(data);
                    break;
                case EPHEMERIS_GPS:
                    canSendGlonassEphemeris = true;
                    canSendGpsEphemeris = false;
                    ParseEphInfoGps(data);
                    break;
                case TIME:
                    canSendGlonassEphemeris = true;
                    canSendGpsEphemeris = true;
                    break;
                default:
                    canSendGlonassEphemeris = true;
                    canSendGpsEphemeris = true;
                    var cd = Code;
                    //OnUnknownPocket?.Invoke(this, new ListByteEventArgs(bCmd.Cast<byte>().ToList()));
                    break;
            }

            if (_version == VersionR.v5)
            {
                if (canSendGlonassEphemeris && !oldcanSendGlonassEphemeris)
                {
                    OnGetEphemeriesGlonass?.Invoke(this, satephemeries);
                    satephemeries.Clear();
                }
                if (canSendGpsEphemeris && !oldcanSendGpsEphemeris)
                {
                    OnGetEphemeriesGps?.Invoke(this, ephemerisGps);
                    ephemerisGps.Clear();
                }
            }
            //OnReadByte?.Invoke(this, new ListByteEventArgs(bCmd.ToList()));
        }
        #endregion


        // Вектор навигационной задачи? 
        private void ParseLocation(byte[] data)
        {
            var pos = new NavigationTaskStateVector();

            pos.PosX = BitConverter.ToDouble(data, 0);
            pos.PosY = BitConverter.ToDouble(data, 8);
            pos.PosZ = BitConverter.ToDouble(data, 16);
            pos.C_gps = BitConverter.ToDouble(data, 24);
            pos.VelocityXDot = BitConverter.ToDouble(data, 32);
            pos.VelocityYDot = BitConverter.ToDouble(data, 40);
            pos.VelocityZDot = BitConverter.ToDouble(data, 48);
            pos.TimescaleDriftRate = BitConverter.ToDouble(data, 56);
            pos.PDOPNorth = BitConverter.ToDouble(data, 64);
            pos.PDOPEast = BitConverter.ToDouble(data, 72);
            pos.PDOPElevation = BitConverter.ToDouble(data, 80);

            pos.C_glonass = BitConverter.ToDouble(data, 96);
            pos.EstimationOfCoordinatesAccuracy = BitConverter.ToDouble(data, 104);
            pos.EstimationOfVelocityAccuracy = BitConverter.ToDouble(data, 112);
            pos.EstimationOf1PPCAccuracy = BitConverter.ToDouble(data, 120);

            //OnGetLocation?.Invoke(this, pos);
        }


        private void ParsePosition(byte[] data)
        {
            var pos = new GeoCoordinates();

            pos.NavigationTaskTime = BitConverter.ToDouble(data, 0);
            pos.Latitude = BitConverter.ToDouble(data, 8);
            pos.Longitude = BitConverter.ToDouble(data, 16);
            pos.GeodeticHeight = BitConverter.ToDouble(data, 24);
            pos.EllipsoidAndGeoidDeviation = BitConverter.ToDouble(data, 32);
            pos.SatsUsedInFix = BitConverter.ToUInt32(data, 40);
            pos.ReceiverState = BitConverter.ToUInt32(data, 44);
            pos.State = new ReceiverState(Convert.ToBoolean((pos.ReceiverState << 29) >> 31), Convert.ToBoolean((pos.ReceiverState << 28) >> 31));
            pos.GDOP = BitConverter.ToDouble(data, 48);
            pos.PDOP = BitConverter.ToDouble(data, 56);
            pos.TDOP = BitConverter.ToDouble(data, 64);
            pos.HDOP = BitConverter.ToDouble(data, 72);
            pos.VDOP = BitConverter.ToDouble(data, 80);
            pos.EstimationOfNavigationTask = BitConverter.ToUInt32(data, 88);
            pos.NumberOfReliableNTinRow = BitConverter.ToUInt32(data, 92);
            pos.PlannedSpeed = BitConverter.ToDouble(data, 96);
            pos.HeadingAngle = BitConverter.ToDouble(data, 104);

            OnGetLocation?.Invoke(this, pos);
        }



        // Обработка Эфемерид (0x58 ... 0x06)
        private void ParseEphInfo(byte[] data)
        {
            var ephemeris = new EphemerisGlonass();

            ephemeris.Toc = BitConverter.ToInt32(data, 0);
            ephemeris.DayNum = BitConverter.ToUInt16(data, 6);
            ephemeris.GeneralizedDateNum = BitConverter.ToUInt16(data, 4);
            ephemeris.PosX = BitConverter.ToDouble(data, 8);
            ephemeris.PosY = BitConverter.ToDouble(data, 16);
            ephemeris.PosZ = BitConverter.ToDouble(data, 24);
            ephemeris.VelocityXDot = BitConverter.ToDouble(data, 32);
            ephemeris.VelocityYDot = BitConverter.ToDouble(data, 40);
            ephemeris.VelocityZDot = BitConverter.ToDouble(data, 48);
            ephemeris.AccelerationX = BitConverter.ToDouble(data, 56);
            ephemeris.AccelerationY = BitConverter.ToDouble(data, 64);
            ephemeris.AccelerationZ = BitConverter.ToDouble(data, 72);
            ephemeris.SVFreqBias = BitConverter.ToDouble(data, 80);
            ephemeris.tauN = BitConverter.ToDouble(data, 88);
            ephemeris.C_utc = BitConverter.ToDouble(data, 96);
            ephemeris.C_gps = BitConverter.ToDouble(data, 104);

            ephemeris.Num = data[119];
            ephemeris.Ft = data[118];
            ephemeris.AgeInfo = data[117];
            ephemeris.tb = data[116];

            satephemeries.Add(ephemeris);


            if (_version == VersionR.v3 && satephemeries.Count == 24)
            {
                OnGetEphemeriesGlonass?.Invoke(this, satephemeries);
                satephemeries.Clear();
            }
            //if (satephemeries.Count == 24)
            //if (canSendEphemerisGlonass)
            //{
            //    OnGetEphemeriesGlonass?.Invoke(this, satephemeries);
            //    satephemeries.Clear();
            //}
        }


        // Обработка Альманаха
        private void ParseAlmInfo(byte[] data)
        {
            var almanac = new AlmanacGlonass();

            almanac.e = BitConverter.ToDouble(data, 0);
            almanac.TDot = BitConverter.ToDouble(data, 8);
            almanac.w = BitConverter.ToDouble(data, 16);
            almanac.deltaT = BitConverter.ToDouble(data, 24);
            almanac.t_lambda = BitConverter.ToDouble(data, 32 );
            almanac.lambda = BitConverter.ToDouble(data, 40 );
            almanac.dI = BitConverter.ToDouble(data, 48);
            almanac.tau = BitConverter.ToDouble(data, 56);
            almanac.DayNum = BitConverter.ToUInt16(data, 66);  //дальше байты отличаются от протокола, т.к. слова приходят перевернутыми
            almanac.Num = BitConverter.ToUInt16(data, 64);
            almanac.f_channel = BitConverter.ToInt16(data, 70);


            var temp = new BitArray(data.Skip(68).Take(2).ToArray()); //inverse?
            almanac.С = temp[0];  //хз они склеивали С и М.
            almanac.Modification = temp[15] ? (byte)1 : (byte)0;
                       
            satAlmanacs.Add(almanac);

            if (satAlmanacs.Count == 24)
            {
                OnGetAlmanacGlonass?.Invoke(this, satAlmanacs);
                satAlmanacs.Clear();
            }
        }


        // Обработка Альманаха
        private void ParseAlmInfoGps(byte[] data)
        {
            
            var almanac = new AlmanacGps();

            almanac.M_0 = BitConverter.ToDouble(data, 0);
            almanac.e = BitConverter.ToDouble(data, 8);
            almanac.sqrt_A = BitConverter.ToDouble(data, 16);
            almanac.OMEGA_0 = BitConverter.ToDouble(data, 24);
            almanac.i_o = BitConverter.ToDouble(data, 32);
            almanac.omega = BitConverter.ToDouble(data, 40);
            almanac.OMEGADOT = BitConverter.ToDouble(data, 48);
            almanac.a_f0 = BitConverter.ToDouble(data, 56);
            almanac.a_f1 = BitConverter.ToDouble(data, 64);  //дальше байты отличаются от протокола, т.к. слова приходят перевернутыми

            int[] temp1 = new int[] { BitConverter.ToInt32(data, 72) };
            BitArray temp = new BitArray(temp1);
            int[] array = new int[1];
            temp.CopyTo(array, 0);
            almanac.t_oa = array[0] << 10 >> 10; 
            almanac.wn_oa = (ushort)(array[0] >> 22); 

            almanac.SV_HEALTH = BitConverter.ToUInt16(data, 78); 
            almanac.PRN = BitConverter.ToUInt16(data, 76);

            almanacsGps.Add(almanac);
            
            if (almanacsGps.Count == 32) 
            {
                
                OnGetAlmanacGps?.Invoke(this, almanacsGps);
                almanacsGps.Clear();
            }
        }


        // Обработка Эфемерид (0x58 ... 0x06)
        private void ParseEphInfoGps(byte[] data)
        {
            //Message?.Invoke(this, "Get new ephemeris GPS (GeoS), bytes = " + data.Length);
            var ephemeris = new EphemerisGps();

            ephemeris.t_ephem = BitConverter.ToInt32(data, 0); 
            ephemeris.delta_n = BitConverter.ToSingle(data, 4); 

            ephemeris.M_0 = BitConverter.ToDouble(data, 8);
            ephemeris.e = BitConverter.ToDouble(data, 16);
            ephemeris.sqrt_A = BitConverter.ToDouble(data, 24);
            ephemeris.OMEGA_0 = BitConverter.ToDouble(data, 32);
            ephemeris.i_o = BitConverter.ToDouble(data, 40);
            ephemeris.omega = BitConverter.ToDouble(data, 48);
            ephemeris.OMEGADOT = BitConverter.ToDouble(data, 56);
            ephemeris.a_f0 = BitConverter.ToDouble(data, 64);

            ephemeris.IDOT = BitConverter.ToSingle(data, 72);
            ephemeris.C_uc = BitConverter.ToSingle(data, 76);

            ephemeris.C_us = BitConverter.ToSingle(data, 80);
            ephemeris.C_rc = BitConverter.ToSingle(data, 84);
            ephemeris.C_rs = BitConverter.ToSingle(data, 88);
            ephemeris.C_ic = BitConverter.ToSingle(data, 92);
            ephemeris.C_is = BitConverter.ToSingle(data, 96);
            ephemeris.T_GD = BitConverter.ToSingle(data, 100);
            ephemeris.a_f2 = BitConverter.ToSingle(data, 104);
            ephemeris.a_f1 = BitConverter.ToSingle(data, 108);

            ephemeris.t_oe = BitConverter.ToInt32(data, 112);
            ephemeris.t_oc = BitConverter.ToInt32(data, 116);

            ephemeris.IODC = BitConverter.ToInt16(data, 122);
            ephemeris.weeknum = BitConverter.ToInt16(data, 120);

            ephemeris.IODE = data[127];
            ephemeris.sv_number = data[126];
            ephemeris.SV_health = data[125];
            ephemeris.SVacc = data[124];


            ephemerisGps.Add(ephemeris);

            if (_version == VersionR.v3 && ephemerisGps.Count == 32)
            {
                //Message?.Invoke(this, "Count ephemeris GPS (GeoS) = " + 32 + "Event");
                OnGetEphemeriesGps?.Invoke(this, ephemerisGps);
                ephemerisGps.Clear();
            }
        }


        // Обработка Альманаха
        private void ParseTimeInfo(byte[] data)
        {
            var timeInfo = new TimeGeoS();

            timeInfo.C_utc = BitConverter.ToDouble(data, 0);
            timeInfo.C_gps = BitConverter.ToDouble(data, 8);
            timeInfo.B1 = BitConverter.ToDouble(data, 16);
            timeInfo.B2 = BitConverter.ToDouble(data, 24);
            timeInfo.Nsutki = BitConverter.ToUInt16(data, 34);
            timeInfo.N4 = BitConverter.ToUInt16(data, 32);
            timeInfo.Nalm = BitConverter.ToUInt16(data, 38);
            
            OnGetTimeInfo?.Invoke(this, timeInfo);
        }


        // Обработка спутников
        private void ParseSatellitesInfo(byte[] data)
        {
            var satInfo = new VisibleSatellites();
            satInfo.Count = BitConverter.ToUInt32(data, 0);
            satInfo.Satellites = new List<Satellite>();
            //string line = String.Empty;
            for (int i = 4; i < data.Length; i += 20)
            {
                var sat = new Satellite();

                sat.Channel = data[3 + i];
                sat.PRN = data[2 + i];
                sat.Letter = BitConverter.ToInt16(data, 0 + i);
                var channelStateWord = BitConverter.ToUInt32(data, 4 + i);
                sat.ChannelState = DecodeChannelState(channelStateWord);
                sat.SNR = BitConverter.ToSingle(data, 8 + i);
                sat.Elevation = BitConverter.ToSingle(data, 12 + i);
                sat.Azimuth = BitConverter.ToSingle(data, 16 + i);
                sat.State = channelStateWord == 0 ? StateSatellite.NotInView : SetSatelliteState(sat.ChannelState);
                //line += "\n Prn =" + sat.PRN + ", Word =" + channelStateWord + ", IsUsedInFix = "+ sat.ChannelState.IsUsedInFix + ", State =" + sat.State;
                //line += "\n Phase =" + sat.ChannelState.PhaseMeasurementIndicator + ", IsUsedInFix = " + sat.ChannelState.IsUsedInFix  + ", HasPresenceOfMultipath =" + sat.ChannelState.HasPresenceOfMultipath + ", GotEphemeris =" + sat.ChannelState.GotEphemeris + ", PowerOn =" + sat.ChannelState.PowerOn;
                satInfo.Satellites.Add(sat);
            }
            //Message?.Invoke(this, "States sats:" + line);
            OnGetSatellitesInfo?.Invoke(this, satInfo);
        }

        private ChannelState DecodeChannelState(uint word)
        {
            var result = new ChannelState();

            result.PhaseMeasurementIndicator = (byte)(word >> 30);
            result.IsUsedInFix = Convert.ToBoolean((word << 2) >> 31);
            result.HasPresenceOfMultipath = Convert.ToBoolean((word << 3) >> 31);
            result.RejectionCode = (byte)((word << 4) >> 28);
            result.IsMeasurementInfoReady = Convert.ToBoolean((word << 8) >> 31);
            result.IsNavigationFrameValid = Convert.ToBoolean((word << 9) >> 31);
            result.GotEphemeris = Convert.ToBoolean((word << 10) >> 31);
            result.GotTime = Convert.ToBoolean((word << 11) >> 31);
            result.HasFrameSync = Convert.ToBoolean((word << 12) >> 31);
            result.HasSymbolSync = Convert.ToBoolean((word << 13) >> 31);
            result.IsPLLCaptured = Convert.ToBoolean((word << 14) >> 31);
            result.PowerOn = Convert.ToBoolean((word << 15) >> 31);

            return result;
        }

        private StateSatellite SetSatelliteState(ChannelState channelState)
        {
            if (channelState.IsUsedInFix)
                return StateSatellite.UsedInFix;

            return StateSatellite.Tracked;
        }


        //копирует диапазон байт списка в массив и инвертирует его
        private byte[] InverseData(List<byte> packet, int index, int len)
        {
            byte[] result = new byte[len];
            Array.Copy(packet.ToArray(), index, result, 0, len);
            Array.Reverse(result);
            return result;
        }

        //копирует диапазон байт списка в массив и инвертирует его
        private byte[] InverseData(byte[] packet, int index, int len)
        {
            byte[] result = new byte[len];
            Array.Copy(packet, index, result, 0, len);
            Array.Reverse(result);
            return result;
        }

    }
}
