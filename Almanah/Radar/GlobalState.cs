﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System;
using System.Windows.Controls;
using CoordFormatLib;

namespace Radar
{
    public partial class RadarControl : INotifyPropertyChanged
    {
        private DateTime _now = new DateTime();
        private int satelliteCount;
        private double latitude;
        private double longitude;
        private double altitude;
        private Status ephState = Status.Empty;
        private Status almState = Status.Empty;
        private Status glonassState = Status.Empty;
        private Status beidouState = Status.Empty;
        private Status galileoState = Status.Empty;
        private DateTime lastUpdateAlm;
        private DateTime lastUpdateEph;
        private DateTime lastUpdateGlonass;

        private DateTime lastUpdateBeidou;
        private DateTime lastUpdateGalileo;

        private string timeBeforeAlm;
        private string timeBeforeEph;
        private string timeBeforeGlonassFiles;
        private bool noGpsTimeFlag;


        public Status EphState
        {
            get => ephState; 
            private set
            {
                if (ephState == value)
                    return;
                ephState = value;
                OnPropertyChanged();
            }
        }

        public Status AlmState
        {
            get => almState;
            private set
            {
                if (almState == value)
                    return;
                almState = value;
                OnPropertyChanged();
            }
        }

        public Status GlonassState
        {
            get => glonassState;
            private set
            {
                if (glonassState == value)
                    return;
                glonassState = value;
                OnPropertyChanged();
            }
        }

        public Status BeidouState
        {
            get => this.beidouState;
            private set
            {
                if (beidouState == value)
                    return;
                beidouState = value;
                OnPropertyChanged();
            }
        }

        public Status GalileoState
        {
            get => this.galileoState;
            private set
            {
                if (galileoState == value)
                    return;
                galileoState = value;
                OnPropertyChanged();
            }
        }


        public DateTime CurrentDateTime
        {
            get => _now; 
            private set
            {
                if (_now == value)
                    return;
                _now = value;
                OnPropertyChanged();
            }
        }

        public int SatelliteCount
        {
            get => satelliteCount; 
            private set
            {
                if (satelliteCount == value)
                    return;
                satelliteCount = value;
                OnPropertyChanged();
            }
        }

        public double Latitude
        {
            get => latitude; 
            private set
            {
                if (latitude == value)
                    return;
                latitude = value;
                OnPropertyChanged();
            }
        }

        public double Longitude
        {
            get => longitude; 
            private set
            {
                if (longitude == value)
                    return;
                longitude = value;
                OnPropertyChanged();
            }
        }

        public double Altitude
        {
            get => altitude; 
            private set
            {
                if (altitude == value)
                    return;
                altitude = value;
                OnPropertyChanged();
            }
        }


        public DateTime LastUpdateAlm
        {
            get => lastUpdateAlm; 
            private set
            {
                if (lastUpdateAlm == value)
                    return;
                lastUpdateAlm = value;
                OnPropertyChanged();
            }
        }


        public DateTime LastUpdateEph
        {
            get => lastUpdateEph; 
            private set
            {
                if (lastUpdateEph == value)
                    return;
                lastUpdateEph = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastUpdateGlonass
        {
            get => lastUpdateGlonass;
            private set
            {
                if (lastUpdateGlonass == value)
                    return;
                lastUpdateGlonass = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastUpdateBeidou
        {
            get => lastUpdateBeidou;
            private set
            {
                if (lastUpdateBeidou == value)
                    return;
                lastUpdateBeidou = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastUpdateGalileo
        {
            get => this.lastUpdateGalileo;
            private set
            {
                if (lastUpdateGalileo == value)
                    return;
                lastUpdateGalileo = value;
                OnPropertyChanged();
            }
        }


        public string TimeBeforeAlm
        {
            get => timeBeforeAlm; 
            private set
            {
                if (timeBeforeAlm == value)
                    return;
                timeBeforeAlm = value;
                OnPropertyChanged();
            }
        }


        public string TimeBeforeEph
        { 
            get => timeBeforeEph; 
            private set
            {
                if (timeBeforeEph == value)
                    return;
                timeBeforeEph = value;
                OnPropertyChanged();
            }
        }

        public string TimeBeforeGlonassFiles
        {
            get => timeBeforeGlonassFiles;
            private set
            {
                if (timeBeforeGlonassFiles == value)
                    return;
                timeBeforeGlonassFiles = value;
                OnPropertyChanged();
            }
        }

        public bool NoGpsTimeFlag
        {
            get => noGpsTimeFlag;
            private set
            {
                if (noGpsTimeFlag == value)
                    return;
                noGpsTimeFlag = value;
                OnPropertyChanged();
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
