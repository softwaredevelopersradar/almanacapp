﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Globalization;
using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Axes;
using Arction.Wpf.SemibindableCharting.EventMarkers;
using Arction.Wpf.SemibindableCharting.Views.ViewPolar;
using System.Threading;
using System.Diagnostics;

namespace Radar
{
    using Arction.Wpf.SemibindableCharting.ChartManager;

    public partial class RadarControl : UserControl
    {
        private LightningChartUltimate _chart;

        PolarSeriesPoint[] dataPoints;
        Stopwatch stopwatch;

        private double mouseAmplitude = 0.0;
        private double mouseAngle = 0.0;
        public event EventHandler<float> SelectPointEvent;
        protected virtual void OnSelectPoint(EventHandler<float> some_ev, float value) => some_ev?.Invoke(this, value);

        PolarEventMarkerCollection myMarkers;

        public RadarControl()
        {
            DataContext = this;
            InitializeComponent();
            string deploymentKey = "lgCAABW2ij+vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD+BCRGnn7c6dwaDiJovCk5g5nFwvJ+G60VSdCrAJ+jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq/B0dVcthh7ezOUzf1uXfOcEJ377/4rwUTR0VbNTCK601EN6/ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM+Q5vztCEz5k+Luaxs+S+OQD3ELg8+y7a/Dv0OhSQkqMDrR/o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry/tAsPPY26Ff3PDl1ItpFWZCzNS/xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi+VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq+F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9+B0YtxFPNBQs=";
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
            this.PolarChartUltimate.ChartManager = new ChartManager { MemoryGarbageCollecting = true };

            _chart = (Content as Grid).Children[0] as LightningChartUltimate;
            _chart.ViewPolar.LegendBox.ShowCheckboxes = false;
            _chart.ViewPolar.Axes[0].Reversed = true;
            _chart.ViewPolar.Axes[0].MinAmplitude = 0;
            _chart.ViewPolar.Axes[0].MaxAmplitude = 90;
            _chart.ViewPolar.Axes[0].AmplitudeLabelsVisible = false;
            _chart.ViewPolar.Axes[0].AutoFormatLabels = false;
            _chart.ViewPolar.Axes[0].SupplyCustomAngleString += AngleAsString;
            stopwatch = new Stopwatch(); stopwatch.Start();
            _now = new DateTime();
            DispatcherTimer timer = new DispatcherTimer(DispatcherPriority.Normal);
            
            timer.Interval = TimeSpan.FromMilliseconds(1000);
            timer.Tick += new EventHandler(TickSystemTime);
            timer.Start();
            


            myMarkers = new PolarEventMarkerCollection();
            //UpdatePoints(test);
            //UpdateTrianglePoints(test2);
            //UpdateGNSSCoordinates(35.632133, 125.785452, 255);
        }

        float[] test = new float[15] { 165.5f, 35.5f, 1, 3, 0, 320, 80, 32, 2, 0, 60, 50, 15, 1, 90 };  //для теста (координаты спутников)
        float[] test2 = new float[15] { 40.5f, 12f, 1, 3, 0, 13, 70, 32, 2, 0, 90, 70, 15, 1, 90 };  //для теста (координаты спутников)


        public DateTime ReceiverTime
        {
            get { return (DateTime)GetValue(ReceiverTimeProperty); }
            set { SetValue(ReceiverTimeProperty, value); }
        }

        public static readonly DependencyProperty ReceiverTimeProperty =
            DependencyProperty.Register("ReceiverTime", typeof(DateTime),
            typeof(RadarControl), new FrameworkPropertyMetadata(new DateTime(), FrameworkPropertyMetadataOptions.None));


        public static readonly DependencyProperty FormatViewCoordSourceProperty = DependencyProperty.Register("FormatViewCoordSource", typeof(CoordFormat), typeof(RadarControl), new FrameworkPropertyMetadata(CoordFormat.DD));
        public CoordFormat FormatViewCoordSource
        {
            get { return (CoordFormat)GetValue(FormatViewCoordSourceProperty); }
            set { SetValue(FormatViewCoordSourceProperty, value); }
        }

        public static readonly DependencyProperty GpsAvailabilityProperty = DependencyProperty.Register("GpsAvailability", typeof(bool), typeof(RadarControl), new FrameworkPropertyMetadata(false));
        public bool GpsAvailability
        {
            get { return (bool)GetValue(GpsAvailabilityProperty); }
            set { SetValue(GpsAvailabilityProperty, value); }
        }

        public static readonly DependencyProperty GlonassAvailabilityProperty = DependencyProperty.Register("GlonassAvailability", typeof(bool), typeof(RadarControl), new FrameworkPropertyMetadata(false));
        public bool GlonassAvailability
        {
            get { return (bool)GetValue(GlonassAvailabilityProperty); }
            set { SetValue(GlonassAvailabilityProperty, value); }
        }

        public static readonly DependencyProperty BeidouAvailabilityProperty = DependencyProperty.Register("BeidouAvailability", typeof(bool), typeof(RadarControl), new FrameworkPropertyMetadata(false));
        public bool BeidouAvailability
        {
            get { return (bool)GetValue(BeidouAvailabilityProperty); }
            set { SetValue(BeidouAvailabilityProperty, value); }
        }

        public static readonly DependencyProperty GalileoAvailabilityProperty = DependencyProperty.Register("GalileoAvailability", typeof(bool), typeof(RadarControl), new FrameworkPropertyMetadata(false));
        public bool GalileoAvailability
        {
            get { return (bool)GetValue(GalileoAvailabilityProperty); }
            set { SetValue(GalileoAvailabilityProperty, value); }
        }


        // Замена градусов на стороны света (на круге)
        private void AngleAsString(object sender, SupplyCustomAngleStringEventArgs args)
        {
            int degrees = (int)Math.Round(180f * args.Angle / Math.PI);
            if (degrees == 0)
                args.AngleAsString = "N";

            else if (degrees == 180)
                args.AngleAsString = "S";

            else if (degrees == 90)
                args.AngleAsString = "E";

            else if (degrees == 270)
                args.AngleAsString = "W";
        }


        #region User commands

        /// <summary>
        /// Update satellites coordinates
        /// </summary>
        public void UpdateTime(DateTime dateTime)
        {
            try
            {
                //_now = DateTime.Now;
                DispatcherTimer timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(500);
                timer.Tick += new EventHandler(TickSystemTime);
                timer.Start();
            }
            catch (Exception e)
            { }
        }

        

        /// <summary>
        /// Update satellites coordinates
        /// </summary>
        public void UpdatePoints(float[] targets)
        {
            try
            {
                dataPoints = new PolarSeriesPoint[targets.Length / 5];
                for (int i = 0; i < targets.Length / 5; i++)
                {
                    dataPoints[i].Angle = targets[i * 5];
                    dataPoints[i].Amplitude = 90 - targets[i * 5 + 1];  //отнимаю от 90, чтобы в центре круга была амплитуда 90
                    dataPoints[i].Value = targets[i * 5 + 2];
                    dataPoints[i].Tag = targets[i * 5 + 3];
                }

                myMarkers = CreateMarkers(dataPoints);

                _chart.ViewPolar.Markers = myMarkers; //возможно сделаю привязку
                _chart.ViewPolar.PointLineSeries[0].Points = dataPoints;
            }
            catch (Exception e)
            { }
        }


        /// <summary>
        /// Update satellites coordinates
        /// </summary>
        public void UpdateTrianglePoints(float[] targets)
        {
            try
            {
                dataPoints = new PolarSeriesPoint[targets.Length / 5];
                for (int i = 0; i < targets.Length / 5; i++)
                {
                    dataPoints[i].Angle = targets[i * 5];
                    dataPoints[i].Amplitude = 90 - targets[i * 5 + 1];  //отнимаю от 90, чтобы в центре круга была амплитуда 90
                    dataPoints[i].Value = targets[i * 5 + 2];
                    dataPoints[i].Tag = targets[i * 5 + 3];
                }

                myMarkers = CreateTriangleMarkers(dataPoints);
                _chart.ViewPolar.Markers.AddRange(myMarkers); //возможно сделаю привязку
                _chart.ViewPolar.PointLineSeries[0].Points = dataPoints;
            }
            catch (Exception e)
            { }
        }

        /// <summary>
        /// Update satellites coordinates
        /// </summary>
        public void UpdatePoints(List<SkyplotPoint> points)
        {
            try
            {
                var dataPointsGps = new List<PolarSeriesPoint>();
                var dataPointsGlonass = new List<PolarSeriesPoint>();
                var dataPointsBeidou = new List<PolarSeriesPoint>();
                var dataPointsGalileo = new List<PolarSeriesPoint>();

                foreach (var point in points)
                {
                    var polarSeriesPoint = new PolarSeriesPoint(point.Azimuth, 90 - point.Elevation, point.PRN, point.State);

                    switch (point.Type)
                    {
                        case GnssType.Gps:
                            dataPointsGps.Add(polarSeriesPoint);
                            break;

                        case GnssType.Glonass:
                            dataPointsGlonass.Add(polarSeriesPoint);
                            break;

                        case GnssType.Beidou:
                            dataPointsBeidou.Add(polarSeriesPoint);
                            break;

                        case GnssType.Galileo:
                            dataPointsGalileo.Add(polarSeriesPoint);
                            break;
                    }
                    
                }

                myMarkers = CreateMarkers(dataPointsGps.ToArray());
                myMarkers.AddRange(CreateTriangleMarkers(dataPointsGlonass.ToArray()));
                myMarkers.AddRange(CreateRectangleMarkers(dataPointsBeidou.ToArray()));
                myMarkers.AddRange(CreateCrossMarkers(dataPointsGalileo.ToArray()));

                _chart.ViewPolar.Markers = myMarkers; //возможно сделаю привязку
                _chart.ViewPolar.PointLineSeries[0].Points = dataPoints;
            }
            catch (Exception e)
            { }
        }

        Color color;
        private PolarEventMarkerCollection CreateMarkers(PolarSeriesPoint[] dataPoints)
        {
            var Markers = new PolarEventMarkerCollection();
            //Markers.Clear();
            foreach (PolarSeriesPoint p in dataPoints)
            {
                PolarEventMarker marker = new PolarEventMarker();
                marker.Amplitude = p.Amplitude;
                marker.AngleValue = p.Angle;
                marker.Label.Text = p.Value.ToString();
                _chart.ViewPolar.PointLineSeries[0].ValueRangePalette.GetColorByValue(Convert.ToDouble(p.Tag), out color);
                marker.Symbol.Color1 = color;
                marker.Symbol.Color2 = ChartTools.CalcGradient(color, Colors.Black, 0);
                marker.Label.Color = Colors.Black;
                marker.Label.Font.Size = 14;
                marker.Label.Distance = 0;
                marker.Label.VerticalAlign = AlignmentVertical.Center;
                marker.Label.HorizontalAlign = AlignmentHorizontal.Center;
                marker.MoveByMouse = false;
                marker.Symbol.GradientFill = GradientFillPoint.Solid;
                marker.Symbol.Shape = Arction.Wpf.SemibindableCharting.Shape.Circle;
                marker.Symbol.Width = 25;
                marker.Symbol.Height = 25;

                Markers.Add(marker);
            }
            return Markers;
        }

        private PolarEventMarkerCollection CreateTriangleMarkers(PolarSeriesPoint[] dataPoints)
        {
            var Markers = new PolarEventMarkerCollection();
            //Markers.Clear();
            foreach (PolarSeriesPoint p in dataPoints)
            {
                PointShapeStyle pss3 = new PointShapeStyle();
                PolarEventMarker marker = new PolarEventMarker();
                //marker.Symbol.Height = 20;
                //marker.Symbol.Width = 20;
                marker.Symbol.Shape = Shape.Triangle;
                marker.Amplitude = p.Amplitude;
                marker.AngleValue = p.Angle;
                marker.Label.Text = p.Value.ToString();
                _chart.ViewPolar.PointLineSeries[1].ValueRangePalette.GetColorByValue(Convert.ToDouble(p.Tag), out color);
                marker.Symbol.Color1 = color;
                marker.Symbol.Color2 = ChartTools.CalcGradient(color, Colors.Black, 0);
                marker.Label.Color = Colors.Black;
                marker.Label.Font.Size = 14;
                marker.Label.Distance = 0;
                marker.Label.VerticalAlign = AlignmentVertical.Center;
                marker.Label.HorizontalAlign = AlignmentHorizontal.Center;
                marker.MoveByMouse = false;
                marker.Symbol.GradientFill = GradientFillPoint.Solid;
                //marker.Symbol.Shape = Arction.Wpf.SemibindableCharting.Shape.Circle;
                marker.Symbol.Width = 25;
                marker.Symbol.Height = 25;

                Markers.Add(marker);
            }
            return Markers;

            //// ap3 is reversed => angle
            //PointShapeStyle pss3 = new PointShapeStyle(_chart);
            //EventMarkerTitle title3 = new EventMarkerTitle(_chart); new EventMarkerTitle(_chart) { Text = "Mark III" },
            //title3.Text = "Mark III";
            //PolarEventMarker paem3 = new PolarEventMarker(_chart.ViewPolar, axis3, pss3, 120, 30, title3, new PointInt(0, 0));
            //paem3.Symbol.Height = 20;
            //paem3.Symbol.Width = 20;
            //paem3.Symbol.Shape = Shape.Triangle;
            //_chart.ViewPolar.Markers.Add(paem3);
        }


        private PolarEventMarkerCollection CreateRectangleMarkers(PolarSeriesPoint[] dataPoints)
        {
            var Markers = new PolarEventMarkerCollection();

            foreach (PolarSeriesPoint p in dataPoints)
            {
                PointShapeStyle pss3 = new PointShapeStyle();
                PolarEventMarker marker = new PolarEventMarker();
                marker.Symbol.Shape = Shape.Rectangle;
                marker.Amplitude = p.Amplitude;
                marker.AngleValue = p.Angle;
                marker.Label.Text = p.Value.ToString();
                _chart.ViewPolar.PointLineSeries[2].ValueRangePalette.GetColorByValue(Convert.ToDouble(p.Tag), out color);
                marker.Symbol.Color1 = color;
                marker.Symbol.Color2 = ChartTools.CalcGradient(color, Colors.Black, 0);
                marker.Label.Color = Colors.Black;
                marker.Label.Font.Size = 14;
                marker.Label.Distance = 0;
                marker.Label.VerticalAlign = AlignmentVertical.Center;
                marker.Label.HorizontalAlign = AlignmentHorizontal.Center;
                marker.MoveByMouse = false;
                marker.Symbol.GradientFill = GradientFillPoint.Solid;
                marker.Symbol.Width = 25;
                marker.Symbol.Height = 25;

                Markers.Add(marker);
            }
            return Markers;
        }


        private PolarEventMarkerCollection CreateCrossMarkers(PolarSeriesPoint[] dataPoints)
        {
            var Markers = new PolarEventMarkerCollection();

            foreach (PolarSeriesPoint p in dataPoints)
            {
                PointShapeStyle pss3 = new PointShapeStyle();
                PolarEventMarker marker = new PolarEventMarker();
                marker.Symbol.Shape = Shape.Cross;
                marker.Amplitude = p.Amplitude;
                marker.AngleValue = p.Angle;
                marker.Label.Text = p.Value.ToString();
                _chart.ViewPolar.PointLineSeries[3].ValueRangePalette.GetColorByValue(Convert.ToDouble(p.Tag), out color);
                marker.Symbol.Color1 = color;
                marker.Symbol.Color2 = ChartTools.CalcGradient(color, Colors.Black, 0);
                marker.Symbol.BodyThickness = 10;
                marker.Label.Color = Colors.Black;
                marker.Label.Font.Size = 14;
                marker.Label.Distance = 0;
                marker.Label.VerticalAlign = AlignmentVertical.Center;
                marker.Label.HorizontalAlign = AlignmentHorizontal.Center;
                marker.MoveByMouse = false;
                marker.Symbol.GradientFill = GradientFillPoint.Solid;
                marker.Symbol.Width = 25;
                marker.Symbol.Height = 25;

                Markers.Add(marker);
            }
            return Markers;
        }


        /// <summary>
        /// Update Ephemeris status
        /// </summary>
        public void UpdateEphemerisState(Status status)
        {
            try
            {
                EphState = status;
            }
            catch { }
        }

        /// <summary>
        /// Update Almanac status
        /// </summary>
        public void UpdateAlmanacState(Status status)
        {
            try
            {
                AlmState = status;
            }
            catch { }
        }

        /// <summary>
        /// Update Ephemeris status
        /// </summary>
        public void UpdateGlonassFilesState(Status status)
        {
            try
            {
                GlonassState = status;
            }
            catch { }
        }

        /// <summary>
        /// Update Receiver coordinates
        /// </summary>
        public void UpdateGNSSCoordinates(double latitude, double longitude, double altitude)
        {
            try
            {
                Latitude = latitude;
                Longitude = longitude;
                Altitude = altitude;
            }
            catch { }
        }


        /// <summary>
        /// Update Receiver coordinates
        /// </summary>
        public void UpdateNoGpsFlag(bool flag)
        {
            try
            {
                NoGpsTimeFlag = flag;
            }
            catch { }
        }


        public void CorrectCurrentTime(DateTime time)
        {
            try
            {
                ReceiverTime = time;
            }
            catch { }
        }


        /// <summary>
        /// Update amount of tracked satellites 
        /// </summary>
        public void UpdateSatCount(int countTrackedSats)
        {
            try
            {
                SatelliteCount = countTrackedSats;
            }
            catch { }
        }


        /// <summary>
        ///Clear
        /// </summary>
        public void ClearAll()
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    UpdatePoints(new float[0] { });
                });

                UpdateGNSSCoordinates(0, 0, 0);

                _timerAlm?.Stop();
                _timerEph?.Stop();

                _timerAlm = null;
                _timerEph = null;
                //_timerAlm?.Stop();
                //_timerEph?.Stop();
            }
            catch { }
        }


        /// <summary>
        /// Update alm last time
        /// </summary>
        public void UpdateLastTimeAlm(DateTime dateTime, int timerMinutes)
        {
            try
            {
                LastUpdateAlm = dateTime;
                AlmState = Status.Updated;
                StartAlmanacTimer(timerMinutes);
            }
            catch { }
        }


        /// <summary>
        /// Update eph last time
        /// </summary>
        public void UpdateLastTimeEph(DateTime dateTime, int timerMinutes)
        {
            try
            {
                LastUpdateEph = dateTime;
                EphState = Status.Updated;
                StartEphemerisTimer(timerMinutes);
            }
            catch { }
        }

        public void UpdateLastTimeGps(DateTime dateTime)
        {
            try
            {
                AlmState = Status.Empty;
                LastUpdateEph = dateTime;
                EphState = Status.Updated;
            }
            catch { }
        }

        /// <summary>
        /// Update glonass last time
        /// </summary>
        public void UpdateLastTimeGlonassFiles(DateTime dateTime, int timerMinutes)
        {
            try
            {
                LastUpdateGlonass = dateTime;
                GlonassState = Status.Updated;
                StartGlonassTimer(timerMinutes);
            }
            catch { }
        }

        public void UpdateLastTimeGlonass(DateTime dateTime)
        {
            try
            {
                LastUpdateGlonass = dateTime;
                GlonassState = Status.Updated;
            }
            catch { }
        }

        public void UpdateLastTimeBeidou(DateTime dateTime, int timerMinutes)
        {
            try
            {
                LastUpdateBeidou = dateTime;
                BeidouState = Status.Updated;
            }
            catch { }
        }

        public void UpdateLastTimeGalileo(DateTime dateTime, int timerMinutes)
        {
            try
            {
                LastUpdateGalileo = dateTime;
                GalileoState = Status.Updated;
            }
            catch { }
        }

        /// <summary>
        /// Update alm last time
        /// </summary>
        public void ContinueTimerAlm(int timerMinutes)
        {
            try
            {
                AlmState = Status.Updated;
                StartAlmanacTimer(timerMinutes);
            }
            catch { }
        }


        /// <summary>
        /// Update eph last time
        /// </summary>
        public void ContinueTimerEph(int timerMinutes)
        {
            try
            {
                EphState = Status.Updated;
                StartEphemerisTimer(timerMinutes);
            }
            catch { }
        }


        /// <summary>
        /// Update eph last time
        /// </summary>
        public void ContinueTimerGlonass(int timerMinutes)
        {
            try
            {
                GlonassState = Status.Updated;
                StartGlonassTimer(timerMinutes);
            }
            catch { }
        }
        #endregion

        //нажатие кнопки мыши
        private void chart_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point mousePos = e.GetPosition(_chart);

            mouseAmplitude = GetMouseAmplitude(mousePos);
            mouseAngle = GetMouseAngle(mousePos);

            if (mouseAmplitude < _chart.ViewPolar.Axes[0].MaxAmplitude)
            {
                _chart.BeginUpdate();
                SelectPoint(mouseAngle, mouseAmplitude);
                _chart.EndUpdate();
            }
        }

        //кнопка мыши отжата
        private void chart_MouseUp(object sender, MouseButtonEventArgs e)
        {
        }

        /// <summary>
        /// Get polar angle at give mouse screen coordinate.
        /// </summary>
        /// <param name="mouseCoord">Mouse screen coordinate.</param>
        /// <returns>Angle in degrees</returns>
        private double GetMouseAngle(Point mouseCoord)
        {
            double angle;
            double amplitude;
            _chart.ViewPolar.Axes[0].CoordToValue((int)Math.Round(mouseCoord.X), (int)Math.Round(mouseCoord.Y), out angle, out amplitude, true);

            return angle;
        }

        /// <summary>
        /// Get polar amplitude at give mouse screen coordinate.
        /// </summary>
        /// <param name="mouseCoord">Mouse screen coordinate.</param>
        /// <returns>Amplitude</returns>
        private double GetMouseAmplitude(Point mouseCoord)
        {
            double angle;
            double amplitude;
            _chart.ViewPolar.Axes[0].CoordToValue((int)Math.Round(mouseCoord.X), (int)Math.Round(mouseCoord.Y), out angle, out amplitude, true);

            return amplitude;
        }

        /// <summary>
        /// Select point 
        /// </summary>
		private void SelectPoint(double pointAngle, double pointAmplitude)
        {
            if (pointAngle < 0)
                pointAngle += 360;

            if (dataPoints == null)
                return;

            int pointCount = dataPoints.Length;
            
            List<PolarSeriesPoint> listSelectedPoints = new List<PolarSeriesPoint>();

            for (int i = 0; i < pointCount; i++)
            {
                if ((dataPoints[i].Angle >= pointAngle - 5 && dataPoints[i].Angle <= pointAngle + 5) &&
                    (dataPoints[i].Amplitude >= pointAmplitude - 5 && dataPoints[i].Amplitude <= pointAmplitude + 5))
                {
                    listSelectedPoints.Add(dataPoints[i]);
                }
            }
            if (myMarkers.Count > dataPoints.Length)
            {
                myMarkers.Remove(myMarkers.Last());
            }
            if (listSelectedPoints.Count != 0)
            {
                OnSelectPoint(SelectPointEvent, listSelectedPoints[0].Value);
                CreateSelectedMarker(listSelectedPoints[0]);
            }
        }

        private void CreateSelectedMarker(PolarSeriesPoint selectedPoint)
        {
            CultureInfo ci = new CultureInfo("en-us");
            PolarEventMarker selectedMarker = new PolarEventMarker();
            selectedMarker.Amplitude = selectedPoint.Amplitude;
            selectedMarker.AngleValue = selectedPoint.Angle;
            selectedMarker.Label.Text = selectedPoint.Angle.ToString("0.0", ci) + "°, " + (90 - selectedPoint.Amplitude).ToString("0.0", ci) + "°";
            selectedMarker.Label.Color = Colors.Cyan;
            selectedMarker.Label.Distance = 0;
            selectedMarker.MoveByMouse = false;
            selectedMarker.Symbol.BorderWidth = 2.0f;
            selectedMarker.Symbol.BorderColor = Colors.Cyan;
            selectedMarker.Symbol.GradientFill = GradientFillPoint.Solid;
            selectedMarker.Symbol.Color1 = Colors.Transparent;
            selectedMarker.Symbol.Shape = Arction.Wpf.SemibindableCharting.Shape.Circle;
            selectedMarker.Symbol.Width = 25;
            selectedMarker.Symbol.Height = 25;

            myMarkers.Add(selectedMarker);

            _chart.ViewPolar.Markers = myMarkers;
        }

        /// <summary>
        /// Select points from table
        /// </summary>
		public void SelectPointFromTable(double angle, double amplitude)
        {
            if (angle < 0)
                angle += 360;
            if (myMarkers.Count > dataPoints.Length)
            {
                myMarkers.Remove(myMarkers.Last());
            }
            var point = new PolarSeriesPoint() {Angle = angle, Amplitude = 90-amplitude };
            CreateSelectedMarker(point);
        }
    }

    public partial class RadarControl 
    {
        public  void Dispose()
        {
            if (_chart != null)
            {
                (Content as Grid).Children.Remove(_chart);

                _chart.Dispose();
                _chart = null;
            }

            //base.DisposedOf();
        }
    }



}
