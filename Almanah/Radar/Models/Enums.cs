﻿

namespace Radar
{
    public enum Status
    {
        Empty,
        NotComplete,
        Collecting,
        Updated,
        Question
    }

    public enum CoordFormat : byte
    {
        DD = 1,
        DD_MM_mm = 2,
        DD_MM_SS_ss = 3
    }


    public enum SatelliteState : byte
    {
        NotInView=0,
        Tracked,
        UsedInFix
    }

    public enum GnssType : byte
    {
        Unknown = 0,
        Gps,
        Glonass,
        Beidou,
        Galileo
        
    }

    public enum Projection : byte
    {
        Geo,
        Mercator
    }
}
