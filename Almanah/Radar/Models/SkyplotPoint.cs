﻿
namespace Radar
{
    public class SkyplotPoint
    {
        public SatelliteState State { get; set; }
        public GnssType Type { get; set; }
        public int PRN { get; set; }
        public float Azimuth { get; set; }
        public float Elevation { get; set; }
    }
}
