﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;

namespace Radar
{
    public class TimeVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Status status = (Status)value;
            Visibility result = Visibility.Hidden;
            if (status == Status.Updated)
            {
                result = Visibility.Visible;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
