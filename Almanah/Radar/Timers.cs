﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Radar
{
    public partial class RadarControl
    {
        private static DispatcherTimer _timerAlm;
        private static DispatcherTimer _timerEph;
        private static DispatcherTimer _timerGlonass;
        static TimeSpan _timeAlm;
        static TimeSpan _timeEph;
        static TimeSpan _timeGlonass;


        private void StartAlmanacTimer(double minutes)
        {
            _timeAlm = TimeSpan.FromMinutes(minutes);
            if (_timerAlm != null)
            {
                _timerAlm.Stop();
                _timerAlm = null;
            }

            _timerAlm = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                TimeBeforeAlm = _timeAlm.ToString(@"'['mm\:ss']'");
                if (_timeAlm == TimeSpan.Zero)
                    _timerAlm.Stop();

                _timeAlm = _timeAlm.Add(TimeSpan.FromSeconds(-1));
            }
            , Application.Current.Dispatcher);
            _timerAlm.Start();
        }


        private void StartEphemerisTimer(double minutes)
        {
            _timeEph = TimeSpan.FromMinutes(minutes);
            if (_timerEph != null)
            {
                _timerEph.Stop();
                _timerEph = null;
            }

            _timerEph = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                TimeBeforeEph = _timeEph.ToString(@"'['mm\:ss']'");
                if (_timeEph == TimeSpan.Zero)
                    _timerEph.Stop();

                _timeEph = _timeEph.Add(TimeSpan.FromSeconds(-1));
            }
            , Application.Current.Dispatcher);
            _timerEph.Start();
        }


        private void StartGlonassTimer(double minutes)
        {
            _timeGlonass = TimeSpan.FromMinutes(minutes);
            if (_timerGlonass != null)
            {
                _timerGlonass.Stop();
                _timerGlonass = null;
            }

            _timerGlonass = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                TimeBeforeGlonassFiles = _timeGlonass.ToString(@"'['mm\:ss']'");
                if (_timeGlonass == TimeSpan.Zero)
                    _timerGlonass.Stop();

                _timeGlonass = _timeGlonass.Add(TimeSpan.FromSeconds(-1));
            }
            , Application.Current.Dispatcher);
            _timerGlonass.Start();
        }


        public void TickSystemTime(object sender, EventArgs e)
        {
            ReceiverTime = ReceiverTime.Add(stopwatch.Elapsed);
            stopwatch.Restart();
            //Task.Run(async () => ReceiverTime =  ReceiverTime.AddSeconds(1)); 
            //CurrentDateTime = CurrentDateTime.AddSeconds(1);
        }

    }
}
